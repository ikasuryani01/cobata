<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
// use Illuminate\Contracts\Auth\Guard;

class MhsMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->guest() || !Auth::guard($guard)->user()->is_admin) {
        if (Auth::guard('mahasiswa')->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
                // return view('errors.503');
                // echo "masuk sini";
                // exit();
            } else if($request->wantsJson()) {
                // return abort(401);
                // echo "masuk bawahnya";
                // exit();
                Auth::guard('mahasiswa')->authenticate();
            } else {
                return response()->view('errors.503');
            }
        }
        // echo "masuk paling bawah";
        // exit();
        return $next($request);
    }
}
