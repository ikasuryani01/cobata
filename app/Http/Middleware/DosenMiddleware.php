<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
// use Illuminate\Contracts\Auth\Guard;

class DosenMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->guest() || !Auth::guard($guard)->user()->is_admin) {
        if (Auth::guard('karyawan')->guest() || Auth::guard('karyawan')->user()->tipe != "ID0") {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else if($request->wantsJson()) {
                // return abort(401);
                Auth::guard('karyawan')->authenticate();
            } else {
                return response()->view('errors.503');
            }
        }
        return $next($request);
    }
}
