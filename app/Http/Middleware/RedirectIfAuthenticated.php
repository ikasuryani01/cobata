<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /*if (Auth::guard($guard)->check()) {
            $sb = Auth::user()->sebagai;
            // echo "lala" . $sb;
            // exit();
            if($sb == "mhs") {
                return redirect('/mhs/');           
            }
            else if($sb == "paj") {
                return redirect('/paj/');            
            } 
            else if($sb == "maf") {
                return redirect('/maf/');           
            }
            else if($sb == "dosen") {
                return redirect('/mhs/');           
            }
            else if($sb == "admin") {
                return redirect('/mhs/');           
            }
            // return redirect('/mhs/');
        }*/

        return $next($request);
    }
}
