<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
// use Illuminate\Contracts\Auth\Guard;

class PajMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->guest() || !Auth::guard($guard)->user()->is_admin) {
        if (Auth::guard('karyawan')->guest() || Auth::guard('karyawan')->user()->getJabatan()->namasingkat != "paj") {
            if ($request->ajax()) {
                // echo "masuk sini paj";
                // exit();
                return response('Unauthorized.', 401);
            } else if($request->wantsJson()) {
                // return abort(401);
                // echo "masuk bawahnya paj";
                // exit();
                Auth::guard('karyawan')->authenticate();
            }
            else
            {
                // echo "masuk bawahnya 3 paj";
                // exit();
                // Auth::guard('karyawan')->authenticate();
                return response()->view('errors.503');
            }
        }
        // echo "masuk plg bawah paj";
        // exit();
        return $next($request);
    }
}
