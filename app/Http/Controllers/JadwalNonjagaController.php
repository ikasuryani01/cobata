<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;

class JadwalNonjagaController extends Controller
{
  /* Master Nonjaga */
  public function showDataNonjaga() {
    //[v] diambil cuma yang semester itu, dari where function tanggal
    $semesteraktif = Models\Semester::getAktif();
    $uasuts = $semesteraktif->getUtsUas();
    if($uasuts == "uas")
    {
      $nonjagas = Models\KaryawanNonjagaUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas)
                      ->get();
    }
    else
    {
      $nonjagas = Models\KaryawanNonjagaUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts)
                      ->get();
    }
    // $nonjagas = Models\KaryawanNonjagaUjian::all();
    return view('maf.master.masternonjaga', array('nonjagas' => $nonjagas, 'datahapus' => 'tidak'));
  }

  public function showDataNonjagas() {
    //[v] diambil cuma yang semester itu, dari where function tanggal
    $nonjagas = Models\KaryawanNonjagaUjian::withTrashed()->get();
    return view('maf.master.masternonjaga', array('nonjagas' => $nonjagas, 'datahapus' => 'ya'));
  }

  public function addDataNonjaga() {
    $karyawans = Models\Karyawan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                  ->where('aktifjagaujian', 'Ya')
                  ->get();
    $enumJamke = Models\KaryawanNonjagaUjian::getEnum('jamke');
    return view('maf.master.masternonjaga-add', array('mode' => 'add', 'karyawans' => $karyawans, 'enumJamke' => $enumJamke));
  }

  public function storeDataNonjaga(Request $r) {
    $npkkaryawan = explode(" ", $r->get('npkkaryawan'));
    $npkkaryawan = $npkkaryawan[0];

    $nonjagabaru = new Models\KaryawanNonjagaUjian(array(
      'npkkaryawan' => $npkkaryawan,
      'tanggal' => $r->get('tanggal'),
      'jamke' => $r->get('jamke'),
      'keterangan' => $r->get('keterangan')
    ));
    $nonjagabaru->save();
    $statussave = "Data Nonjaga berhasil ditambahkan";
    return redirect('/maf/nonjaga')->with('statussave', $statussave);
  }

  public function editDataNonjaga($id) {
    $data = Models\KaryawanNonjagaUjian::withTrashed()->where('id', $id)->first();
    $enumJamke = Models\KaryawanNonjagaUjian::getEnum('jamke');
    $karyawans = Models\Karyawan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                  ->where('aktifjagaujian', 'Ya')
                  ->get();
    return view('maf.master.masternonjaga-add', array('mode' => 'edit', 'karyawans' => $karyawans, 'data' => $data, 'enumJamke' => $enumJamke));
  }

  public function updateDataNonjaga(Request $r, $id) {
    $npkkaryawan = explode(" ", $r->get('npkkaryawan'));
    $npkkaryawan = $npkkaryawan[0];

    $nonjaga = Models\KaryawanNonjagaUjian::withTrashed()->where('id', $id)->first();
    $nonjaga->npkkaryawan = $npkkaryawan;
    $nonjaga->tanggal = $r->get('tanggal');
    $nonjaga->jamke = $r->get('jamke');
    $nonjaga->keterangan = $r->get('keterangan');
    $nonjaga->save();

    $statussave = "Data Nonjaga berhasil diubah";
    return redirect('/maf/nonjaga')->with('statussave', $statussave);
  }

  public function deleteDataNonjaga($id) {
    $nonjaga = Models\KaryawanNonjagaUjian::where('id', $id)->first();
    // $nonjaga->deleted_by = Auth::user()->id;
    $nonjaga->save();
    $nonjaga->delete();
    return back();
  }

  public function restoreDataNonjaga($id) {
    $nonjaga = Models\KaryawanNonjagaUjian::withTrashed()->where('id', $id)->first();
    $nonjaga->deleted_by = null;
    $nonjaga->save();
    $nonjaga->restore();
    return back();
  }

  public function forcedeleteDataNonjaga($id) {
    $nonjaga = Models\KaryawanNonjagaUjian::withTrashed()->where('id', $id)->first();
    $nonjaga->forceDelete();
    return back();
  }
  /* end of master nonjaga */
}
