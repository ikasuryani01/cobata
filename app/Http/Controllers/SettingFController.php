<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;


class SettingFController extends Controller
{
  public function showDataPrioritas() {
    $data = Models\PrioritasTerima::all();
    return view('maf.pengaturan.masterprioritas', array('data' => $data, 'datahapus' => 'tidak'));
  }

  public function showDataPrioritass() {
    $data = Models\PrioritasTerima::withTrashed()->get();
    return view('maf.pengaturan.masterprioritas', array('data' => $data, 'datahapus' => 'ya'));
  }

  public function addDataPrioritas() {
    $enumStatusAktif = Models\PrioritasTerima::getEnum('statusaktif');
    return view('maf.pengaturan.masterprioritas-add', array('mode' => 'add', 'enumStatusAktif' => $enumStatusAktif));
  }

  public function aktifkanPrioritas($id) {
    $prioritas = Models\PrioritasTerima::whereId($id)->first()->AktifkanPrioritas();
    return back();
  }

  public function storeDataPrioritas(Request $r) {
    $prioritasbaru = new Models\PrioritasTerima(array(
      'namakomponen' => $r->get('namakomponen'),
      'kodekomponen' => $r->get('kodekomponen'),
      'urutan' => $r->get('urutan'),
      'statusaktif' => $r->get('statusaktif'),
      'idfakultas' => $r->get('idfakultas')
    ));
    $prioritasbaru->save();
    $status = "Data Prioritas Berhasil Ditambahkan.";
    return redirect('/maf/masterprioritas')->with('status', $status);
  }

  public function editDataPrioritas($id) {
    $prioritas = Models\PrioritasTerima::withTrashed()->where('id', $id)->first();
    $enumStatusAktif = Models\PrioritasTerima::getEnum('statusaktif');

    return view('maf.pengaturan.masterprioritas-add', array('mode' => 'edit', 'prioritas' => $prioritas, 'enumStatusAktif' => $enumStatusAktif));
  }

  public function updateDataPrioritas(Request $r, $id) {
    $prioritas = Models\PrioritasTerima::withTrashed()->where('id', $id)->first();
    $prioritas->tahunajaran = $r->get('tahunajaran');
    $prioritas->prioritas = $r->get('prioritas');
    $prioritas->tanggalmulaiuts = $r->get('tanggalmulaiuts');
    $prioritas->tanggalselesaiuts = $r->get('tanggalselesaiuts');
    $prioritas->tanggalmulaiuas = $r->get('tanggalmulaiuas');
    $prioritas->tanggalselesaiuas = $r->get('tanggalselesaiuas');
    $prioritas->statusaktif = $r->get('statusaktif');
    $prioritas->save();

    $statussave = "Data Prioritas berhasil diubah";
    return redirect('/maf/masterprioritas')->with('statussave', $statussave);
  }

  public function deleteDataPrioritas($id) {
    $prioritas = Models\PrioritasTerima::where('id', $id)->first();
    // $prioritas->deleted_by = Auth::user()->id;
    $prioritas->statusaktif = 'Tidak';
    $prioritas->save();
    $prioritas->delete();
    return back();
  }

  public function restoreDataPrioritas($id) {
    $prioritas = Models\PrioritasTerima::withTrashed()->where('id', $id)->first();
    $prioritas->deleted_by = null;
    $prioritas->save();
    $prioritas->restore();
    return back();
  }

  public function forcedeleteDataPrioritas($id) {
    $prioritas = Models\PrioritasTerima::withTrashed()->where('id', $id)->first();
    $prioritas->forceDelete();
    return back();
  }
}
