<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class PrasyaratMkController extends Controller
{
        /*Master Prasyarat*/
    public function showDataPrasyarat(Request $r)
    {
        $idsemester = $r->get('ids');
        $semester = Models\Semester::all();
        $prasyarats = Models\PrasyaratMataKuliah::all();
        return view('paj.masterprasyarat', array('prasyarats' => $prasyarats, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function showDataPrasyarats(Request $r)
    {
        $idsemester = $r->get('ids');
        $semester = Models\Semester::all();
        $prasyarats = Models\PrasyaratMataKuliah::withTrashed()->all();
        return view('paj.masterprasyarat', array('prasyarats' => $prasyarats, 'datahapus' => 'ya', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function addDataPrasyarat()
    {
        $matakuliahs = Models\Matakuliah::withTrashed()->get();
        $enumNisbi = Models\PrasyaratMataKuliah::getEnum('nisbi');
        return view('paj.masterprasyarat-add', array('mode' => 'add', 'enumNisbi' => $enumNisbi, 'matakuliahs' => $matakuliahs));
    }

    public function storeDataPrasyarat(Request $r)
    {
        $kodemk = explode(" ", $r->get('kodemk'));
        $kodemk = $kodemk[0];

        $kodemksyarat = explode(" ", $r->get('kodemksyarat'));
        $kodemksyarat = $kodemksyarat[0];
        $prasyaratbaru = new Models\PrasyaratMataKuliah(array(
                'kodemk' => $kodemk,
                'kodemksyarat' => $kodemksyarat,
                'nisbi' => $r->get('nisbi')
            ));
        $prasyaratbaru->save();

        $statussave = "Data Prasyarat berhasil ditambahkan";
        return redirect('/paj/masterprasyarat')->with('statussave', $statussave);
    }

    public function editDataPrasyarat($id)
    {
        $matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
            ->where(function($query)
                {
                    $idsemesteraktif = Models\Semester::getAktif()->id;
                    $kodemksudah = Models\PrasyaratMataKuliah::where('idsemester', $idsemesteraktif)->select('kodemk')->get();
                    return $query->whereNotIn('kodemk', $kodemksudah);
                })
            ->get();
        $enumHari = Models\PrasyaratMataKuliah::getEnum('hari');
        $prasyarat = Models\PrasyaratMataKuliah::whereId($id)->first();
        return view('paj.masterprasyarat-add', array('mode' => 'edit', 'prasyarat' => $prasyarat, 'enumHari' => $enumHari, 'matakuliah' => $matakuliah));
    }

    public function updateDataPrasyarat(Request $r, $id)
    {
        $prasyarat = Models\PrasyaratMataKuliah::withTrashed()->where('id', $id)->first();
        $prasyarat->mingguke = $r->get('mingguke');
        $prasyarat->hari = $r->get('hari');
        $prasyarat->jamke = $r->get('jamke');
        $prasyarat->kodemk = $r->get('kodemk');
        $prasyarat->save();

        $statussave = "Data Prasyarat berhasil diubah";
        return redirect('/paj/masterprasyarat')->with('statussave', $statussave);
    }

    public function deleteDataPrasyarat($nrp)
    {
        $prasyarat = Models\PrasyaratMataKuliah::where('nrp', $nrp)->first();
        $prasyarat->deleted_by = Auth::user()->id;
        $prasyarat->save();
        $prasyarat->delete();
        return back();
    }

    public function restoreDataPrasyarat($nrp)
    {
        $prasyarat = Models\PrasyaratMataKuliah::withTrashed()->where('nrp', $nrp)->first();
        $prasyarat->deleted_by = null;
        $prasyarat->save();
        $prasyarat->restore();
        return back();
    }

    public function forcedeleteDataPrasyarat($nrp)
    {
        $prasyarat = Models\PrasyaratMataKuliah::withTrashed()->where('nrp', $nrp)->first();
        $prasyarat->forceDelete();
        return back();
    }
    /*end of master prasyarat*/
}
