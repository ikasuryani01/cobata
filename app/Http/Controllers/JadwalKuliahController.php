<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class JadwalKuliahController extends Controller
{
    /*Master Kuliah*/
    public function showDataKuliah(Request $r)
    {
        $idsemester = $r->get('ids');
        if(!empty($idsemester))
        {
            if($idsemester != 0)
            {
                $idkelas = Models\KelasParalel::where('idsemester', $idsemester)->get(['id']);
            }
        }
        else
        {
            $idsemester = Models\Semester::where('statusaktif', 'Ya')->first()->id;
            $idkelas = Models\KelasParalel::where('idsemester', $idsemester)->get(['id']);
            
        }
        $kuliahs = Models\JadwalKuliah::whereIn('idkelasparalel', $idkelas)->get();
        $semester = Models\Semester::all();
        return view('paj.masterkuliah', array('kuliahs' => $kuliahs, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function showDataKuliahs(Request $r)
    {
        $idsemester = $r->get('ids');
        if(!empty($idsemester))
        {
            if($idsemester != 0)
            {
                $idkelas = Models\KelasParalel::where('idsemester', $idsemester)->get(['id']);
            }
        }
        else
        {
            $idsemester = Models\Semester::where('statusaktif', 'Ya')->first()->id;
            $idkelas = Models\KelasParalel::where('idsemester', $idsemester)->get(['id']);
            
        }
        $kuliahs = Models\JadwalKuliah::withTrashed()->whereIn('idkelasparalel', $idkelas)->get();
        $semester = Models\Semester::all();
        return view('paj.masterkuliah', array('kuliahs' => $kuliahs, 'datahapus' => 'ya', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function addDataKuliah()
    {
        $idsemesteraktif = Models\Semester::getAktif()->id;
        // dapetin kode kp yang aktif semester ini
        // ambil smua kode yang belum ada di jadwal kuliah skrg
        // $idkelassudah = Models\JadwalKuliah::select('idkelasparalel')->get(); //nda jadi pake ini soalnya kan 1 kelas bs banyak jadwal
        $kelas = Models\KelasParalel::where('statusaktif', 'Ya')
                    ->where('idsemester', $idsemesteraktif)
                    // ->whereNotIn('id', $idkelassudah)
                    ->get();

        $ruangan = Models\Ruangan::where('untukkuliah', 'Ya')->get();
        $enumHari = Models\JadwalKuliah::getEnum('hari');
        return view('paj.masterkuliah-add', array('mode' => 'add', 'enumHari' => $enumHari, 'kelas' => $kelas, 'ruangan' => $ruangan));
    }

    public function storeDataKuliah(Request $r)
    {
        $kuliahbaru = new Models\JadwalKuliah(array(
                'idruangan' => $r->get('idruangan'),
                'hari' => $r->get('hari'),
                'jammasuk' => $r->get('jammasuk'),
                'jamkeluar' => $r->get('jamkeluar'),
                'idkelasparalel' => $r->get('idkelasparalel')
            ));
        $kuliahbaru->save();
        $statussave = "Data Kuliah berhasil ditambahkan";
        return back()->with('status', $statussave);
    }

    public function editDataKuliah($id)
    {
        $idsemesteraktif = Models\Semester::getAktif()->id;
        $idkelassudah = Models\JadwalKuliah::select('idkelasparalel')->get();
        $kuliah = Models\JadwalKuliah::withTrashed()->whereId($id)->first();
        $kelas = Models\KelasParalel::where('statusaktif', 'Ya')
                    ->where('idsemester', $idsemesteraktif)
                    ->get();
        $ruangan = Models\Ruangan::where('untukkuliah', 'Ya')->get();
        $enumHari = Models\JadwalKuliah::getEnum('hari');
        $kuliah = Models\JadwalKuliah::whereId($id)->first();
        return view('paj.masterkuliah-add', array('mode' => 'edit', 'enumHari' => $enumHari, 'kelas' => $kelas, 'ruangan' => $ruangan, 'kuliah' => $kuliah));
    }

    public function updateDataKuliah(Request $r, $id)
    {
        $kuliah = Models\JadwalKuliah::withTrashed()->where('id', $id)->first();
        $kuliah->idruangan = $r->get('idruangan');
        $kuliah->idkelasparalel = $r->get('idkelasparalel');
        $kuliah->jammasuk = $r->get('jammasuk');
        $kuliah->jamkeluar = $r->get('jamkeluar');
        $kuliah->hari = $r->get('hari');
        $kuliah->save();

        $statussave = "Data Kuliah berhasil diubah";
        return redirect('/paj/masterkuliah')->with('status', $statussave);
    }

    public function deleteDataKuliah($id)
    {
        $kuliah = Models\JadwalKuliah::where('id', $id)->first();
        // $kuliah->deleted_by = Auth::user()->id;
        // $kuliah->save();
        $kuliah->delete();
        return back()->with('status', "Berhasil dihapus.");
    }

    public function restoreDataKuliah($id)
    {
        $kuliah = Models\JadwalKuliah::withTrashed()->where('id', $id)->first();
        // $kuliah->deleted_by = null;
        // $kuliah->save();
        $kuliah->restore();
        return back();
    }

    public function forcedeleteDataKuliah($id)
    {
        $kuliah = Models\JadwalKuliah::withTrashed()->where('id', $id)->first();
        $kuliah->forceDelete();
        return back();
    }
    /*end of master kuliah*/
}
