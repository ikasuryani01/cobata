<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models;

use Auth;
use DB;

class ProsesHasilFPPController extends Controller
{
    /*Hasil FPP*/
    public function showHasilFppIndex(Request $r)
    {
      $kps = Models\KelasParalel::where('statusaktif', 'Ya')->where('idsemester', Models\Semester::getAktifId())->get();
      return view('maf.proseshasilfpp.hasilfpp', array('datas' => $kps, 'datahapus' => 'tidak'));
      /*$idsemester = $r->get('ids');
      if(!empty($idsemester))
      {
          if($idsemester != 0)
          {
              $fpps = Models\Fpp::where('idsemester', $idsemester)->get();
          }
      }
      else
      {
          $idsemester = Models\Semester::getAktif()->id;
          $fpps = Models\Fpp::where('idsemester', $idsemester)->get();
      }
      $datas = Models\DaftarFppKelasMahasiswa::whereIn('idfpp', $fpps)->get();
      $semester = Models\Semester::all();
      return view('paj.hasilfpp', array('datas' => $datas, 'fpps' => $fpps, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));*/
    }

    public function lihatDetailKelas($idkp)
    {
      $datapendaftar = Models\DaftarFppKelasMahasiswa::where('idkelasparalel', $idkp)->get();
      $kp = Models\KelasParalel::whereId($idkp)->first();
      // print_r($datapendaftar[0]->mahasiswa);
      // exit();
      return view('maf.proseshasilfpp.detailhasilfpp', array('datapendaftar' => $datapendaftar, 'kp' => $kp, 'mode' => 'show'));
    }

    public function prosesSemua()
    {
      $idfpp = Models\Fpp::getTerbaruId();
      // if($idfpp != 0)
      // {
        $idjurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan')->get();
        $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusans)->select('kodemk')->distinct()->get();

        /*$jurkmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusans)->select('kodemk')->distinct()->get();
        $kmk = Models\Matakuliah::whereIn('kodemk', $jurkmk)
                ->where('flagskripsi', '!=', 'Ya')
                ->where('statusbuka', 'Ya')
                ->select('kodemk')->get();*/
        
        // [udah] harusnya nnti ditambai pengecekan cuma ambil mk yang di fakultas / jurusan itu, br select idkp, br wherein id idkp nnti
        // [?] hrsnya dia cm buka KP dr mata kuliah yang buka jadi nda prlu dicek lg disini ambil kodemk yang statusnya buka apa nggak. ntar malah jd ada yg nda keproses gara MK tutup tp kp buka dan ada yg daftar
        $kelas = Models\KelasParalel::where('idsemester', Models\Semester::getAktifId()) //ambil yg smster ini
                    ->whereIn('kodemk', $kmk) //ambil yg dibawah fakultas itu
                    ->where('statusaktif', 'Ya') //yang buka
                    ->get();

        // [note] isi sementara ini cuma ambil yang lagi pending aja jd cm hasil perwalian yang pending aja yg diproses
        $kelasdanisinya = $kelas->each(function ($item, $key) {
          $jumlah = Models\DaftarFppKelasMahasiswa::where('idkelasparalel', $item->id)
                    ->where('status', 'Pending')
                    ->count();
          $item->isisementara = $jumlah;
          // echo $item . " " . $key . "<br>";
        });
        $kelas = $kelasdanisinya->where('isisementara', '>', '0')->sortBy('isisementara');
        /*print_r($kelas);
        exit();*/
        foreach ($kelas as $kls) {
            $kls->prosesPerwalian($idfpp);
        }
        exit();
      /*}
      else
      {
        $status = "Tidak ada FPP yang sedang berjalan saat ini";
      }*/

      // exit();
      return back();
    }

    public function publikasiSemua($jenisf)
    {
      //jgn lupa nnti disamain sm show daftarmk
      $idkps = Models\KelasParalel::where('statusaktif', 'Ya')->select('id')->get(); 
      $idfpps = Models\Fpp::where('idsemester', Models\Semester::getAktifId())->get();
      $idfpp = $idfpps->where('jenis', $jenisf)->first();
      // dd($idfpp);
      
      $datas = Models\DaftarFppKelasMahasiswa::whereIn('idkelasparalel', $idkps)
                    ->where('idfpp', $idfpp->id)
                    ->update(['statuspublikasi' => 'Ya']);

      return back();
    }

    public function publikasibyKelas($idkelas)
    {
      $datas = Models\DaftarFppKelasMahasiswa::where('idkelasparalel', $idkelas)
                ->update(['statuspublikasi' => 'Ya']);
      return back();
    }

    public function tidakPublikasibyKelas($idkelas)
    {
      $datas = Models\DaftarFppKelasMahasiswa::where('idkelasparalel', $idkelas)
                ->update(['statuspublikasi' => 'Tidak']);
      return back();
    }

    public function editHasilFppbyId($iddata)
    {
      $datapendaftarkelas = Models\DaftarFppKelasMahasiswa::whereId($iddata)->first();
      $enumStatusTerima = Models\DaftarFppKelasMahasiswa::getEnum('status');
      
      return view('maf.proseshasilfpp.detailhasilfpp', array('datapendaftarkelas' => $datapendaftarkelas, 'mode' => 'editsatu', 'enumStatusTerima' => $enumStatusTerima));
    }

    public function simpaneditHasilFppbyId(Request $r)
    {
      $upd = Models\DaftarFppKelasMahasiswa::whereId($r->get('iddata'))
              ->update(['status' => $r->get('status'), 'alasan' => $r->get('alasan'), 'statuspublikasi' => $r->get('statuspublikasi')]);
      $data = Models\DaftarFppKelasMahasiswa::whereId($r->get('iddata'))->first()->idkelasparalel;
      return redirect('maf/hasilfpp/dtl-'.$data);
    }

    public function editHasilFppbyKelas($idkelas)
    {
      $datapendaftarkelas = Models\DaftarFppKelasMahasiswa::where('idkelasparalel', $idkelas)->get();
      $enumStatusTerima = Models\DaftarFppKelasMahasiswa::getEnum('status');

      return view('maf.proseshasilfpp.detailhasilfpp', array('datapendaftarkelas' => $datapendaftarkelas, 'mode' => 'edit', 'enumStatusTerima' => $enumStatusTerima));
    }

    public function simpaneditHasilFppbyKelas(Request $r)
    {
      $datapendaftarkelas = Models\DaftarFppKelasMahasiswa::where('idkelasparalel', $r->get('idkelas'))->get();
      foreach ($datapendaftarkelas as $data) {
        $upd = Models\DaftarFppKelasMahasiswa::whereId($data->id)
              ->update(['status' => $r->get('status-'.$data->id)]);
      }
      return redirect('maf/hasilfpp/dtl-'.$datapendaftarkelas[0]->idkelasparalel);
    }

    public function transferMhsindex($idkelas)
    {
      $datapendaftarkelas = Models\DaftarFppKelasMahasiswa::where('idkelasparalel', $idkelas)->get();
      $idkelasjadwalsama = $datapendaftarkelas[0]->kp->getIdJadwalSama();
      $enumStatusTerima = Models\DaftarFppKelasMahasiswa::getEnum('status');

      $kps = Models\KelasParalel::whereIn('id', $idkelasjadwalsama)->get();

      if(count($kps) > 0)
        $statempty = 1;
      else
        $statempty = 0;

      return view('maf.proseshasilfpp.detailhasilfpp', array('datapendaftarkelas' => $datapendaftarkelas, 'mode' => 'transfer', 'kps' => $kps, 'statempty' => $statempty, 'enumStatusTerima' => $enumStatusTerima));
    }

    public function simpanTransferMhs(Request $r)
    {
      // dd($r);
      $idkelas = $r->get('idkelas');
      $iddatas = $r->get('cek');
      $alasan = "Transfer mahasiswa dari KP ". $r->get('kodekpasal') ." oleh " . Auth::guard('karyawan')->user()->npk;
      foreach ($iddatas as $id) {
        $upd = Models\DaftarFppKelasMahasiswa::whereId($id)
              ->update(['idkelasparalel' => $idkelas, 'status' => $r->get('status-'.$id), 'alasan' => $alasan, 'statuspublikasi' => 'Ya']);
      }

      return redirect('maf/hasilfpp/dtl-'.$idkelas);
    }
    
    /*end of hasil fpp*/
}
