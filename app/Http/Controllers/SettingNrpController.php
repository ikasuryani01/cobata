<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class SettingNrpController extends Controller
{
    /*Master Settingnrp*/
    public function showDataSettingnrp(Request $r)
    {
        $idsemester = $r->get('ids');
        if(!empty($idsemester))
        {
            if($idsemester != 0)
            {
                $kelas = Models\KelasParalel::where('idsemester', $idsemester)->select('id')->get();
            }
        }
        else
        {
            $idsemester = Models\Semester::getAktif()->id;
            $kelas = Models\KelasParalel::where('idsemester', $idsemester)->select('id')->get();
        }
        $settingnrps = Models\SettingNrp::whereIn('idkelasparalel', $kelas)->get();
        $semester = Models\Semester::all();
        return view('paj.mastersettingnrp', array('settingnrps' => $settingnrps, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function showDataSettingnrps(Request $r)
    {
        $idsemester = $r->get('ids');
        if(!empty($idsemester))
        {
            if($idsemester != 0)
            {
                $kelas = Models\KelasParalel::where('idsemester', $idsemester)->select('id')->get();
            }
        }
        else
        {
            $idsemester = Models\Semester::getAktif()->id;
            $kelas = Models\KelasParalel::where('idsemester', $idsemester)->select('id')->get();
        }
        $settingnrps = Models\SettingNrp::withTrashed()->whereIn('idkelasparalel', $kelas)->get();
        $semester = Models\Semester::all();
        return view('paj.mastersettingnrp', array('settingnrps' => $settingnrps, 'datahapus' => 'ya', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function addDataSettingnrp()
    {
        $kps = Models\KelasParalel::where('statusaktif', 'Ya')
            ->where(function($query)
                {
                    $kodekpsudah = Models\Settingnrp::select('idkelasparalel')->get();
                    return $query->whereNotIn('id', $kodekpsudah);
                })
            ->get();
        return view('paj.mastersettingnrp-add', array('mode' => 'add', 'kps' => $kps));
    }

    public function storeDataSettingnrp(Request $r)
    {
        $stringmk = explode(" ", $r->get('idkelasparalel'));
        $kodemk = $stringmk[0];
        $kodekp = $stringmk[count($stringmk)-1];

        $idkelasparalel = Models\KelasParalel::where('kodemk', $kodemk)->where('kodekp', $kodekp)->select('id')->first()->id;

        $settingnrpbaru = new Models\SettingNrp(array(
                'idkelasparalel' => $idkelasparalel,
                'nrpawal' => $r->get('nrpawal'),
                'nrpakhir' => $r->get('nrpakhir')
            ));
        $settingnrpbaru->save();

        $statussave = "Data Setting NRP berhasil ditambahkan";
        return redirect('/paj/mastersettingnrp')->with('statussave', $statussave);
    }

    public function editDataSettingnrp($id)
    {
        $matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
            ->where(function($query)
                {
                    $idsemesteraktif = Models\Semester::getAktif()->id;
                    $kodemksudah = Models\AmpuDosenMatakuliahSemester::where('idsemester', $idsemesteraktif)->select('kodemk')->get();
                    return $query->whereNotIn('kodemk', $kodemksudah);
                })
            ->get();
        $enumHari = Models\AmpuDosenMatakuliahSemester::getEnum('hari');
        $settingnrp = Models\AmpuDosenMatakuliahSemester::whereId($id)->first();
        return view('paj.mastersettingnrp-add', array('mode' => 'edit', 'settingnrp' => $settingnrp, 'enumHari' => $enumHari, 'matakuliah' => $matakuliah));
    }

    public function updateDataSettingnrp(Request $r, $id)
    {
        $settingnrp = Models\AmpuDosenMatakuliahSemester::withTrashed()->where('id', $id)->first();
        $settingnrp->mingguke = $r->get('mingguke');
        $settingnrp->hari = $r->get('hari');
        $settingnrp->jamke = $r->get('jamke');
        $settingnrp->kodemk = $r->get('kodemk');
        $settingnrp->save();

        $statussave = "Data Settingnrp berhasil diubah";
        return redirect('/paj/mastersettingnrp')->with('statussave', $statussave);
    }

    public function deleteDataSettingnrp($nrp)
    {
        $settingnrp = Models\AmpuDosenMatakuliahSemester::where('nrp', $nrp)->first();
        $settingnrp->deleted_by = Auth::user()->id;
        $settingnrp->save();
        $settingnrp->delete();
        return back();
    }

    public function restoreDataSettingnrp($nrp)
    {
        $settingnrp = Models\AmpuDosenMatakuliahSemester::withTrashed()->where('nrp', $nrp)->first();
        $settingnrp->deleted_by = null;
        $settingnrp->save();
        $settingnrp->restore();
        return back();
    }

    public function forcedeleteDataSettingnrp($nrp)
    {
        $settingnrp = Models\AmpuDosenMatakuliahSemester::withTrashed()->where('nrp', $nrp)->first();
        $settingnrp->forceDelete();
        return back();
    }
    /*end of master settingnrp*/
}
