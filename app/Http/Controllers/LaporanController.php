<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Auth;
use DB;
use PDF;
use Excel;

class LaporanController extends Controller
{
  public function showLaporan()
  {
    return view('maf.laporan.menu');
  }

  public function laporanHasilPerwalianMhs()
  {
    $kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $mahasiswas = Models\Mahasiswa::whereIn('idjurusan', $idjurusan)->select('nrp')->get();

    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    return view('maf.laporan.laporanhasilperwalian', array('mahasiswas' => $mahasiswas, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
  }

  public function laporanHasilPerwalianMhsFilter(Request $r)
  {
  	$kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();
    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();

    if($r->get('idjurusan') != 0)
    {
    	$filterdipakai['idjurusan'] = $r->get('idjurusan');
	    $mahasiswas = Models\Mahasiswa::where('idjurusan', $filterdipakai['idjurusan'])->select('nrp')->get();
	    $kalimatfilter = "Jurusan " . $datajurusan->where('IdJurusan', $filterdipakai['idjurusan'])->first()->Nama;
    }
    else
    {
	    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
	    $mahasiswas = Models\Mahasiswa::whereIn('idjurusan', $idjurusan)->select('nrp')->get();
	    $kalimatfilter = "Semua jurusan";

    }
    
    if($r->get('submitfilter') == "PDF")
    {
    	$pdf = PDF::loadView('maf.laporan.printhasilperwalian-pdf', array('mahasiswas' => $mahasiswas, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
      return $pdf->stream();
    }
    else if($r->get('submitfilter') == "Excel")
    {
      Excel::create('Laporan Hasil Perwalian Mahasiswa', function($excel) use($presensis, $tahun, $kalimatfilter, $filterdipakai, $tanggal, $datapikets, $semesteraktif) {
          $excel->sheet('Sheet 1', function($sheet) use($presensis, $tahun, $kalimatfilter, $filterdipakai, $tanggal, $datapikets, $semesteraktif) {
              $sheet->loadView('maf.laporan.printhasilperwalian-excel', array('mahasiswas' => $mahasiswas, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
          });
      })->export('xls');
    }
    return view('maf.laporan.laporanhasilperwalian', array('mahasiswas' => $mahasiswas, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
  }

  public function laporanKapkelas()
  {
  	$kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();

    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    return view('maf.laporan.laporankapkelas', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
  }

	public function laporanKapkelasFilter(Request $r)
	{
		$kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    if($r->get('idjurusan') != 0)
    {
    	$filterdipakai['idjurusan'] = $r->get('idjurusan');
    	$kmk = Models\JurusanDaftarMatakuliah::where('idjurusan', $filterdipakai['idjurusan'])->select('kodemk')->get();
	    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
	    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();
	    $kalimatfilter = "Jurusan " . $datajurusan->where('IdJurusan', $filterdipakai['idjurusan'])->first()->Nama;
    }
    else
    {
	    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
	    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
	    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
	    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();
	    $kalimatfilter = "Semua jurusan";
    }
    
    if($r->get('submitfilter') == "PDF")
    {
    	$pdf = PDF::loadView('maf.laporan.printkapkelas-pdf', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
      return $pdf->stream();
    }

    return view('maf.laporan.laporankapkelas', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
	}

	public function laporanKelasTutup()
	{
  	$kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Tutup')->with('mk')->get();

    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    return view('maf.laporan.laporankelastutup', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
	}

	public function laporanKelasTutupFilter(Request $r)
	{
		$kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    if($r->get('idjurusan') != 0)
    {
    	$filterdipakai['idjurusan'] = $r->get('idjurusan');
    	$kmk = Models\JurusanDaftarMatakuliah::where('idjurusan', $filterdipakai['idjurusan'])->select('kodemk')->get();
	    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
	    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Tutup')->with('mk')->get();
	    $kalimatfilter = "Jurusan " . $datajurusan->where('IdJurusan', $filterdipakai['idjurusan'])->first()->Nama;
    }
    else
    {
	    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
	    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
	    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
	    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Tutup')->with('mk')->get();
	    $kalimatfilter = "Semua jurusan";
    }
    
    if($r->get('submitfilter') == "PDF")
    {
    	$pdf = PDF::loadView('maf.laporan.printkelastutup-pdf', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
      return $pdf->stream();
    }

    return view('maf.laporan.laporankelastutup', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
	}

	public function laporanRekapPeserta()
	{
  	$kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();

    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    return view('maf.laporan.laporanrekappeserta', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
	}

	public function laporanRekapPesertaFilter(Request $r)
	{
		$kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    if($r->get('idjurusan') != 0)
    {
    	$filterdipakai['idjurusan'] = $r->get('idjurusan');
    	$kmk = Models\JurusanDaftarMatakuliah::where('idjurusan', $filterdipakai['idjurusan'])->select('kodemk')->get();
	    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
	    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();
	    $kalimatfilter = "Jurusan " . $datajurusan->where('IdJurusan', $filterdipakai['idjurusan'])->first()->Nama;
    }
    else
    {
	    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
	    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
	    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
	    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();
	    $kalimatfilter = "Semua jurusan";
    }
    
    if($r->get('submitfilter') == "PDF")
    {
    	$pdf = PDF::loadView('maf.laporan.printrekappeserta-pdf', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
      return $pdf->stream();
    }

    return view('maf.laporan.laporanrekappeserta', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
	}

	public function laporanFormRekap()
	{
  	$kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();

    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    return view('maf.laporan.laporanformrekap', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));		
	}

	public function laporanFormRekapFilter(Request $r)
	{
		$kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    if($r->get('idjurusan') != 0)
    {
    	$filterdipakai['idjurusan'] = $r->get('idjurusan');
    	$kmk = Models\JurusanDaftarMatakuliah::where('idjurusan', $filterdipakai['idjurusan'])->select('kodemk')->get();
	    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
	    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();
	    $kalimatfilter = "Jurusan " . $datajurusan->where('IdJurusan', $filterdipakai['idjurusan'])->first()->Nama;
    }
    else
    {
	    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
	    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
	    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
	    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();
	    $kalimatfilter = "Semua jurusan";
    }
    
    if($r->get('submitfilter') == "PDF")
    {
    	$pdf = PDF::loadView('maf.laporan.printformrekap-pdf', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
      return $pdf->stream();
    }

    return view('maf.laporan.laporanformrekap', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
	}

	public function laporanDaftarIsiKelas()
	{
    $kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $fpp = Models\Fpp::where('idfakultas', $idfakultas)->where('idsemester', $semesteraktif->id)->get();
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();

    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();
    
    return view('maf.laporan.laporandaftarisikelas', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif, 'fpp' => $fpp));
	}

	public function laporanDaftarIsiKelasFilter(Request $r)
	{
    $kalimatfilter = "";
    $filterdipakai = array();
    $mahasiswas = array();
    $filterdipakai['idjurusan'] = 0;

    $semesteraktif = Models\Semester::getAktif();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan', 'Nama')->get();

    $fpp = Models\Fpp::where('idfakultas', $idfakultas)->where('idsemester', $semesteraktif->id)->get();
    
    if($r->get('idjurusan') != 0)
    {
      $filterdipakai['idjurusan'] = $r->get('idjurusan');
      $kmk = Models\JurusanDaftarMatakuliah::where('idjurusan', $filterdipakai['idjurusan'])->select('kodemk')->get();
      $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
      $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();
      $kalimatfilter = "Jurusan " . $datajurusan->where('IdJurusan', $filterdipakai['idjurusan'])->first()->Nama;
    }
    else
    {
      $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
      $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
      $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
      $kelasparalels = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->with('mk')->get();
      $kalimatfilter = "Semua jurusan";
    }
    
    if($r->get('submitfilter') == "PDF")
    {
      $pdf = PDF::loadView('maf.laporan.printdaftarisikelas-pdf', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif, 'fpp' => $fpp));
      return $pdf->stream();
    }

    return view('maf.laporan.laporandaftarisikelas', array('kelasparalels' => $kelasparalels, 'datajurusan' => $datajurusan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif, 'fpp' => $fpp));
	}

  public function laporanUjianNrp()
  {
    $kalimatfilter = "";
    $filterdipakai = array();
    $presensis = array();
    $tahun = array();
    $filterdipakai['from'] = "";
    $filterdipakai['to'] = "";

    $semesteraktif = Models\Semester::getAktif();
    $jenisujian = $semesteraktif->getUtsUas();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $idkp = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->select('id')->get();
    $presensis = Models\PresensiUjian::whereIn('idkelasparalel', $idkp)
                  ->where(function($query) use ($jenisujian, $semesteraktif) {
                      if($jenisujian == "uas")
                      {
                        return $query->where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas);
                      }
                      else
                      {
                        return $query->where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts);
                      }
                    })
                  ->get();
    $tanggal = $presensis->pluck('tanggal')->unique()->all();

    $uasuts = $semesteraktif->getUtsUas();
    if($uasuts == "uas")
    {
      $tanggalmulai = $semesteraktif->tanggalmulaiuas;
      $tanggalselesai = $semesteraktif->tanggalselesaiuas;
    }
    else
    {
      $tanggalmulai = $semesteraktif->tanggalmulaiuts;
      $tanggalselesai = $semesteraktif->tanggalselesaiuts;
    }
    
    return view('maf.laporan.laporanjadwalujiannrp', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'tanggalmulai' => $tanggalmulai, 'tanggalselesai' => $tanggalselesai, 'semesteraktif' => $semesteraktif));
  }

  public function laporanUjianNrpFilter(Request $r)
  {
    $kalimatfilter = "";
    $filterdipakai = array();
    $presensis = array();
    $tahun = array();
    $filterdipakai['from'] = "";
    $filterdipakai['to'] = "";

    $semesteraktif = Models\Semester::getAktif();
    $jenisujian = $semesteraktif->getUtsUas();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $idkp = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->select('id')->get();
    if($r->get('from') != "" && $r->get('to') != "")
    {
      $filterdipakai['from'] = date("Y-m-d", strtotime($r->get('from')));
      $filterdipakai['to'] = date("Y-m-d", strtotime($r->get('to')));
      $kalimatfilter = "Menampilkan dari tanggal ". $filterdipakai['from'] . " sampai " . $filterdipakai['to'];
    }
    else if($r->get('from') != "")
    {
      $filterdipakai['from'] = date("Y-m-d", strtotime($r->get('from')));
      if($r->get('to') == "")
      {
        if($jenisujian == "uas")
          $filterdipakai['to'] = $semesteraktif->tanggalselesaiuas;
        else
          $filterdipakai['to'] = $semesteraktif->tanggalselesaiuts;
      }
      $kalimatfilter = "Menampilkan dari tanggal ". $filterdipakai['from'];
    }
    else if($r->get('to') != "")
    {
      $filterdipakai['to'] = date("Y-m-d", strtotime($r->get('to')));
      if($r->get('from') == "")
      {
        if($jenisujian == "uas")
          $filterdipakai['from'] = $semesteraktif->tanggalmulaiuas;
        else
          $filterdipakai['from'] = $semesteraktif->tanggalmulaiuts;
      }
      $kalimatfilter = "Menampilkan sampai tanggal " . $filterdipakai['to'];
    }
    else
    {
      if($jenisujian == "uas")
      {
        $filterdipakai['from'] = $semesteraktif->tanggalmulaiuas;
        $filterdipakai['to'] = $semesteraktif->tanggalselesaiuas;
      }
      else
      {
        $filterdipakai['from'] = $semesteraktif->tanggalmulaiuts;
        $filterdipakai['to'] = $semesteraktif->tanggalselesaiuts;
      }
      $kalimatfilter = "Menampilkan seluruh tanggal ujian";
    }
    $presensis = Models\PresensiUjian::whereIn('idkelasparalel', $idkp)
                  ->where(function($query) use ($filterdipakai) {
                      return $query->where('tanggal', '>=', $filterdipakai['from'])->where('tanggal', '<=', $filterdipakai['to']);
                    })
                  ->get();
    $tanggal = $presensis->pluck('tanggal')->unique()->all();
    
    if($r->get('submitfilter') == "PDF")
    {
      $pdf = PDF::loadView('maf.laporan.printjadwalujiannrp-pdf', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'semesteraktif' => $semesteraktif, 'tanggalmulai' => $filterdipakai['from'], 'tanggalselesai' => $filterdipakai['to']));
      return $pdf->stream();
    }
    else if($r->get('submitfilter') == "Excel")
    {
      Excel::create('Laporan Jadwal Ujian Lengkap', function($excel) use($presensis, $tahun, $kalimatfilter, $filterdipakai, $tanggal, $datapikets, $semesteraktif) {
          $excel->sheet('Sheet 1', function($sheet) use($presensis, $tahun, $kalimatfilter, $filterdipakai, $tanggal, $datapikets, $semesteraktif) {
              $sheet->loadView('maf.laporan.printjadwalujiannrp-excel', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'semesteraktif' => $semesteraktif, 'tanggalmulai' => $filterdipakai['from'], 'tanggalselesai' => $filterdipakai['to']));
          });
      })->export('xls');
    }
    return view('maf.laporan.laporanjadwalujiannrp', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'semesteraktif' => $semesteraktif, 'tanggalmulai' => $filterdipakai['from'], 'tanggalselesai' => $filterdipakai['to']));
  }

	public function laporanUjianPenjaga()
	{
    $kalimatfilter = "";
    $filterdipakai = array();
    $presensis = array();
    $tahun = array();
    $filterdipakai['from'] = "";
    $filterdipakai['to'] = "";

    $semesteraktif = Models\Semester::getAktif();
    $jenisujian = $semesteraktif->getUtsUas();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $idkp = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->select('id')->get();
    $presensis = Models\PresensiUjian::whereIn('idkelasparalel', $idkp)
                  ->where(function($query) use ($jenisujian, $semesteraktif) {
                      if($jenisujian == "uas")
                      {
                        return $query->where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas);
                      }
                      else
                      {
                        return $query->where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts);
                      }
                    })
                  ->get();
    $tanggal = $presensis->pluck('tanggal')->unique()->all();

    $semesteraktif = Models\Semester::getAktif();
    $uasuts = $semesteraktif->getUtsUas();
    if($uasuts == "uas")
    {
      $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas)
                      ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                      ->get();
      $tanggalmulai = $semesteraktif->tanggalmulaiuas;
      $tanggalselesai = $semesteraktif->tanggalselesaiuas;
    }
    else
    {
      $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts)
                      ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                      ->get();
      $tanggalmulai = $semesteraktif->tanggalmulaiuts;
      $tanggalselesai = $semesteraktif->tanggalselesaiuts;
    }
    
    return view('maf.laporan.laporanjadwalujianpenjaga', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'datapikets' => $datapikets, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'tanggalmulai' => $tanggalmulai, 'tanggalselesai' => $tanggalselesai, 'semesteraktif' => $semesteraktif));
	}

	public function laporanUjianPenjagaFilter(Request $r)
	{
    $kalimatfilter = "";
    $filterdipakai = array();
    $presensis = array();
    $tahun = array();
    $filterdipakai['from'] = "";
    $filterdipakai['to'] = "";

    $semesteraktif = Models\Semester::getAktif();
    $jenisujian = $semesteraktif->getUtsUas();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $idkp = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->select('id')->get();
    if($r->get('from') != "" && $r->get('to') != "")
    {
      $filterdipakai['from'] = date("Y-m-d", strtotime($r->get('from')));
      $filterdipakai['to'] = date("Y-m-d", strtotime($r->get('to')));
      $kalimatfilter = "Menampilkan dari tanggal ". $filterdipakai['from'] . " sampai " . $filterdipakai['to'];
    }
    else if($r->get('from') != "")
    {
      $filterdipakai['from'] = date("Y-m-d", strtotime($r->get('from')));
      if($r->get('to') == "")
      {
        if($jenisujian == "uas")
          $filterdipakai['to'] = $semesteraktif->tanggalselesaiuas;
        else
          $filterdipakai['to'] = $semesteraktif->tanggalselesaiuts;
      }
      $kalimatfilter = "Menampilkan dari tanggal ". $filterdipakai['from'];
    }
    else if($r->get('to') != "")
    {
      $filterdipakai['to'] = date("Y-m-d", strtotime($r->get('to')));
      if($r->get('from') == "")
      {
        if($jenisujian == "uas")
          $filterdipakai['from'] = $semesteraktif->tanggalmulaiuas;
        else
          $filterdipakai['from'] = $semesteraktif->tanggalmulaiuts;
      }
      $kalimatfilter = "Menampilkan sampai tanggal " . $filterdipakai['to'];
    }
    else
    {
      if($jenisujian == "uas")
      {
        $filterdipakai['from'] = $semesteraktif->tanggalmulaiuas;
        $filterdipakai['to'] = $semesteraktif->tanggalselesaiuas;
      }
      else
      {
        $filterdipakai['from'] = $semesteraktif->tanggalmulaiuts;
        $filterdipakai['to'] = $semesteraktif->tanggalselesaiuts;
      }
      $kalimatfilter = "Menampilkan seluruh tanggal ujian";
    }
    $presensis = Models\PresensiUjian::whereIn('idkelasparalel', $idkp)
                  ->where(function($query) use ($filterdipakai) {
                      return $query->where('tanggal', '>=', $filterdipakai['from'])->where('tanggal', '<=', $filterdipakai['to']);
                    })
                  ->get();
    $tanggal = $presensis->pluck('tanggal')->unique()->all();
    $semesteraktif = Models\Semester::getAktif();
    $uasuts = $semesteraktif->getUtsUas();
    if($uasuts == "uas")
    {
      $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas)
                      ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                      ->get();
    }
    else
    {
      $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts)
                      ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                      ->get();
    }
    
    if($r->get('submitfilter') == "PDF")
    {
      $pdf = PDF::loadView('maf.laporan.printjadwalujianpenjaga-pdf', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'datapikets' => $datapikets, 'semesteraktif' => $semesteraktif, 'tanggalmulai' => $filterdipakai['from'], 'tanggalselesai' => $filterdipakai['to']));
      return $pdf->stream();
    }
    else if($r->get('submitfilter') == "Excel")
    {
      Excel::create('Laporan Jadwal Ujian Lengkap', function($excel) use($presensis, $tahun, $kalimatfilter, $filterdipakai, $tanggal, $datapikets, $semesteraktif) {
          $excel->sheet('Sheet 1', function($sheet) use($presensis, $tahun, $kalimatfilter, $filterdipakai, $tanggal, $datapikets, $semesteraktif) {
              $sheet->loadView('maf.laporan.printjadwalujianpenjaga-excel', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'datapikets' => $datapikets, 'semesteraktif' => $semesteraktif, 'tanggalmulai' => $filterdipakai['from'], 'tanggalselesai' => $filterdipakai['to']));
          });
      })->export('xls');
    }
    return view('maf.laporan.laporanjadwalujianpenjaga', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'datapikets' => $datapikets, 'semesteraktif' => $semesteraktif, 'tanggalmulai' => $filterdipakai['from'], 'tanggalselesai' => $filterdipakai['to']));
	}

	public function laporanUjianRuangan()
  {
    $kalimatfilter = "";
    $filterdipakai = array();
    $presensis = array();
    $tahun = array();
    $filterdipakai['from'] = "";
    $filterdipakai['to'] = "";

    $semesteraktif = Models\Semester::getAktif();
    $jenisujian = $semesteraktif->getUtsUas();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $idkp = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->select('id')->get();
    $presensis = Models\PresensiUjian::whereIn('idkelasparalel', $idkp)
                  ->where(function($query) use ($jenisujian, $semesteraktif) {
                      if($jenisujian == "uas")
                      {
                        return $query->where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas);
                      }
                      else
                      {
                        return $query->where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts);
                      }
                    })
                  ->get();
    $tanggal = $presensis->pluck('tanggal')->unique()->all();

    $semesteraktif = Models\Semester::getAktif();
    $uasuts = $semesteraktif->getUtsUas();
    if($uasuts == "uas")
    {
      $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas)
                      ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                      ->get();
      $tanggalmulai = $semesteraktif->tanggalmulaiuas;
      $tanggalselesai = $semesteraktif->tanggalselesaiuas;
    }
    else
    {
      $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts)
                      ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                      ->get();
      $tanggalmulai = $semesteraktif->tanggalmulaiuts;
      $tanggalselesai = $semesteraktif->tanggalselesaiuts;
    }
    
    return view('maf.laporan.laporanjadwalujianruangan', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'datapikets' => $datapikets, 'tanggalmulai' => $tanggalmulai, 'tanggalselesai' => $tanggalselesai, 'semesteraktif' => $semesteraktif));
  }

  public function laporanUjianRuanganFilter(Request $r)
  {
    $kalimatfilter = "";
    $filterdipakai = array();
    $presensis = array();
    $tahun = array();
    $filterdipakai['from'] = "";
    $filterdipakai['to'] = "";

    $semesteraktif = Models\Semester::getAktif();
    $jenisujian = $semesteraktif->getUtsUas();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $idjurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusan)->select('kodemk')->get();
    $mk = Models\Matakuliah::whereIn('kodemk', $kmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
    $idkp = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->select('id')->get();
    if($r->get('from') != "" && $r->get('to') != "")
    {
    	$filterdipakai['from'] = date("Y-m-d", strtotime($r->get('from')));
    	$filterdipakai['to'] = date("Y-m-d", strtotime($r->get('to')));
    	$kalimatfilter = "Menampilkan dari tanggal ". $filterdipakai['from'] . " sampai " . $filterdipakai['to'];
    }
    else if($r->get('from') != "")
    {
    	$filterdipakai['from'] = date("Y-m-d", strtotime($r->get('from')));
    	if($r->get('to') == "")
    	{
    		if($jenisujian == "uas")
    			$filterdipakai['to'] = $semesteraktif->tanggalselesaiuas;
    		else
    			$filterdipakai['to'] = $semesteraktif->tanggalselesaiuts;
    	}
    	$kalimatfilter = "Menampilkan dari tanggal ". $filterdipakai['from'];
    }
    else if($r->get('to') != "")
    {
    	$filterdipakai['to'] = date("Y-m-d", strtotime($r->get('to')));
    	if($r->get('from') == "")
    	{
    		if($jenisujian == "uas")
    			$filterdipakai['from'] = $semesteraktif->tanggalmulaiuas;
    		else
    			$filterdipakai['from'] = $semesteraktif->tanggalmulaiuts;
    	}
    	$kalimatfilter = "Menampilkan sampai tanggal " . $filterdipakai['to'];
    }
    else
    {
    	if($jenisujian == "uas")
    	{
  			$filterdipakai['from'] = $semesteraktif->tanggalmulaiuas;
  			$filterdipakai['to'] = $semesteraktif->tanggalselesaiuas;
    	}
  		else
  		{
  			$filterdipakai['from'] = $semesteraktif->tanggalmulaiuts;
  			$filterdipakai['to'] = $semesteraktif->tanggalselesaiuts;
  		}
  		$kalimatfilter = "Menampilkan seluruh tanggal ujian";
    }
    $presensis = Models\PresensiUjian::whereIn('idkelasparalel', $idkp)
                  ->where(function($query) use ($filterdipakai) {
                      return $query->where('tanggal', '>=', $filterdipakai['from'])->where('tanggal', '<=', $filterdipakai['to']);
                    })
                  ->get();
    $tanggal = $presensis->pluck('tanggal')->unique()->all();
    $semesteraktif = Models\Semester::getAktif();
    $uasuts = $semesteraktif->getUtsUas();
    if($uasuts == "uas")
    {
      $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas)
                      ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                      ->get();
    }
    else
    {
      $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)
                      ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts)
                      ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                      ->get();
    }
    
    if($r->get('submitfilter') == "PDF")
    {
      $pdf = PDF::loadView('maf.laporan.printjadwalujianruangan-pdf', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'datapikets' => $datapikets, 'semesteraktif' => $semesteraktif, 'tanggalmulai' => $filterdipakai['from'], 'tanggalselesai' => $filterdipakai['to']));
      return $pdf->stream();
    }
    else if($r->get('submitfilter') == "Excel")
    {
      Excel::create('Laporan Jadwal Ujian Lengkap', function($excel) use($presensis, $tahun, $kalimatfilter, $filterdipakai, $tanggal, $datapikets, $semesteraktif) {
          $excel->sheet('Sheet 1', function($sheet) use($presensis, $tahun, $kalimatfilter, $filterdipakai, $tanggal, $datapikets, $semesteraktif) {
              $sheet->loadView('maf.laporan.printjadwalujianruangan-excel', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'datapikets' => $datapikets, 'semesteraktif' => $semesteraktif, 'tanggalmulai' => $filterdipakai['from'], 'tanggalselesai' => $filterdipakai['to']));
          });
      })->export('xls');
    }
    return view('maf.laporan.laporanjadwalujianruangan', array('presensis' => $presensis, 'tahun' => $tahun, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'tanggal' => $tanggal, 'datapikets' => $datapikets, 'semesteraktif' => $semesteraktif, 'tanggalmulai' => $filterdipakai['from'], 'tanggalselesai' => $filterdipakai['to']));
  }

	public function laporanJagaUjianKaryawan()
	{
    $kalimatfilter = "";
    $filterdipakai = array();

    $semesteraktif = Models\Semester::getAktif();
    // $jenisujian = $semesteraktif->getUtsUas();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
    $allkaryawan = Models\Karyawan::where('idfakultas', $idfakultas)->where('aktifjagaujian', 'Ya')
                    ->orderBy('tipe', 'asc')->get();
    
    return view('maf.laporan.laporanjagaujiankaryawan', array('allkaryawan' => $allkaryawan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));    
	}

	public function laporanJagaUjianKaryawanFilter(Request $r)
	{
    $kalimatfilter = "";
    $filterdipakai = array();

    $semesteraktif = Models\Semester::getAktif();
    $jenisujian = $semesteraktif->getUtsUas();

    $idfakultas = Auth::guard('karyawan')->user()->idfakultas;

    if($r->get('npkdicari') != "")
    {
      $allkaryawan = Models\Karyawan::where('npk', $r->get('npkdicari'))->get();
    }
    else
    {
      $allkaryawan = Models\Karyawan::where('idfakultas', $idfakultas)->where('aktifjagaujian', 'Ya')
                    ->orderBy('tipe', 'asc')->get();
    }
    
    if($r->get('submitfilter') == "PDF")
    {
      $pdf = PDF::loadView('maf.laporan.printjagaujiankaryawan-pdf', array('allkaryawan' => $allkaryawan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
      return $pdf->stream();
    }
    else if($r->get('submitfilter') == "Excel")
    {
      Excel::create('Laporan Jadwal Ujian Lengkap', function($excel) use($allkaryawan, $kalimatfilter, $filterdipakai, $semesteraktif) {
          $excel->sheet('Sheet 1', function($sheet) use($allkaryawan, $kalimatfilter, $filterdipakai, $semesteraktif) {
              $sheet->loadView('maf.laporan.printjadwalujianruangan-excel', array('allkaryawan' => $allkaryawan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
          });
      })->export('xls');
    }
    return view('maf.laporan.laporanjagaujiankaryawan', array('allkaryawan' => $allkaryawan, 'kal' => $kalimatfilter, 'filterdipakai' => $filterdipakai, 'semesteraktif' => $semesteraktif));
	}
}
