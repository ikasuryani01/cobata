<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;

class KaryawanJurusanController extends Controller
{
  public function showDataKJurusan() 
  {
    $jurusans = Models\Jurusan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
    $karyawans = Models\Karyawan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->select('npk', 'nama')->get();
    $npkkaryawan = $karyawans->pluck('npk')->all();
    $data = Models\KaryawanDaftarJurusan::whereIn('npkkaryawan', $npkkaryawan)->orderBy('updated_at', 'desc')->get();
    $enumStatusAktif = Models\KaryawanDaftarJurusan::getEnum('statusaktif');
    $enumIsUtama = Models\KaryawanDaftarJurusan::getEnum('isutama');
    $enumJagaUjian = Models\KaryawanDaftarJurusan::getEnum('statusjagaujian');
    return view('maf.pengaturan.masterkelolajurusan', array('data' => $data, 'jurusans' => $jurusans, 'karyawans' => $karyawans, 'enumIsUtama' => $enumIsUtama, 'enumStatusAktif' => $enumStatusAktif, 'enumJagaUjian' => $enumJagaUjian, 'datahapus' => 'tidak'));
  }

  public function showDataKJurusans() 
  {
    $jurusans = Models\Jurusan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
    $karyawans = Models\Karyawan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->select('npk', 'nama')->get();
    $npkkaryawan = $karyawans->pluck('npk')->all();
    $data = Models\KaryawanDaftarJurusan::withTrashed()->whereIn('npkkaryawan', $npkkaryawan)->orderBy('updated_at', 'desc')->get();
    $enumStatusAktif = Models\KaryawanDaftarJurusan::getEnum('statusaktif');
    $enumIsUtama = Models\KaryawanDaftarJurusan::getEnum('isutama');
    $enumStatusJagaUjian = Models\KaryawanDaftarJurusan::getEnum('statusjagaujian');
    return view('maf.pengaturan.masterkelolajurusan', array('data' => $data, 'jurusans' => $jurusans, 'karyawans' => $karyawans, 'enumIsUtama' => $enumIsUtama, 'enumStatusAktif' => $enumStatusAktif, 'enumJagaUjian' => $enumJagaUjian, 'datahapus' => 'ya'));
  }

  public function storeDataKJurusan(Request $r) 
  {
  	if ($r->get('isutama') == 'Ya') {
  		$data = Models\KaryawanDaftarJurusan::withTrashed()
  			->where('npkkaryawan', $r->get('npk'))
  			->where('isutama', 'Ya')
  			->update(['isutama' => 'Tidak']);
  	}

	  $data = new Models\KaryawanDaftarJurusan(array(
        'npkkaryawan' => $r->get('npk'),
        'idjurusan' => $r->get('idjurusan'),
        'statusaktif' => $r->get('statusaktif'),
        'isutama' => $r->get('isutama'),
        'statusjagaujian' => $r->get('statusjagaujian')
  	));
  	$data->save();

  	$status = "Data berhasil ditambahkan";
    return redirect('/maf/jurusan')->with('status', $status);
  }

  public function updateDataKJurusan(Request $r) 
  {
  	if ($r->get('isutama') == 'Ya') {
  		$data = Models\KaryawanDaftarJurusan::withTrashed()
  			->where('npk', $r->get('npk'))
  			->where('isutama', 'Ya')
  			->update(['isutama' => 'Tidak']);
  	}
    $data = Models\KaryawanDaftarJurusan::withTrashed()->where('npk', $r->get('npk'))
              ->update(['isutama' => $r->get('isutama'), 'isaktif' => $r->get('isaktif'), 'idjurusan' => $r->get('idjurusan')]);

    $status = "Data berhasil diubah";
    return redirect('/maf/jurusan')->with('status', $status);
  }

  public function deleteDataKJurusan($npk, $idjurusan) {
    $Jabatan = Models\KaryawanDaftarJurusan::where('npk', $npk)->where('idjurusan', $idjurusan)->delete();
    // $Jabatan->delete();
    return redirect('maf/jurusan');
  }

  public function restoreDataKJurusan($npk, $idjurusan) {
    $Jabatan = Models\KaryawanDaftarJurusan::withTrashed()->where('npk', $npk)->where('idjurusan', $idjurusan)->restore();
    return back();
  }

  public function forcedeleteDataKJurusan($npk, $idjurusan) {
    $Jabatan = Models\KaryawanDaftarJurusan::withTrashed()->where('npk', $npk)->where('idjurusan', $idjurusan)->forceDelete();
    return back();
  }
  /* end of master Jabatan */
}
