<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;

class RuanganUjianController extends Controller
{
	/* Presensi Ujian */
  public function showJadwalUjian(Request $r) 
  {
  	//pilihan ids = jur r mk pju
    $ids = $r->get('ids');
    if(empty($ids))
    {
    	$ids = "mk";
    }

    $jurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan')->get();
    $kmk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $jurusans)->select('kodemk')->get();

    if($ids == "mk")
    {
    	$datas = Models\JadwalUjian::where('idsemester', Models\Semester::getAktifId())
    						->whereIn('kodemk', $kmk)
    						->has('mk.kp') //ambil yang punya mk sm kp aja
    						->get();

    	return view('maf.ruangujian.jadwalujian', array('datas' => $datas, 'ids' => $ids));
    }
    else if($ids == "r")
    {
      $datas = Models\Ruangan::get();

      return view('maf.ruangujian.jadwalujian', array('datas' => $datas, 'ids' => $ids));
    }
    else if($ids == "ju")
    {
    	
    }
    else if($ids == "pju")
    {
      $semesteraktif = Models\Semester::getAktif();
      $uasuts = $semesteraktif->getUtsUas();
      if($uasuts == "uas")
      {
        $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)
                        ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas)
                        ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                        ->get();
      }
      else
      {
        $datapikets = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)
                        ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts)
                        ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                        ->get();
      }

      $semuadosen = Models\Karyawan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                        ->where('tipe', 'ID0')
                        ->get();
      $semuakaryawan = Models\Karyawan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                        ->where('tipe', '!=', 'ID0')
                        ->get();
      $minggumaks = Models\JadwalUjian::where('idsemester', Models\Semester::getAktifId())->max('mingguke');
      $hari = Models\JadwalUjian::getEnum('hari');
      $jamke = Models\JadwalUjian::getEnum('jamke');
      $slotujian = Models\JadwalUjian::getSlotUjian(Models\Semester::getAktifId());
      // print_r($slotujian);
      // exit();
      return view('maf.ruangujian.jadwalujian', array('datapikets' => $datapikets, 'semesteraktif' => $semesteraktif, 'ids' => $ids, 'semuadosen' => $semuadosen, 'semuakaryawan' => $semuakaryawan, 'minggumaks' => $minggumaks, 'hari' => $hari, 'jamke' => $jamke, 'slotujian' => $slotujian));
    }
  }

  public function showDataPresensiUjian($idjadwalujian) 
  {
    $ju = Models\JadwalUjian::whereId($idjadwalujian)->first();
    $mk = Models\Matakuliah::where('kodemk', $ju->kodemk)->first();
    $datas = $ju->presensiUjian();
    return view('maf.ruangujian.presensiujian', array('datas' => $datas, 'mk' => $mk, 'idjadwalujian' => $idjadwalujian));
  }

  public function showDetailPresensiUjian($idpresensiujian) 
  {
    $datas = Models\PresensiUjian::whereId($idpresensiujian)->first();
    return view('maf.ruangujian.presensiujian-dtl', array('datas' => $datas));
  }

  public function prosesRuanganUjian() 
  {
    $jmlminggu = Models\JadwalUjian::where('idsemester', Models\Semester::getAktifId())->max('mingguke');
    $arrayhari = Models\JadwalUjian::getEnum('hari');
    $arrayjam = Models\JadwalUjian::getEnum('jamke');
    $semesteraktif = Models\Semester::getAktif();

    $uasuts = $semesteraktif->getUtsUas();

    $statustidakcukup = array();

    for ($i = 1; $i <= $jmlminggu; $i++) 
    {
      foreach ($arrayhari as $hari) 
      {
        $tanggal = "";
        if($uasuts == "uas")
          $tanggal = $semesteraktif->getTanggalUas($i, $hari);
        else
          $tanggal = $semesteraktif->getTanggalUts($i, $hari);
        //uda dpt tanggal
        if($hari != "Sabtu" && $hari != "Minggu")
        {
          foreach ($arrayjam as $jam) 
          {
            $jml = Models\JadwalUjian::where('mingguke', $i)
                    ->where('hari', $hari)
                    ->where('jamke', $jam)
                    ->where('tiperuangan'.$uasuts, 'Kelas')
                    ->where('tipeujian'.$uasuts, 'Tulis')
                    ->has('mk.kp')
                    ->get();

            if($jml->count() > 0)
            {
              $ruangans = Models\Ruangan::where('jenis', 'Kelas')
                            ->where('untukujian', 'Ya')
                            ->orderBy('kapasitasujian', 'desc')
                            ->orderBy('gedung', 'asc')
                            ->orderBy('lantai', 'asc')
                            ->get();

              $indeksruangan = 0;
              $kapruangan = $ruangans[$indeksruangan]->kapasitasujian;
              $maksruangan = $ruangans->count();
              $maksruangan--;

              /*$kmk = Models\JadwalUjian::where('mingguke', $i)
                      ->where('hari', $hari)
                      ->where('jamke', $jam)
                      ->where('tiperuangan'.$uasuts, 'Kelas') //yg lab nanti
                      ->where('tipeujian'.$uasuts, 'Tulis') // yg tugas sm presentasi jgn lupa diarahkan ke paj masing2
                      ->select('kodemk')
                      ->get();*/
              $kmk = $jml->pluck('kodemk')->unique()->all();
              // print_r($kmk);

              $mks = Models\Matakuliah::whereIn('kodemk', $kmk)->with('kp')->get();          

              foreach ($mks as $mk) 
              {
                if(count($mk->kp))
                {
                  foreach ($mk->kp as $datakp) 
                  {
                    if($datakp->adaPesertaKuliah())
                    {
                      echo "<br> datakp->adaPesertaKuliah() = " . $datakp->adaPesertaKuliah() . "<br>";
                      $jumlahpeserta = $datakp->hitungPesertaKuliah();
                      echo "<br> datakp->hitungPesertaKuliah() = " . $datakp->hitungPesertaKuliah() . "<br>";
                      $indekspesertakuliah = 0;
                      echo "<br> indekspesertakuliah = " . $indekspesertakuliah . "<br>";
                      while ($jumlahpeserta > 0) 
                      {
                        if($indeksruangan == $maksruangan) //indeks uda sama = ruangan nda cukup
                        {
                          echo "<br> indeksruangan = " . $indeksruangan . "<br>";
                          echo "<br> maksruangan = " . $maksruangan . "<br>";
                          $statusno = "Ruangan TIDAK CUKUP: Minggu ke-".$i.", Hari:" .$hari. " Jam ke-" .$jam. " Kode MK dan KP:" .$datakp->kodemk. " ". $datakp->kodekp.".<br>";
                          echo $statusno;
                          array_push($statustidakcukup, $statusno);
                          break;
                        }
                        else if($jumlahpeserta <= $kapruangan) //cukup 1 ruangan
                        {
                          echo "<br> masuk ke else if yang cukup 1 ruangan<br>";
                          echo "<br> jumlahpeserta = " . $jumlahpeserta . "<br>";
                          echo "<br> kapruangan = " . $kapruangan . "<br>";


                          $pb = new Models\PresensiUjian(array(
                            'tanggal' => $tanggal,
                            'idruangan' => $ruangans[$indeksruangan]->id,
                            'idkelasparalel' => $datakp->id,
                            'nrpawal' => $datakp->pesertaKuliah()[$indekspesertakuliah]->nrp,
                            'nrpakhir' => $datakp->pesertaKuliah()->pop()->nrp,
                            'jumlahpeserta' => $jumlahpeserta
                          ));
                          $pb->save();
                          
                          $kapruangan -= $jumlahpeserta;
                          $jumlahpeserta = 0;
                          
                          echo "<br> kapruangan - jumlahpeserta = " . $kapruangan . "<br>";

                        }
                        else if ($jumlahpeserta > $kapruangan) //nda cukup 1 ruangan
                        {
                          echo "<br> masuk ke else if yang NDA cukup 1 ruangan<br>";
                          echo "<br> jumlahpeserta = " . $jumlahpeserta . "<br>";
                          echo "<br> kapruangan = " . $kapruangan . "<br>";

                          $nextindekspeserta = $indekspesertakuliah + $kapruangan - 1;
                          echo "<br> nextindekspeserta = " . $nextindekspeserta . "<br>";
                          echo "<br> indekspesertakuliah = " . $indekspesertakuliah . "<br>";

                          $pb = new Models\PresensiUjian(array(
                            'tanggal' => $tanggal,
                            'idruangan' => $ruangans[$indeksruangan]->id,
                            'idkelasparalel' => $datakp->id,
                            'nrpawal' => $datakp->pesertaKuliah()[$indekspesertakuliah]->nrp,
                            'nrpakhir' => $datakp->pesertaKuliah()[$nextindekspeserta]->nrp,
                            'jumlahpeserta' => $kapruangan
                          ));
                          $pb->save();

                          $indekspesertakuliah = $nextindekspeserta + 1;
                          $jumlahpeserta -= $kapruangan;
                          $kapruangan = 0;
                        }

                        //pengecekan ruangan
                        if ($kapruangan == 0) 
                        {
                          $indeksruangan++;
                          if($indeksruangan != $maksruangan)
                          {
                            $kapruangan = $ruangans[$indeksruangan]->kapasitasujian;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

            $jmllab = Models\JadwalUjian::where('mingguke', $i)
                    ->where('hari', $hari)
                    ->where('jamke', $jam)
                    ->where('tiperuangan'.$uasuts, 'Laboratorium')
                    ->where('tipeujian'.$uasuts, 'Tulis')
                    ->has('mk.kp')
                    ->get();

            if($jmllab->count() > 0)
            {
              $ruangans = Models\Ruangan::where('jenis', 'Laboratorium')
                            ->where('untukujian', 'Ya')
                            ->orderBy('kapasitasujian', 'desc')
                            ->orderBy('gedung', 'asc')
                            ->orderBy('lantai', 'asc')
                            ->get();

              $indeksruangan = 0;
              $kapruangan = $ruangans[$indeksruangan]->kapasitasujian;
              $maksruangan = $ruangans->count();
              $maksruangan--;

              /*$kmk = Models\JadwalUjian::where('mingguke', $i)
                      ->where('hari', $hari)
                      ->where('jamke', $jam)
                      ->where('tiperuangan'.$uasuts, 'Kelas') //yg lab nanti
                      ->where('tipeujian'.$uasuts, 'Tulis') // yg tugas sm presentasi jgn lupa diarahkan ke paj masing2
                      ->select('kodemk')
                      ->get();*/
              $kmk = $jmllab->pluck('kodemk')->unique()->all();
              // print_r($kmk);

              $mks = Models\Matakuliah::whereIn('kodemk', $kmk)->with('kp')->get();          

              foreach ($mks as $mk) 
              {
                if(count($mk->kp))
                {
                  foreach ($mk->kp as $datakp) 
                  {
                    if($datakp->adaPesertaKuliah())
                    {
                      echo "<br> datakp->adaPesertaKuliah() = " . $datakp->adaPesertaKuliah() . "<br>";
                      $jumlahpeserta = $datakp->hitungPesertaKuliah();
                      echo "<br> datakp->hitungPesertaKuliah() = " . $datakp->hitungPesertaKuliah() . "<br>";
                      $indekspesertakuliah = 0;
                      echo "<br> indekspesertakuliah = " . $indekspesertakuliah . "<br>";
                      while ($jumlahpeserta > 0) {
                        if($indeksruangan == $maksruangan) //indeks uda sama = ruangan nda cukup
                        {
                          echo "<br> indeksruangan = " . $indeksruangan . "<br>";
                          echo "<br> maksruangan = " . $maksruangan . "<br>";
                          $statusno = "Ruangan tidak cukup: Minggu ke-".$i.", Hari:" .$hari. " Jam ke-" .$jam. " Kode MK dan KP:" .$datakp->kodemk. " ". $datakp->kodekp.".<br>";
                          echo $statusno;
                          array_push($statustidakcukup, $statusno);
                          break;
                        }
                        else if($jumlahpeserta <= $kapruangan) //cukup 1 ruangan
                        {
                          echo "<br> masuk ke else if yang cukup 1 ruangan<br>";
                          echo "<br> jumlahpeserta = " . $jumlahpeserta . "<br>";
                          echo "<br> kapruangan = " . $kapruangan . "<br>";


                          $pb = new Models\PresensiUjian(array(
                            'tanggal' => $tanggal,
                            'idruangan' => $ruangans[$indeksruangan]->id,
                            'idkelasparalel' => $datakp->id,
                            'nrpawal' => $datakp->pesertaKuliah()[$indekspesertakuliah]->nrp,
                            'nrpakhir' => $datakp->pesertaKuliah()->pop()->nrp,
                            'jumlahpeserta' => $jumlahpeserta
                          ));
                          $pb->save();
                          
                          $kapruangan -= $jumlahpeserta;
                          $jumlahpeserta = 0;
                          
                          echo "<br> kapruangan - jumlahpeserta = " . $kapruangan . "<br>";

                        }
                        else if ($jumlahpeserta > $kapruangan) //nda cukup 1 ruangan
                        {
                          echo "<br> masuk ke else if yang NDA cukup 1 ruangan<br>";
                          echo "<br> jumlahpeserta = " . $jumlahpeserta . "<br>";
                          echo "<br> kapruangan = " . $kapruangan . "<br>";

                          $nextindekspeserta = $indekspesertakuliah + $kapruangan - 1;
                          echo "<br> nextindekspeserta = " . $nextindekspeserta . "<br>";
                          echo "<br> indekspesertakuliah = " . $indekspesertakuliah . "<br>";

                          $pb = new Models\PresensiUjian(array(
                            'tanggal' => $tanggal,
                            'idruangan' => $ruangans[$indeksruangan]->id,
                            'idkelasparalel' => $datakp->id,
                            'nrpawal' => $datakp->pesertaKuliah()[$indekspesertakuliah]->nrp,
                            'nrpakhir' => $datakp->pesertaKuliah()[$nextindekspeserta]->nrp,
                            'jumlahpeserta' => $kapruangan
                          ));
                          $pb->save();

                          $indekspesertakuliah = $nextindekspeserta + 1;
                          $jumlahpeserta -= $kapruangan;
                          $kapruangan = 0;
                        }

                        //pengecekan ruangan
                        if ($kapruangan == 0) 
                        {
                          $indeksruangan++;
                          if($indeksruangan != $maksruangan)
                          {
                            $kapruangan = $ruangans[$indeksruangan]->kapasitasujian;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

            $jmltugaspre = Models\JadwalUjian::where('mingguke', $i)
                    ->where('hari', $hari)
                    ->where('jamke', $jam)
                    // ->where('tiperuangan'.$uasuts, 'Laboratorium')
                    ->where('tipeujian'.$uasuts, '!=', 'Tulis')
                    ->has('mk.kp')
                    ->get();

            if($jmltugaspre->count() > 0)
            {
              $kmk = $jmltugaspre->pluck('kodemk')->unique()->all();

              $mks = Models\Matakuliah::whereIn('kodemk', $kmk)->with('kp')->get();          

              foreach ($mks as $mk) 
              {
                if(count($mk->kp))
                {
                  foreach ($mk->kp as $datakp) 
                  {
                    if($datakp->adaPesertaKuliah())
                    {
                      $idjur = $datakp->pesertaKuliah()[0]->idjurusan;
                      $idrs = Models\Ruangan::where('jenis', 'Paj')
                            ->where('idjurusan', $idjur)
                            ->exists();
                      if($idrs)
                      {
                        $idrs = Models\Ruangan::where('jenis', 'Paj')
                            ->where('idjurusan', $idjur)->first()->id;
                      }
                      else
                        $idrs = 15; //alihin ke paj infor smntara

                      $pb = new Models\PresensiUjian(array(
                        'tanggal' => $tanggal,
                        'idruangan' => $idrs,
                        'idkelasparalel' => $datakp->id,
                        'nrpawal' => $datakp->pesertaKuliah()[0]->nrp,
                        'nrpakhir' => $datakp->pesertaKuliah()->pop()->nrp,
                        'jumlahpeserta' => $jumlahpeserta
                      ));
                      $pb->save();
                      
                      $jumlahpeserta = 0;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return redirect('/maf/presensiujian');
  }

  public function cariRuanganLain($idjadwalujian) 
  {
    $ju = Models\JadwalUjian::whereId($idjadwalujian)->first();
    $mk = Models\Matakuliah::where('kodemk', $ju->kodemk)->first();
    $datas = $ju->presensiUjian();

    $mkjadwalujiansama = Models\JadwalUjian::where('mingguke', $ju->mingguke)
                          ->where('hari', $ju->hari)
                          ->where('jamke', $ju->jamke)
                          ->select('kodemk')
                          ->get();

    $idkps = Models\KelasParalel::whereIn('kodemk', $mkjadwalujiansama)->select('id')->get();

    $ruangankepake = Models\PresensiUjian::whereIn('idkelasparalel', $idkps)->groupBy('idruangan')->selectRaw('sum(jumlahpeserta) as isiruangan, idruangan')->get();
    // print_r($ruangankepake[0]->isiruangan);
    // exit();
    $ruanganfull = array();
    foreach ($ruangankepake as $r) {
      $objr = Models\Ruangan::whereId($r->idruangan)->first();
      // dd($objr);
      if(!is_null($objr) && ($objr->kapasitasujian + $objr->cadangankapujian) <= $r->isiruangan)
      {
        // echo $objr->kapasitasujian+$objr->cadangankapujian;
        array_push($ruanganfull, $r->idruangan);
      }
    }
    // dd($mkjadwalujiansama);
    // exit();

    $ruangantersedia = Models\Ruangan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                        ->whereNotIn('id', $ruanganfull)
                        ->get();
    // dd($ruangantersedia);
    $semuaruangan = Models\Ruangan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                        ->select('id', 'nama')
                        ->get();

    return view('maf.ruangujian.cariruanganlain', array('datas' => $datas, 'mk' => $mk, 'idjadwalujian' => $idjadwalujian, 'ruangantersedia' => $ruangantersedia, 'semuaruangan' => $semuaruangan));
  }

  public function cariPenjagaLain($idjadwalujian) 
  {
    $ju = Models\JadwalUjian::whereId($idjadwalujian)->first();

    $mkjadwalujiansama = Models\JadwalUjian::where('mingguke', $ju->mingguke)
                          ->where('hari', $ju->hari)
                          ->where('jamke', $ju->jamke)
                          ->select('kodemk')
                          ->get();

    $idkps = Models\KelasParalel::whereIn('kodemk', $mkjadwalujiansama)->select('id')->get();

    $datas = Models\PresensiUjian::whereIn('idkelasparalel', $idkps)->orderBy('idkelasparalel', 'asc')->get();

    $dosentersedia = $ju->getDosenTersedia();
    $karyawantersedia = $ju->getKaryawanTersedia();

    return view('maf.ruangujian.caripenjagalain', array('datas' => $datas, 'idjadwalujian' => $idjadwalujian, 'dosentersedia' => $dosentersedia, 'karyawantersedia' => $karyawantersedia));
  }

  public function prosesPenjagaUjian() 
  {
    $idjurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan')->get();

    $allnpkdosens = Models\Karyawan::join('karyawan_daftar_jurusans', 'karyawans.npk', '=', 'karyawan_daftar_jurusans.npkkaryawan')
                      ->whereIn('karyawan_daftar_jurusans.idjurusan', $idjurusans)
                      ->where('karyawan_daftar_jurusans.isutama', "Ya")
                      ->where('karyawan_daftar_jurusans.statusaktif', 'Ya')
                      ->where('karyawan_daftar_jurusans.statusjagaujian', 'Ya')
                      ->where('karyawans.tipe', 'ID0')
                      ->get();
    // dd($allnpkdosens);
    $allnpkkaryawans = Models\Karyawan::where('tipe', '!=', 'ID0')
                      ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                      ->get(['npk']);
    // dd($allnpkkaryawans);
    $jmlminggu = Models\JadwalUjian::where('idsemester', Models\Semester::getAktifId())->max('mingguke');
    echo "<br>jmlminggu: " . $jmlminggu . "<br>";

    $arrayhari = Models\JadwalUjian::getEnum('hari');
    echo "<br>arrayhari: "; print_r($arrayhari); echo "<br>";

    $arrayjam = Models\JadwalUjian::getEnum('jamke');
    echo "<br>arrayjam: "; print_r($arrayjam); echo "<br>";

    $semesteraktif = Models\Semester::getAktif();

    $uasuts = $semesteraktif->getUtsUas();
    echo "<br>uasuts: " . $uasuts . "<br>";

    $tiperuangan = "";
    $tipeujian = "";

    //bagi penjaga ujian
    for ($i = 1; $i <= $jmlminggu; $i++) 
    {
      foreach ($arrayhari as $hari) 
      {
        if($hari != "Sabtu" && $hari != "Minggu")
        {
          $tanggal = "";
          if($uasuts == "uas")
            $tanggal = $semesteraktif->getTanggalUas($i, $hari);
          else
            $tanggal = $semesteraktif->getTanggalUts($i, $hari);

          echo "<br>tanggal: " . $tanggal . "<br>";

          foreach ($arrayjam as $jam) 
          {
            // nda ada cek mk buka krn asumsi uda yg diproses bakal buka smua
            $jml = Models\JadwalUjian::where('mingguke', $i)
                    ->where('hari', $hari)
                    ->where('jamke', $jam)
                    ->where('tiperuangan'.$uasuts, 'Kelas')
                    ->where('tipeujian'.$uasuts, 'Tulis')
                    ->has('mk.kp')
                    ->count();

            if($jml > 0)
            {
              $npkdosennext = "";
              $npkkaryawannext = "";

              //get all kodemk di jam itu -> mk x, y, z
              $kmk = Models\JadwalUjian::where('mingguke', $i)
                      ->where('hari', $hari)
                      ->where('jamke', $jam)
                      ->where('tiperuangan'.$uasuts, 'Kelas') // yang lab nanti
                      ->where('tipeujian'.$uasuts, 'Tulis') // yg tugas sm presentasi jgn lupa diarahkan ke paj masing2
                      ->select('kodemk')
                      ->get();

              //get id kp nya dr kodemk tadi -> kp x.a, x.b, y.a, y.b
              $kps = Models\KelasParalel::whereIn('kodemk', $kmk)->select('id')->get();

              //get id ruangan yang ada di presensinya -> idruangan 8->xa, 9->xa dan xb, dst, sama dosen dan karyawan nya jg yg uda jaga jam itu
              $presensijamitu = Models\PresensiUjian::where('tanggal', $tanggal)
                                ->whereIn('idkelasparalel', $kps)
                                ->get();
              echo "Data presensi: ";
              print("<pre>");
              print_r($presensijamitu);
              print("</pre>");
              echo "<br><br>";

              $idruangankepake = $presensijamitu->pluck('idruangan')->unique()->all();
              $npkdosensudahjaga = $presensijamitu->pluck('npkdosenjaga')->all();
              $npkkrysudahjaga = $presensijamitu->pluck('npkkaryawanjaga')->all();
              
              echo "Data dosen uda jaga: ";
              print("<pre>");
              print_r($npkdosensudahjaga);
              print("</pre>");
              echo "<br><br>";

              echo "Data Karyawan uda jaga: ";
              print("<pre>");
              print_r($npkkrysudahjaga);
              print("</pre>");
              echo "<br><br>";

              $npkkaryawannonjaga = Models\KaryawanNonjagaUjian::where('tanggal', $tanggal)
                                        ->where(function($query) use ($jam) {
                                            return $query->where('jamke', $jam)
                                                ->orWhere('jamke', 'Semua');
                                        })
                                        ->select('npkkaryawan')
                                        ->distinct()
                                        ->get();
              $npkkaryawannonjaga = $npkkaryawannonjaga->keyBy('npkkaryawan')->all();
              echo "Data Karyawan non jaga: ";
              print("<pre>");
              print_r($npkkaryawannonjaga);
              print("</pre>");
              echo "<br><br>";

              $npkkaryawanpiket = Models\PiketUjian::where('tanggal', $tanggal)
                                    ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                                    ->first();
              if(!empty($npkkaryawanpiket))
              {
                $npkkaryawanpiket = $npkkaryawanpiket->npk;
              }
              else
              {
                $npkkaryawanpiket = "-";
              }

              echo "Data Karyawan piket tanggal $tanggal: ";
              echo $npkkaryawanpiket;
              echo "<br><br>";

              //ini uda perjam kondisinya
              foreach ($idruangankepake as $idruangan) 
              {
                $npkkaryawannext = "-";
                $npkdosennext = "-";

                /*pilih karyawan*/
                $allnpkkaryawansurut = $allnpkkaryawans->sortBy('jumlahjaga')->keyBy('npk'); //ditampung baru
                // echo "x";
                $ambil = $allnpkkaryawansurut->first()->npk;
                // echo "y";
                while($npkkaryawannext == "-" && count($allnpkkaryawansurut) > 0)
                {
                  // echo "0";
                  // $ambil = $allnpkkaryawans[$indeks]->npk;
                  if(!array_key_exists($ambil, $npkkaryawannonjaga)) //pastiin nda ada di daftar nonjaga
                  {
                    // echo "1";
                    if(!in_array($ambil, $npkkrysudahjaga)) //pastiin nda ada di daftar presensi yang uda ada
                    {
                      // echo "2";
                      if($ambil != $npkkaryawanpiket)
                      {
                        // echo "3";
                        $npkkaryawannext = $ambil;
                        array_push($npkkrysudahjaga, $ambil);
                        // $npkkrysudahjaga[$npkkaryawannext] = "";
                        $allnpkkaryawans->where('npk', $ambil)->first()->jumlahjaga += 1;
                        $allnpkkaryawansurut->forget($ambil);
                        // echo "4";
                        // exit();
                        break;
                      }
                    }
                  }

                  if($allnpkkaryawansurut->count() > 0)
                  {
                    $allnpkkaryawansurut->forget($ambil);
                    $ambil = $allnpkkaryawansurut->first()->npk;
                  }
                  else
                  {
                    break;
                  }

                }

                if ($npkkaryawannext == "-") {
                  $npkkaryawannext = null;
                }

                /*pilih dosen*/
                $allnpkdosensurut = $allnpkdosens->sortBy('jumlahjaga')->keyBy('npk'); //ditampung baru
                $ambil = $allnpkdosensurut->first()->npk;
                // cari dosen2 pengajar kp2 itu dulu
                foreach ($kps as $kp) 
                {
                  $kp = Models\KelasParalel::whereId($kp->id)->first();
                  if(!array_key_exists($kp->npkdosenpengajar, $npkkaryawannonjaga)) //pastiin nda ada di daftar nonjaga
                  {
                    echo "1";
                    if(!in_array($kp->npkdosenpengajar, $npkdosensudahjaga)) //pastiin nda ada di daftar presensi yang uda ada
                    {
                      echo "2";
                      if($kp->npkdosenpengajar != $npkkaryawanpiket) //pastiin nda ada di daftar piket hari itu
                      {
                        // echo $kp->npkdosenpengajar;
                        if(!is_null($kp->npkdosenpengajar))
                        {
                          $npkdosennext = $kp->npkdosenpengajar;
                          // echo $npkdosennext;
                          // print_r($allnpkdosens);
                          array_push($npkdosensudahjaga, $npkdosennext);
                          // $npkdosensudahjaga[$npkdosennext] = "";
                          $allnpkdosens->where('npk', $npkdosennext)->first()->jumlahjaga += 1;
                          $allnpkdosensurut->forget($npkdosennext);
                          break;
                        }
                        
                      }                          
                    }
                  }
                }

                if($npkdosennext == "-") //masih nda dpt, cari lagi di jurusannya
                {
                  $nrp = $presensijamitu->pluck('nrpawal')->all();
                  $idjurusan = Models\Mahasiswa::where('nrp', $nrp[0])->first()->idjurusan;
                  $dosenjurusanitu = $allnpkdosensurut->where('idjurusan', $idjurusan)->all();

                  foreach($dosenjurusanitu as $d)
                  {
                    $ambil = $d->npk;
                    if(!array_key_exists($ambil, $npkkaryawannonjaga)) //pastiin nda ada di daftar nonjaga
                    {
                      if(!in_array($ambil, $npkdosensudahjaga)) //pastiin nda ada di daftar presensi yang uda ada
                      {
                        if($ambil != $npkkaryawanpiket)
                        {
                          $npkdosennext = $ambil;
                          array_push($npkdosensudahjaga, $npkdosennext);
                          // $npkdosensudahjaga[$npkdosennext] = "";
                          $allnpkdosens->where('npk', $npkdosennext)->first()->jumlahjaga += 1;
                          $allnpkdosensurut->forget($npkdosennext);
                          break; //uda dpt jadi hrs di break
                        }
                      }
                    }
                  }
                }
                
                if ($npkdosennext == "-") 
                {
                  //masih blm dpt juga? cari di fakultas itu: pake smua yg di allnpkdosens
                  // $allnpkdosensurut = $allnpkdosens->sortBy('jumlahjaga')->keyBy('npk');
                  $ambil = $allnpkdosensurut->first()->npk;
                  while($npkdosennext == "-" && count($allnpkdosensurut) > 0)
                  {
                    if(!array_key_exists($ambil, $npkkaryawannonjaga)) //pastiin nda ada di daftar nonjaga
                    {
                      if(!in_array($ambil, $npkdosensudahjaga)) //pastiin nda ada di daftar presensi yang uda ada
                      {
                        if($ambil != $npkkaryawanpiket) //pastiin nda ada di piket
                        {
                          $npkdosennext = $ambil;
                          array_push($npkdosensudahjaga, $npkdosennext);
                          // $npkdosensudahjaga[$npkdosennext] = "";
                          $allnpkdosens->where('npk', $npkdosennext)->first()->jumlahjaga += 1;
                          $allnpkdosensurut->forget($npkdosennext);
                          break;
                        }
                      }
                    }

                    if($allnpkdosensurut->count() > 0)
                    {
                      $allnpkdosensurut->forget($ambil);
                      $ambil = $allnpkdosensurut->first()->npk;
                    }
                    else
                    {
                      break;
                    }
                  }
                }
                
                if ($npkdosennext == "-") {
                  $npkdosennext = null;
                }

                $presensiupdate = Models\PresensiUjian::where('tanggal', $tanggal)
                                ->whereIn('idkelasparalel', $kps)
                                ->where('idruangan', $idruangan)
                                ->update(['npkdosenjaga' => $npkdosennext, 'npkkaryawanjaga' => $npkkaryawannext]);
              }
            }
          }
        }
      }
    }

    //bagi jadwal piket
    for ($i = 1; $i <= $jmlminggu; $i++) 
    {
      foreach ($arrayhari as $hari) 
      {
        if($hari != "Sabtu" && $hari != "Minggu")
        {
          echo "Masuk foreach hari : $hari - $i";
          echo "<br>";
          $tanggal = "";
          if($uasuts == "uas")
          {
            $tanggal = $semesteraktif->getTanggalUas($i, $hari); 
            $npkkaryawansudahpiket = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)
                                ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas)
                                ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                                ->select('npk')->get();
          }
          else
          {
            $tanggal = $semesteraktif->getTanggalUts($i, $hari);
            $npkkaryawansudahpiket = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)
                                ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts)
                                ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                                ->select('npk')->get();
          }
          if( ($uasuts == "uas" && $tanggal <= $semesteraktif->tanggalselesaiuas && $tanggal >= $semesteraktif->tanggalmulaiuas) || ($uasuts == "uts" && $tanggal <= $semesteraktif->tanggalselesaiuts && $tanggal >= $semesteraktif->tanggalmulaiuts) )
          {
            $npkkaryawansudahpiket = $npkkaryawansudahpiket->keyBy('npk')->all(); //ini uda jd array

            /*echo "Tanggal: $tanggal";
            echo "<br>";
            echo "NPK Karyawan sudah piket: ";
            echo "<br>";
            print "<pre>";
            print_r($npkkaryawansudahpiket);
            print "</pre>";
            echo "<br>";
            echo "<br>";
            if(!array_key_exists("ajkjs", $npkkaryawansudahpiket))
              echo "haha";
            else
              echo "huah";
            exit();*/

            $npkkaryawannonjaga = Models\KaryawanNonjagaUjian::where('tanggal', $tanggal)
                                      ->select('npkkaryawan')
                                      ->distinct()
                                      ->get();

            $npkkaryawannonjaga = $npkkaryawannonjaga->keyBy('npkkaryawan')->all(); //ini uda jd array

            /*echo "NPK Karyawan Non jaga: ";
            echo "<br>";
            print "<pre>";
            print_r($npkkaryawannonjaga);
            print "</pre>";
            echo "<br>";
            echo "<br>";
            exit();*/

            /*pilih dosen dulu*/
            // $allnpkdosens = $allnpkdosens->sortBy('jumlahjaga')->keyBy('npk'); //urutkan jumlah jaga asc
            $allnpkdosensurut = $allnpkdosens->sortBy('jumlahjaga')->keyBy('npk');
            foreach ($allnpkdosensurut as $value) {
              echo "AWALNYAA -> NPK: $value->npk - Jumlahjaga: " . $value->jumlahjaga;
              echo "<br>";
            }
            // echo "All NPK Dosens sort: ";
            // print "<pre>";
            // print_r($allnpkdosensurut);
            // print "</pre>";
            // echo "<br>";
            // echo "<br>";
            // exit();

            $indeks = 0;
            $npkpiket = "-";
            $ambil = "-";
            $jmlloop = 1;
            echo "Indeks: $indeks, NPK Piket - : $npkpiket";
            echo "<br>";
            echo "<br>";

            $ambil = $allnpkdosensurut->first()->npk;
            // reset($allnpkdosens);
            // $ambil = key($allnpkdosens);
            // dd($ambil);
            // exit();

            while($npkpiket == "-" && count($allnpkdosensurut) > 0 && $jmlloop < 3)
            {
              echo "MASUK WHILE -";
              echo "<br>";
              
              echo "Ambil: $ambil";
              echo "<br>";

              if(!array_key_exists($ambil, $npkkaryawannonjaga)) //pastiin nda ada di daftar nonjaga
              {
                echo "masuk in array nda ada di daftar non jaga";
                echo "<br>";
                echo $jmlloop*4;
                echo "<br>";

                if(!array_key_exists($ambil, $npkkaryawansudahpiket) || $allnpkdosens->where('npk', $ambil)->first()->jumlahjaga < $jmlloop*4)
                {
                  echo "masuk in array nda ada di daftar sudah piket";
                  echo "<br>";
                  $npkpiket = $ambil;
                  echo "npk piket jadi : $npkpiket";
                  echo "<br>";
                  // $indeksd = $allnpkdosens->search($ambil, true);
                  // $indeksd = array_search($npkpiket, array_column($allnpkdosens->toArray(), 'npk'));
                  // echo "indeksd: " . $indeksd;
                  $allnpkdosens->where('npk', $ambil)->first()->jumlahjaga+=4;
                  // echo "lalala: " . $allnpkdosens[$ambil]->jumlahjaga;
                  foreach ($allnpkdosensurut as $value) {
                    echo "-> NPK: " . $value->npk ." - "."Jumlahjaga: " . $value->jumlahjaga;
                    echo "<br>";
                  }
                  
                  break;
                  // array_push($npkkaryawanpiket, $ambil); //nda prlu soalnya perhari ya di ambil lagi
                  // $allnpkdosens[$indeks]->jumlahjaga = $allnpkdosens[$indeks]->jumlahjaga + 4; //dianggap nambah 4 kali jaga (?)
                  // break;
                }
              }
              $allnpkdosensurut->forget($ambil);
              // next($allnpkdosens);
              // $allnpkdosens->e

              if($allnpkdosensurut->count() > 0)
              {
                // echo "belum empteh count: " . $allnpkdosensurut->count();
                // echo "All NPK Dosens masuk if tidak empty: ";
                // print "<pre>";
                // print_r($allnpkdosensurut);
                // print "</pre>";
                // echo "<br>";
                // echo "<br>";
                $ambil = $allnpkdosensurut->first()->npk;
                // echo "<br> ambil skrg jadi $ambil <br>";
              }
              else
              {
                // break;
                $allnpkdosensurut = $allnpkdosens->sortBy('jumlahjaga')->keyBy('npk'); //direstart
                $ambil = $allnpkdosensurut->first()->npk;
                $jmlloop++; 
                echo "<br> JMLLOOP : $jmlloop <br>";
              }
            }

            echo "<br> npkpiket akhir: " . $npkpiket;
            // exit();

            // if ($npkpiket == "-") {
            //   $npkpiket = null;
            // }

            if(Models\PiketUjian::where('tanggal', $tanggal)->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->exists())
            {
              $data = Models\PiketUjian::where('tanggal', $tanggal)->first();
              $data->npk = $npkpiket;
              $data->save();
            }
            else
            {
              $datapiket = new Models\PiketUjian(array(
                    'tanggal' => $tanggal,
                    'npk' => $npkpiket,
                    'idfakultas' => Auth::guard('karyawan')->user()->idfakultas
                ));
              $datapiket->save();
            }
              
          }
        }
      }
    }
    return redirect('/maf/presensiujian');
  }

  public function refreshNRP(Request $r)
  {
    $preslama = Models\PresensiUjian::whereId($r->get('idpresensiujian'))->first();
    $ruanganlama = Models\Ruangan::find($preslama->idruangan);
    if(is_null($ruanganlama))
    {
      $ruanganlama = new Models\Ruangan(array(
                        'id' => 0,
                        'kapasitasujian' => 0,
                        'cadangankapujian' => 0
                      ));
    }
    $ruanganbaru = Models\Ruangan::find($r->get('idruangan'));
    // echo $preslama->id . " allalalaa ";
    $flagbisapindah = false;
    $status = "";
    // dd($ruanganbaru);
    if($ruanganbaru->isFullPerJadwal($r->get('idjadwalujian'))) //kl dia masih kosong (msh ada sisa kaps), despite mungkin dia ada isinya yg skrg
    {
      if($r->get('jumlahpeserta') > $preslama->jumlahpeserta) // kl misal dia kebutuhannya jd lbh besar dr sblmnya
      {
        if($ruanganbaru->id == $ruanganlama->id) //kl misal ruangannya sama
        {
          $selisih = $preslama->jumlahpeserta - $r->get('jumlahpeserta');
          if($selisih <= $ruanganbaru->sisaKapPerJadwal($r->get('idjadwalujian'))) //cek selisihnya, apa msh <= sisakap
          {
            $flagbisapindah = true;
          }
          else
          {
            $sisa = $ruanganbaru->sisaKapPerJadwal($r->get('idjadwalujian'));
            $status = "Tidak bisa pindah karena ruangan ini sudah tidak cukup, " . $selisih ." > ". $sisa;
          }
        }
        else //kl beda ruangannya
        {
          if($r->get('jumlahpeserta') <= $ruanganbaru->sisaKapPerJadwal($r->get('idjadwalujian'))) //cek apa msh <= sisakap
          {
            $flagbisapindah = true;
          }
          else
          {
            $jumbaru = $r->get('jumlahpeserta');
            $sisa = $ruanganbaru->sisaKapPerJadwal($r->get('idjadwalujian'));
            $status = "Tidak bisa pindah karena ruangan baru tidak cukup, ". $jumbaru ." > " .$sisa;
          }
        }
      }
      else if($r->get('jumlahpeserta') < $preslama->jumlahpeserta) //kl misal kbutuhannya jd lbh kecil
      {
        if($ruanganbaru->id == $ruanganlama->id) //kl misal ruangannya sama
        {
          //ruangannya sama dan kbutuhannya lebih kecil --> pasti bisa
          $flagbisapindah = true;
        }
        else //kl beda ruangannya, ttp hrs cek dl
        {
          if($r->get('jumlahpeserta') <= $ruanganbaru->sisaKapPerJadwal($r->get('idjadwalujian'))) //cek apa msh <= sisakap
          {
            $flagbisapindah = true;
          }
          else
          {
            $jumbaru = $r->get('jumlahpeserta');
            $sisa = $ruanganbaru->sisaKapPerJadwal($r->get('idjadwalujian'));
            $status = "Tidak bisa pindah karena ruangan baru tidak cukup, ". $jumbaru . " > " . $sisa;
          }
        }
      }
      else if($r->get('jumlahpeserta') == $preslama->jumlahpeserta)
      {
        if($ruanganbaru->id == $ruanganlama->id) //kl misal ruangannya sama
        {
          //ruangan dan kebutuhannya sama, nggak pindah ini km dibodohi wkwk
          $status = "Tidak mendeteksi perpindahan ruangan.";
        }
        else //kl beda ruangannya, ttp hrs cek dl
        {
          if($r->get('jumlahpeserta') <= $ruanganbaru->sisaKapPerJadwal($r->get('idjadwalujian'))) //cek apa msh <= sisakap
          {
            $flagbisapindah = true;
          }
          else
          {
            $jumbaru = $r->get('jumlahpeserta');
            $sisa = $ruanganbaru->sisaKapPerJadwal($r->get('idjadwalujian'));
            $status = "Tidak bisa pindah karena ruangan baru tidak cukup, " . $jumbaru . " > " . $sisa;
          }
        }
      }
    }
    else // dia flagbisapindahnya ttp false, ruangan baru fulls
    {
      $status = "Ruangan barunya full, isi: " . $ruanganbaru->hitungIsiPerJadwal($r->get('idjadwalujian')) . "/kap: ". $ruanganbaru->kapasitasujian+$ruanganbaru->cadangankapujian;
    }

    if($flagbisapindah)
    {
      $kumpulanpresensi = Models\PresensiUjian::where('idkelasparalel', $preslama->idkelasparalel)->get();

      $pesertaKuliahKPitu = $preslama->kp->pesertaKuliah();

      // dd($pesertaKuliahKPitu);

      $flagbeda = false;
      $indekspesertakuliah = 0;
      $nextindekspeserta = 0;
      $totalpesertakuliahkpitu = $pesertaKuliahKPitu->count();

      foreach ($kumpulanpresensi as $p) {
        if($totalpesertakuliahkpitu != 0)
        {
          if($p->id == $preslama->id) //ktmu yg berubah
          {
            $flagbeda = true;

            $presensilamahapus =  Models\PresensiUjian::whereId($r->get('idpresensiujian'))->first();
            $presensilamahapus->delete(); //ini uda kedelete

            $nextindekspeserta = $indekspesertakuliah + $r->get('jumlahpeserta') - 1;
            if($p->idruangan == $r->get('idruangan'))
            {
              $npkdosenjaga = $p->npkdosenjaga;
              $npkkaryawanjaga = $p->npkkaryawanjaga;
            }
            else //kl ruangannya beda
            {
              if(Models\PresensiUjian::where('tanggal', $p->tanggal)->where('idruangan', $r->get('idruangan'))->count() > 0)
              {
                $tmp = Models\PresensiUjian::where('tanggal', $p->tanggal)->where('idruangan', $r->get('idruangan'))->first();
                $npkdosenjaga = $tmp->npkdosenjaga;
                $npkkaryawanjaga = $tmp->npkkaryawanjaga;
              }
              else
              {
                $npkdosenjaga = null;
                $npkkaryawanjaga = null;
              }
            }

            //insert baru
            $pb = new Models\PresensiUjian(array(
                    'tanggal' => $p->tanggal,
                    'idruangan' => $r->get('idruangan'),
                    'idkelasparalel' => $p->idkelasparalel,
                    'nrpawal' => $pesertaKuliahKPitu[$indekspesertakuliah]->nrp,
                    'nrpakhir' => $pesertaKuliahKPitu[$nextindekspeserta]->nrp,
                    'npkdosenjaga' => $npkdosenjaga,
                    'npkkaryawanjaga' => $npkkaryawanjaga,
                    'jumlahpeserta' => $r->get('jumlahpeserta')
                  ));
            $pb->save();

            $totalpesertakuliahkpitu = $totalpesertakuliahkpitu - $r->get('jumlahpeserta');

            $indekspesertakuliah = $nextindekspeserta + 1;
          }
          else if($flagbeda)
          {
            $presensilamahapus =  Models\PresensiUjian::whereId($p->id)->first();
            $presensilamahapus->delete(); //ini uda kedelete

            //insert baru, sblmnya cek jumlah
            if($totalpesertakuliahkpitu > $p->jumlahpeserta) //diinsert sesuai data lama
            {
              $nextindekspeserta = $indekspesertakuliah + $p->jumlahpeserta - 1;
              $pb = new Models\PresensiUjian(array(
                      'tanggal' => $p->tanggal,
                      'idruangan' => $p->idruangan,
                      'idkelasparalel' => $p->idkelasparalel,
                      'nrpawal' => $pesertaKuliahKPitu[$indekspesertakuliah]->nrp,
                      'nrpakhir' => $pesertaKuliahKPitu[$nextindekspeserta]->nrp,
                      'npkdosenjaga' => $p->npkdosenjaga,
                      'npkkaryawanjaga' => $p->npkkaryawanjaga,
                      'jumlahpeserta' => $p->jumlahpeserta
                    ));
              $pb->save();

              $totalpesertakuliahkpitu = $totalpesertakuliahkpitu - $p->jumlahpeserta;

              $indekspesertakuliah = $nextindekspeserta + 1;
            }
            else //diinsert sisanya aja
            {
              $nextindekspeserta = $indekspesertakuliah + $totalpesertakuliahkpitu - 1;
              $pb = new Models\PresensiUjian(array(
                      'tanggal' => $p->tanggal,
                      'idruangan' => $p->idruangan,
                      'idkelasparalel' => $p->idkelasparalel,
                      'nrpawal' => $pesertaKuliahKPitu[$indekspesertakuliah]->nrp,
                      'nrpakhir' => $pesertaKuliahKPitu[$nextindekspeserta]->nrp,
                      'npkdosenjaga' => $p->npkdosenjaga,
                      'npkkaryawanjaga' => $p->npkkaryawanjaga,
                      'jumlahpeserta' => $totalpesertakuliahkpitu
                    ));
              $pb->save();
              
              $totalpesertakuliahkpitu = 0;

              $indekspesertakuliah = $nextindekspeserta + 1;
            }
          }
          else //flag false dan dia ga sama idnya, brrti kyk masih awal" gitu msh sama"
          {
            //dikurangi jumlahnya sama ubah indeks
            $totalpesertakuliahkpitu = $totalpesertakuliahkpitu - $p->jumlahpeserta;
            $nextindekspeserta = $indekspesertakuliah + $p->jumlahpeserta - 1;
            $indekspesertakuliah = $nextindekspeserta + 1;
          }
        }
        else //mesti delete i yg sisanya slama foreach blm habis
        {
          $presensilamahapus =  Models\PresensiUjian::whereId($p->id)->first();
          $presensilamahapus->delete(); //ini uda kedelete
        }
      }

      if($totalpesertakuliahkpitu > 0)
      {
        $nextindekspeserta = $indekspesertakuliah + $totalpesertakuliahkpitu - 1;
        $pb = new Models\PresensiUjian(array(
              'tanggal' => $preslama->tanggal,
              'idruangan' => null,
              'idkelasparalel' => $p->idkelasparalel,
              'nrpawal' => $pesertaKuliahKPitu[$indekspesertakuliah]->nrp,
              'nrpakhir' => $pesertaKuliahKPitu[$nextindekspeserta]->nrp,
              'jumlahpeserta' => $totalpesertakuliahkpitu
            ));
        $pb->save();

        $totalpesertakuliahkpitu = 0;

        $indekspesertakuliah = $nextindekspeserta + 1; 
      }

      return back()->with('status', "Berhasil memindahkan dan mengatur ulang NRP");
    }
    else
    {
      return back()->with('status', $status);
    }
  }

  public function refreshPenjagaUjian(Request $r)
  {
    $presdiganti = Models\PresensiUjian::whereId($r->get('idpresensiujian'))->first();
    $ju = Models\JadwalUjian::whereId($r->get('idjadwalujian'))->first();

    $dosenbaru = $r->get('npkdosenjaga');
    $karyawanbaru = $r->get('npkkaryawanjaga');

    $dosentersedia = $ju->getDosenTersedia();
    $karyawantersedia = $ju->getKaryawanTersedia();

    //get all kodemk di jam itu -> mk x, y, z
    $kmk = Models\JadwalUjian::where('mingguke', $ju->mingguke)
            ->where('hari', $ju->hari)
            ->where('jamke', $ju->jamke)
            ->select('kodemk')
            ->get();

    $idkps = Models\KelasParalel::whereIn('kodemk', $kmk)->select('id')->get();

    if($dosenbaru != $presdiganti->npkdosenjaga && $dosentersedia->contains('npk', $dosenbaru) && $karyawanbaru != $presdiganti->npkkaryawanjaga && $karyawantersedia->contains('npk', $karyawanbaru))
    {
      $presensiupdate = Models\PresensiUjian::where('tanggal', $presdiganti->tanggal)
                        ->whereIn('idkelasparalel', $idkps)
                        ->where('idruangan', $presdiganti->idruangan)
                        ->update(['npkdosenjaga' => $dosenbaru, 'npkkaryawanjaga' => $karyawanbaru]);
      
      return back()->with('status', 'Berhasil memperbarui Penjaga Ujian');
    }
    else if($dosenbaru != $presdiganti->npkdosenjaga && $dosentersedia->contains('npk', $dosenbaru) && $karyawanbaru == $presdiganti->npkkaryawanjaga)
    {
      //nggak ganti karyawan
      $presensiupdate = Models\PresensiUjian::where('tanggal', $presdiganti->tanggal)
                        ->whereIn('idkelasparalel', $idkps)
                        ->where('idruangan', $presdiganti->idruangan)
                        ->update(['npkdosenjaga' => $dosenbaru]);
      
      return back()->with('status', 'Berhasil memperbarui Dosen Penjaga Ujian');
    }
    else if($karyawanbaru != $presdiganti->npkkaryawanjaga && $dosentersedia->contains('npk', $karyawanbaru) && $dosenbaru == $presdiganti->npkdosenjaga)
    {
      //dosennya nggak ganti
      $presensiupdate = Models\PresensiUjian::where('tanggal', $presdiganti->tanggal)
                        ->whereIn('idkelasparalel', $idkps)
                        ->where('idruangan', $presdiganti->idruangan)
                        ->update(['npkkaryawanjaga' => $karyawanbaru]);
      
      return back()->with('status', 'Berhasil memperbarui Karyawan Penjaga Ujian');
    }
    else
    {
      if(!$dosentersedia->contains('npk', $dosenbaru))
      {
        return back()->with('status', 'Dosen sudah menjaga');
      }
      elseif(!$karyawantersedia->contains('npk', $karyawanbaru))
      {
        return back()->with('status', 'Karyawan sudah menjaga');
      }
    }

    dd($dosentersedia);
    exit();
  }

  public function updatePiket(Request $r)
  {
    $id = $r->get('id');
    // echo $id;
    // exit();
    $tanggal = $r->get('tanggal-'.$id);
    $npkbaru = $r->get('karyawans-npk-'.$id);
    $karyawansudahjaga = Models\PresensiUjian::where('tanggal', $tanggal)->select('npkdosenjaga')->distinct()->get();
    // echo $npkbaru; echo "<br>";
    // print_r($karyawansudahjaga->toArray());
    // exit();
    $karyawannonjaga = Models\KaryawanNonjagaUjian::where('tanggal', $tanggal)->select('npkkaryawan')->distinct()->get();

    foreach ($karyawansudahjaga as $datasudahjaga) 
    {
      if($npkbaru == $datasudahjaga->npkdosenjaga)
      {
        return back()->with('status', 'Tidak berhasil memperbarui. Dosen sudah ada menjaga ruang ujian pada tanggal tersebut.');
      }
    }
    foreach ($karyawannonjaga as $data) 
    {
      if($npkbaru == $data->npkkaryawan)
      {
        return back()->with('status', 'Tidak berhasil memperbarui. Dosen tidak bisa menjaga pada tanggal tersebut.');
      }
    }
    $data = Models\PiketUjian::where('id', $id)->first();
    $data->npk = $r->get('karyawans-npk-'.$id);
    $data->save();
    return back();

    /*if(!in_array($npkbaru, $karyawansudahjaga->toArray()))
    {
      echo "masuksini";
      if(!in_array($npkbaru, $karyawannonjaga->toArray()))
      {
        echo "ma";
        exit();
        $data = Models\PiketUjian::where('id', $id)->first();
        $data->npk = $r->get('karyawans-npk-'.$id);
        $data->save();
        return back();
      }
      else
        return back()->with('status', 'Tidak berhasil memperbarui. Karyawan tidak bisa menjaga pada tanggal tersebut.');
    }
    else
      return back()->with('status', 'Tidak berhasil memperbarui. Sudah ada menjaga ruang ujian pada tanggal tersebut.');*/ 
  }
}
