<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class DosenPengampuMkController extends Controller
{
    /*Master Dosenmk*/
    public function showDataDosenmk(Request $r)
    {
        $idsemester = $r->get('ids');
        if(!empty($idsemester))
        {
            if($idsemester != 0)
            {
                $dosenmks = Models\AmpuDosenMatakuliahSemester::where('idsemester', $idsemester)->get();
            }
        }
        else
        {
            $idsemester = Models\Semester::getAktif()->id;
            $dosenmks = Models\AmpuDosenMatakuliahSemester::where('idsemester', $idsemester)->get();
        }
        $semester = Models\Semester::all();
        return view('paj.masterdosenmk', array('dosenmks' => $dosenmks, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function showDataDosenmks(Request $r)
    {
        $idsemester = $r->get('ids');
        if(!empty($idsemester))
        {
            if($idsemester != 0)
            {
                $dosenmks = Models\AmpuDosenMatakuliahSemester::withTrashed()->where('idsemester', $idsemester)->get();
            }
        }
        else
        {
            $idsemester = Models\Semester::getAktif()->id;
            $dosenmks = Models\AmpuDosenMatakuliahSemester::withTrashed()->where('idsemester', $idsemester)->get();
        }
        $semester = Models\Semester::all();
        return view('paj.masterdosenmk', array('dosenmks' => $dosenmks, 'datahapus' => 'ya', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function addDataDosenmk()
    {
        $matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
            ->where(function($query)
                {
                    $idsemesteraktif = Models\Semester::getAktif()->id;
                    $kodemksudah = Models\AmpuDosenMatakuliahSemester::where('idsemester', $idsemesteraktif)->select('kodemk')->get();
                    return $query->whereNotIn('kodemk', $kodemksudah);
                })
            ->get();
        $dosens = Models\Karyawan::where('tipe', 'ID0')->get();
        // $enumHari = Models\AmpuDosenMatakuliahSemester::getEnum('hari');
        return view('paj.masterdosenmk-add', array('mode' => 'add', 'matakuliah' => $matakuliah, 'dosens' => $dosens));
    }

    public function storeDataDosenmk(Request $r)
    {
        $kodemk = explode(" ", $r->get('kodemk'));
        $kodemk = $kodemk[0];

        $npkdosen = explode(" ", $r->get('npkdosen'));
        $npkdosen = $npkdosen[0];

        $idsemesteraktif = Models\Semester::getAktif()->id;
        $dosenmkbaru = new Models\AmpuDosenMatakuliahSemester(array(
                'kodemk' => $kodemk,
                'npkdosen' => $npkdosen,
                'idsemester' => $idsemesteraktif
            ));
        $dosenmkbaru->save();
        $statussave = "Data Dosenmk berhasil ditambahkan";
        return redirect('/paj/masterdosenmk')->with('statussave', $statussave);
    }

    public function editDataDosenmk($id)
    {
        $matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
            ->where(function($query)
                {
                    $idsemesteraktif = Models\Semester::getAktif()->id;
                    $kodemksudah = Models\AmpuDosenMatakuliahSemester::where('idsemester', $idsemesteraktif)->select('kodemk')->get();
                    return $query->whereNotIn('kodemk', $kodemksudah);
                })
            ->get();
        $enumHari = Models\AmpuDosenMatakuliahSemester::getEnum('hari');
        $dosenmk = Models\AmpuDosenMatakuliahSemester::whereId($id)->first();
        return view('paj.masterdosenmk-add', array('mode' => 'edit', 'dosenmk' => $dosenmk, 'enumHari' => $enumHari, 'matakuliah' => $matakuliah));
    }

    public function updateDataDosenmk(Request $r, $id)
    {
        $dosenmk = Models\AmpuDosenMatakuliahSemester::withTrashed()->where('id', $id)->first();
        $dosenmk->mingguke = $r->get('mingguke');
        $dosenmk->hari = $r->get('hari');
        $dosenmk->jamke = $r->get('jamke');
        $dosenmk->kodemk = $r->get('kodemk');
        $dosenmk->save();

        $statussave = "Data Dosenmk berhasil diubah";
        return redirect('/paj/masterdosenmk')->with('statussave', $statussave);
    }

    public function deleteDataDosenmk($nrp)
    {
        $dosenmk = Models\AmpuDosenMatakuliahSemester::where('nrp', $nrp)->first();
        $dosenmk->deleted_by = Auth::user()->id;
        $dosenmk->save();
        $dosenmk->delete();
        return back();
    }

    public function restoreDataDosenmk($nrp)
    {
        $dosenmk = Models\AmpuDosenMatakuliahSemester::withTrashed()->where('nrp', $nrp)->first();
        $dosenmk->deleted_by = null;
        $dosenmk->save();
        $dosenmk->restore();
        return back();
    }

    public function forcedeleteDataDosenmk($nrp)
    {
        $dosenmk = Models\AmpuDosenMatakuliahSemester::withTrashed()->where('nrp', $nrp)->first();
        $dosenmk->forceDelete();
        return back();
    }
    /*end of master dosenmk*/
}
