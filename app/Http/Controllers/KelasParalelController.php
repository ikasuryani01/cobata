<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class KelasParalelController extends Controller
{
    /*Master KelasParalel*/
    public function showDataKelasParalel(Request $r)
    {
        //hrsnya ini diambil dari mk
    	$idsemester = $r->get('ids');
        $kmk = Models\JurusanDaftarMatakuliah::where('idjurusan', Auth::guard('karyawan')->user()->getIdJurusan())->select('kodemk')->distinct()->get();
        $matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
                        ->whereIn('kodemk', $kmk)->select('kodemk')->get();
    	if(!empty($idsemester))
    	{
    		if($idsemester != 0)
    		{
    			$kps = Models\KelasParalel::where('idsemester', $idsemester)->whereIn('kodemk', $kmk)->get();
    		}
    	}
    	else
    	{
    		$kps = Models\KelasParalel::whereIn('kodemk', $kmk)->where('idsemester', Models\Semester::getAktifId())->get();
            $idsemester = Models\Semester::getAktifId();
    	}
    	$semester = Models\Semester::all();

    	return view('paj.masterkp', array('kps' => $kps, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function showDataKelasParalels(Request $r)
    {
        //hrsnya ini diambil dari mk
    	$idsemester = $r->get('ids');
        $kmk = Models\JurusanDaftarMatakuliah::where('idjurusan', Auth::guard('karyawan')->user()->getIdJurusan())->select('kodemk')->distinct()->get();
        $matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
                        ->whereIn('kodemk', $kmk)->select('kodemk')->get();
    	if(!empty($idsemester))
    	{
    		if($idsemester != 0)
    		{
    			$kps = Models\KelasParalel::withTrashed()->where('idsemester', $idsemester)->whereIn('kodemk', $kmk)->get();
    		}
    	}
    	else
    	{
    		$kps = Models\KelasParalel::withTrashed()->whereIn('kodemk', $kmk)->where('idsemester', Models\Semester::getAktifId())->get();
        }
    	$semester = Models\Semester::all();
    	return view('paj.masterkp', array('kps' => $kps, 'datahapus' => 'ya', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function addDataKelasParalel()
    {
        $kmk = Models\JurusanDaftarMatakuliah::where('idjurusan', Auth::guard('karyawan')->user()->getIdJurusan())->select('kodemk')->distinct()->get();
	    $matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
                        ->whereIn('kodemk', $kmk)
                		->where(function($query)
                        {
                        	$idsemesteraktif = Models\Semester::getAktif()->id;
                        	$kodemksudah = Models\KelasParalel::where('idsemester', $idsemesteraktif)->select('kodemk')->get();
                        	return $query->whereNotIn('kodemk', $kodemksudah);
                        })
		                ->get();
    	$dosen = Models\Karyawan::where('tipe', 'ID0')->get(['nama', 'npk']);
    	/*print_r($dosen);
    	exit();*/
    	$enumStatusAktif = Models\KelasParalel::getEnum('statusaktif');
    	return view('paj.masterkp-add', array('mode' => 'add', 'enumStatusAktif' => $enumStatusAktif, 'matakuliah' => $matakuliah, 'dosen' => $dosen));
    }

    public function storeDataKelasParalel(Request $r)
    {
      $kodemk = explode(" ", $r->get('kodemk'));
      $kodemk = $kodemk[0];
    	$idsemesteraktif = Models\Semester::getAktif()->id;
        $jumlahkp = $r->get('jumlahkp');

        for ($i = 1; $i <= $jumlahkp ; $i++) { 
            $kpbaru = new Models\KelasParalel(array(
                'kodekp' => $r->get('kodekp-' . $i),
                'kapasitas' => $r->get('kapasitas-' . $i),
                'statusaktif' => $r->get('statusaktif-' . $i),
                'npkdosenpengajar' => $r->get('npkdosenpengajar-' . $i),
                'idsemester' => $idsemesteraktif,
                'kodemk' => $kodemk,
            ));
            $kpbaru->save();
        }
        
        $statussave = "Data Kelas Paralel berhasil ditambahkan";
    	return redirect('/paj/masterkp')->with('statussave', $statussave);
    }

    public function aktifkanKelasParalel($id)
    {
        $kp = Models\KelasParalel::whereId($id)
                ->update(['statusaktif' => 'Ya']);
        return back();
    }

    public function nonaktifkanKelasParalel($id)
    {
        $kp = Models\KelasParalel::whereId($id)
                ->update(['statusaktif' => 'Tidak']);
        return back();
    }

    public function editDataKelasParalel($id)
    {
      $dosen = Models\Karyawan::where('tipe', 'ID0')->get(['nama', 'npk']);
    	$kp = Models\KelasParalel::whereId($id)->first();
    	return view('paj.masterkp-add', array('mode' => 'edit', 'kp' => $kp, 'dosen' => $dosen));
    }

    public function updateDataKelasParalel(Request $r, $id)
    {
    	$kp = Models\KelasParalel::withTrashed()->where('id', $id)->first();
        if($kp->kodekp != $r->get('kodekp')) // dia ganti kodekp
        {
            $cekkodekpdobel = Models\KelasParalel::withTrashed()->where('kodekp', $r->get('kodekp'))->where('kodemk', $r->get('kodemk'))->count();
            if($cekkodekpdobel == 0) // blm ada nama kp itu, bs diganti
            {
                $kp->kodekp = $r->get('kodekp');
                $kp->kapasitas = $r->get('kapasitas');
                $kp->statusaktif = $r->get('statusaktif');
                $kp->npkdosenpengajar = $r->get('npkdosenpengajar');
                $kp->save();
            }
            else
            {
                $status = "Tidak bisa mengubah nama KP. Data sudah ada";
                return back()->with('status', $status)->withInput();
            }
        }
        else //dia ga ganti kode kpnya
        {
            $kp->kapasitas = $r->get('kapasitas');
            $kp->statusaktif = $r->get('statusaktif');
            $kp->npkdosenpengajar = $r->get('npkdosenpengajar');
            $kp->save();
        }
        $statussave = "Data Kelas Paralel berhasil diubah";
        
        return redirect('/paj/masterkp')->with('statussave', $statussave);
    }

    public function deleteDataKelasParalel($id)
    {
    	$kp = Models\KelasParalel::where('id', $id)->first();
        $kp->delete();
        return back();
    }

    public function restoreDataKelasParalel($id)
    {
    	$kp = Models\KelasParalel::withTrashed()->where('id', $id)->first();
        $kp->restore();
        return back();
    }

    public function forcedeleteDataKelasParalel($id)
    {
        $kp = Models\KelasParalel::withTrashed()->where('id', $id)->first();
        $kp->forceDelete();
        return back();
    }
    /*end of master kp*/
}
