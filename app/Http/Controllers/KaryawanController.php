<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class KaryawanController extends Controller
{
    /*Master Karyawan*/
    public function showDataKaryawan(Request $r)
    {
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            $npks = Models\KaryawanDaftarJurusan::where('idjurusan', Auth::guard('karyawan')->user()->getIdJurusan())->select('npkkaryawan')->distinct()->get();            
            $karyawans = Models\Karyawan::whereIn('npk', $npks)->get();
            return view('paj.masterkaryawan', array('karyawans' => $karyawans, 'datahapus' => 'tidak'));
        }
        else if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "maf")
        {
            $idjurusan = $r->get('ids');
            $namajurusan = "";
            if(!empty($idjurusan) && $idjurusan != 0)
            {
                $namajurusan = "Jurusan " . Models\Jurusan::where('IdJurusan', $idjurusan)->first()->Nama;
                $npks = Models\KaryawanDaftarJurusan::where('idjurusan', $idjurusan)->select('npkkaryawan')->distinct()->get();
                $karyawans = Models\Karyawan::whereIn('npk', $npks)->get();
            }
            else
            {
                $namajurusan = "Semua Jurusan";
                $idjurusan = 0;
                $karyawans = Models\Karyawan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
            }
            
            $jurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
            return view('maf.master.masterkaryawan', array('ids' => $idjurusan, 'karyawans' => $karyawans, 'datahapus' => 'tidak', 'jurusans' => $jurusans, 'namajurusan' => $namajurusan));
        }
    }

    public function showDataKaryawans()
    {
    	if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            $npks = Models\KaryawanDaftarJurusan::where('idjurusan', Auth::guard('karyawan')->user()->getIdJurusan())->select('npkkaryawan')->distinct()->get();
            $karyawans = Models\Karyawan::withTrashed()->whereIn('npk', $npks)->get();
            return view('paj.masterkaryawan', array('karyawans' => $karyawans, 'datahapus' => 'ya'));
        }
        else if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "maf")
        {
            $idjurusan = $r->get('ids');
            $namajurusan = "";
            if(!empty($idjurusan) && $idjurusan != 0)
            {
                $namajurusan = "Jurusan " . Models\Jurusan::where('IdJurusan', $idjurusan)->first()->Nama;
                $npks = Models\KaryawanDaftarJurusan::where('idjurusan', $idjurusan)->select('npkkaryawan')->distinct()->get();
                $karyawans = Models\Karyawan::withTrashed()->whereIn('npk', $npks)->get();
            }
            else
            {
                $namajurusan = "Semua Jurusan";
                $idjurusan = 0;
                $karyawans = Models\Karyawan::withTrashed()->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
            }
            
            $jurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
            return view('maf.master.masterkaryawan', array('ids' => $idjurusan, 'karyawans' => $karyawans, 'datahapus' => 'ya', 'jurusans' => $jurusans, 'namajurusan' => $namajurusan));
        }
    }

    public function addDataKaryawanExternal()
    {
        $mhs = DB::connection('mysql2b')->select("SELECT * FROM karyawan");
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return view('paj.masterkaryawan-addext', array('mhs' => $mhs));
        }
        else
        {
            return view('maf.master.masterkaryawan-addext', array('mhs' => $mhs));
        }
    }

    public function storeDataKaryawanExternal()
    {
        $coba = DB::select("SELECT * FROM karyawans ORDER BY npk INTO OUTFILE 'C://xampp/htdocs/ta/public/backupdb/karyawans_" . (new \DateTime())->format('Y.m.d.H.i.s') . ".csv' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n'");
        // $hapusdata = DB::select(DB::raw('TRUNCATE TABLE mata_kuliahs'));
        $mhs = DB::connection('mysql2b')->select("SELECT * FROM karyawan");
        foreach($mhs as $m)
        {
            if (!Models\Karyawan::where('npk', '=', $m->NPK)->exists()) 
            {
                $karyawanbaru = new Models\Karyawan(array(
                    'npk' => $r->get('npk'),
                    'nidn' => $r->get('nidn'),
                    'gelardepan' => $r->get('gelardepan'),
                    'gelarbelakang' => $r->get('gelarbelakang'),
                    'nama' => $r->get('nama'),
                    'tipe' => $r->get('tipe'),
                    'jenis' => $r->get('jenis'),
                    'idfakultas' => $r->get('idfakultas'),
                    // 'idsatker' => $r->get('idsatker'),
                    'idstatusaktif' => $r->get('idstatusaktif'),
                    'aktifjagaujian' => $r->get('aktifjagaujian')
                ));
                $karyawanbaru->save();
            }
        }
        $statussave = "Data berhasil ditambahkan";
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return redirect('/paj/masterkaryawan')->with('statussave', $statussave);
        }
        else
        {
            return redirect('/maf/masterkaryawan')->with('statussave', $statussave);
        }
    }

    public function addDataKaryawan()
    {
    	$enumTipe = Models\Karyawan::getEnum('tipe');
    	$enumJenis = Models\Karyawan::getEnum('jenis');
        $dataJurusan = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->get();

    	// $dataFakultas = Models\Fakultas::all(['IdFakultas', 'Nama']);
    	// $dataSatker = Models\SatuanKerja::all(['id', 'nama']);

        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return view('paj.masterkaryawan-add', array('mode' => 'add', 'enumTipe' => $enumTipe, 'enumJenis' => $enumJenis));
        }
        else
        {
            return view('maf.master.masterkaryawan-add', array('mode' => 'add', 'enumTipe' => $enumTipe, 'enumJenis' => $enumJenis, 'dataJurusan' => $dataJurusan));
        }   	
    }

    public function storeDataKaryawan(Request $r)
    {
        $karyawanbaru = new Models\Karyawan(array(
                'npk' => $r->get('npk'),
                'gelardepan' => $r->get('gelardepan'),
                'gelarbelakang' => $r->get('gelarbelakang'),
                'nama' => $r->get('nama'),
                'tipe' => $r->get('tipe'),
                'jenis' => $r->get('jenis'),
                'idfakultas' => $r->get('idfakultas'),
                'idstatusaktif' => $r->get('idstatusaktif'),
                'aktifjagaujian' => $r->get('aktifjagaujian')
            ));
        $karyawanbaru->save();

        $datajur = new Models\KaryawanDaftarJurusan(array(
                'npkkaryawan' => $r->get('npk'),
                'idjurusan' => $r->get('idjurusan'),
                'statusaktif' => 'Ya',
                'isutama' => 'Ya',
                'statusjagaujian' => 'Ya',
            ));
        $datajur->save();
        $statussave = "Data Karyawan berhasil ditambahkan";
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return redirect('/paj/masterkaryawan')->with('status', $statussave);
        }
        else
        {
            return redirect('/maf/masterkaryawan')->with('status', $statussave);
        }
    }

    public function editDataKaryawan($npk)
    {
    	$karyawan = Models\Karyawan::withTrashed()->where('npk', $npk)->first();
    	$enumTipe = Models\Karyawan::getEnum('tipe');
    	$enumJenis = Models\Karyawan::getEnum('jenis');
    	// $dataFakultas = Models\Fakultas::all(['IdFakultas', 'Nama']);
    	// $dataSatker = Models\SatuanKerja::all(['id', 'nama']);

        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return view('paj.masterkaryawan-add', array('mode' => 'edit', 'karyawan' => $karyawan, 'enumTipe' => $enumTipe, 'enumJenis' => $enumJenis));
        }
        else
        {
            return view('maf.master.masterkaryawan-add', array('mode' => 'edit', 'karyawan' => $karyawan, 'enumTipe' => $enumTipe, 'enumJenis' => $enumJenis));
        }
    }

    public function updateDataKaryawan(Request $r, $npk)
    {
    	$karyawan = Models\Karyawan::withTrashed()->where('npk', $npk)->first();
    	$karyawan->npk = $r->get('npk');
    	$karyawan->nidn = $r->get('nidn');
    	$karyawan->gelardepan = $r->get('gelardepan');
    	$karyawan->nama = $r->get('nama');
    	$karyawan->gelarbelakang = $r->get('gelarbelakang');
    	$karyawan->tipe = $r->get('tipe');
    	$karyawan->idfakultas = $r->get('idfakultas');
    	$karyawan->idstatusaktif = $r->get('idstatusaktif');
    	$karyawan->jenis = $r->get('jenis');
    	// $karyawan->idsatker = $r->get('idsatker');
        $karyawan->save();

        $statussave = "Data Karyawan berhasil diubah";

        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return redirect('/paj/masterkaryawan')->with('statussave', $statussave);
        }
    	else
        {
            return redirect('/maf/masterkaryawan')->with('statussave', $statussave);
        }
    }

    public function deleteDataKaryawan($npk)
    {
    	$karyawan = Models\Karyawan::where('npk', $npk)->first();
        $karyawan->delete();
        return back();
    }

    public function restoreDataKaryawan($npk)
    {
    	$karyawan = Models\Karyawan::withTrashed()->where('npk', $npk)->first();
        $karyawan->restore();
        return back();
    }

    public function forcedeleteDataKaryawan($npk)
    {
        $karyawan = Models\Karyawan::withTrashed()->where('npk', $npk)->first();
        $karyawan->forceDelete();
        return back();
    }
    /*end of master karyawan*/
}
