<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class MahasiswaController extends Controller
{
    /*Master Mahasiswa*/
    public function showDataMahasiswa(Request $r)
    {
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            $mahasiswas = Models\Mahasiswa::where('idjurusan', Auth::guard('karyawan')->user()->getIdJurusan())->get();
            return view('paj.mastermhs', array('mahasiswas' => $mahasiswas, 'datahapus' => 'tidak'));
        }
        else if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "maf")
        {
            $idjurusan = $r->get('ids');
            $namajurusan = "";
            if(!empty($idjurusan) && $idjurusan != 0)
            {
                $namajurusan = "Jurusan " . Models\Jurusan::where('IdJurusan', $idjurusan)->first()->Nama;
                $mahasiswas = Models\Mahasiswa::where('idjurusan', $idjurusan)->get();
            }
            else
            {
                $namajurusan = "Semua Jurusan";
                $idjurusan = 0;
                $idjurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan')->get();
                $mahasiswas = Models\Mahasiswa::whereIn('idjurusan', $idjurusans)->get();
            }
            
            $jurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
            return view('maf.master.mastermhs', array('ids' => $idjurusan, 'mahasiswas' => $mahasiswas, 'datahapus' => 'tidak', 'jurusans' => $jurusans, 'namajurusan' => $namajurusan));
        }    	
    }

    public function showDataMahasiswas(Request $r)
    {
    	if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            $mahasiswas = Models\Mahasiswa::withTrashed()->where('idjurusan', Auth::guard('karyawan')->user()->getIdJurusan())->get();
            return view('paj.mastermhs', array('mahasiswas' => $mahasiswas, 'datahapus' => 'ya'));
        }
        else if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "maf")
        {
            $idjurusan = $r->get('ids');
            $namajurusan = "";
            if(!empty($idjurusan) && $idjurusan != 0)
            {
                $namajurusan = "Jurusan " . Models\Jurusan::where('IdJurusan', $idjurusan)->first()->Nama;
                $mahasiswas = Models\Mahasiswa::where('idjurusan', $idjurusan)->get();
            }
            else
            {
                $namajurusan = "Semua Jurusan";
                $idjurusan = 0;
                $idjurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan')->get();
                $mahasiswas = Models\Mahasiswa::withTrashed()->whereIn('idjurusan', $idjurusans)->get();
            }
            
            $jurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
            return view('maf.master.mastermhs', array('ids' => $idjurusan, 'mahasiswas' => $mahasiswas, 'datahapus' => 'ya', 'jurusans' => $jurusans, 'namajurusan' => $namajurusan));
        } 
    }

    public function addDataMahasiswaExternal()
    {
        $mhs = DB::connection('mysql2b')->select("SELECT * FROM mahasiswa");
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return view('paj.mastermhs-addext', array('mhs' => $mhs));
        }
        else
        {
            return view('maf.master.mastermhs-addext', array('mhs' => $mhs));
        }
    }

    public function storeDataMahasiswaExternal()
    {
        $coba = DB::select("SELECT * FROM mahasiswas ORDER BY nrp INTO OUTFILE 'C://xampp/htdocs/ta/public/backupdb/mahasiswas_" . (new \DateTime())->format('Y.m.d.H.i.s') . ".csv' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n'");
        // $hapusdata = DB::select(DB::raw('TRUNCATE TABLE mata_kuliahs'));
        $mhs = DB::connection('mysql2b')->select("SELECT * FROM mahasiswa");
        foreach($mhs as $m)
        {
            if (!Models\Mahasiswa::where('nrp', '=', $m->NRP)->exists()) 
            {
                $mhsbar = new Models\Mahasiswa(array(
                    'nrp' => $m->NRP,
                    'nama' => $m->Nama,
                    'tahunakademikterima' => $m->ThnAkademikTerima,
                    'semesterterima' => $m->SemesterTerima,
                    'idjurusan' => $m->IdJurusan,
                    'kodestatus' => $m->KodeStatus,
                    'tglstatus' => $m->TglStatus,
                    'statuslunasupp' => $m->StatusLunasUPP,
                    'ipktanpae' => $m->IPKTanpaE,
                    'skskumtanpae' => $m->SKSKumTanpaE,
                    'sksmaxdepan' => $m->SksMaxDepan,
                    'masastudi' => $m->MasaStudi
                ));
                $mhsbaru->save();
            }
        }
        $statussave = "Data berhasil ditambahkan";
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return redirect('/paj/mastermhs')->with('statussave', $statussave);
        }
        else
        {
            return redirect('/maf/mastermhs')->with('statussave', $statussave);
        }
    }

    public function addDataMahasiswa()
    {
    	$enumSemester = Models\Mahasiswa::getEnum('semesterterima');
    	$dataJurusan = Models\Jurusan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan', 'Nama')->get();
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return view('paj.mastermhs-add', array('mode' => 'add', 'enumSemester' => $enumSemester, 'dataJurusan' => $dataJurusan));
        }
        else
        {
            return view('maf.master.mastermhs-add', array('mode' => 'add', 'enumSemester' => $enumSemester, 'dataJurusan' => $dataJurusan));
        }
    	
    }

    public function storeDataMahasiswa(Request $r)
    {
        $mhsbaru = new Models\Mahasiswa(array(
            'nrp' => $r->get('nrp'),
            'nama' => $r->get('nama'),
            'tahunakademikterima' => $r->get('tahunakademikterima'),
            'semesterterima' => $r->get('semesterterima'),
            'idjurusan' => $r->get('idjurusan'),
            'kodestatus' => $r->get('kodestatus'),
            'statuslunasupp' => $r->get('statuslunasupp'),
            'ipktanpae' => $r->get('ipktanpae'),
            'skskumtanpae' => $r->get('skskumtanpae'),
            'sksmaxdepan' => $r->get('sksmaxdepan'),
            'masastudi' => $r->get('masastudi'),
            'nilaitoefl' => $r->get('nilaitoefl'),
            'asisten' => $r->get('asisten')
        ));
        $mhsbaru->save();
        $statussave = "Data Mahasiswa berhasil ditambahkan";
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return redirect('/paj/mastermhs')->with('statussave', $statussave);
        }
        else
        {
            return redirect('/maf/mastermhs')->with('statussave', $statussave);
        }
    	
    }

    public function editDataMahasiswa($nrp)
    {
    	$mahasiswa = Models\Mahasiswa::withTrashed()->where('nrp', $nrp)->first();
    	$enumSemester = Models\Mahasiswa::getEnum('semesterterima');
        $dataJurusan = Models\Jurusan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan', 'Nama')->get();
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return view('paj.mastermhs-add', array('mahasiswa' => $mahasiswa, 'mode' => 'edit', 'enumSemester' => $enumSemester, 'dataJurusan' => $dataJurusan));
        }
        else
        {
            return view('maf.master.mastermhs-add', array('mahasiswa' => $mahasiswa, 'mode' => 'edit', 'enumSemester' => $enumSemester, 'dataJurusan' => $dataJurusan));
        }    	
    }

    public function updateDataMahasiswa(Request $r, $nrp)
    {
    	$mhs = Models\Mahasiswa::withTrashed()->where('nrp', $nrp)->first();
    	$mhs->nrp = $r->get('nrp');
    	$mhs->nama = $r->get('nama');
    	$mhs->tahunakademikterima = $r->get('tahunakademikterima');
    	$mhs->semesterterima = $r->get('semesterterima');
    	$mhs->idjurusan = $r->get('idjurusan');
    	$mhs->kodestatus = $r->get('kodestatus');
    	$mhs->statuslunasupp = $r->get('statuslunasupp');
    	$mhs->ipktanpae = $r->get('ipktanpae');
    	$mhs->skskumtanpae = $r->get('skskumtanpae');
    	$mhs->sksmaxdepan = $r->get('sksmaxdepan');
    	$mhs->masastudi = $r->get('masastudi');
    	$mhs->nilaitoefl = $r->get('nilaitoefl');
    	$mhs->asisten = $r->get('asisten');
        $mhs->save();

        $statussave = "Data Mahasiswa berhasil diubah";
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        {
            return redirect('/paj/mastermhs')->with('statussave', $statussave);
        }
        else
        {
            return redirect('/maf/mastermhs')->with('statussave', $statussave);
        }
    }

    public function deleteDataMahasiswa($nrp)
    {
    	$mhs = Models\Mahasiswa::where('nrp', $nrp)->first();
        $mhs->delete();
        return back();
    }

    public function restoreDataMahasiswa($nrp)
    {
    	$mhs = Models\Mahasiswa::withTrashed()->where('nrp', $nrp)->first();
        $mhs->restore();
        return back();
    }

    public function forcedeleteDataMahasiswa($nrp)
    {
        $mhs = Models\Mahasiswa::withTrashed()->where('nrp', $nrp)->first();
        $mhs->forceDelete();
        return back();
    }
    /*end of master mahasiswa*/
}
