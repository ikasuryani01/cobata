<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;


class PrioritasTerimaController extends Controller
{
  /* Master Prioritas */
  public function showDataPrioritas() {
    $data = Models\PrioritasTerima::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
    return view('maf.pengaturan.masterprioritas', array('data' => $data, 'datahapus' => 'tidak'));
  }

  public function showDataPrioritass() {
    $data = Models\PrioritasTerima::withTrashed()->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
    return view('maf.pengaturan.masterprioritas', array('data' => $data, 'datahapus' => 'ya'));
  }

  public function addDataPrioritas() {
    $enumStatusAktif = Models\PrioritasTerima::getEnum('statusaktif');
    return view('maf.pengaturan.masterprioritas-add', array('mode' => 'add', 'enumStatusAktif' => $enumStatusAktif));
  }

  public function aktifkanPrioritas($id) {
    $prioritas = Models\PrioritasTerima::withTrashed()
            ->whereId($id)
            ->update(['statusaktif' => 'Ya']);
    return back()->with('status', 'Berhasil Diaktifkan.');
  }

  public function storeDataPrioritas(Request $r) {
    $prioritasbaru = new Models\PrioritasTerima(array(
      'namakomponen' => $r->get('namakomponen'),
      'kodekomponen' => $r->get('kodekomponen'),
      'urutan' => $r->get('urutan'),
      'statusaktif' => $r->get('statusaktif'),
      'idfakultas' => $r->get('idfakultas')
    ));
    $prioritasbaru->save();
    $status = "Data Prioritas Berhasil Ditambahkan.";
    return redirect('/maf/masterprioritas')->with('status', $status);
  }

  public function editDataPrioritas($id) {
    $prioritas = Models\PrioritasTerima::withTrashed()->where('id', $id)->first();
    $enumStatusAktif = Models\PrioritasTerima::getEnum('statusaktif');

    return view('maf.pengaturan.masterprioritas-add', array('mode' => 'edit', 'prioritas' => $prioritas, 'enumStatusAktif' => $enumStatusAktif));
  }

  public function updateDataPrioritas(Request $r, $id) {
    $prioritas = Models\PrioritasTerima::withTrashed()->where('id', $id)->first();
    $prioritas->namakomponen = $r->get('namakomponen');
    $prioritas->kodekomponen = $r->get('kodekomponen');
    $prioritas->urutan = $r->get('urutan');
    $prioritas->statusaktif = $r->get('statusaktif');
    $prioritas->save();

    $statussave = "Data Prioritas berhasil diubah";
    return redirect('/maf/masterprioritas')->with('status', $statussave);
  }

  public function deleteDataPrioritas($id) {
    $prioritas = Models\PrioritasTerima::where('id', $id)->first();
    $prioritas->statusaktif = 'Tidak';
    $prioritas->save();
    $prioritas->delete();
    return back()->with('status', 'Data Berhasil dihapus.');
  }

  public function restoreDataPrioritas($id) {
    $prioritas = Models\PrioritasTerima::withTrashed()->where('id', $id)->first();
    $prioritas->restore();
    return back();
  }

  public function forcedeleteDataPrioritas($id) {
    $prioritas = Models\PrioritasTerima::withTrashed()->where('id', $id)->first();
    $prioritas->forceDelete();
    return back();
  }
  /* end of master prioritas */
}
