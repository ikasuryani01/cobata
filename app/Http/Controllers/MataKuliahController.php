<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class MataKuliahController extends Controller
{
    /*Master Matakuliah*/
    public function showDataMatakuliah(Request $r)
    {
      if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
      {
        $idjurusan = Auth::guard('karyawan')->user()->getIdJurusan();
        $kodemk = Models\JurusanDaftarMatakuliah::where('idjurusan', $idjurusan)->select('kodemk')->get();
        $mks = Models\Matakuliah::whereIn('kodemk', $kodemk)->get();
        
        return view('paj.mastermk', array('mks' => $mks, 'datahapus' => 'tidak'));
      }
    	else if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "maf")
      {
        $jurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)
                    ->where('Aktif', 'Y')->get();

        $idjurusan = $r->get('ids');
        if(!empty($idjurusan))
        {
          if($idjurusan != 0)
          {
            $kodemk = Models\JurusanDaftarMatakuliah::where('idjurusan', $idjurusan)->select('kodemk')->get();
            $mks = Models\Matakuliah::whereIn('kodemk', $kodemk)->get();
          }
        }
        else
        {
          $idjurusans = $jurusans->pluck('IdJurusan')->all();
          $kodemk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusans)->select('kodemk')->get();
          $mks = Models\Matakuliah::whereIn('kodemk', $kodemk)->get();
        }
        return view('maf.master.mastermk', array('mks' => $mks, 'datahapus' => 'tidak', 'jurusans' => $jurusans, 'ids' => $idjurusan));
      }
    }

    public function showDataMatakuliahs(Request $r)
    {
      if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
      {
        $idjurusan = Auth::guard('karyawan')->user()->getIdJurusan();
        $kodemk = Models\JurusanDaftarMatakuliah::where('idjurusan', $idjurusan)->select('kodemk')->get();
        $mks = Models\Matakuliah::withTrashed()->whereIn('kodemk', $kodemk)->get();
        
        return view('paj.mastermk', array('mks' => $mks, 'datahapus' => 'ya'));
      }
      else if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "maf")
      {
        $jurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)
                    ->where('Aktif', 'Y')->get();

        $idjurusan = $r->get('ids');
        if(!empty($idjurusan))
        {
          if($idjurusan != 0)
          {
            $kodemk = Models\JurusanDaftarMatakuliah::where('idjurusan', $idjurusan)->select('kodemk')->get();
            $mks = Models\Matakuliah::withTrashed()->whereIn('kodemk', $kodemk)->get();
          }
        }
        else
        {
          $idjurusans = $jurusans->pluck('IdJurusan')->all();
          $kodemk = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusans)->select('kodemk')->get();
          $mks = Models\Matakuliah::withTrashed()->whereIn('kodemk', $kodemk)->get();
        }
        return view('maf.master.mastermk', array('mks' => $mks, 'datahapus' => 'ya', 'jurusans' => $jurusans, 'ids' => $idjurusan));
      }
    }

    public function aktifkansemuaMataKuliah()
    {
        $mks = Models\Matakuliah::all();
        foreach ($mks as $mk) {
            $mk->aktifkan();
        }
        return back();
    }        

    public function addDataMatakuliahExternal()
    {
      $mks = DB::connection('mysql2d')->select("SELECT * FROM matakuliahs");

      if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        return view('paj.mastermk-addext', array('mks' => $mks));
      else
        return view('maf.master.mastermk-addext', array('mks' => $mks));
    }

    public function storeDataMatakuliahExternal()
    {
        $coba = DB::select("SELECT * FROM mata_kuliahs ORDER BY kodemk INTO OUTFILE 'C://xampp/htdocs/ta/public/backupdb/mata_kuliahs_" . (new \DateTime())->format('Y.m.d.H.i.s') . ".csv' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n'");
        // $hapusdata = DB::select(DB::raw('SET foreign_key_checks = 0; TRUNCATE TABLE mata_kuliahs; SET foreign_key_checks = 1;'));
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('mata_kuliahs')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $mks = DB::connection('mysql2d')->select("SELECT * FROM matakuliahs");
        foreach($mks as $mk)
        {
            $mkbaru = new Models\Matakuliah(array(
                    'kodemk' => $mk->kodeMK,
                    'nama' => $mk->namaMK,
                    'sks' => $mk->sks,
                    'jenis' => $mk->status,
                ));
            $mkbaru->save();
        }
      if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        return redirect('/paj/mastermk');
      else
        return redirect('/maf/mastermk');
    }

    public function addDataMatakuliah()
    {
      $enumTipe = Models\Matakuliah::getEnum('tipe');
      $enumJenis = Models\Matakuliah::getEnum('jenis');
    	$enumKurikulum = Models\JurusanDaftarMatakuliah::getEnum('kurikulum');
      if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        return view('paj.mastermk-add', array('mode' => 'add', 'enumTipe' => $enumTipe, 'enumJenis' => $enumJenis, 'enumKurikulum' => $enumKurikulum));
      else
        return view('maf.master.mastermk-add', array('mode' => 'add', 'enumTipe' => $enumTipe, 'enumJenis' => $enumJenis));
    }

    public function aktifkanMataKuliah($kodemk)
    {
      $mk = Models\Matakuliah::where('kodemk', $kodemk)->first();
      if($mk->statusbuka == "Ya")
        $mk->statusbuka = "Tidak";
      else
        $mk->statusbuka = "Ya";
      $mk->save();
      return back();
    }

    public function storeDataMatakuliah(Request $r)
    {
      if(Models\Matakuliah::where('kodemk', $r->get('kodemk'))->exists())
      {
        $statussave = "Tidak dapat menyimpan data karena kode MK sudah terdaftar.";
        return back()->with('status', $statussave)->withInput();
      }
      else
      {
        $mkbaru = new Models\Matakuliah(array(
              'kodemk' => $r->get('kodemk'),
              'nama' => $r->get('nama'),
              'namaeng' => $r->get('namaeng'),
              'sks' => $r->get('sks'),
              'jenis' => $r->get('jenis'),
              'flagskripsi' => $r->get('flagskripsi'),
              'tipe' => $r->get('tipe'),
              'statusbuka' => $r->get('statusbuka')
          ));
        $mkbaru->save();
        $hubjurusan = new Models\JurusanDaftarMatakuliah(array(
              'kodemk' => $r->get('kodemk'),
              'idjurusan' => $r->get('idjurusan'),
              'sksminimal' => $r->get('sksminimal'),
              'semester' => $r->get('semester'),
              'kurikulum' => $r->get('kurikulum')
          ));
        $hubjurusan->save();
        $statussave = "Data Matakuliah berhasil ditambahkan";
      }
      if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        return redirect('/paj/mastermk')->with('status', $statussave);
      else
        return redirect('/maf/mastermk')->with('status', $statussave);
    }

    public function editDataMatakuliah($kodemk)
    {
    	$mk = Models\Matakuliah::withTrashed()->where('kodemk', $kodemk)->first();
      $enumTipe = Models\Matakuliah::getEnum('tipe');
    	$enumJenis = Models\Matakuliah::getEnum('jenis');
      $enumKurikulum = Models\JurusanDaftarMatakuliah::getEnum('kurikulum');

      if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        return view('paj.mastermk-add', array('mode' => 'edit', 'enumTipe' => $enumTipe, 'enumJenis' => $enumJenis, 'mk' => $mk, 'enumKurikulum' => $enumKurikulum));
      else
        return view('maf.master.mastermk-add', array('mode' => 'edit', 'enumTipe' => $enumTipe, 'enumJenis' => $enumJenis, 'mk' => $mk));
    }

    public function updateDataMatakuliah(Request $r, $kodemk)
    {
    	$mk = Models\Matakuliah::withTrashed()->where('kodemk', $kodemk)->first();
    	$mk->kodemk = $r->get('kodemk');
    	$mk->nama = $r->get('nama');
    	$mk->namaeng = $r->get('namaeng');
    	$mk->sks = $r->get('sks');
    	$mk->jenis = $r->get('jenis');
    	$mk->flagskripsi = $r->get('flagskripsi');
    	$mk->tipe = $r->get('tipe');
    	$mk->statusbuka = $r->get('statusbuka');
      $mk->save();

      if(!Models\JurusanDaftarMatakuliah::where('idjurusan', $r->get('idjurusan'))->where('kodemk', $r->get('kodemk'))->exists())
      {
        $hubjurusan = new Models\JurusanDaftarMatakuliah(array(
              'kodemk' => $r->get('kodemk'),
              'idjurusan' => $r->get('idjurusan'),
              'sksminimal' => $r->get('sksminimal'),
              'semester' => $r->get('semester'),
              'kurikulum' => $r->get('kurikulum')
          ));
        $hubjurusan->save();
      }
      else
      {
        $d = Models\JurusanDaftarMatakuliah::where('idjurusan', $r->get('idjurusan'))->where('kodemk', $r->get('kodemk'))->update(['sksminimal' => $r->get('sksminimal'), 'semester' => $r->get('semester'), 'kurikulum' => $r->get('kurikulum')]);
      }

      $statussave = "Data Matakuliah berhasil diubah";
    	if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
        return redirect('/paj/mastermk')->with('status', $statussave);
      else
        return redirect('/maf/mastermk')->with('status', $statussave);
    }

    public function deleteDataMatakuliah($kodemk)
    {
    	$mk = Models\Matakuliah::where('kodemk', $kodemk)->first();
      $mk->delete();
      return back()->with('status', 'Berhasil menghapus data');
    }

    public function restoreDataMatakuliah($kodemk)
    {
    	$mk = Models\Matakuliah::withTrashed()->where('kodemk', $kodemk)->first();
      $mk->restore();
      return back();
    }

    public function forcedeleteDataMatakuliah($kodemk)
    {
      $mk = Models\Matakuliah::withTrashed()->where('kodemk', $kodemk)->first();
      $mk->forceDelete();
      return back();
    }
    /*end of master mk*/
}
