<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class JadwalUjianController extends Controller
{
    
    /*Master Ujian*/
    public function showDataUjian(Request $r)
    {
    	$idsemester = $r->get('ids');
    	if(!empty($idsemester))
    	{
    		if($idsemester != 0)
    		{
    			$ujians = Models\JadwalUjian::where('idsemester', $idsemester)->get();
    		}
    	}
    	else
    	{
            $idsemester = Models\Semester::getAktifId();
    		$ujians = Models\JadwalUjian::where('idsemester', $idsemester)->get();
    	}
    	$semester = Models\Semester::all();
    	return view('paj.masterujian', array('ujians' => $ujians, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function showDataUjians(Request $r)
    {
    	$idsemester = $r->get('ids');
    	if(!empty($idsemester))
    	{
    		if($idsemester != 0)
    		{
    			$ujians = Models\JadwalUjian::withTrashed()->where('idsemester', $idsemester)->get();
    		}
    	}
    	else
    	{
            $idsemester = Models\Semester::getAktifId();
    		$ujians = Models\JadwalUjian::withTrashed()->where('idsemester', $idsemester)->get();
    	}
    	$semester = Models\Semester::all();
    	return view('paj.masterujian', array('ujians' => $ujians, 'datahapus' => 'ya', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function addDataUjian()
    {
    	$matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
    		->where(function($query)
    			{
    				$idsemesteraktif = Models\Semester::getAktif()->id;
    				$kodemksudah = Models\JadwalUjian::where('idsemester', $idsemesteraktif)->select('kodemk')->get();
    				return $query->whereNotIn('kodemk', $kodemksudah);
	            })
    		->get();
        // $matakuliah = Models\Matakuliah::All();
    	$enumHari = Models\JadwalUjian::getEnum('hari');
        $enumtipeujianuts = Models\JadwalUjian::getEnum('tipeujianuts');
        $enumtipeujianuas = Models\JadwalUjian::getEnum('tipeujianuas');
        $enumtiperuanganuas = Models\JadwalUjian::getEnum('tiperuanganuas');
        $enumtiperuanganuts = Models\JadwalUjian::getEnum('tiperuanganuts');
    	return view('paj.masterujian-add', array('mode' => 'add', 'enumHari' => $enumHari, 'matakuliah' => $matakuliah, 'matakuliah' => $matakuliah, 'enumtiperuanganuas' => $enumtiperuanganuas, 'enumtipeujianuas' => $enumtipeujianuas, 'enumtipeujianuts' => $enumtipeujianuts, 'enumtiperuanganuts' => $enumtiperuanganuts));
    }

    public function storeDataUjian(Request $r)
    {
        $stringmk = explode(" ", $r->get('kodemk'));
        $kodemk = $stringmk[0];

    	$idsemesteraktif = Models\Semester::getAktif()->id;
        $ujianbaru = new Models\JadwalUjian(array(
                'mingguke' => $r->get('mingguke'),
                'hari' => $r->get('hari'),
                'jamke' => $r->get('jamke'),
                'kodemk' => $kodemk,
                'idsemester' => $idsemesteraktif
            ));
        $ujianbaru->save();
        $statussave = "Data Ujian berhasil ditambahkan";
    	// return redirect('/paj/masterujian')->with('statussave', $statussave);
        return back()->with('status', $statussave)->withInput();
    }

    public function editDataUjian($id)
    {
    	$matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
    		->where(function($query)
    			{
    				$idsemesteraktif = Models\Semester::getAktif()->id;
    				$kodemksudah = Models\JadwalUjian::where('idsemester', $idsemesteraktif)->select('kodemk')->get();
    				return $query->whereNotIn('kodemk', $kodemksudah);
	            })
    		->get();
        $enumHari = Models\JadwalUjian::getEnum('hari');
        $enumtipeujianuts = Models\JadwalUjian::getEnum('tipeujianuts');
        $enumtipeujianuas = Models\JadwalUjian::getEnum('tipeujianuas');
        $enumtiperuanganuas = Models\JadwalUjian::getEnum('tiperuanganuas');
        $enumtiperuanganuts = Models\JadwalUjian::getEnum('tiperuanganuts');
         
    	$ujian = Models\JadwalUjian::whereId($id)->first();
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
    	   return view('paj.masterujian-add', array('mode' => 'edit', 'ujian' => $ujian, 'enumHari' => $enumHari, 'matakuliah' => $matakuliah, 'enumtiperuanganuas' => $enumtiperuanganuas, 'enumtipeujianuas' => $enumtipeujianuas, 'enumtipeujianuts' => $enumtipeujianuts, 'enumtiperuanganuts' => $enumtiperuanganuts));
        else
            return view('maf.ruangujian.masterujian-add', array('mode' => 'edit', 'ujian' => $ujian, 'enumHari' => $enumHari, 'matakuliah' => $matakuliah, 'enumtiperuanganuas' => $enumtiperuanganuas, 'enumtipeujianuas' => $enumtipeujianuas, 'enumtipeujianuts' => $enumtipeujianuts, 'enumtiperuanganuts' => $enumtiperuanganuts));
    }

    public function updateDataUjian(Request $r, $id)
    {
    	$ujian = Models\JadwalUjian::withTrashed()->where('id', $id)->first();
    	$ujian->mingguke = $r->get('mingguke');
    	$ujian->hari = $r->get('hari');
        $ujian->jamke = $r->get('jamke');
        $ujian->tipeujianuts = $r->get('tipeujianuts');
        $ujian->tiperuanganuts = $r->get('tiperuanganuts');
        $ujian->tipeujianuas = $r->get('tipeujianuas');
    	$ujian->tiperuanganuas = $r->get('tiperuanganuas');
    	// $ujian->kodemk = $r->get('kodemk');
        $ujian->save();

        $statussave = "Data Ujian berhasil diubah";
        if(Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "paj")
            return redirect('/paj/masterujian')->with('status', $statussave);
        else
            return redirect('maf/presensiujian')->with('status', $statussave);
    }

    public function deleteDataUjian($id)
    {
    	$ujian = Models\JadwalUjian::where('id', $id)->first();
        $ujian->delete();
        return back()->with('status', "Berhasil dihapus.");
    }

    public function restoreDataUjian($id)
    {
    	$ujian = Models\JadwalUjian::withTrashed()->where('id', $id)->first();
        $ujian->restore();
        return back();
    }

    public function forcedeleteDataUjian($id)
    {
        $ujian = Models\JadwalUjian::withTrashed()->where('id', $id)->first();
        $ujian->forceDelete();
        return back();
    }
    /*end of master ujian*/
}
