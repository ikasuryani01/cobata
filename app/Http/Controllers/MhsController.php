<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models;

use Auth;

class MhsController extends Controller
{
    public function showBeranda()
    {
      $semester = Models\Semester::getAktif();
    	return view('mhs.beranda', array('semester' => $semester));
    }

    public function showProfil()
    {
      $mhs = Models\Mahasiswa::where('nrp', Auth::guard('mahasiswa')->user()->nrp)->first();
      return view('mhs.profil', array('mhs' => $mhs));
    }

    public function showJadwalKuliah(Request $r)
    {
        $idsemesteraktif = Models\Semester::getAktifId();
        $viewnrp = "tidak";
        $kuliahs = 0;
        if((!is_null($r->get('nrp'))) && $r->get('nrp') == 'y') //ini jadwal dia sndiri
        {
            $viewnrp = "ya";
            $nrp = Auth::guard('mahasiswa')->user()->nrp; //nnti ambil dr auth user
            //ambil yang khusus diambil mhs itu
            $idfpps = Models\Fpp::where('idsemester', $idsemesteraktif)->select('id')->get();
            $idkelasdiambil = Models\DaftarFppKelasMahasiswa::whereIn('idfpp', $idfpps)
                                ->where('nrp', $nrp)
                                ->where('status', "Diterima")
                                ->select('idkelasparalel')
                                ->get();
        }
        else //smua tp dr jurusan dia
        {
            // nnti stlah urusan jurusan mk selesai dan dari auth user dapat jurusannya
            
            $idjurusan = Auth::guard('mahasiswa')->user()->idjurusan; //ambil dr auth user
            $kodemk = Models\JurusanDaftarMatakuliah::where('idjurusan', $idjurusan)
                        // ->where('statusaktif', 'Ya') //mau pake deleted at kan buat aktifnya
                        ->select('kodemk')
                        ->distinct()
                        ->get();
            $idkelasdiambil = Models\KelasParalel::whereIn('kodemk', $kodemk)
                                ->where('statusaktif', 'Ya')
                                ->where('idsemester', $idsemesteraktif)
                                ->select('id')
                                ->distinct()
                                ->get();
            
            // $idkelasdiambil = Models\KelasParalel::where('idsemester', $idsemesteraktif)->select('id')->get();
        }
        $kuliahs = Models\JadwalKuliah::whereIn('idkelasparalel', $idkelasdiambil)->get();
    	return view('mhs.jadwalkuliah', array('kuliahs' => $kuliahs, 'viewnrp' => $viewnrp));
    }

    public function showJadwalUjian(Request $r)
    {
        $idsemesteraktif = Models\Semester::getAktifId();
        $viewnrp = "tidak";
        $jadwalmahasiswa = 0;
        if((!is_null($r->get('nrp'))) && $r->get('nrp') == 'y') 
        {
            $viewnrp = "ya";
            $nrp = Auth::guard('mahasiswa')->user()->nrp;
            // dpeting jadwalnya mhsnya skrg
            $idfppsemesterini = Models\Fpp::where('idsemester', $idsemesteraktif)->select('id')->get();
            $idkelasterdaftar = Models\DaftarFppKelasMahasiswa::where('nrp', $nrp)
                            ->whereIn('idfpp', $idfppsemesterini)
                            ->where('status', "Diterima")
                            ->select('idkelasparalel')
                            ->get();
            $kodemk = Models\KelasParalel::whereIn('id', $idkelasterdaftar)->select('kodemk')->distinct()->get();
        }
        else
        {
            // nnti stlah urusan jurusan mk selesai dan dari auth user dapat jurusannya
            
            $idjurusan = Auth::guard('mahasiswa')->user()->idjurusan; //ambil dr auth user
            $kodemk = Models\JurusanDaftarMatakuliah::where('idjurusan', $idjurusan)
                        // ->where('statusaktif', 'Ya') //mau pake deleted at kan buat aktifnya
                        ->select('kodemk')
                        ->get();
            
            // $kodemk = Models\MataKuliah::select('kodemk')->get();
        }
        $jadwalmahasiswa = Models\JadwalUjian::whereIn('kodemk', $kodemk)->where('idsemester', $idsemesteraktif)->get();
        return view('mhs.jadwalujian', array('ujians' => $jadwalmahasiswa, 'viewnrp' => $viewnrp));
    }

    public function showInformasiMataKuliah()
    {
      $mks = Models\JurusanDaftarMatakuliah::where('idjurusan', Auth::guard('mahasiswa')->user()->idjurusan)->select('kodemk')->distinct()->with('mk.kp')->get();
      // dd($mks[0]->mk);
      // exit();
    	return view('mhs.informasimk', array('mks' => $mks));
    }

    public function showTranskrip()
    {
      $datas = Models\Transkrip::where('nrpmhs', Auth::guard('mahasiswa')->user()->nrp)->with('mk', 'semester')->get();
      return view('mhs.transkrip', array('datas' => $datas));
    }

    public function showRiwayatPerwalian()
    {
      $fpp = Models\Fpp::getSekarang();

      $idfpp = Models\Fpp::where('idsemester', Models\Semester::getAktifId())
                ->where('idfakultas', Auth::guard('mahasiswa')->user()->jurusan->IdFakultas)
                ->select('id')->get();

      $nrp = Auth::guard('mahasiswa')->user()->nrp;
      $datas = Models\DaftarFppKelasMahasiswa::where('nrp', $nrp)
                ->whereIn('idfpp', $idfpp)
                ->orderBy('idfpp', 'asc')
                ->orderBy('status', 'desc')
                ->get();

      return view('mhs.historyperwalian', array('fpp' => $fpp, 'datas' => $datas));
    }

    /*Daftar MK - FPP*/
    public function showDaftarMataKuliah()
    {
        return view('mhs.daftarmk');
    }
    
    public function showTableDaftarMk()
    {
        $fpp = Models\Fpp::getSekarang();

        $idfpp = Models\Fpp::where('idsemester', Models\Semester::getAktifId())
                  ->where('idfakultas', Auth::guard('mahasiswa')->user()->jurusan->IdFakultas)
                  ->select('id')->get();

        $nrp = Auth::guard('mahasiswa')->user()->nrp;

        $datas = Models\DaftarFppKelasMahasiswa::where('nrp', $nrp)
                  ->whereIn('idfpp', $idfpp)
                  ->orderBy('idfpp', 'asc')
                  ->orderBy('status', 'desc')
                  ->with('kp.mk')
                  ->get();
        // dd($datas);
        // exit();
        $jurkodemk = Models\JurusanDaftarMatakuliah::where('idjurusan', Auth::guard('mahasiswa')->user()->idjurusan)->select('kodemk')->get();
        $kodemk = Models\MataKuliah::whereIn('kodemk', $jurkodemk)
                    ->where('statusbuka', 'Ya')
                    ->select('kodemk')
                    ->get();
        $kps = Models\KelasParalel::where('statusaktif', 'Ya')
                ->where('idsemester', Models\Semester::getAktifId())
                ->whereIn('kodemk', $kodemk)
                ->get();
        return view('mhs.daftarmktabel', array('fpp' => $fpp, 'kps' => $kps, 'datas' => $datas));
    }

    public function addDaftarMk()
    {
      /* 
          [v] jgn lupa cek apa nrp sama sama yg lg login,
          [v] cek bner nggak fpp nya lagi buka dan lagi di semester yang lagi aktif (?), cek juga kelasnya, dll
      */

      $status = "";
      $nrp = $_GET['p'];
      $id = $_GET['k'];
      $idfpp = $_GET['f'];
      $cekawal = false;

      $fppskrg = Models\Fpp::getSekarang();

      if($nrp == Auth::guard('mahasiswa')->user()->nrp)
      {
        if($idfpp == $fppskrg->id && $fppskrg->idsemester == Models\Semester::getAktifId())
        {
          $cekkelas = Models\KelasParalel::whereId($id)->where('statusaktif', 'Ya')->count();
          if($cekkelas == 1)
          {
            $cekawal = true;
          }
        }
      }

      $caridata = Models\DaftarFppKelasMahasiswa::withTrashed()->where('nrp', $nrp)->where('idkelasparalel', $id)->where('idfpp', $idfpp)->count();
      
      $data = new Models\DaftarFppKelasMahasiswa(array(
                  'nrp' => $nrp,
                  'idkelasparalel' => $id,
                  'idfpp' => $idfpp,
                  'status' => 'Pending',
              ));//buat objek aja buat dipake pengecekan, jgn di save
      
      $status = "";
      $classnotif = "info";

      // all pengecekan ini kalau false return back + alasannya apa jadi alert
      if($data->cekMaksSks() && $cekawal) //kl uda maksimal sks nya nda bs ditambah lg
      {
        if($data->cekSksMin()) //buat mk kayak KP TA
        {
          if($data->cekPrasyarat()) //cek prasyarat
          {
            if($data->cekSyaratLain()) //buat bing ft
            {
              if($data->cekAmbilSama()) //  uda diambil pas masa FPP ini apa blm
              {
                if(true) // !$data->cekPernahAmbil() cek sama yg ditranskrip
                {
                  if($data->cekjadwalKuliah()) //cek ada jadwal kuliah yang nabrak gak sama yang diinput skrg
                  {
                    if($data->cekJadwalUjian()) //cek ada jadwal ujian yang nabrak gak sama yang diinput skrg
                    {
                      //stlah lulus smua pengecekan
                      if($fppskrg->urutanterima == "Batch")
                      {
                        if($caridata == 0)
                        {
                          //data blm ada sm skali, tambahin baru
                          $daftarmk = new Models\DaftarFppKelasMahasiswa(array(
                                          'nrp' => $nrp,
                                          'idkelasparalel' => $id,
                                          'idfpp' => $idfpp,
                                          'status' => 'Pending',
                                      ));
                          $daftarmk->save();
                          $status = "Berhasil ditambahkan";
                          $classnotif = "success";
                        }
                        else
                        {
                          //data uda prnah ada
                          $data = Models\DaftarFppKelasMahasiswa::withTrashed()->where('nrp', $nrp)->where('idkelasparalel', $id)->where('idfpp', $idfpp)->first();
                          if($data->deleted_at != null) //brrti dia kehapus sblmnya
                          {
                            $data->deleted_at = null;
                            $data->save();
                            $data->restore(); //kt restore
                            $status = "Berhasil diaktifkan kembali";
                            $classnotif = "success";
                          }
                          else
                          {
                            $status = "Tidak dapat mendaftarkan karena data sudah ada";
                            $classnotif = "warning";
                          }
                        }
                      }
                      else if($fppskrg->urutanterima == "Langsung")
                      {
                        if(!$data->kp->isFull())
                        {
                          //msh bs diterima
                          $stat = "Diterima";
                          $alasan = "Langsung Terima sesuai Kapasitas";
                          $classnotif = "success";
                        }
                        else
                        {
                          $stat = "Ditolak";
                          $alasan = "Kelas Penuh";
                          $classnotif = "danger";
                        }
                        $daftarmk = new Models\DaftarFppKelasMahasiswa(array(
                                        'nrp' => $nrp,
                                        'idkelasparalel' => $id,
                                        'idfpp' => $idfpp,
                                        'status' => $stat,
                                        'alasan' => $alasan,
                                        'statuspublikasi' => 'Ya'
                                    ));
                        $daftarmk->save();
                        $status = "Pendaftaran kelas: $stat";
                      }
                    }
                    else 
                    {
                      $status = "Terdapat Jadwal Ujian yang sama dengan Mata Kuliah ini";
                      $classnotif = "danger";                      
                    }
                  }
                  else
                  {
                    $status = "Terdapat Jadwal Kuliah yang sama dengan Mata Kuliah ini";
                    $classnotif = "danger";
                  }
                }
                else
                {
                  $status = "Mata Kuliah ini sudah pernah diambil sebelumnya";
                  $classnotif = "danger";
                }
              }
              else
              {
                $status = "Mata Kuliah ini sudah didaftarkan";
                $classnotif = "danger";
              }
            }
            else
            {
              $status = "Terdapat syarat lain yang tidak terlampaui";
              $classnotif = "danger";
            }
          }
          else 
          {
            $status = "Belum lulus mata kuliah prasyarat";
            $classnotif = "danger";
          }
        }
        else
        {
          $status = "Belum mencapai SKS minimal untuk mengambil Mata Kuliah";
          $classnotif = "danger";
        }
      }
      else
      {
        $status = "SKS tidak mencukupi";
        $classnotif = "danger";
      }

      return back()->with('status', $status)->with('classnotif', $classnotif);
    }

    public function deleteDaftarMk()
    {
        $nrp = $_GET['p'];
        $id = $_GET['k'];
        $idfpp = $_GET['f'];

        $mk = Models\DaftarFppKelasMahasiswa::where('nrp', $nrp)
                ->where('idkelasparalel', $id)
                ->where('idfpp', $idfpp)
                ->first()
                ->delete();
        $status = "Pendaftaran berhasil dibatalkan";
        $classnotif = "info";
        return back()->with('status', $status)->with('classnotif', $classnotif);
    }

    public function lihatDetailMk()
    {
    }
    /*end of FPP*/
}
