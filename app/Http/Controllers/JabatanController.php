<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;

class JabatanController extends Controller
{
  public function showDataJabatan() 
  {
    $jabatans = Models\Jabatan::all();
    $karyawans = Models\Karyawan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->select('npk', 'nama')->get();
    $npkkaryawan = $karyawans->pluck('npk')->all();
    $data = Models\KaryawanPunyaJabatan::whereIn('npk', $npkkaryawan)->orderBy('updated_at', 'desc')->get();
    $enumIsAktif = Models\KaryawanPunyaJabatan::getEnum('isaktif');
    $enumIsUtama = Models\KaryawanPunyaJabatan::getEnum('isutama');
    return view('maf.pengaturan.masterkelolajabatan', array('data' => $data, 'jabatans' => $jabatans, 'karyawans' => $karyawans, 'enumIsUtama' => $enumIsUtama, 'enumIsAktif' => $enumIsAktif, 'datahapus' => 'tidak'));
  }

  public function showDataJabatans() 
  {
    $jabatans = Models\Jabatan::all();
    $karyawans = Models\Karyawan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->select('npk', 'nama')->get();
    $npkkaryawan = $karyawans->pluck('npk')->all();
    $data = Models\KaryawanPunyaJabatan::withTrashed()->whereIn('npk', $npkkaryawan)->orderBy('updated_at', 'desc')->get();
    $enumIsAktif = Models\KaryawanPunyaJabatan::getEnum('isaktif');
    $enumIsUtama = Models\KaryawanPunyaJabatan::getEnum('isutama');
    return view('maf.pengaturan.masterkelolajabatan', array('data' => $data, 'jabatans' => $jabatans, 'karyawans' => $karyawans, 'enumIsUtama' => $enumIsUtama, 'enumIsAktif' => $enumIsAktif, 'datahapus' => 'ya'));
  }

  public function storeDataJabatan(Request $r) 
  {
  	if ($r->get('isutama') == 'Ya') {
		$data = Models\KaryawanPunyaJabatan::withTrashed()
			->where('npk', $r->get('npk'))
			->where('isutama', 'Ya')
			->update(['isutama' => 'Tidak']);
	}

	$data = new Models\KaryawanPunyaJabatan(array(
        'npk' => $r->get('npk'),
        'idjabatan' => $r->get('idjabatan'),
        'isaktif' => $r->get('isaktif'),
        'isutama' => $r->get('isutama')
	));
	$data->save();

	$status = "Data berhasil ditambahkan";
    return redirect('/maf/jabatan')->with('status', $status);
  }

  public function updateDataJabatan(Request $r) 
  {
  	if ($r->get('isutama') == 'Ya') {
  		$data = Models\KaryawanPunyaJabatan::withTrashed()
  			->where('npk', $r->get('npk'))
  			->where('isutama', 'Ya')
  			->update(['isutama' => 'Tidak']);
  	}
    $data = Models\KaryawanPunyaJabatan::withTrashed()->where('npk', $r->get('npk'))
              ->update(['isutama' => $r->get('isutama'), 'isaktif' => $r->get('isaktif'), 'idjabatan' => $r->get('idjabatan')]);

    $status = "Data berhasil diubah";
    return redirect('/maf/jabatan')->with('status', $status);
  }

  public function deleteDataJabatan($npk, $idjabatan) {
    $Jabatan = Models\KaryawanPunyaJabatan::where('npk', $npk)->where('idjabatan', $idjabatan)->delete();
    // $Jabatan->delete();
    return redirect('maf/jabatan');
  }

  public function restoreDataJabatan($npk, $idjabatan) {
    $Jabatan = Models\KaryawanPunyaJabatan::withTrashed()->where('npk', $npk)->where('idjabatan', $idjabatan)->restore();
    return back();
  }

  public function forcedeleteDataJabatan($npk, $idjabatan) {
    $Jabatan = Models\KaryawanPunyaJabatan::withTrashed()->where('npk', $npk)->where('idjabatan', $idjabatan)->forceDelete();
    return back();
  }
  /* end of master Jabatan */
}
