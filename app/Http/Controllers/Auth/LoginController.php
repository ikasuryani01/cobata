<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/customredirect';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function username()
    {
        return 'nrpnpk';
    }

    public function login(Request $request)
    {
      $this->validateLogin($request);
      
      // $ds = ldap_connect("192.168.20.15");  //alamat server LDAP Ubaya
      // ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3); //wajib disetting

      // if ($ds) 
      // { //kalau berhasil connect ke server LDAP
      //   $sr = ldap_search($ds, "ou=people,dc=ubaya,dc=ac,dc=id", "uid=" . $request->get('nrpnpk'));
      //   $entry = ldap_first_entry($ds, $sr);
      //   $dn = ldap_get_dn($ds, $entry);
      //   $r = @ldap_bind($ds, "$dn", $request->get('password'));

      //   if ($r) 
      //   { //kalau berhasil login dengan password tersebut
      //     $sr = ldap_search($ds, "ou=people, dc=ubaya, dc=ac, dc=id", "uid=" . $request->get('nrpnpk'));
      //     $info = ldap_get_entries($ds, $sr);
      //     $jumlah_hasil = ldap_count_entries($ds, $sr);

      //     //proses ambil data
      //     $givenname = $info[0]["givenname"][0];
      //     $uid = $info[0]["uid"][0];

      //     // If the class is using the ThrottlesLogins trait, we can automatically throttle
      //     // the login attempts for this application. We'll key this by the username and
      //     // the IP address of the client making these requests into this application.
      //     if ($this->hasTooManyLoginAttempts($request)) {
      //         $this->fireLockoutEvent($request);

      //         return $this->sendLockoutResponse($request);
      //     }

          // $credentials = $this->credentials($request);

          // if ($this->guard()->attempt($credentials, $request->has('remember'))) {
          //     return $this->sendLoginResponse($request);
          // }

          if ($request->get('jenis') == "nrp") 
          {
            // $nrp = $info[0]["x-ubaya-nrp"][0];
            $nrp = $request->get('nrpnpk');
            if (Auth::guard('mahasiswa')->attempt(['nrp' => $nrp]))
            {
              return $this->sendLoginResponse($request, "mahasiswa");
            }
            else
              return back()->with('status', 'Username atau password salah.');

          }
          else if ($request->get('jenis') == "npk") 
          {
            // $npk = $info[0]["x-ubaya-npk"][0];
            $npk = $request->get('nrpnpk');
            if (Auth::guard('karyawan')->attempt(['npk' => $npk]))
            {
              return $this->sendLoginResponse($request, "karyawan");
            }
            else
              return back()->with('status', 'Username atau password salah.');

          }

          $this->incrementLoginAttempts($request);

          return $this->sendFailedLoginResponse($request);
      //   }
      //   else
      //   {
      //     return back()->with('status', 'Username atau password salah.');
      //     return $this->sendFailedLoginResponse($request);
      //     // echo "h";
      //     // exit();
      //     //username tidak ada atau password salah
      //   }
      // }
      // else 
      // {
      //   return back()->with('status', 'Periksa koneksi ke server');

      //   return $this->sendFailedLoginResponse($request);
      //   // echo "i";
      //   // exit();
      //   //tidak bisa connect ke LDAP server
      // }

      // If the class is using the ThrottlesLogins trait, we can automatically throttle
      // the login attempts for this application. We'll key this by the username and
      // the IP address of the client making these requests into this application.
      
      // if ($this->hasTooManyLoginAttempts($request)) {
      //     $this->fireLockoutEvent($request);

      //     return $this->sendLockoutResponse($request);
      // }

      // // $credentials = $this->credentials($request);

      // // if ($this->guard()->attempt($credentials, $request->has('remember'))) {
      // //     return $this->sendLoginResponse($request);
      // // }

      // if ($request->get('jenis') == "nrp") 
      // {
      //     if (Auth::guard('mahasiswa')->attempt(['nrp' => $request->get('nrpnpk')]))
      //     {
      //         return $this->sendLoginResponse($request, "mahasiswa");
      //     }
      // }
      // else if ($request->get('jenis') == "npk") 
      // {
      //     if (Auth::guard('karyawan')->attempt(['npk' => $request->get('nrpnpk')]))
      //     {
      //         return $this->sendLoginResponse($request, "karyawan");                
      //     }
      // }

      // If the login attempt was unsuccessful we will increment the number of attempts
      // to login and redirect the user back to the login form. Of course, when this
      // user surpasses their maximum number of attempts they will get locked out.
      // $this->incrementLoginAttempts($request);

      // return $this->sendFailedLoginResponse($request);
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required',
        ]);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request, $jenis)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);
        if($jenis == "mahasiswa")
        {
            return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended('/mhs/');
        }
        else if($jenis == "karyawan")
        {
            return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended('/customredirect');
        }
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
