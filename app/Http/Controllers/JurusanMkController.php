<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;

class JurusanMkController extends Controller
{
    public function showDataJurusanMk(Request $r)
    {
    	$ids = $r->get('ids');
    	if(empty($ids))
    	{
    		$ids = "jur";
    	}

    	if($ids == "jur")
    	{
    		$datas = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
    	}
    	else if($ids == "mk")
    	{
    		$datas = Models\Matakuliah::get();
            // dd($datas);
    	}    	
    	$jurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)
					->select('IdJurusan', 'Nama')
    				->get();
    	return view('maf.master.jurusanmk', array('datas' => $datas, 'ids' => $ids, 'datahapus' => 'ya', 'jurusans' => $jurusans));
    }

    public function simpanJurusanMK(Request $r)
    {
    	$kodemk = $r->get('kodemk');
    	$jurusan = $r->get('idjurusan');
    	$idjurusansf = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan')->get();
    	//dihapus dulu data yang uda ada, br diperbarui
    	// $data = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusansf)->where('kodemk', $kodemk)->get()->update;
        $data = Models\JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusansf)->where('kodemk', $kodemk)
          ->update(['deleted_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);

    	// foreach ($data as $d) {
    	// 	$d->deleted_at = date('Y-m-d H:i:s');
     //        $d->updated_at = date('Y-m-d H:i:s');
     //        $d->save();
    	// }

    	//insert baru
    	foreach ($jurusan as $j) {
    		if(Models\JurusanDaftarMatakuliah::where('idjurusan', $j)->where('kodemk', $kodemk)->exists())
    		{
    			$d = Models\JurusanDaftarMatakuliah::where('idjurusan', $j)->where('kodemk', $kodemk)->first()->restore();
    		}
    		else
    		{
    			$databaru = new Models\JurusanDaftarMatakuliah(array (
	    				'idjurusan' => $j,
	    				'kodemk' => $kodemk
	    			));
	    		$databaru->save();
    		}
    	}
    	return back();
    }

    public function showAllMk($idjur)
    {
    	$datamks = Models\Matakuliah::all();
    	$idj = $idjur;
        $nama = Models\Jurusan::where('IdJurusan', $idjur)->first()->Nama;
    	return view('maf.master.jurusanpilihmk', array('datamks' => $datamks, 'idj' => $idjur, 'nama' => $nama));
    }

    public function deleteJurusanPilihMk(Request $r)
    {
    	$j = $r->get('j');
    	$kmk = $r->get('km');
    	$d = Models\JurusanDaftarMatakuliah::where('idjurusan', $j)->where('kodemk', $kmk)->first()->delete();
    	return back();
    }

    public function addJurusanPilihMk(Request $r)
    {
    	$j = $r->get('j');
    	$kmk = $r->get('km');
    	if(Models\JurusanDaftarMatakuliah::where('idjurusan', $j)->where('kodemk', $kmk)->exists())
		{
			$d = Models\JurusanDaftarMatakuliah::where('idjurusan', $j)->where('kodemk', $kmk)->first()->restore();
		}
		else
		{
			$databaru = new Models\JurusanDaftarMatakuliah(array (
            				'idjurusan' => $j,
            				'kodemk' => $kmk
            			));
    		$databaru->save();
		}
		return back();
    }
}
