<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;

class SemesterController extends Controller
{
  /* Master Semester */
  public function showDataSemester() {
    $data = Models\Semester::all();
    return view('maf.pengaturan.mastersemester', array('data' => $data, 'datahapus' => 'tidak'));
  }

  public function showDataSemesters() {
    $data = Models\Semester::withTrashed()->get();
    return view('maf.pengaturan.mastersemester', array('data' => $data, 'datahapus' => 'ya'));
  }

  public function addDataSemester() {
    $enumSemester = Models\Semester::getEnum('semester');
    return view('maf.pengaturan.mastersemester-add', array('mode' => 'add', 'enumSemester' => $enumSemester));
  }

  public function aktifkanSemester($id) {
    $semester = Models\Semester::whereId($id)->first()->AktifkanSemester();
    return back();
  }

  public function storeDataSemester(Request $r) {
    $semesterbaru = new Models\Semester();
    if($r->get('tanggalmulaiuts') == "")
      $tanggalmulaiuts = null;
    else
      $tanggalmulaiuts = $r->get('tanggalmulaiuts');

    if($r->get('tanggalselesaiuts') == "")
      $tanggalselesaiuts = null;
    else
      $tanggalselesaiuts = $r->get('tanggalselesaiuts');
    
    if($r->get('tanggalmulaiuas') == "")
      $tanggalmulaiuas = null;
    else
      $tanggalmulaiuas = $r->get('tanggalmulaiuas');
    
    if($r->get('tanggalselesaiuas') == "")
      $tanggalselesaiuas = null;
    else
      $tanggalselesaiuas = $r->get('tanggalselesaiuas');

    if ($semesterbaru->cekDobel($r->get('tahunajaran'), $r->get('semester'))) {
      $semesterbaru = new Models\Semester(array(
        'tahunajaran' => $r->get('tahunajaran'),
        'semester' => $r->get('semester'),
        'tanggalmulaiuts' => $tanggalmulaiuts,
        'tanggalselesaiuts' => $tanggalselesaiuts,
        'tanggalmulaiuas' => $tanggalmulaiuas,
        'tanggalselesaiuas' => $tanggalselesaiuas,
        'statusaktif' => 'Tidak'
      ));
      $semesterbaru->save();

      if ($r->get('statusaktif') == 'Ya') {
        $berhasil = $semesterbaru->AktifkanSemester();
        // nampung berhasil apa nggak
      }
      $status = "Data Semester berhasil ditambahkan";
    } else {
      $status = "Tidak bisa menambahkan. Data Semester sudah ada";
      return back()->with('status', $status)->withInput();
    }
    return redirect('/maf/mastersemester')->with('status', $status);
  }

  public function editDataSemester($id) {
    $semester = Models\Semester::withTrashed()->where('id', $id)->first();
    $enumSemester = Models\Semester::getEnum('semester');

    return view('maf.pengaturan.mastersemester-add', array('mode' => 'edit', 'semester' => $semester, 'enumSemester' => $enumSemester));
  }

  public function updateDataSemester(Request $r, $id) {
    if($r->get('tanggalmulaiuts') == "")
      $tanggalmulaiuts = null;
    else
      $tanggalmulaiuts = $r->get('tanggalmulaiuts');

    if($r->get('tanggalselesaiuts') == "")
      $tanggalselesaiuts = null;
    else
      $tanggalselesaiuts = $r->get('tanggalselesaiuts');
    
    if($r->get('tanggalmulaiuas') == "")
      $tanggalmulaiuas = null;
    else
      $tanggalmulaiuas = $r->get('tanggalmulaiuas');
    
    if($r->get('tanggalselesaiuas') == "")
      $tanggalselesaiuas = null;
    else
      $tanggalselesaiuas = $r->get('tanggalselesaiuas');

    $semester = Models\Semester::withTrashed()->where('id', $id)->first();
    if ($semester->cekDobel($r->get('tahunajaran'), $r->get('semester'))) {
      $semester->tahunajaran = $r->get('tahunajaran');
      $semester->semester = $r->get('semester'); 
      $semester->tanggalmulaiuts = $tanggalmulaiuts;
      $semester->tanggalselesaiuts = $tanggalselesaiuts;
      $semester->tanggalmulaiuas = $tanggalmulaiuas;
      $semester->tanggalselesaiuas = $tanggalselesaiuas;
      $semester->statusaktif = $r->get('statusaktif');
      $semester->save();
      $statussave = "Data Semester berhasil diubah";
      return redirect('/maf/mastersemester')->with('status', $statussave);
    }
    else
    {
      $statussave = "Tidak berhasil memperbarui. Data sudah ada.";
      return back()->with('status', $statussave);
    }
  }

  public function deleteDataSemester($id) {
    $semester = Models\Semester::where('id', $id)->first();
    $semester->statusaktif = 'Tidak';
    $semester->save();
    $semester->delete();
    return back()->with('status', 'Data berhasil dihapus.');
  }

  public function restoreDataSemester($id) {
    $semester = Models\Semester::withTrashed()->where('id', $id)->first();
    if ($semester->cekDobel($semester->tahunajaran, $semester->semester)) {
      $semester->restore();
      $status = "Berhasil membatalkan penghapusan.";
    }
    else
      $status = "Tidak berhasil membatalkan penghapusan. Data sudah ada.";
    return back()->with('status', $status);
  }

  public function forcedeleteDataSemester($id) {
    $semester = Models\Semester::withTrashed()->where('id', $id)->first();
    $semester->forceDelete();
    return back();
  }
  /* end of master semester */
}
