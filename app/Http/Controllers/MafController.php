<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;
use PDF;

class MafController extends Controller {

  public function showBeranda() 
  {
    return view('maf.beranda');
  }

  /* Presensi Kuliah */
  public function showDataPresensiKuliah() {
    $idkelas = Models\KelasParalel::where('idsemester', Models\Semester::getAktifId())->select('id')->get();
    $kuliahs = Models\JadwalKuliah::whereIn('idkelasparalel', $idkelas)->get();
    return view('maf.presensikuliah', array('kuliahs' => $kuliahs));
  }

  public function showDetailPresensiKuliah($idjadwalkuliah) {
    $kuliah = Models\JadwalKuliah::whereId($idjadwalkuliah)->first();
    return view('maf.presensikuliah-dtl', array('kuliah' => $kuliah));
  }
  /* end of presensi kuliah */

  /* Master KelasParalel */
  public function showDataKelasParalel(Request $r) {
    $idsemester = $r->get('ids');
    if (!empty($idsemester)) {
      if ($idsemester != 0) {
        $kps = Models\KelasParalel::where('idsemester', $idsemester)->get();
      }
    } else {
      $idsemester = Models\Semester::getAktifId();
      $kps = Models\KelasParalel::where('idsemester', $idsemester)->get();
    }
    $semester = Models\Semester::all();
    return view('maf.master.masterkp', array('kps' => $kps, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));
  }

  public function showDataKelasParalels(Request $r) {
    $idsemester = $r->get('ids');
    if (!empty($idsemester)) {
      if ($idsemester != 0) {
        $kps = Models\KelasParalel::withTrashed()->where('idsemester', $idsemester)->get();
      }
    } else {
      $idsemester = Models\Semester::getAktifId();
      $kps = Models\KelasParalel::withTrashed()->where('idsemester', $idsemester)->get();
    }
    $semester = Models\Semester::all();
    return view('maf.master.masterkp', array('kps' => $kps, 'datahapus' => 'ya', 'semester' => $semester, 'ids' => $idsemester));
  }

  public function addDataKelasParalel() {
    $matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
        /*          ->where(function($query)
         {
         $idsemesteraktif = Models\Semester::getAktif()->id;
         $kodemksudah = Models\KelasParalel::where('idsemester', $idsemesteraktif)->select('kodemk')->get();
         return $query->whereNotIn('kodemk', $kodemksudah);
         }) */
        ->get();
    $dosen = Models\Karyawan::where('tipe', 'ID0')->get(['nama', 'npk']);
    /* print_r($dosen);
     exit(); */
    $enumStatusAktif = Models\KelasParalel::getEnum('statusaktif');
    return view('maf.master.masterkp-add', array('mode' => 'add', 'enumStatusAktif' => $enumStatusAktif, 'matakuliah' => $matakuliah, 'dosen' => $dosen));
  }

  public function storeDataKelasParalel(Request $r) {
    $kodemk = explode(" ", $r->get('kodemk'));
    $kodemk = $kodemk[0];
    $idsemesteraktif = Models\Semester::getAktif()->id;
    $jumlahkp = $r->get('jumlahkp');

    for ($i = 1; $i <= $jumlahkp; $i++) {
      $kpbaru = new Models\KelasParalel(array(
        'kodekp' => $r->get('kodekp-' . $i),
        'kapasitas' => $r->get('kapasitas-' . $i),
        'statusaktif' => $r->get('statusaktif-' . $i),
        'npkdosenpengajar' => $r->get('npkdosenpengajar-' . $i),
        'idsemester' => $idsemesteraktif,
        'kodemk' => $kodemk
      ));
      $kpbaru->save();
    }

    $statussave = "Data Kelas Paralel berhasil ditambahkan";
    return redirect('/maf/kelas')->with('status', $statussave);
  }

  public function aktifkanKelasParalel($id) {
    $kp = Models\KelasParalel::whereId($id)
        ->update(['statusaktif' => 'Ya']);
    return back();
  }

  public function nonaktifkanKelasParalel($id) {
    $kp = Models\KelasParalel::whereId($id)
        ->update(['statusaktif' => 'Tidak']);
    return back();
  }

  public function editDataKelasParalel($id) {
    $dosen = Models\Karyawan::where('tipe', 'ID0')->get(['nama', 'npk']);
    $kp = Models\KelasParalel::whereId($id)->first();
    return view('maf.master.masterkp-add', array('mode' => 'edit', 'kp' => $kp, 'dosen' => $dosen));
  }

  public function updateDataKelasParalel(Request $r, $id) {
    $kodemk = explode(" ", $r->get('kodemk'));
    $kodemk = $kodemk[0];
    $kp = Models\KelasParalel::withTrashed()->where('id', $id)->first();
    if ($kp->kodekp != $r->get('kodekp')) { // dia ganti kodekp
      $cekkodekpdobel = Models\KelasParalel::withTrashed()->where('kodekp', $r->get('kodekp'))->where('kodemk', $kodemk)->count();
      if ($cekkodekpdobel == 0) { // blm ada nama kp itu, bs diganti
        $kp->kodekp = $r->get('kodekp');
        $kp->kapasitas = $r->get('kapasitas');
        $kp->statusaktif = $r->get('statusaktif');
        $kp->npkdosenpengajar = $r->get('npkdosenpengajar');
        $kp->save();
      } else {
        $status = "Tidak bisa mengubah nama KP. Data sudah ada";
        return back()->with('status', $status)->withInput();
      }
    } else { //dia ga ganti kode kpnya
      $kp->kapasitas = $r->get('kapasitas');
      $kp->statusaktif = $r->get('statusaktif');
      $kp->npkdosenpengajar = $r->get('npkdosenpengajar');
      $kp->save();
    }
    $statussave = "Data Kelas Paralel berhasil diubah";
    return redirect('/maf/kelas')->with('status', $statussave);
  }

  public function tambahDummyDataKelasParalel()
  {
    //tambah KP
    /*$kodemk = Models\Matakuliah::where('statusbuka', 'Ya')->where('kodemk', 'LIKE', '16%')->select('kodemk')->get()->toArray();

    $jumlahkp = mt_rand(1, 3);

    $arrkodekp = array('1' => 'A', '2' => 'B', '3' => 'C');
    $npkdosenpengajar = Models\Karyawan::where('tipe', 'ID0')->select('npk')->get()->toArray();

    for($j = 1; $j <= 100; $j++) {
      for ($i = 1; $i <= $jumlahkp ; $i++) { 
          $kapasitas = mt_rand(30, 40);
          $kodekp = $arrkodekp[$i];
          $npkdosen = $npkdosenpengajar[array_rand($npkdosenpengajar, 1)]['npk'];
          $kmk = $kodemk[array_rand($kodemk, 1)]['kodemk'];
          if(!Models\KelasParalel::where('kodemk', $kmk)->where('kodekp', $kodekp)->exists())
          {
            $kpbaru = new Models\KelasParalel(array(
                'kodekp' => $kodekp,
                'kapasitas' => $kapasitas,
                'statusaktif' => 'Ya',
                'npkdosenpengajar' => $npkdosen,
                'idsemester' => 11,
                'kodemk' => $kmk,
            ));
            $kpbaru->save();
          }
          
      }
    }*/

    //tambahjadwalkuliah
    /*$kelass = Models\KelasParalel::select('id')->get()->toArray();
    $ruangans = Models\Ruangan::select('id')->get()->toArray();
    
    for ($i = 1; $i <= 20 ; $i++) {
      $idkp = $kelass[array_rand($kelass, 1)]['id'];
      $idr = $ruangans[array_rand($ruangans, 1)]['id'];
      if(!Models\JadwalKuliah::where('idkelasparalel', $idkp)->exists())
      {
        $kuliahbaru = new Models\JadwalKuliah(array(
                'idruangan' => $idr,
                'hari' => 'Kamis',
                'jammasuk' => '13:00',
                'jamkeluar' => '15:45',
                'idkelasparalel' => $idkp
            ));
        $kuliahbaru->save();
      }
    }*/

    //tambahjadwalujian TI
    // $kodemk = Models\JurusanDaftarMatakuliah::where('idjurusan', 20)->select('kodemk')->get()->toArray();
    // $idsemesteraktif = Models\Semester::getAktifId();
    // for ($i = 1; $i <= 5 ; $i++) {
    //   $kmk = $kodemk[array_rand($kodemk, 1)]['kodemk'];
    //   // $idr = $ruangans[array_rand($ruangans, 1)]['id'];
    //   if(!Models\JadwalUjian::where('kodemk', $kmk)->exists())
    //   {
    //     $ujianbaru = new Models\JadwalUjian(array(
    //             'mingguke' => 3,
    //             'hari' => 'Senin',
    //             'jamke' => mt_rand(1,4),
    //             'kodemk' => $kmk,
    //             'idsemester' => $idsemesteraktif
    //         ));
    //     $ujianbaru->save();
    //   }
    // }

    //tambah mhs
    /*$arrayNama = array('Billy', 'Budi', 'Anna', 'Maria', 'Paris', 'Christine', 'Aldo', 'Daiva', 'Meliza', 'Calvin', 'Jimmy', 'Tonny', 'Wina', 'Sri', 'Suryani', 'Kusuma', 'Daniel', 'Ami', 'Bella', 'Charlie', 'Wijaya', 'Wirya', 'Halim', 'Surya', 'Matulatan', 'Winata', 'Tan', 'Tandywijaya', 'Salim', 'Brian', 'Yohanes', 'Anastasia', 'Fatmawati');
    for ($i = 1; $i <= 89 ; $i++) {
      $Nama = $arrayNama[array_rand($arrayNama, 1)] . " " . $arrayNama[array_rand($arrayNama, 1)];
      if($i < 10)
      {
        $nrp = "16071400" . $i;
      }
      else if($i < 100)
      {
        $nrp = "1607140" . $i;
      }
      else
      {
        $nrp = "160714" . $i;
      }
      
      if(!Models\Mahasiswa::where('nrp', $nrp)->exists())
      {
        $mhsbaru = new Models\Mahasiswa(array(
            'nrp' => $nrp,
            'nama' => $Nama,
            'tahunakademikterima' => '2014',
            'semesterterima' => 'Gasal',
            'idjurusan' => 20,
            'kodestatus' => null,
            'statuslunasupp' => null,
            'ipktanpae' => 3,
            'skskumtanpae' => 90,
            'sksmaxdepan' => 24,
            'masastudi' => 5,
            'nilaitoefl' => 450,
            'asisten' => 'Tidak'
        ));
        $mhsbaru->save();
      }
    }*/


    $statussave = "Data Kelas Paralel berhasil ditambahkan";
    return redirect('/maf/kelas')->with('status', $statussave);
  }

  public function deleteDataKelasParalel($id) {
    $kp = Models\KelasParalel::where('id', $id)->first();
    $kp->delete();
    return back()->with('status', "Data berhasil dihapus.");
  }

  public function restoreDataKelasParalel($id) {
    $kp = Models\KelasParalel::withTrashed()->where('id', $id)->first();
    $kp->restore();
    return back();
  }

  public function forcedeleteDataKelasParalel($id) {
    $kp = Models\KelasParalel::withTrashed()->where('id', $id)->first();
    $kp->forceDelete();
    return back();
  }
  /* end of master kp */

}
