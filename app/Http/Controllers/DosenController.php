<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;

class DosenController extends Controller
{
    public function showBeranda()
    {
    	return view('dosen.beranda');
    }

    /*Data Kelas*/
    public function showDataKelas(Request $r)
    {
    	$ids = $r->get('ids');
    	if(empty($ids))
    	{
    		$ids = Models\Semester::getAktifId();
    	}
    	$semester = Models\Semester::All();

    	$dataampu = Models\AmpuDosenMatakuliahSemester::where('npkdosen', Auth::guard('karyawan')->user()->npk)
    									->where('idsemester', $ids)
    									->get();
    	
    	$dataajar = Models\KelasParalel::where('npkdosenpengajar', Auth::guard('karyawan')->user()->npk)
    								->where('statusaktif', 'Ya')
    								->where('idsemester', $ids)
    								->get();

    	return view('dosen.daftarkp', array('dataampu' => $dataampu, 'dataajar' => $dataajar, 'semester' => $semester, 'ids' => $ids));
    }
    /*end of data kelas*/

    /*Data Mengajar*/
    public function showDataMengajar(Request $r)
    {
        $ids = $r->get('ids');
        if(empty($ids))
        {
            $ids = Models\Semester::getAktifId();
        }
        $semester = Models\Semester::All();

        $dataajar = Models\KelasParalel::where('npkdosenpengajar', Auth::guard('karyawan')->user()->npk)
                                    ->where('statusaktif', 'Ya')
                                    ->where('idsemester', $ids)
                                    ->with('jadwalkuliah')
                                    ->get();

        return view('dosen.datamengajar', array('dataajar' => $dataajar, 'semester' => $semester, 'ids' => $ids));
    }
    /*end of data mengajar*/

    /*Data Jaga Ujian*/
    public function showDataJaga(Request $r)
    {
        $user = Auth::guard('karyawan')->user();

        $presensis = $user->getDataJaga();

        return view('dosen.datajaga', array('presensis' => $presensis));
    }
    /*end of data jaga ujian*/


    /*Kasus Khusus*/
    public function showKKIndex(Request $r)
    {
        $fpp = Models\Fpp::getSekarang();
    	$statnrp = "belum";
    	$nrp = $r->get('nrpkk');
        $kelas = Models\KelasParalel::where('statusaktif', 'Ya')
                    ->where('idsemester', Models\Semester::getAktifId())
                    ->get();
    	if($nrp != "")
    	{
    		$statnrp = "sudah";

    		$idfpps = Models\Fpp::where('idsemester', Models\Semester::getAktifId())
				    			->select('id')
				    			->get();
    		$datas = Models\DaftarFppKelasMahasiswa::withTrashed()
				    			->where('nrp', $nrp)
				    			->whereIn('idfpp', $idfpps)
				    			->with('fpp', 'kp')
				    			->get();

    		return view('dosen.kasuskhususindex', array('datas' => $datas, 'statnrp' => $statnrp, 'fpp' => $fpp, 'kelas' => $kelas));
    	}
    	else
    	{
    		return view('dosen.kasuskhususindex', array('statnrp' => $statnrp, 'fpp' => $fpp, 'kelas' => $kelas));
    	}
    }

    public function batalMkKK($id)
    {
    	$data = Models\DaftarFppKelasMahasiswa::whereId($id)->first();
    	$data->status = "Ditolak";
    	$data->alasan = "Dihapus KK oleh ". Auth::guard('karyawan')->user()->npk;
    	$data->save();
    	$data->delete();
    	return back();
    }

    public function undoBatalMkKK($id)
    {
    	$data = Models\DaftarFppKelasMahasiswa::withTrashed()->whereId($id)->first();
    	$data->status = "Diterima";
    	$data->alasan = "Batal dihapus KK oleh ". Auth::guard('karyawan')->user()->npk;
    	$data->save();
    	$data->restore();
    	return back();
    }

    public function tambahmkKK(Request $r)
    {
        $status = "";
        $nrp = $r->get('nrp');
        $id = $r->get('idkelasparalel');
        $idfpp = $r->get('idfpp');
        // $cekawal = false;

        $fppskrg = Models\Fpp::getSekarang();

        // if($nrp == Auth::guard('mahasiswa')->user()->nrp)
        // {
        //     if($idfpp == $fppskrg->id && $fppskrg->idsemester == Models\Semester::getAktifId())
        //     {
        //       $cekkelas = Models\KelasParalel::whereId($id)->where('statusaktif', 'Ya')->count();
        //       if($cekkelas == 1)
        //       {
        //         $cekawal = true;
        //       }
        //     }
        // }

        $caridata = Models\DaftarFppKelasMahasiswa::withTrashed()->where('nrp', $nrp)->where('idkelasparalel', $id)->where('idfpp', $idfpp)->count();

        $data = new Models\DaftarFppKelasMahasiswa(array(
                  'nrp' => $nrp,
                  'idkelasparalel' => $id,
                  'idfpp' => $idfpp,
                  'status' => 'Pending',
              ));//buat objek aja buat dipake pengecekan, jgn di save

        $status = "";
        $classnotif = "info";

        // all pengecekan ini kalau false return back + alasannya apa jadi alert
        if($data->cekMaksSks()) //kl uda maksimal sks nya nda bs ditambah lg && $cekawal
        {
            if($data->cekSksMin()) //buat mk kayak KP TA
            {
              if($data->cekPrasyarat()) //cek prasyarat
              {
                if($data->cekSyaratLain()) //buat bing ft
                {
                  if($data->cekAmbilSama()) //  uda diambil pas masa FPP ini apa blm
                  {
                    if(true) // !$data->cekPernahAmbil() cek sama yg ditranskrip
                    {
                      if($data->cekjadwalKuliah()) //cek ada jadwal kuliah yang nabrak gak sama yang diinput skrg
                      {
                        if($data->cekJadwalUjian()) //cek ada jadwal ujian yang nabrak gak sama yang diinput skrg
                        {
                          //stlah lulus smua pengecekan
                          if($fppskrg->urutanterima == "Batch")
                          {
                            if($caridata == 0)
                            {
                              //data blm ada sm skali, tambahin baru
                              $daftarmk = new Models\DaftarFppKelasMahasiswa(array(
                                              'nrp' => $nrp,
                                              'idkelasparalel' => $id,
                                              'idfpp' => $idfpp,
                                              'status' => 'Pending',
                                          ));
                              $daftarmk->save();
                              $status = "Berhasil ditambahkan";
                              $classnotif = "success";
                            }
                            else
                            {
                              //data uda prnah ada
                              $data = Models\DaftarFppKelasMahasiswa::withTrashed()->where('nrp', $nrp)->where('idkelasparalel', $id)->where('idfpp', $idfpp)->first();
                              if($data->deleted_at != null) //brrti dia kehapus sblmnya
                              {
                                $data->deleted_at = null;
                                $data->save();
                                $data->restore(); //kt restore
                                $status = "Berhasil diaktifkan kembali";
                                $classnotif = "success";
                              }
                              else
                              {
                                $status = "Tidak dapat mendaftarkan karena data sudah ada";
                                $classnotif = "warning";
                              }
                            }
                          }
                          else if($fppskrg->urutanterima == "Langsung")
                          {
                            if(!$data->kp->isFull())
                            {
                              //msh bs diterima
                              $stat = "Diterima";
                              $alasan = "Langsung Terima sesuai Kapasitas";
                              $classnotif = "success";
                            }
                            else
                            {
                              $stat = "Ditolak";
                              $alasan = "Kelas Penuh";
                              $classnotif = "danger";
                            }
                            $daftarmk = new Models\DaftarFppKelasMahasiswa(array(
                                            'nrp' => $nrp,
                                            'idkelasparalel' => $id,
                                            'idfpp' => $idfpp,
                                            'status' => $stat,
                                            'alasan' => $alasan,
                                            'statuspublikasi' => 'Ya'
                                        ));
                            $daftarmk->save();
                            $status = "Pendaftaran kelas: $stat";
                          }
                        }
                        else 
                        {
                          $status = "Terdapat Jadwal Ujian yang sama dengan Mata Kuliah ini";
                          $classnotif = "danger";                      
                        }
                      }
                      else
                      {
                        $status = "Terdapat Jadwal Kuliah yang sama dengan Mata Kuliah ini";
                        $classnotif = "danger";
                      }
                    }
                    else
                    {
                      $status = "Mata Kuliah ini sudah pernah diambil sebelumnya";
                      $classnotif = "danger";
                    }
                  }
                  else
                  {
                    $status = "Mata Kuliah ini sudah didaftarkan";
                    $classnotif = "danger";
                  }
                }
                else
                {
                  $status = "Terdapat syarat lain yang tidak terlampaui";
                  $classnotif = "danger";
                }
              }
              else 
              {
                $status = "Belum lulus mata kuliah prasyarat";
                $classnotif = "danger";
              }
            }
        else
        {
          $status = "Belum mencapai SKS minimal untuk mengambil Mata Kuliah";
          $classnotif = "danger";
        }
        }
        else
        {
        $status = "SKS tidak mencukupi";
        $classnotif = "danger";
        }

        return back()->with('status', $status)->with('classnotif', $classnotif);
    }
    /*end of KK*/
}
