public function prosesPenjagaUjian() 
{
  $idjurusans = Models\Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan')->get();
  $allnpkdosens = Models\Karyawan::join('karyawan_daftar_jurusans', 'karyawans.npk', '=', 'karyawan_daftar_jurusans.npkkaryawan')
                    ->whereIn('karyawan_daftar_jurusans.idjurusan', $idjurusans)
                    ->where('karyawan_daftar_jurusans.isutama', "Ya")
                    ->where('karyawan_daftar_jurusans.statusaktif', 'Ya')
                    ->where('karyawan_daftar_jurusans.statusjagaujian', 'Ya')
                    ->where('karyawans.tipe', 'ID0')
                    ->get();
  $allnpkkaryawans = Models\Karyawan::where('tipe', '!=', 'ID0')
                    ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                    ->get(['npk']);
  $jmlminggu = Models\JadwalUjian::where('idsemester', Models\Semester::getAktifId())->max('mingguke');
  $arrayhari = Models\JadwalUjian::getEnum('hari');
  $arrayjam = Models\JadwalUjian::getEnum('jamke');
  $semesteraktif = Models\Semester::getAktif();
  $uasuts = $semesteraktif->getUtsUas();
  $tiperuangan = "";
  $tipeujian = "";

  for ($i = 1; $i <= $jmlminggu; $i++) 
  {
    foreach ($arrayhari as $hari) 
    {
      if($hari != "Sabtu" && $hari != "Minggu")
      {
        $tanggal = "";
        if($uasuts == "uas")
          $tanggal = $semesteraktif->getTanggalUas($i, $hari);
        else
          $tanggal = $semesteraktif->getTanggalUts($i, $hari);

        foreach ($arrayjam as $jam) 
        {
          $jml = Models\JadwalUjian::where('mingguke', $i)
                  ->where('hari', $hari)
                  ->where('jamke', $jam)
                  ->where('tiperuangan'.$uasuts, 'Kelas')
                  ->where('tipeujian'.$uasuts, 'Tulis')
                  ->has('mk.kp')
                  ->count();
          if($jml > 0)
          {
            $npkdosennext = "";
            $npkkaryawannext = "";
            $kmk = Models\JadwalUjian::where('mingguke', $i)
                    ->where('hari', $hari)
                    ->where('jamke', $jam)
                    ->where('tiperuangan'.$uasuts, 'Kelas') 
                    ->where('tipeujian'.$uasuts, 'Tulis')
                    ->select('kodemk')
                    ->get();
            $kps = Models\KelasParalel::whereIn('kodemk', $kmk)->select('id')->get();
            $presensijamitu = Models\PresensiUjian::where('tanggal', $tanggal)
                              ->whereIn('idkelasparalel', $kps)
                              ->get();
            $idruangankepake = $presensijamitu->pluck('idruangan')->unique()->all();
            $npkdosensudahjaga = $presensijamitu->pluck('npkdosenjaga')->all();
            $npkkrysudahjaga = $presensijamitu->pluck('npkkaryawanjaga')->all();
            $npkkaryawannonjaga = Models\KaryawanNonjagaUjian::where('tanggal', $tanggal)
                                      ->where(function($query) use ($jam) {
                                          return $query->where('jamke', $jam)
                                              ->orWhere('jamke', 'Semua');
                                      })
                                      ->select('npkkaryawan')
                                      ->distinct()
                                      ->get();
            $npkkaryawannonjaga = $npkkaryawannonjaga->keyBy('npkkaryawan')->all();
            $npkkaryawanpiket = Models\PiketUjian::where('tanggal', $tanggal)
                                  ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                                  ->first();

            if(!empty($npkkaryawanpiket))
              $npkkaryawanpiket = $npkkaryawanpiket->npk;
            else
              $npkkaryawanpiket = "-";

            foreach ($idruangankepake as $idruangan) 
            {
              $npkkaryawannext = "-";
              $npkdosennext = "-";
              $allnpkkaryawansurut = $allnpkkaryawans->sortBy('jumlahjaga')->keyBy('npk');
              $ambil = $allnpkkaryawansurut->first()->npk;
              while($npkkaryawannext == "-" && count($allnpkkaryawansurut) > 0)
              {
                if(!array_key_exists($ambil, $npkkaryawannonjaga))
                {
                  if(!in_array($ambil, $npkkrysudahjaga))
                  {
                    if($ambil != $npkkaryawanpiket)
                    {
                      $npkkaryawannext = $ambil;
                      array_push($npkkrysudahjaga, $ambil);
                      $allnpkkaryawans->where('npk', $ambil)->first()->jumlahjaga += 1;
                      $allnpkkaryawansurut->forget($ambil);
                      break;
                    }
                  }
                }

                if($allnpkkaryawansurut->count() > 0)
                {
                  $allnpkkaryawansurut->forget($ambil);
                  $ambil = $allnpkkaryawansurut->first()->npk;
                }
                else
                  break;
              }
              if ($npkkaryawannext == "-") {
                $npkkaryawannext = null;
              }
              $allnpkdosensurut = $allnpkdosens->sortBy('jumlahjaga')->keyBy('npk');
              $ambil = $allnpkdosensurut->first()->npk;
              foreach ($kps as $kp) 
              {
                $kp = Models\KelasParalel::whereId($kp->id)->first();
                if(!array_key_exists($kp->npkdosenpengajar, $npkkaryawannonjaga))
                {
                  if(!in_array($kp->npkdosenpengajar, $npkdosensudahjaga))
                  {
                    if($kp->npkdosenpengajar != $npkkaryawanpiket)
                    {
                      $npkdosennext = $kp->npkdosenpengajar;
                      array_push($npkdosensudahjaga, $npkdosennext);
                      $allnpkdosens->where('npk', $npkdosennext)->first()->jumlahjaga += 1;
                      $allnpkdosensurut->forget($npkdosennext);
                      break;
                    }                          
                  }
                }
              }
              if($npkdosennext == "-")
              {
                $nrp = $presensijamitu->pluck('nrpawal')->all();
                $idjurusan = Models\Mahasiswa::where('nrp', $nrp[0])->first()->idjurusan;
                $dosenjurusanitu = $allnpkdosensurut->where('idjurusan', $idjurusan)->all();
                foreach($dosenjurusanitu as $d)
                {
                  $ambil = $d->npk;
                  if(!array_key_exists($ambil, $npkkaryawannonjaga))
                  {
                    if(!in_array($ambil, $npkdosensudahjaga))
                    {
                      if($ambil != $npkkaryawanpiket)
                      {
                        $npkdosennext = $ambil;
                        array_push($npkdosensudahjaga, $npkdosennext);
                        $allnpkdosens->where('npk', $npkdosennext)->first()->jumlahjaga += 1;
                        $allnpkdosensurut->forget($npkdosennext);
                        break;
                      }
                    }
                  }
                }
              }
              
              if ($npkdosennext == "-") 
              {
                $ambil = $allnpkdosensurut->first()->npk;
                while($npkdosennext == "-" && count($allnpkdosensurut) > 0)
                {
                  if(!array_key_exists($ambil, $npkkaryawannonjaga))
                  {
                    if(!in_array($ambil, $npkdosensudahjaga))
                    {
                      if($ambil != $npkkaryawanpiket)
                      {
                        $npkdosennext = $ambil;
                        array_push($npkdosensudahjaga, $npkdosennext);
                        $allnpkdosens->where('npk', $npkdosennext)->first()->jumlahjaga += 1;
                        $allnpkdosensurut->forget($npkdosennext);
                        break;
                      }
                    }
                  }
                  if($allnpkdosensurut->count() > 0)
                  {
                    $allnpkdosensurut->forget($ambil);
                    $ambil = $allnpkdosensurut->first()->npk;
                  }
                  else
                    break;
                }
              }
              if ($npkdosennext == "-") {
                $npkdosennext = null;
              }
              $presensiupdate = Models\PresensiUjian::where('tanggal', $tanggal)
                              ->whereIn('idkelasparalel', $kps)
                              ->where('idruangan', $idruangan)
                              ->update(['npkdosenjaga' => $npkdosennext, 'npkkaryawanjaga' => $npkkaryawannext]);
            }
          }
        }
      }
    }
  }

  for ($i = 1; $i <= $jmlminggu; $i++) 
  {
    foreach ($arrayhari as $hari) 
    {
      if($hari != "Sabtu" && $hari != "Minggu")
      {
        $tanggal = "";
        if($uasuts == "uas")
        {
          $tanggal = $semesteraktif->getTanggalUas($i, $hari); 
          $npkkaryawansudahpiket = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)
                              ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas)
                              ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                              ->select('npk')->get();
        }
        else
        {
          $tanggal = $semesteraktif->getTanggalUts($i, $hari);
          $npkkaryawansudahpiket = Models\PiketUjian::where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)
                              ->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts)
                              ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                              ->select('npk')->get();
        }
        if( ($uasuts == "uas" && $tanggal <= $semesteraktif->tanggalselesaiuas && $tanggal >= $semesteraktif->tanggalmulaiuas) || ($uasuts == "uts" && $tanggal <= $semesteraktif->tanggalselesaiuts && $tanggal >= $semesteraktif->tanggalmulaiuts) )
        {
          $npkkaryawansudahpiket = $npkkaryawansudahpiket->keyBy('npk')->all();
          $npkkaryawannonjaga = Models\KaryawanNonjagaUjian::where('tanggal', $tanggal)
                                    ->select('npkkaryawan')
                                    ->distinct()
                                    ->get();
          $npkkaryawannonjaga = $npkkaryawannonjaga->keyBy('npkkaryawan')->all();
          $indeks = 0;
          $npkpiket = "-";
          $ambil = "-";
          $jmlloop = 1;
          $ambil = $allnpkdosensurut->first()->npk;
          while($npkpiket == "-" && count($allnpkdosensurut) > 0 && $jmlloop < 3)
          {
            if(!array_key_exists($ambil, $npkkaryawannonjaga)) //pastiin nda ada di daftar nonjaga
            {
              if(!array_key_exists($ambil, $npkkaryawansudahpiket) || $allnpkdosens->where('npk', $ambil)->first()->jumlahjaga < $jmlloop*4)
              {
                $npkpiket = $ambil;
                $allnpkdosens->where('npk', $ambil)->first()->jumlahjaga+=4;                
                break;
              }
            }
            $allnpkdosensurut->forget($ambil);
            if($allnpkdosensurut->count() > 0)
              $ambil = $allnpkdosensurut->first()->npk;
            else
            {
              $allnpkdosensurut = $allnpkdosens->sortBy('jumlahjaga')->keyBy('npk'); //direstart
              $ambil = $allnpkdosensurut->first()->npk;
              $jmlloop++; 
            }
          }

          if(Models\PiketUjian::where('tanggal', $tanggal)->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->exists())
          {
            $data = Models\PiketUjian::where('tanggal', $tanggal)->first();
            $data->npk = $npkpiket;
            $data->save();
          }
          else
          {
            $datapiket = new Models\PiketUjian(array(
                  'tanggal' => $tanggal,
                  'npk' => $npkpiket,
                  'idfakultas' => Auth::guard('karyawan')->user()->idfakultas
              ));
            $datapiket->save();
          }
            
        }
      }
    }
  }
  return redirect('/maf/presensiujian');
}