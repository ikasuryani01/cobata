<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class TranskripController extends Controller
{
        /*Master Transkripmhs*/
    public function showDataTranskripmhs()
    {
        $transkrips = Models\Transkrip::all();
        return view('paj.mastertranskripmhs', array('transkrips' => $transkrips, 'datahapus' => 'tidak'));
    }

    public function showDataTranskripmhss()
    {
        $transkrips = Models\Transkrip::withTrashed()->get();
        return view('paj.mastertranskripmhs', array('transkrips' => $transkrips, 'datahapus' => 'ya'));
    }

    public function addDataTranskripmhsExternal()
    {
        $datas = DB::connection('mysql2d')->select("SELECT * FROM transkrips");
        return view('paj.mastertranskripmhs-addext', array('datas' => $datas));
    }

    public function storeDataTranskripmhsExternal()
    {
        $coba = DB::select("SELECT * FROM transkrips ORDER BY kodemk INTO OUTFILE 'C://xampp/htdocs/ta/public/backupdb/transkrips_" . (new \DateTime())->format('Y.m.d.H.i.s') . ".csv' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n'");
        // $hapusdata = DB::select(DB::raw('TRUNCATE TABLE mata_kuliahs'));
        $data = DB::connection('mysql2d')->select("SELECT * FROM transkrips");
        foreach($data as $d)
        {
            if (Models\Semester::where('semester', '=', $d->semester)->where('tahunajaran', '=', $d->tahun)->exists()) 
            {
                $idsemester = Models\Semester::where('semester', '=', $d->semester)->where('tahunajaran', '=', $d->tahun)->first()->id;
            }
            else 
            {
                $semesterbaru = new Models\Semester(array(
                        'tahunajaran' => $d->tahun,
                        'semester' => $d->semester,
                    ));
                $semesterbaru->save();
                $idsemester = $semesterbaru->id;
            }
            $transkripbaru = new Models\Transkrip(array(
                        'kodemk' => $d->kodeMK,
                        'nrpmhs' => $d->nrp,
                        'nisbi' => $d->nisbi,
                        'idsemester' => $idsemester,
                    ));
            $transkripbaru->save();
        }
        return redirect('/paj/mastertranskripmhs');
    }

    public function addDataTranskripmhs()
    {
        $dataSemester = Models\Semester::all();
        return view('paj.mastertranskripmhs-add', array('mode' => 'add', 'dataSemester' => $dataSemester));
    }

    public function storeDataTranskripmhs(Request $r)
    {
        $mkbaru = new Models\Transkrip(array(
                        'kodemk' => $d->kodeMK,
                        'nrpmhs' => $d->nrp,
                        'nisbi' => $d->nisbi,
                        'idsemester' => $idsemester,
                    ));
            $transkripbaru->save();
        $statussave = "Data Transkrip Mahasiswa berhasil ditambahkan";
        return redirect('/paj/mastertranskripmhs')->with('statussave', $statussave);
    }

    public function editDataTranskripmhs($id)
    {
        $transkrips = Models\Transkrip::withTrashed()->where('id', $id)->first();
        $dataSemester = Models\Semester::all();
        return view('paj.mastertranskripmhs-add', array('mode' => 'edit', 'dataSemester' => $dataSemester, 'transkrips' => $transkrips));
    }

    public function updateDataTranskripmhs(Request $r, $id)
    {
        $mk = Models\Transkrip::withTrashed()->where('kodemk', $kodemk)->first();
        $mk->kodemk = $r->get('kodemk');
        $mk->nrpmhs = $r->get('nrpmhs');
        $mk->nisbi = $r->get('nisbi');
        $mk->idsemester = $r->get('idsemester');
        $mk->save();

        $statussave = "Data Transkripmhs berhasil diubah";
        return redirect('/paj/mastertranskripmhs')->with('statussave', $statussave);
    }

    public function deleteDataTranskripmhs($id)
    {
        $mk = Models\Transkrip::where('id', $id)->first();
        $mk->deleted_by = Auth::user()->id;
        $mk->save();
        $mk->delete();
        return back();
    }

    public function restoreDataTranskripmhs($id)
    {
        $mk = Models\Transkrip::withTrashed()->where('id', $id)->first();
        $mk->deleted_by = null;
        $mk->save();
        $mk->restore();
        return back();
    }

    public function forcedeleteDataTranskripmhs($id)
    {
        $mk = Models\Transkrip::withTrashed()->where('id', $id)->first();
        $mk->forceDelete();
        return back();
    }
    /*end of master transkripmhs*/
}
