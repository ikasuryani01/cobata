<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class KonversiMkController extends Controller
{
        /*Master Konversi*/
    public function showDataKonversi(Request $r)
    {
        $idsemester = $r->get('ids');
        if($idsemester == "")
        {
            $idsemester = Models\Semester::getAktifId();
        }
        $semester = Models\Semester::all();
        $konversis = Models\KonversiMataKuliah::all();
        return view('paj.masterkonversi', array('konversis' => $konversis, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function showDataKonversis(Request $r)
    {
        $idsemester = $r->get('ids');
        if($idsemester == "")
        {
            $idsemester = Models\Semester::getAktifId();
        }
        $semester = Models\Semester::all();
        $konversis = Models\KonversiMataKuliah::withTrashed()->all();
        return view('paj.masterkonversi', array('konversis' => $konversis, 'datahapus' => 'ya', 'semester' => $semester, 'ids' => $idsemester));
    }

    public function addDataKonversi()
    {
        $matakuliahs = Models\Matakuliah::withTrashed()->get();
        $enumOperand = Models\KonversiMataKuliah::getEnum('operand');
        $enumKurikulum = Models\KonversiMataKuliah::getEnum('kurikulum');
        return view('paj.masterkonversi-add', array('mode' => 'add', 'enumOperand' => $enumOperand, 'enumKurikulum' => $enumKurikulum, 'matakuliahs' => $matakuliahs));
    }

    public function storeDataKonversi(Request $r)
    {
        $jmlmklama = $r->get('jmlmklama');
        $jmlmkbaru = $r->get('jmlmkbaru');

        if($jmlmkbaru >= $jmlmklama) // kl 1-1 atau 3-1, dst
        {
            for ($i = 1; $i <= $jmlmkbaru ; $i++) { 
                $kodemkbaru = $r->get('kodemkbaru-'.$i);
                $kodemkbaru = explode(" ", $kodemkbaru);
                $kodemkbaru = $kodemkbaru[0];

                $kodemklama = $r->get('kodemklama-1');
                $kodemklama = explode(" ", $kodemklama);
                $kodemklama = $kodemklama[0];
                $konversibaru = new Models\KonversiMataKuliah(array(
                        'kodemkawal' => $kodemklama,
                        'kodemkbaru' => $kodemkbaru,
                        'operand' => $r->get('operand'),
                        'kurikulum' => $r->get('kurikulum'),
                        'tanggalberlaku' => $r->get('tanggalberlaku')
                    ));
                $konversibaru->save();
            }
        }
        else //pasti yang lama yg lbh banyak
        {
            for ($i = 1; $i <= $jmlmklama ; $i++) { 
                $kodemklama = $r->get('kodemklama-'.$i);
                $kodemklama = explode(" ", $kodemklama);
                $kodemklama = $kodemklama[0];

                $kodemkbaru = $r->get('kodemkbaru-1');
                $kodemkbaru = explode(" ", $kodemkbaru);
                $kodemkbaru = $kodemkbaru[0];
                $konversibaru = new Models\KonversiMataKuliah(array(
                        'kodemkawal' => $kodemklama,
                        'kodemkbaru' => $kodemkbaru,
                        'operand' => $r->get('operand'),
                        'kurikulum' => $r->get('kurikulum'),
                        'tanggalberlaku' => $r->get('tanggalberlaku')
                    ));
                $konversibaru->save();
            }
        }

        $statussave = "Data Konversi berhasil ditambahkan";
        return redirect('/paj/masterkonversi')->with('statussave', $statussave);
    }

    public function editDataKonversi($id)
    {
        $matakuliah = Models\Matakuliah::where('statusbuka', 'Ya')
            ->where(function($query)
                {
                    $idsemesteraktif = Models\Semester::getAktif()->id;
                    $kodemksudah = Models\KonversiMataKuliah::where('idsemester', $idsemesteraktif)->select('kodemk')->get();
                    return $query->whereNotIn('kodemk', $kodemksudah);
                })
            ->get();
        $enumHari = Models\KonversiMataKuliah::getEnum('hari');
        $konversi = Models\KonversiMataKuliah::whereId($id)->first();
        return view('paj.masterkonversi-add', array('mode' => 'edit', 'konversi' => $konversi, 'enumHari' => $enumHari, 'matakuliah' => $matakuliah));
    }

    public function updateDataKonversi(Request $r, $id)
    {
        $konversi = Models\KonversiMataKuliah::withTrashed()->where('id', $id)->first();
        $konversi->mingguke = $r->get('mingguke');
        $konversi->hari = $r->get('hari');
        $konversi->jamke = $r->get('jamke');
        $konversi->kodemk = $r->get('kodemk');
        $konversi->save();

        $statussave = "Data Konversi berhasil diubah";
        return redirect('/paj/masterkonversi')->with('statussave', $statussave);
    }

    public function deleteDataKonversi($nrp)
    {
        $konversi = Models\KonversiMataKuliah::where('nrp', $nrp)->first();
        $konversi->deleted_by = Auth::user()->id;
        $konversi->save();
        $konversi->delete();
        return back();
    }

    public function restoreDataKonversi($nrp)
    {
        $konversi = Models\KonversiMataKuliah::withTrashed()->where('nrp', $nrp)->first();
        $konversi->deleted_by = null;
        $konversi->save();
        $konversi->restore();
        return back();
    }

    public function forcedeleteDataKonversi($nrp)
    {
        $konversi = Models\KonversiMataKuliah::withTrashed()->where('nrp', $nrp)->first();
        $konversi->forceDelete();
        return back();
    }
    /*end of master konversi*/
}
