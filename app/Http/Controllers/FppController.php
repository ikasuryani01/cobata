<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;


class FppController extends Controller
{
  /* Master Jadwalfpp */
  public function showDataJadwalFpp() {
    $data = Models\Fpp::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
    return view('maf.pengaturan.masterjadwalfpp', array('data' => $data, 'datahapus' => 'tidak'));
  }

  public function showDataJadwalFpps() {
    $data = Models\Fpp::withTrashed()->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->get();
    return view('maf.pengaturan.masterjadwalfpp', array('data' => $data, 'datahapus' => 'ya'));
  }

  public function addDataJadwalfpp() {
    $enumJenis = Models\Fpp::getEnum('jenis');
    $semesteraktif = Models\Semester::getAktif();
    $enumSemester = Models\Semester::all();
    $enumUrutanTerima = Models\Fpp::getEnum('urutanterima');
    return view('maf.pengaturan.masterjadwalfpp-add', array('mode' => 'add', 'enumJenis' => $enumJenis, 'semesteraktif' => $semesteraktif, 'enumSemester' => $enumSemester, 'enumUrutanTerima' => $enumUrutanTerima));
  }

  public function storeDataJadwalfpp(Request $r) {
    if (!Models\Fpp::where('jenis', $r->get('jenis'))->where('idsemester', $r->get('idsemester'))->exists()) {
      $jadwalfppbaru = new Models\Fpp(array(
        'jenis' => $r->get('jenis'),
        'waktuselesai' => $r->get('tanggalselesai') . " " . $r->get('waktuselesai'),
        'waktubuka' => $r->get('tanggalbuka') . " " . $r->get('waktubuka'),
        'urutanterima' => $r->get('urutanterima'),
        'idsemester' => $r->get('idsemester'),
        'idfakultas' => $r->get('idfakultas'),
      ));
      $jadwalfppbaru->save();
      $status = "Data Jadwalfpp berhasil ditambahkan";
    } else {
      $status = "Tidak bisa menambahkan. Data Jadwalfpp sudah ada";
      return back()->with('status', $status)->withInput();
    }

    return redirect('/maf/masterjadwalfpp')->with('status', $status);
  }

  public function editDataJadwalfpp($id) {
    $data = Models\Fpp::withTrashed()->where('id', $id)->first();
    $enumJenis= Models\Fpp::getEnum('jenis');
    $enumUrutanTerima = Models\Fpp::getEnum('urutanterima');

    return view('maf.pengaturan.masterjadwalfpp-add', array('mode' => 'edit', 'data' => $data, 'enumJenis' => $enumJenis, 'enumUrutanTerima' => $enumUrutanTerima));
  }

  public function updateDataJadwalfpp(Request $r, $id) {
    $jadwalfpp = Models\Fpp::withTrashed()->where('id', $id)->first();
    $jadwalfpp->waktubuka = $r->get('tanggalbuka') . " " . $r->get('waktubuka');
    $jadwalfpp->waktuselesai = $r->get('tanggalselesai') . " " . $r->get('waktuselesai');
    $jadwalfpp->urutanterima = $r->get('urutanterima');
    $jadwalfpp->save();
    $statussave = "Data Jadwal FPP berhasil diubah";
    return redirect('/maf/masterjadwalfpp')->with('status', $statussave);
  }

  public function deleteDataJadwalfpp($id) {
    $jadwalfpp = Models\Fpp::where('id', $id)->first();
    $jadwalfpp->delete();
    return back()->with('status', "Data berhasil dihapus.");
  }

  public function restoreDataJadwalfpp($id) {
    $jadwalfpp = Models\Fpp::withTrashed()->where('id', $id)->first();
    if (!Models\Fpp::where('jenis', $jadwalfpp->jenis)->where('idsemester', $jadwalfpp->idsemester)->exists()) {
      $jadwalfpp->restore();
      $status = "Penghapusan berhasil dibatalkan";
    }
    else
    {
      $status = "Pembatalan penghapusan gagal. Data " . $jadwalfpp->jenis ." ". $jadwalfpp->semester->semester ." ". $jadwalfpp->semester->tahunajaran . " sudah ada";
    }
    return back()->with('status', $status);
  }

  public function forcedeleteDataJadwalfpp($id) {
    $jadwalfpp = Models\Fpp::withTrashed()->where('id', $id)->first();
    $jadwalfpp->forceDelete();
    return back();
  }
  /* end of master jadwalfpp */
}
