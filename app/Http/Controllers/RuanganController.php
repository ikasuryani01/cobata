<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models;
use DB;
use Auth;

class RuanganController extends Controller
{
    /* Master Ruangan */
  public function showDataRuangan() {
    $ruangans = Models\Ruangan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->orderBy('nama', 'asc')->get();
    return view('maf.master.masterruangan', array('ruangans' => $ruangans, 'datahapus' => 'tidak'));
  }

  public function showDataRuangans() {
    $ruangans = Models\Ruangan::withTrashed()->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->orderBy('nama', 'asc')->get();
    return view('maf.master.masterruangan', array('ruangans' => $ruangans, 'datahapus' => 'ya'));
  }

  public function addDataRuangan() {
    $enumJenis = Models\Ruangan::getEnum('jenis');
    return view('maf.master.masterruangan-add', array('mode' => 'add', 'enumJenis' => $enumJenis));
  }

  public function storeDataRuangan(Request $r) {
    $ruanganbaru = new Models\Ruangan(array(
      'nama' => $r->get('nama'),
      'kapasitas' => $r->get('kapasitas'),
      'kapasitasujian' => $r->get('kapasitasujian'),
      'gedung' => $r->get('gedung'),
      'lantai' => $r->get('lantai'),
      'noruang' => $r->get('noruang'),
      'jenis' => $r->get('jenis'),
      'untukujian' => $r->get('untukujian'),
      'untukkuliah' => $r->get('untukkuliah')
    ));
    $ruanganbaru->save();
    $statussave = "Data Ruangan berhasil ditambahkan";
    return redirect('/maf/ruangan')->with('statussave', $statussave);
  }

  public function editDataRuangan($id) {
    $ruangan = Models\Ruangan::withTrashed()->where('id', $id)->first();
    $enumJenis = Models\Ruangan::getEnum('jenis');

    return view('maf.master.masterruangan-add', array('mode' => 'edit', 'ruangan' => $ruangan, 'enumJenis' => $enumJenis));
  }

  public function updateDataRuangan(Request $r, $id) {
    $ruangan = Models\Ruangan::withTrashed()->where('id', $id)->first();
    $ruangan->nama = $r->get('nama');
    $ruangan->kapasitas = $r->get('kapasitas');
    $ruangan->kapasitasujian = $r->get('kapasitasujian');
    $ruangan->gedung = $r->get('gedung');
    $ruangan->lantai = $r->get('lantai');
    $ruangan->noruang = $r->get('noruang');
    $ruangan->jenis = $r->get('jenis');
    $ruangan->untukujian = $r->get('untukujian');
    $ruangan->untukkuliah = $r->get('untukkuliah');
    $ruangan->save();

    $statussave = "Data Ruangan berhasil diubah";
    return redirect('/maf/ruangan')->with('statussave', $statussave);
  }

  public function deleteDataRuangan($id) {
    $ruangan = Models\Ruangan::where('id', $id)->first();
    $ruangan->deleted_by = Auth::user()->id;
    $ruangan->save();
    $ruangan->delete();
    return back();
  }

  public function restoreDataRuangan($id) {
    $ruangan = Models\Ruangan::withTrashed()->where('id', $id)->first();
    $ruangan->deleted_by = null;
    $ruangan->save();
    $ruangan->restore();
    return back();
  }

  public function forcedeleteDataRuangan($id) {
    $ruangan = Models\Ruangan::withTrashed()->where('id', $id)->first();
    $ruangan->forceDelete();
    return back();
  }
  /* end of master ruangan */
}
