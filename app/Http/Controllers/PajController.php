<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class PajController extends Controller
{
    public function showBeranda()
    {
      $semester = Models\Semester::getAktif();
      return view('paj.beranda', array('semester' => $semester));
    }

    /*Hasil FPP*/
    public function showHasilFppIndex(Request $r)
    {
      $kps = Models\KelasParalel::where('statusaktif', 'Ya')->get();
      return view('paj.hasilfpp', array('datas' => $kps, 'datahapus' => 'tidak'));
      /*$idsemester = $r->get('ids');
      if(!empty($idsemester))
      {
          if($idsemester != 0)
          {
              $fpps = Models\Fpp::where('idsemester', $idsemester)->get();
          }
      }
      else
      {
          $idsemester = Models\Semester::getAktif()->id;
          $fpps = Models\Fpp::where('idsemester', $idsemester)->get();
      }
      $datas = Models\DaftarFppKelasMahasiswa::whereIn('idfpp', $fpps)->get();
      $semester = Models\Semester::all();
      return view('paj.hasilfpp', array('datas' => $datas, 'fpps' => $fpps, 'datahapus' => 'tidak', 'semester' => $semester, 'ids' => $idsemester));*/
    }

    public function lihatDetailKelas($idkp)
    {
      $datapendaftar = Models\DaftarFppKelasMahasiswa::where('idkelas', $idkp)->get();
      $kp = Models\KelasParalel::whereId($idkp)->first();
      // print_r($datapendaftar[0]->mahasiswa);
      // exit();
      return view('paj.detailhasilfpp', array('datapendaftar' => $datapendaftar, 'kp' => $kp, 'mode' => 'show'));
    }

    public function prosesSemua()
    {
      $idfpp = Models\Fpp::getSekarangId();
      if($idfpp != 0)
      {
        // harusnya nnti ditambai pengecekan cuma ambil mk yang di fakultas / jurusan itu, br select idkp, br wherein id idkp nnti
        $kelas = Models\KelasParalel::where('idsemester', Models\Semester::getAktifId())
                    ->where('statusaktif', 'Ya')
                    ->get();
        $kelasdanisinya = $kelas->each(function ($item, $key) {
          $jumlah = Models\DaftarFppKelasMahasiswa::where('idkelas', $item->id)
                    ->where('status', 'Pending')
                    ->count();
          $item->isisementara = $jumlah;
          // echo $item . " " . $key . "<br>";
        });
        $kelas = $kelasdanisinya->where('isisementara', '>', '0')->sortBy('isisementara');
        // print_r($kelasdanisinyaurut);
        // exit();
        foreach ($kelas as $kls) {
            $kls->prosesPerwalian($idfpp);
        }
      }
      else
      {
        echo "Tidak ada FPP yang sedang berjalan saat ini";
      }

      // exit();
      return back();
    }

    public function publikasiSemua($jenisf)
    {
      //jgn lupa nnti disamain sm show daftarmk
      $idkps = Models\KelasParalel::where('statusaktif', 'Ya')->select('id')->get(); 
      $idfpps = Models\Fpp::where('idsemester', Models\Semester::getAktifId())->get();
      $idfpp = $idfpps->where('jenis', $jenisf)->first();
      // dd($idfpp);
      
      $datas = Models\DaftarFppKelasMahasiswa::whereIn('idkelas', $idkps)
                    ->where('idfpp', $idfpp->id)
                    ->update(['statuspublikasi' => 'Ya']);

      return back();
    }

    public function publikasibyKelas($idkelas)
    {
      $datas = Models\DaftarFppKelasMahasiswa::where('idkelas', $idkelas)
                ->update(['statuspublikasi' => 'Ya']);
      return back();
    }

    public function tidakPublikasibyKelas($idkelas)
    {
      $datas = Models\DaftarFppKelasMahasiswa::where('idkelas', $idkelas)
                ->update(['statuspublikasi' => 'Tidak']);
      return back();
    }

    public function editHasilFppbyId($iddata)
    {
      $datapendaftarkelas = Models\DaftarFppKelasMahasiswa::whereId($iddata)->first();
      $enumStatusTerima = Models\DaftarFppKelasMahasiswa::getEnum('status');
      
      return view('paj.detailhasilfpp', array('datapendaftarkelas' => $datapendaftarkelas, 'mode' => 'editsatu'));
    }

    public function editHasilFppbyKelas($idkelas)
    {
      $datapendaftarkelas = Models\DaftarFppKelasMahasiswa::where('idkelas', $idkp)->get();
      $enumStatusTerima = Models\DaftarFppKelasMahasiswa::getEnum('status');

      return view('paj.detailhasilfpp', array('datapendaftarkelas' => $datapendaftarkelas, 'mode' => 'edit'));
    }


    /*end of hasil fpp*/

    /*Laporan*/
    public function showMenuLaporan()
    {
        /*$kp = Models\KelasParalel::whereId(4)->first();
        $idfpp = Models\Fpp::getSekarangId();
        // echo $idfpp;
        $kp->prosesPerwalian($idfpp);
        // print_r($array);
        exit();*/
        return view('paj.laporan.menu');
    }

}