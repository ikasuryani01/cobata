<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models;

use Auth;
use DB;

class GuestController extends Controller
{
    public function Redirects()    
    {
      // echo Auth::guard('karyawan')->user()->getJabatan();
      if(!Auth::guard('karyawan')->guest() || !Auth::guard('karyawan')->guest())
      {
        $jabatan = Auth::guard('karyawan')->user()->getJabatan()->namasingkat;

        if ($jabatan == "paj") 
        {
           return redirect('/paj/');
        }

        else if (Auth::guard('karyawan')->user()->getJabatan()->namasingkat == "maf") 
        {
           return redirect('/maf/');
        }

        else if ($jabatan == "aa" || $jabatan == "dosen" || $jabatan == "kalab" || $jabatan == "kajur")
        {
            return redirect('/dosen/');
        }
      }
      elseif (!Auth::guard('mahasiswa')->guest()) {
        return redirect('/mhs/');
      }
      else
      {
        $semester = Models\Semester::getAktif();
        return view('guest.beranda', array('semester' => $semester));
      }
    }

    public function viewBeranda()
    {
      $semester = Models\Semester::getAktif();
      return view('guest.beranda', array('semester' => $semester));
    }
    
    public function JadkulIndex()
    {
      $idfakultas = 6; //buat FT dulu
      $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->get();
      return view("guest.jadwalkuliahmenu", array('datajurusan' => $datajurusan));
    }

    public function JadkulJurusan($idjur)
    {
      $namajur = Models\Jurusan::where('IdJurusan', $idjur)->first()->Nama;
      $jurusanmk = Models\JurusanDaftarMataKuliah::where('idjurusan', $idjur)->select('kodemk')->get();
      $mk = Models\Matakuliah::whereIn('kodemk', $jurusanmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
      $kp = Models\KelasParalel::whereIn('kodemk', $mk)->where('statusaktif', 'Ya')->where('idsemester', Models\Semester::getAktifId())->select('id')->get();
      $kuliahs = Models\Jadwalkuliah::whereIn('idkelasparalel', $kp)->get();
      // dd($kp);
      return view('guest.jadwalkuliah', array('kuliahs' => $kuliahs, 'namajur' => $namajur));
    }

    public function JadwalujianIndex()
    {
      $idfakultas = 6; //buat FT dulu
      $datajurusan = Models\Jurusan::where('IdFakultas', $idfakultas)->get();
      return view("guest.jadwalujianmenu", array('datajurusan' => $datajurusan));
    }

    public function JadwalujianJurusan($idjur)
    {
      $namajur = Models\Jurusan::where('IdJurusan', $idjur)->first()->Nama;
      $jurusanmk = Models\JurusanDaftarMataKuliah::where('idjurusan', $idjur)->select('kodemk')->get();
      $mk = Models\Matakuliah::whereIn('kodemk', $jurusanmk)->where('statusbuka', 'Ya')->select('kodemk')->get();
      $ujians = Models\JadwalUjian::whereIn('kodemk', $mk)->where('idsemester', Models\Semester::getAktifId())->get();
      return view('guest.jadwalujian', array('ujians' => $ujians, 'namajur' => $namajur));
    }

}
