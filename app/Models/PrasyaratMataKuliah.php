<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrasyaratMataKuliah extends BaseModel
{
    protected $guarded = ['id'];
    
    public function mk()
    {
    	return $this->belongsTo('App\Models\MataKuliah', 'kodemk', 'kodemk');
    }

    public function mksyarat()
    {
    	return $this->belongsTo('App\Models\MataKuliah', 'kodemksyarat', 'kodemk');
    }
}
