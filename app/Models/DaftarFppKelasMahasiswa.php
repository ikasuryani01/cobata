<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DaftarFppKelasMahasiswa extends BaseModel
{
    protected $guarded = ['id'];

    public function fpp()
    {
    	return $this->belongsTo('App\Models\Fpp', 'idfpp', 'id');
    }

    public function mahasiswa()
    {
    	return $this->belongsTo('App\Models\Mahasiswa', 'nrp', 'nrp');
    }

    public function kp()
    {
    	return $this->belongsTo('App\Models\KelasParalel', 'idkelasparalel', 'id');
    }

    public function cekMaksSks()
    {
      $sisa = $this->mahasiswa->cekSisaSks();
      $dataskrg = DaftarFppKelasMahasiswa::where('nrp', $this->nrp)
          ->where('idfpp', $this->idfpp)
          ->where('status', 'Pending')
          ->get();
      $totalsksskrg = 0;
      foreach ($dataskrg as $value) {
          $totalsksskrg += $value->kp->mk->sks;
      }
      if($sisa-$totalsksskrg >= $this->kp->mk->sks)
          return true;
      else
          return false;
    }

    public function cekSksMin() //return true kl lulus
    {
      $mhs = Mahasiswa::where('nrp', $this->nrp)->first();
      $idjur = $mhs->idjurusan;
      $data = JurusanDaftarMatakuliah::where('idjurusan', $idjur)
                  ->where('kodemk', $this->kp->kodemk)
                  ->orderBy('kurikulum', 'desc') //ambil yg terbaru kl ada di bbrp kurikulum
                  ->first(); 
      if(!empty($data)) //nggak ada datanya .-. ada apa" ?
      {
          /*if($this->kp->mk->flagskripsi == "Ya")
          {
              $sksmin = $data->mahasiswa->jurusan->SksMinSkripsi;
          }
          else
          {*/
              $sksmin = $data->sksminimal;
          // }
          if(!is_null($sksmin)) 
          {
              if($sksmin <= $mhs->skskumtanpae)
              {
                  return true; //mncukupi
              }
              else //sksnya nggak cukup
              {
                  return false;
              }
          }
          else
          {
              return true; //sksminnya null, no syarat
          }
      }
      else
      {
          return false;
      }
    }

    public function cekSyaratLain() //return true kl lulus
    {
        $mhs = Mahasiswa::where('nrp', $this->nrp)->first();
        //cek lulus toefl buat b.ing ft
        if($mhs->IdFakultas == 6 && $this->kp->kodemk == "1600A002")
        {
            if($this->mahasiswa->nilaitoefl < 475)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        else
            return true; 
    }

    public function cekAmbilSama()
    {
      $idsemesteraktif = Semester::getAktifId();
      $idfppsemesterini = Fpp::where('idsemester', $idsemesteraktif)->select('id')->get();
      $kodemkskrg = array();
      $dataskrg = DaftarFppKelasMahasiswa::where('nrp', $this->nrp)
                    ->whereIn('idfpp', $idfppsemesterini)
                    ->where('status', '!=', 'Ditolak')
                    ->where('status', '!=', 'Dibatalkan')
                    // ->select('kodemk')
                    ->get();
      foreach ($dataskrg as $daftar) {
        array_push($kodemkskrg, $daftar->kp->mk->kodemk);
      }
      if (in_array($this->kp->mk->kodemk, $kodemkskrg)) {
        return false; //uda ada, dia uda ambil kp lainnya atau uda ambil di fpp sblm ini
      }
      else
        return true;
    }

    public function cekPernahAmbil()
    {
      $transkripmkitu = Transkrip::where('nrpmhs', $this->nrp)->where('kodemk', $this->kp->mk->kodemk)->first();
      if(!empty($transkripmkitu))
          return ($transkripmkitu->mk->cekLulusMk($transkripmkitu->nisbi)); //false kl dia blm lulus (dan brrti blh ambil mk ini lagi)
      else
          return false; //blm prnah ngambil
    }

    public function cekjadwalKuliah()
    {
      $idsemesteraktif = Semester::getAktifId();
      $jadwalkpini = $this->kp->jadwalkuliah;
      
      // dpeting jadwalnya mhsnya skrg
      $idfppsemesterini = Fpp::where('idsemester', $idsemesteraktif)->select('id')->get();
      $idkelasterdaftar = DaftarFppKelasMahasiswa::where('nrp', $this->nrp)
                      ->whereIn('idfpp', $idfppsemesterini)
                      ->select('idkelasparalel')
                      ->get();
      $jadwalmahasiswa = JadwalKuliah::whereIn('idkelasparalel', $idkelasterdaftar)->get();
      
      foreach ($jadwalkpini as $jadwal) {
          foreach ($jadwalmahasiswa as $jadwalada) {
              if($jadwal->hari == $jadwalada->hari)
              {
                  if ($jadwal->jammasuk > $jadwalada->jammasuk && $jadwal->jammasuk < $jadwalada->jamkeluar) 
                  //kalau jammasuknya ada diantara jam masuk dan keluar dr jadwalnya dia
                  {
                     return false;
                  }
              }
          }
      }
      return true;
    }

    public function cekJadwalUjian()
    {
      $idsemesteraktif = Semester::getAktifId();
      $jadwalkpini = $this->kp->mk->jadwalujian;
      
      // dpeting jadwalnya mhsnya skrg
      $idfppsemesterini = Fpp::where('idsemester', $idsemesteraktif)->select('id')->get();
      $idkelasterdaftar = DaftarFppKelasMahasiswa::where('nrp', $this->nrp)
                      ->whereIn('idfpp', $idfppsemesterini)
                      ->select('idkelasparalel')
                      ->get();
      $kodemk = KelasParalel::whereIn('id', $idkelasterdaftar)->select('kodemk')->distinct()->get();
      $jadwalmahasiswa = JadwalUjian::whereIn('kodemk', $kodemk)->where('idsemester', $idsemesteraktif)->get();
      // dd($jadwalmahasiswa);
      // exit();
      
      foreach ($jadwalkpini as $jadwal) {
          foreach ($jadwalmahasiswa as $jadwalada) {
              if($jadwal->mingguke == $jadwalada->mingguke)
              {
                  if($jadwal->hari == $jadwalada->hari)
                  {
                      if ($jadwal->jamke == $jadwalada->jamke) 
                      {
                         return false;
                      }
                  }
              }
          }
      }
      return true;
    }

    public function cekPrasyarat() //ngecek uda lulus prasyaratnya apa blm
    {
      $kodemk = $this->kp->mk->kodemk;
      // echo $kodemk;
      // exit();
      if(PrasyaratMataKuliah::where('kodemk', $kodemk)->exists()) //brrti ada prasyaratnya
      {
        $arrprasyarat = PrasyaratMataKuliah::where('kodemk', $kodemk)->get(); //prasyaratnya bs banyak
        $flaglulus = true;
        foreach ($arrprasyarat as $prasyarat) 
        {
          $flaglulus = $prasyarat->mksyarat->cekLulusMkDenganNrp($this->nrp); //true atau false dimasukin sini
          if(!$flaglulus) //kl false (brrti blm lulus mk syarat)
          {
            //cek mk syarat itu ada konversinya apa nggak
            if(KonversiMataKuliah::where('kodemkawal', $prasyarat->kodemksyarat)->exists())
            {
              $kurikulums1 = KonversiMataKuliah::where('kodemkawal', $prasyarat->kodemksyarat)->select('kurikulum')->distinct()->get();
              // foreach kurikulum, ambil data konversinya, di cek apa ada di transkrip dan uda lulus, return kl ada
              foreach ($kurikulums1 as $k) 
              {
                $aturankonversi1 = KonversiMataKuliah::where('kodemkawal', $prasyarat->kodemksyarat)->where('kurikulum', $k)->get();
                if($aturankonversi1[0]->operand != "Atau") //mungkin "dan" atau "-"
                {
                  $status = $aturankonversi1[0]->mkbaru->cekLulusMkDenganNrpDanNisbi($this->nrp, $prasyarat->nisbi);
                  foreach ($aturankonversi1 as $a) 
                  {
                    $statustmp = $a->mkbaru->cekLulusMkDenganNrpDanNisbi($this->nrp, $prasyarat->nisbi);
                    if($statustmp && $status) //harus sama2 true
                    {
                      $flaglulus = true;
                    } 
                    else
                    {
                      return false; //ada mk konversinya (dari prasyaratnya) yg blm lulus
                    }    
                  }   
                }
                else if($aturankonversi1[0]->operand == "Atau")
                {
                  $status = $aturankonversi1[0]->mkbaru->cekLulusMkDenganNrpDanNisbi($this->nrp, $prasyarat->nisbi);
                  foreach ($aturankonversi1 as $a) 
                  {
                    $statustmp = $a->mkbaru->cekLulusMkDenganNrpDanNisbi($this->nrp, $prasyarat->nisbi);
                    if($statustmp || $status) //salah satunya true masih bisa
                    {
                        $flaglulus = true;
                    }
                    else
                    {
                      $flaglulus = false; //ada mk konversinya (dari prasyaratnya) yg blm lulus, tp mungkin slanjutnya bs jd true jd ditampung dl
                    }  
                  }   
                }
              }
            }
            if(KonversiMataKuliah::where('kodemkbaru', $prasyarat->kodemksyarat)->exists())
            {
              //trus di cek uda diambil blm konversinya dr mksyarat itu
              $kurikulums1 = KonversiMataKuliah::where('kodemkawal', $prasyarat->kodemksyarat)->select('kurikulum')->distinct()->get();
              // foreach kurikulum, ambil data konversinya, di cek apa ada di transkrip dan uda lulus, return kl ada
              foreach ($kurikulums1 as $k) 
              {
                $aturankonversi1 = KonversiMataKuliah::where('kodemkawal', $prasyarat->kodemksyarat)->where('kurikulum', $k)->get();
                if($aturankonversi1[0]->operand != "Atau") //mungkin "dan" atau "-"
                {
                  $status = $aturankonversi1[0]->mkbaru->cekLulusMkDenganNrpDanNisbi($this->nrp, $prasyarat->nisbi);
                  foreach ($aturankonversi1 as $a) 
                  {
                    $statustmp = $a->mkbaru->cekLulusMkDenganNrpDanNisbi($this->nrp, $prasyarat->nisbi);
                    if($statustmp && $status) //harus sama2 true
                    {
                      $flaglulus = true;
                    } 
                    else
                    {
                      return false; //ada mk konversinya (dari prasyaratnya) yg blm lulus
                    }    
                  }   
                }
                else if($aturankonversi1[0]->operand == "Atau")
                {
                  $status = $aturankonversi1[0]->mkbaru->cekLulusMkDenganNrpDanNisbi($this->nrp, $prasyarat->nisbi);
                  foreach ($aturankonversi1 as $a) 
                  {
                    $statustmp = $a->mkbaru->cekLulusMkDenganNrpDanNisbi($this->nrp, $prasyarat->nisbi);
                    if($statustmp || $status) //salah satunya true masih bisa
                    {
                        $flaglulus = true;
                    }
                    else
                    {
                      $flaglulus = false; //ada mk konversinya (dari prasyaratnya) yg blm lulus, tp mungkin slanjutnya bs jd true jd ditampung dl
                    }  
                  }   
                }
              }
            }

            if(!$flaglulus) //kl nilainya masih false stlah cek 2 konversi, brrti mmg blm lulus, return false
            {
              return $flaglulus;
            }
            //nggak ada elsenya, kl nggak masuk di 2 itu ya brrti ga prlu ubah nilainya $flaglulus
          }
        }
      }
      else //MK ini nggak ada prasyaratnya
      {
        return true;
      }
    }

    public function cekBakalHilang()
    {
        // ---------------------------------------------
        /*  MK Lama  | MK Baru | Operand | Kurikulum */
        // ---------------------------------------------
        //     A     |    B    |   Dan   |   2015 
        //     A     |    C    |   Dan   |   2015
        //     D     |    A    |   Atau  |   2015
        //     E     |    A    |   Atau  |   2015
        //     A     |    F    |   Atau  |   2010
        //     A     |    G    |   Atau  |   2010

        // asumsi gak akan ada
        //     A     |    H    |   Dan   |   2015
        //     A     |    I    |   Dan   |   2015
        // kl ada brrti A = B dan C dan H dan I
        // atau
        //     A     |    H    |   Atau   |   2015
        //     A     |    I    |   Atau   |   2015
        // rulenya bakal ikut operand pertama (?)
        

        // di select distinct kurikulumnya dulu
        if(KonversiMataKuliah::where('kodemkawal', $this->kp->mk->kodemk)->exists())
        {
            $kurikulums1 = KonversiMataKuliah::where('kodemkawal', $this->kp->mk->kodemk)->select('kurikulum')->distinct()->get();
            // foreach kurikulum, ambil data konversinya, di cek apa ada di transkrip dan uda lulus, return kl ada
            foreach ($kurikulums1 as $k) {
                $aturankonversi1 = KonversiMataKuliah::where('kodemkawal', $this->kp->mk->kodemk)->where('kurikulum', $k)->get();
                if($aturankonversi1[0]->operand == "Dan")
                {
                    $status = $aturankonversi1[0]->mkbaru->cekLulusMkDenganNrp($this->nrp);
                    foreach ($aturankonversi1 as $a) {
                        $statustmp = $a->mkbaru->cekLulusMkDenganNrp($this->nrp);
                        // if($ak->operand == "Dan")
                        // {
                            if($statustmp && $status) //harus sama2 true
                            {
                                return true; //ya, dia bakal hilang mk yang mau diambil soalnya uda prnah diambil sblmnya dan lulus
                            }
                        // }
                            
                    }   
                }
                else if($aturankonversi1[0]->operand == "Atau")
                {
                    $status = $aturankonversi1[0]->mkbaru->cekLulusMkDenganNrp($this->nrp);
                    foreach ($aturankonversi1 as $a) {
                        $statustmp = $a->mkbaru->cekLulusMkDenganNrp($this->nrp);
                        if($statustmp || $status) //salah satunya true uda ga bisa
                        {
                            return true; //ya, dia bakal hilang mk yang mau diambil soalnya uda prnah diambil sblmnya dan lulus
                        }
                    }   
                }
            }
        }

        if(KonversiMataKuliah::where('kodemkbaru', $this->kp->mk->kodemk)->exists())
        {
            $kurikulums2 = KonversiMataKuliah::where('kodemkbaru', $this->kp->mk->kodemk)->select('kurikulum')->distinct()->get();
            // foreach kurikulum, ambil data konversinya, di cek apa ada di transkrip dan uda lulus, return kl ada
            foreach ($kurikulums2 as $k) {
                $aturankonversi2 = KonversiMataKuliah::where('kodemkbaru', $this->kp->mk->kodemk)->where('kurikulum', $k)->get();
                if($aturankonversi2[0]->operand == "Dan")
                {
                    $status = $aturankonversi2[0]->mkawal->cekLulusMkDenganNrp($this->nrp);
                    foreach ($aturankonversi2 as $a) {
                        $statustmp = $a->mkbaru->cekLulusMkDenganNrp($this->nrp);
                        if($statustmp && $status) //harus sama2 true
                        {
                            return true; //ya, dia bakal hilang mk yang mau diambil soalnya uda prnah diambil sblmnya dan lulus
                        }
                    }   
                }
                else if($aturankonversi2[0]->operand == "Atau")
                {
                    $status = $aturankonversi2[0]->mkawal->cekLulusMkDenganNrp($this->nrp);
                    foreach ($aturankonversi2 as $a) {
                        $statustmp = $a->mkbaru->cekLulusMkDenganNrp($this->nrp);
                        if($statustmp || $status) //salah satunya true uda ga bisa
                        {
                            return true; //ya, dia bakal hilang mk yang mau diambil soalnya uda prnah diambil sblmnya dan lulus
                        }
                    }   
                }
            }
        }
        /* end */
        return false;
    }

    public function cekSemesternya()
    {
        if($this->mahasiswa->getSemester() == $this->kp->mk->getSemester($this->mahasiswa->idjurusan))
            return true;
        else
            return false;
    } 
    
}