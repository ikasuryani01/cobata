<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Fpp extends BaseModel
{
    protected $guarded = ['id'];

    public function semester()
    {
    	return $this->belongsTo('App\Models\Semester', 'idsemester', 'id');
    }

    public static function getTerbaruId() //yang uda selesai
    {
        $idfakultas = 0;
        if (Auth::guard('mahasiswa')->guest())
        {
            $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
        } 
        else if(Auth::guard('karyawan')->guest())
        {
            $idjurusan = Auth::guard('mahasiswa')->user()->idjurusan;
            $idfakultas = Jurusan::where('IdJurusan', $idjurusan)->first()->IdFakultas;
        }

        $data = Fpp::where('idsemester', Semester::getAktifId())
                ->where('idfakultas', $idfakultas)
                ->where('waktuselesai','<=', date("Y-m-d H:i:s")) //cari yg uda slsai
                ->orderBy('waktuselesai', 'desc') //cari yg terbaru
                ->first();

        if(!empty($data))
        {
            return $data->id;
        }
        else
        {
            return 0;
        }
        // return $data;
    }

    public static function getSekarangId()
    {
    	$datenow = date("Y-m-d H:i:s");
        $idfakultas = 0;
        if (Auth::guard('mahasiswa')->guest())
        {
            $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
        } 
        else if(Auth::guard('karyawan')->guest())
        {
            $idjurusan = Auth::guard('mahasiswa')->user()->idjurusan;
            $idfakultas = Jurusan::where('IdJurusan', $idjurusan)->first()->IdFakultas;
        }
    	
    	$fpp = Fpp::where('idsemester', Semester::getAktifId())
    			->where('idfakultas', $idfakultas)
    			->where('waktubuka','<=', $datenow)
				->where('waktuselesai','>=', $datenow)
				->orderBy('jenis') //otomatis asc -> fpp 1, fpp 2, kk (sesuai urutan enum di db)
    			->first();

    	if(!empty($fpp))
    	{
    		return $fpp->id;
    	}
    	else
    	{
    		return 0;
    	}
    }

    public static function getSekarang()
    {
        $datenow = date("Y-m-d H:i:s");
        $idfakultas = 0;
        if (Auth::guard('mahasiswa')->guest())
        {
            $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
        } 
        else if(Auth::guard('karyawan')->guest())
        {
            $idjurusan = Auth::guard('mahasiswa')->user()->idjurusan;
            $idfakultas = Jurusan::where('IdJurusan', $idjurusan)->first()->IdFakultas;
        }
        
        $fpp = Fpp::where('idsemester', Semester::getAktifId())
                ->where('idfakultas', $idfakultas)
                ->where('waktubuka','<=', $datenow)
                ->where('waktuselesai','>=', $datenow)
                ->orderBy('jenis') //otomatis asc -> fpp 1, fpp 2, kk (sesuai urutan enum di db)
                ->first();

        if(!empty($fpp))
        {
            return $fpp;
        }
        else
        {
            return 0;
        }
    }
}
