<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PresensiUjian extends BaseModel
{
    protected $guarded = ['id'];

    public function ruangan()
    {
    	return $this->belongsTo('App\Models\Ruangan', 'idruangan', 'id');
    }

    public function kp()
    {
    	return $this->belongsTo('App\Models\KelasParalel', 'idkelasparalel', 'id');
    }

    public function dosenjaga()
    {
    	return $this->belongsTo('App\Models\Karyawan', 'npkdosenjaga', 'npk');
    }

    public function karyawanjaga()
    {
    	return $this->belongsTo('App\Models\Karyawan', 'npkkaryawanjaga', 'npk');
    }

    public function getDaftarMhs()
    {
        $peserta = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->idkelasparalel)
                    ->where('status', "Diterima")
                    ->select('nrp')
                    ->orderBy('nrp', 'asc')
                    ->get();
        // dd($peserta);
        // exit();
        $daftarmhs = array();
        $flagambil = false;
        foreach ($peserta as $p) {
            if($p->nrp == $this->nrpawal)
            {
                $flagambil = true;
            }
            if($flagambil)
            {
                array_push($daftarmhs, $p->nrp);
            }
            if($p->nrp == $this->nrpakhir)
            {
                $flagambil = false;
            }
        }
        return $daftarmhs;
    }

    public function getRowspanMk()
    {
        // $kodemkini = $this->kp->kodemk;
        $idkps = KelasParalel::where('kodemk', $this->kp->kodemk)->where('statusaktif', 'Ya')->select('id')
                    ->get();
        $jumlah = PresensiUjian::whereIn('idkelasparalel', $idkps)->count();
        return $jumlah;
    }
}
