<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmpuDosenMatakuliahSemester extends BaseModel
{
    protected $guarded = ['id'];
    protected $table = 'dosen_ampu_matakuliahs';

    public function mk()
    {
    	return $this->belongsTo('App\Models\MataKuliah', 'kodemk', 'kodemk');
    }

    public function dosen()
    {
    	return $this->belongsTo('App\Models\Karyawan', 'npkdosen', 'npk');
    }
}
