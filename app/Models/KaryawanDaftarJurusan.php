<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KaryawanDaftarJurusan extends BaseModel
{
    protected $guarded = ['id'];
    public function jurusan()
    {
    	return $this->belongsTo('App\Models\Jurusan', 'idjurusan', 'IdJurusan');
    }

    public function karyawan()
    {
    	return $this->belongsTo('App\Models\Karyawan', 'npkkaryawan', 'npk');
    }
}
