<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends BaseModel
{
    protected $guarded = ['id'];
    public function hitungIsiPerJadwal($idjadwalujian)
    {
    	$ju = JadwalUjian::whereId($idjadwalujian)->first();

    	$mkjadwalujiansama = JadwalUjian::where('mingguke', $ju->mingguke)
                          ->where('hari', $ju->hari)
                          ->where('jamke', $ju->jamke)
                          ->select('kodemk')
                          ->get();

	    $idkps = KelasParalel::whereIn('kodemk', $mkjadwalujiansama)->select('id')->get();

	    $ruangankepake = PresensiUjian::whereIn('idkelasparalel', $idkps)
		    				->where('idruangan', $this->id)
		    				->selectRaw('sum(jumlahpeserta) as isiruangan')
		    				->get();

		if(empty($ruangankepake[0]->isiruangan))
		{
			return 0;
		}
	    return $ruangankepake[0]->isiruangan;
    }

    public function isFullPerJadwal($idjadwalujian)
    {
    	$ju = JadwalUjian::whereId($idjadwalujian)->first();

    	$mkjadwalujiansama = JadwalUjian::where('mingguke', $ju->mingguke)
                          ->where('hari', $ju->hari)
                          ->where('jamke', $ju->jamke)
                          ->select('kodemk')
                          ->get();

	    $idkps = KelasParalel::whereIn('kodemk', $mkjadwalujiansama)->select('id')->get();

	    $ruangankepake = PresensiUjian::whereIn('idkelasparalel', $idkps)
		    				->where('idruangan' , $this->id)
		    				->selectRaw('sum(jumlahpeserta) as isiruangan')
		    				->get();
		$isi = 0;
		if(empty($ruangankepake[0]->isiruangan))
		{
			$isi = 0;
		}
	    else $isi =  $ruangankepake[0]->isiruangan;

	    if(($this->kapasitasujian + $this->cadangankapujian) <= $isi)
	    {
	    	return false;
	    }
	    else return true;
    }

    public function sisaKapPerJadwal($idjadwalujian)
    {
    	$ju = JadwalUjian::whereId($idjadwalujian)->first();

    	$mkjadwalujiansama = JadwalUjian::where('mingguke', $ju->mingguke)
                          ->where('hari', $ju->hari)
                          ->where('jamke', $ju->jamke)
                          ->select('kodemk')
                          ->get();

	    $idkps = KelasParalel::whereIn('kodemk', $mkjadwalujiansama)->select('id')->get();

	    $ruangankepake = PresensiUjian::whereIn('idkelasparalel', $idkps)
		    				->where('idruangan', $this->id)
		    				->selectRaw('sum(jumlahpeserta) as isiruangan')
		    				->get();

		if(empty($ruangankepake[0]->isiruangan))
		{
			$isi = 0;
		}
	    else 
    	{
    		$isi =  $ruangankepake[0]->isiruangan;
    	}
	    
	    $sisa = $this->kapasitasujian + $this->cadangankapujian - $isi;
	    return $sisa;   
    }
}
