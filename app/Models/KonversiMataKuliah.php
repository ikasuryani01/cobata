<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KonversiMataKuliah extends BaseModel
{
    protected $guarded = ['id'];

    public function mkbaru()
    {
    	// $hasil = Models\MataKuliah::where('kodemk', $this->kodemkbaru)->first();
    	// return $hasil;
    	return $this->belongsTo('App\Models\MataKuliah', 'kodemkbaru', 'kodemk');
    }

    public function mkawal()
    {
    	// $hasil = Models\MataKuliah::where('kodemk', $this->kodemkawal)->first();
    	// return $hasil;
    	return $this->belongsTo('App\Models\MataKuliah', 'kodemkawal', 'kodemk');
    }
}
