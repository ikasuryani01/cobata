<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table = 'jurusan';

    public function countMK()
    {
    	$hasil = JurusanDaftarMatakuliah::where('idjurusan', $this->IdJurusan)
    				->select('kodemk')
    				->count();
    	return $hasil;
    }
}
