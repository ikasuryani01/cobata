<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class Karyawan extends Authenticatable
{
    use SoftDeletes;

    protected $primaryKey = 'npk';
    protected $connection = 'mysql';
    protected $guarded = [];
    protected $hidden = array('jumlahjaga');

    // protected $attributes = array(
    //   'jumlahjaga' => 0
    // );

    public function getjumlahjagaAttribute()
    {
        if(empty($this->attributes['jumlahjaga']))
        {
            $this->attributes['jumlahjaga'] = 0;
        }
        return $this->attributes['jumlahjaga'];
    }

    public function setjumlahjagaAttribute($value)
    {
        $this->attributes['jumlahjaga'] = $value;
    }

    public function getnamalengkapAttribute()
    {
        $hasil = $this->gelardepan . $this->nama . $this->gelarbelakang;
        return $hasil;
    }

    public static function getEnum($name){
        $instance = new static; // create an instance of the model to be able to get the table name
        $type = DB::select( DB::raw('SHOW COLUMNS FROM '.$instance->getTable().' WHERE Field = "'.$name.'"') )[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach(explode(',', $matches[1]) as $value){
            $v = trim( $value, "'" );
            $enum[] = $v;
        }
        return $enum;
    }

    public function fakultas()
    {
    	return $this->belongsTo('App\Models\fakultas', 'idfakultas', 'IdFakultas');
    }
    
    public function getIdJurusan()
    {
        $data = KaryawanDaftarJurusan::where('npkkaryawan', $this->npk)
                        ->where('statusaktif', 'Ya')
                        ->where('isutama', 'Ya')
                        ->first();
        return $data->idjurusan;
    }

    public function getJurusan()
    {
        $data = KaryawanDaftarJurusan::where('npkkaryawan', $this->npk)
                        ->where('statusaktif', 'Ya')
                        ->where('isutama', 'Ya')
                        ->first();
        $data = Jurusan::where('IdJurusan', $data->idjurusan)->first();
        return $data;
    }

    public function getJabatan()
    {
        $data = KaryawanPunyaJabatan::where('npk', $this->npk)
                        ->where('isaktif', 'Ya')
                        ->where('isutama', 'Ya')
                        ->get();

        if($data->count() == 0)
        {
            $data = KaryawanPunyaJabatan::where('npk', $this->npk)
                        ->where('isaktif', 'Ya')
                        ->first();
        }
        else
        {
            $data = KaryawanPunyaJabatan::where('npk', $this->npk)
                        ->where('isaktif', 'Ya')
                        ->where('isutama', 'Ya')
                        ->first();
        }

        if(!is_null($data) || !empty($data))
        {
            $jabatan = Jabatan::whereId($data->idjabatan)->first();
        }
        else
        {
            if($this->tipe == "ID0")
            {
                $jabatan = Jabatan::where('namasingkat', 'dosen')->first();
            }
            else
            {
                return false;
            }
        }
        
        return $jabatan;
    }

    public function getJumlahJaga()
    {
        //get semester aktif
        //trus pake uts-uas, jd jumlah jaga itu dihitung per uas uts, bds tgl hr ini (method utsuas)
        //br trus select smua presensi yang between tanggal di tabel semester
        //count group by nrp
        //return hasil count

        $tanggalawal = "";
        $tanggalakhir = "";
        $cek = "";
        $semester = Semester::getAktif();
        if($semester->getUtsUas() == "uas")
        {
            $tanggalawal = $semester->tanggalmulaiuas;
            $tanggalakhir = $semester->tanggalselesaiuas;
        }
        else
        {
            $tanggalawal = $semester->tanggalmulaiuts;
            $tanggalakhir = $semester->tanggalselesaiuts;
        }

        if($this->tipe == "ID0")
        {
            $cek = "npkdosenjaga";
        }
        else
        {
            $cek = "npkkaryawanjaga";
        }

        $jmljaga = PresensiUjian::where('tanggal', '<=', $tanggalakhir)
                    ->where('tanggal', '>=', $tanggalawal)
                    ->where($cek, $this->npk)
                    ->groupBy('idruangan', 'tanggal')
                    ->count();
                    // ->select('idruangan', 'tanggal')->get();
        // print_r($jmljaga);


        return $jmljaga;
    }

    public function isPiket($mingguke, $hari, $utsuas)
    {
        if($utsuas == "uts")
            $tanggal = Semester::getAktif()->getTanggalUts($mingguke, $hari);
        else
            $tanggal = Semester::getAktif()->getTanggalUas($mingguke, $hari);
        if(PiketUjian::where('npk', $this->npk)->where('tanggal', $tanggal)->exists())
            return true;
        else
            return false;
    }

    /*public function getJaga($minggu, $hari)
    {
        $semesteraktif = Semester::getAktif();
        if($semesteraktif->getUtsUas() == "uts")
        {
            //gettanggal
            $tanggal = $semesteraktif->getTanggalUts($minggu, $hari);
            //cek presensi
            $presensi = PresensiUjian::where('tanggal', $tanggal)
                            ->where(function($query) {
                                return $query->where('npkdosenjaga', $this->npk)
                                    ->orWhere('npkkaryawanjaga', $this->npk);
                            })
                            ->exists();
            if($presensi)
            {
                $presensi = PresensiUjian::where('tanggal', $tanggal)
                            ->where(function($query) {
                                return $query->where('npkdosenjaga', $this->npk)
                                    ->orWhere('npkkaryawanjaga', $this->npk);
                            })->get();
                foreach ($presensi as $p) {
                    $ju = $p->mk->jadwalujian;
                    $hasil = "$minggu - $hari - " . $ju->jamke . " - " . $p->ruangan->nama;

                }
                
            }
        }
        else
        {

        }

    }*/

    public function getruanganjaga($minggu, $hari, $jam)
    {
        $semesteraktif = Semester::getAktif();
        if($semesteraktif->getUtsUas() == "uts")
        {
            //gettanggal
            $tanggal = $semesteraktif->getTanggalUts($minggu, $hari);
        }
        else
        {
            $tanggal = $semesteraktif->getTanggalUas($minggu, $hari);
        }
        //cek presensi
        $npk = $this->npk;
        $presensi = PresensiUjian::where('tanggal', $tanggal)
                        ->where(function($query) use ($npk){
                            return $query->where('npkdosenjaga', $npk)
                                ->orWhere('npkkaryawanjaga', $npk);
                        })
                        ->get();
        $hasil = array();
        if(count($presensi) > 0)
        {
            // $hasil .= "b";
            $presensi = PresensiUjian::where('tanggal', $tanggal)
                        ->where(function($query) use ($npk){
                            return $query->where('npkdosenjaga', $npk)
                                ->orWhere('npkkaryawanjaga', $npk);
                        })
                        ->get();
            foreach ($presensi as $p) 
            {
                if(count($p->kp->mk->jadwalujian) > 0)
                {
                    foreach ($p->kp->mk->jadwalujian as $ju) 
                    {
                        if($ju->jamke == $jam)
                        {
                            // $hasil .= $p->ruangan->nama;
                            if(!in_array($p->ruangan->nama, $hasil))
                            {
                                array_push($hasil, $p->ruangan->nama);
                            }
                        }
                    }
                }     
            }
        }
        return $hasil;
    }

    public function getMKjaga($minggu, $hari, $jam)
    {
        $semesteraktif = Semester::getAktif();
        if($semesteraktif->getUtsUas() == "uts")
        {
            //gettanggal
            $tanggal = $semesteraktif->getTanggalUts($minggu, $hari);
        }
        else
        {
            $tanggal = $semesteraktif->getTanggalUas($minggu, $hari);
        }
        //cek presensi
        $npk = $this->npk;
        $presensi = PresensiUjian::where('tanggal', $tanggal)
                        ->where(function($query) use ($npk){
                            return $query->where('npkdosenjaga', $npk)
                                ->orWhere('npkkaryawanjaga', $npk);
                        })
                        ->get();
        $hasil = array();
        if(count($presensi) > 0)
        {
            // $hasil .= "b";
            $presensi = PresensiUjian::where('tanggal', $tanggal)
                        ->where(function($query) use ($npk){
                            return $query->where('npkdosenjaga', $npk)
                                ->orWhere('npkkaryawanjaga', $npk);
                        })
                        ->get();
            foreach ($presensi as $p) 
            {
                $st = $p->kp->mk->nama . " (" . $p->kp->kodekp. ") : " . $p->jumlahpeserta;
                array_push($hasil, $st);
            }
        }
        return $hasil;
    }

    public function getDataJaga()
    {
        $idkp = KelasParalel::where('idsemester', Semester::getAktifId())
                    ->where('statusaktif', 'Ya')
                    ->select('id')
                    ->get();

        $semesteraktif = Semester::getAktif();
        $tipe = $this->tipe;
        $npk = $this->npk;
        $jenisujian = $semesteraktif->getUtsUas();

        $presensis = PresensiUjian::whereIn('idkelasparalel', $idkp)
                        ->where(function($query) use ($tipe, $npk) {
                          if($tipe == "ID0")
                          {
                            return $query->where('npkdosenjaga', '=', $npk);
                          }
                          else
                          {
                            return $query->where('npkkaryawanjaga', '=', $npk);
                          }
                        })
                        ->where(function($query) use ($jenisujian, $semesteraktif) {
                          if($jenisujian == "uas")
                          {
                            return $query->where('tanggal', '>=', $semesteraktif->tanggalmulaiuas)->where('tanggal', '<=', $semesteraktif->tanggalselesaiuas);
                          }
                          else
                          {
                            return $query->where('tanggal', '>=', $semesteraktif->tanggalmulaiuts)->where('tanggal', '<=', $semesteraktif->tanggalselesaiuts);
                          }
                        })
                        ->orderBy('tanggal', 'asc')->get();

        return $presensis;
    }
}
