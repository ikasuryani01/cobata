<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DateTime;

class Semester extends BaseModel
{
    protected $guarded = ['id'];

    public function fpp()
    {
        return $this->hasMany('App\Models\Fpp', 'idsemester', 'id');
    }
    public function AktifkanSemester()
    {
    	/*update smster lain yang aktif jadi nda aktif dulu*/
    	$semesterAktif = Semester::withTrashed()
			    	->where('statusaktif', 'Ya')
			    	->update(['statusaktif' => 'Tidak']);

		try {
			$this->statusaktif = 'Ya';
			$this->save();
			return 1;
		} catch (Exception $e) {
			return 0;
		}
    }

    public function cekDobel($tahunajaran, $semester)
    {
    	$hitung = Semester::where('tahunajaran', $tahunajaran)->where('semester', $semester)->count();
    	if($hitung == 0)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

    public static function getAktif()
    {
        if(!empty(Semester::where('statusaktif', 'Ya')->first()))
        {
            $data = Semester::where('statusaktif', 'Ya')->first();
            return $data;
        }
        else
        {
            return false;
        }
    }

    public static function getAktifId()
    {
        if(!empty(Semester::where('statusaktif', 'Ya')->first()))
        {
            $data = Semester::where('statusaktif', 'Ya')->first();
            return $data->id;
        }
        else
        {
            return false;
        }
    }

    public function getUtsUas()
    {
        $tanggalawal = date("Y-m-d");
    
        if($tanggalawal > $this->tanggalselesaiuts)
        {
            return 'uas';
        }
        else
        {
            return 'uts';
        }
    }

    public function getTanggalUas($minggu, $hari)
    {
        $tanggalawal = $this->tanggalmulaiuas;
        $haripertama = date('l', strtotime($tanggalawal));
        $day = date('w', strtotime($tanggalawal))-1;
        $tanggalseninminggupertama = date('Y-m-d', strtotime($tanggalawal . '-'. $day .' days'));
        // echo $minggu;
        // exit();
        $minggu = $minggu - 1;
        $angkahari = 0;

        switch ($hari) {
            case 'Senin':
                $angkahari = 1;
                break;
            case 'Selasa':
                $angkahari = 2;
                break;
            case 'Rabu':
                $angkahari = 3;
                break;
            case 'Kamis':
                $angkahari = 4;
                break;
            case 'Jumat':
                $angkahari = 5;
                break;
            case 'Sabtu':
                $angkahari = 6;
                break;
            case 'Minggu':
                $angkahari = 7;
                break;
        }

        $angkahari--;

        $tanggal = date('Y-m-d', strtotime($tanggalseninminggupertama . '+'. $minggu .' weeks' . $angkahari .' days'));

        return $tanggal;
    }

    public function getTanggalUts($minggu, $hari)
    {
        $tanggalawal = $this->tanggalmulaiuts;
        $haripertama = date('l', strtotime($tanggalawal));
        $day = date('w', strtotime($tanggalawal))-1;
        $tanggalseninminggupertama = date('Y-m-d', strtotime($tanggalawal . '-'. $day .' days'));
        // echo $minggu;
        // exit();
        $minggu = $minggu - 1;
        $angkahari = 0;

        switch ($hari) {
            case 'Senin':
                $angkahari = 1;
                break;
            case 'Selasa':
                $angkahari = 2;
                break;
            case 'Rabu':
                $angkahari = 3;
                break;
            case 'Kamis':
                $angkahari = 4;
                break;
            case 'Jumat':
                $angkahari = 5;
                break;
            case 'Sabtu':
                $angkahari = 6;
                break;
            case 'Minggu':
                $angkahari = 7;
                break;
        }

        $angkahari--;

        $tanggal = date('Y-m-d', strtotime($tanggalseninminggupertama . '+'. $minggu .' weeks' . $angkahari .' days'));

        return $tanggal;
    }

}
