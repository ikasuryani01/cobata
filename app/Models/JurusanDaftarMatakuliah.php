<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JurusanDaftarMatakuliah extends BaseModel
{
    protected $guarded = [];

    public function mk()
    {
    	return $this->belongsTo('App\Models\Matakuliah', 'kodemk', 'kodemk');
    }

    public function jurusan()
    {
    	return $this->belongsTo('App\Models\Jurusan', 'IdJurusan', 'idjurusan');
    }    
}
