<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class JadwalUjian extends BaseModel
{
    protected $guarded = ['id'];

    public function mk()
    {
    	return $this->belongsTo('App\Models\MataKuliah', 'kodemk', 'kodemk');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Models\Semester', 'idsemester', 'id');
    }

    public function gettipeujianAttribute()
    {
      if(Semester::getAktif()->getutsuas() == "uts")
      {
        return $this->tipeujianuts;
      }
      else
        return $this->tipeujianuas;
    }

    public function gettiperuanganAttribute()
    {
      if(Semester::getAktif()->getutsuas() == "uts")
      {
        return $this->tiperuanganuts;
      }
      else
        return $this->tiperuanganuas;
    }

    public function presensiUjian()
    {
    	if(KelasParalel::where('kodemk', $this->kodemk)->exists())
    	{
    		$idkelasparalel = KelasParalel::where('kodemk', $this->kodemk)
	    						->where('idsemester', Semester::getAktifId())
	    						->select('id')
	    						->get();

	    	$data = PresensiUjian::whereIn('idkelasparalel', $idkelasparalel)
                        ->orderBy('idkelasparalel', 'asc')
	    				->get();
                        
	    	if(!empty($data))
	    	{
	    		return $data;
	    	}
    	}
    }

    public function jumlahUjianPerJam()
    {
      $kodemk = JadwalUjian::where('jamke', $this->jamke)
                ->where('hari', $this->hari)
                ->where('mingguke', $this->mingguke)
                ->where('idsemester', Semester::getAktifId())
                ->select('kodemk')
                ->distinct()
                ->get();
      $idkps = KelasParalel::whereIn('kodemk', $kodemk)->select('id')->get();
      $presensi = PresensiUjian::whereIn('idkelasparalel', $idkps)->count();
      return $presensi;
    }

    public static function getSlotUjian($idsemester)
    {
      $idjurusans = Jurusan::where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan')->distinct()->get();
      $kodemk = JurusanDaftarMatakuliah::whereIn('idjurusan', $idjurusans)->select('kodemk')->distinct()->get();
      $kodemk = MataKuliah::whereIn('kodemk', $kodemk)->where('statusbuka', 'Ya')->select('kodemk')->distinct()->get();

      $slot = JadwalUjian::where('idsemester', $idsemester)->whereIn('kodemk', $kodemk)->select('mingguke', 'hari', 'jamke')->distinct()->orderBy('mingguke', 'asc')->orderBy('hari', 'asc')->orderBy('jamke', 'asc')->get();
      return $slot;
      // print_r($slot);
      // exit();
    }

    public function getIdruangan($nrp)
    {
      return false;
    }

    public function getDosenTersedia()
    {
      $idjurusans = Jurusan::where('IdFakultas', Auth::guard('karyawan')->user()->idfakultas)->select('IdJurusan')->get();

      $kmk = JadwalUjian::where('mingguke', $this->mingguke)
            ->where('hari', $this->hari)
            ->where('jamke', $this->jamke)
            ->select('kodemk')
            ->get();

      //get id kp nya dr kodemk tadi -> kp x.a, x.b, y.a, y.b
      $kps = KelasParalel::whereIn('kodemk', $kmk)->select('id')->get();
      if(Semester::getAktif()->getUtsUas() == "uas")
      {
        $tanggal = Semester::getAktif()->getTanggalUas($this->mingguke, $this->hari);
      }
      else
      {
        $tanggal = Semester::getAktif()->getTanggalUts($this->mingguke, $this->hari);
      }

      $npkdosensudahjaga = PresensiUjian::where('tanggal', $tanggal)
                            ->whereIn('idkelasparalel', $kps)
                            ->where('npkdosenjaga', '!=', null)
                            ->select('npkdosenjaga')
                            ->distinct()
                            ->get();

      $jam = $this->jamke;
      $npkkaryawannonjaga = KaryawanNonjagaUjian::where('tanggal', $this->tanggal)
                              ->where(function($query) use ($jam) {
                                  return $query->where('jamke', $jam)
                                      ->orWhere('jamke', 'Semua');
                              })
                              ->select('npkkaryawan')
                              ->distinct()
                              ->get();

      $hasil = Karyawan::join('karyawan_daftar_jurusans', 'karyawans.npk', '=', 'karyawan_daftar_jurusans.npkkaryawan')
                          ->whereIn('karyawan_daftar_jurusans.idjurusan', $idjurusans)
                          ->where('karyawan_daftar_jurusans.statusaktif', 'Ya')
                          ->where('karyawan_daftar_jurusans.statusjagaujian', 'Ya')
                          ->where('karyawans.tipe', 'ID0')
                          ->whereNotIn('npk', $npkdosensudahjaga)
                          ->whereNotIn('npk', $npkkaryawannonjaga)
                          ->selectRaw('karyawans.*')
                          ->distinct()
                          ->get();

      return $hasil;
    }

    public function getKaryawanTersedia()
    {
      $kmk = JadwalUjian::where('mingguke', $this->mingguke)
            ->where('hari', $this->hari)
            ->where('jamke', $this->jamke)
            ->select('kodemk')
            ->get();

      //get id kp nya dr kodemk tadi -> kp x.a, x.b, y.a, y.b
      $kps = KelasParalel::whereIn('kodemk', $kmk)->select('id')->get();
      
      if(Semester::getAktif()->getUtsUas() == "uas")
      {
        $tanggal = Semester::getAktif()->getTanggalUas($this->mingguke, $this->hari);
      }
      else
      {
        $tanggal = Semester::getAktif()->getTanggalUts($this->mingguke, $this->hari);
      }

      $npkkaryawansudahjaga = PresensiUjian::where('tanggal', $tanggal)
                              ->whereIn('idkelasparalel', $kps)
                              ->where('npkkaryawanjaga', '!=', null)
                              ->select('npkkaryawanjaga')
                              ->distinct()
                              ->get();

      $jam = $this->jamke;
      $npkkaryawannonjaga = KaryawanNonjagaUjian::where('tanggal', $this->tanggal)
                              ->where(function($query) use ($jam) {
                                  return $query->where('jamke', $jam)
                                      ->orWhere('jamke', 'Semua');
                              })
                              ->select('npkkaryawan')
                              ->distinct()
                              ->get();

      $hasil = Karyawan::where('tipe', '!=', 'ID0')
                          ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
                          ->whereNotIn('npk', $npkkaryawansudahjaga)
                          ->whereNotIn('npk', $npkkaryawannonjaga)
                          ->distinct()
                          ->get();

      return $hasil;
    }

}
