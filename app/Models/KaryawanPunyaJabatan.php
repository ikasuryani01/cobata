<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KaryawanPunyaJabatan extends BaseModel
{
	protected $guarded = ['id'];
	
    public function jabatan()
    {
    	return $this->belongsTo('App\Models\Jabatan', 'idjabatan', 'id');
    }

    public function karyawan()
    {
    	return $this->belongsTo('App\Models\Karyawan', 'npk', 'npk');
    }
}
