<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class Mahasiswa extends Authenticatable
{
    use SoftDeletes;

    protected $primaryKey = 'nrp';
    protected $guarded = [];

    public static function getEnum($name){
        $instance = new static; // create an instance of the model to be able to get the table name
        $type = DB::select( DB::raw('SHOW COLUMNS FROM '.$instance->getTable().' WHERE Field = "'.$name.'"') )[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach(explode(',', $matches[1]) as $value){
            $v = trim( $value, "'" );
            $enum[] = $v;
        }
        return $enum;
    }

    public function jurusan()
    {
    	return $this->belongsTo('App\Models\Jurusan', 'idjurusan', 'IdJurusan');
    }

    public function isAsisten()
    {
    	if($this->asisten == "Ya")
    		return true;
    	else
    		return false;
    }

    public function isAngkatanTua()
    {
    	$tahuntua = 3; //diatas 3 tahun uda tua
    	//sbnrnya ada atribut tahun akademik terima sih.. ada masa studi juga
    	if(strlen($this->nrp) == 7) // 6-13-4-087
    	{
    		$tahunmasuk = substr($this->nrp, 1, 2);
    		if($tahunmasuk[0] == '9')
    		{
    			$tahunmasuk = ("19" . $tahunmasuk) + 0;
    		}
    		else
    		{
    			$tahunmasuk = ("20" . $tahunmasuk) + 0;
    		}
    	}
    	else //1604-14-087
    	{
    		$tahunmasuk = substr($this->nrp, 4, 2);
    		$tahunmasuk = ("20" . $tahunmasuk) + 0; 
    	}
    	$tahunsekarang = date("Y") + 0; //ini smacam parse int
    	if($tahunsekarang - $tahunmasuk >= $tahuntua)
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }

    public function isInSNRP($idkelas)
    {
    	$datasnrp = SettingNrp::where('idkelas', $idkelas)->exists();
    	if($datasnrp)
    	{
    		$datasnrp = SettingNrp::where('idkelas', $idkelas)->first();
    		if($this->nrp >= $datasnrp->nrpawal && $this->nrp <= $datasnrp->nrpakhir)
    			return true;
    		else
    			return false;
    	}
    	return false;
    }

    public function getSemester() //ada atribut masa studi sbnernya di tabel mhs (?)
    {
    	$semesteraktif = Semester::getAktif();
    	$tahun = substr($semesteraktif->tahunajaran, 5, 4) + 0; //ex: 2013/2014, diambil tahun yg lbh besarnya (yg blkg)
    	$tahunmasuk = "";
    	if(strlen($this->nrp) == 7) // 6-13-4-087
    	{
    		$tahunmasuk = substr($this->nrp, 1, 2);
    		if($tahunmasuk[0] == '9')
    		{
    			$tahunmasuk = ("19" . $tahunmasuk) + 0;
    		}
    		else
    		{
    			$tahunmasuk = ("20" . $tahunmasuk) + 0;
    		}
    	}
    	else //1604-14-087
    	{
    		$tahunmasuk = substr($this->nrp, 4, 2);
    		$tahunmasuk = ("20" . $tahunmasuk) + 0; 
    	}

    	$semester = 0;
    	if($semesteraktif->semester == "Gasal")
    	{
    		$semester = (($tahun - $tahunmasuk)*2) - 1;
    	}
    	else if($semesteraktif->semester == "Genap")
    	{
    		$semester = (($tahun - $tahunmasuk)*2);
    	}

    	return $semester;

    }

    public function cekSisaSks() //cm cek yang diterima
    {
        $idfpps = Fpp::where('idsemester', Semester::getAktifId())
                ->select('id')
                ->get();

        //jumlah yg diterima aja
        $dataskrg = DaftarFppKelasMahasiswa::where('nrp', $this->nrp)
            ->whereIn('idfpp', $idfpps)
            ->where('status', 'Diterima')
            ->get();

        $totalsksskrg = 0;
        foreach ($dataskrg as $value) {
            $totalsksskrg += $value->kp->mk->sks;
        }

        return $this->sksmaxdepan-$totalsksskrg;
    }

    public function cekSisaSksdanPending()
    {
        $idfpps = Fpp::where('idsemester', Semester::getAktifId())
                ->select('id')
                ->get();

        //jumlah yg diterima aja
        $dataskrg = DaftarFppKelasMahasiswa::where('nrp', $this->nrp)
            ->whereIn('idfpp', $idfpps)
            ->where('status', 'Diterima')
            ->get();
            
        $datapendingskrg = DaftarFppKelasMahasiswa::where('nrp', $this->nrp)
            ->where('idfpp', Fpp::getsekarang()->id )
            ->where('status', 'Pending')
            ->get();

        $totalsksskrg = 0;
        foreach ($dataskrg as $value) {
            $totalsksskrg += $value->kp->mk->sks;
        }

        foreach ($datapendingskrg as $value) {
            $totalsksskrg += $value->kp->mk->sks;
        }

        return $this->sksmaxdepan-$totalsksskrg;
    }

    public function getDaftarKodeMk()
    {
        $idfpps = Fpp::where('idsemester', Semester::getAktifId())
                ->select('id')
                ->get();
        $idkps = DaftarFppKelasMahasiswa::where('nrp', $this->nrp)
                    ->whereIn('idfpp', $idfpps)
                    ->where('status', 'Diterima')
                    ->select('idkelasparalel')->get();
        $hasil = "";

        if($idkps->count() > 0)
        {
            $count = 0;
            foreach ($idkps as $id) {
                // $hasil .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                $count++;
                $kp = KelasParalel::where('id', $id->idkelasparalel)->first();
                $hasil .= $kp->kodemk . $kp->kodekp;
                if($count < $idkps->count())
                    $hasil .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                /*if($count % 8 == 0)
                {
                    $hasil .= " <br> ";
                }*/
            }
            
            $hasil .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            foreach ($idkps as $id) {
                // $hasil .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                $count++;
                $kp = KelasParalel::where('id', $id->idkelasparalel)->first();
                $hasil .= $kp->kodemk . $kp->kodekp;
                if($count < $idkps->count()*2)
                    $hasil .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                /*if($count % 8 == 0)
                {
                    $hasil .= " <br> ";
                }*/
            }
            
            $hasil .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            foreach ($idkps as $id) {
                // $hasil .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                $count++;
                $kp = KelasParalel::where('id', $id->idkelasparalel)->first();
                $hasil .= $kp->kodemk . $kp->kodekp;
                if($count < $idkps->count()*3)
                    $hasil .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                /*if($count % 8 == 0)
                {
                    $hasil .= " <br> ";
                }*/
            }

        }
        

        return $idkps;
    }
}
