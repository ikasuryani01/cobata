<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class KelasParalel extends BaseModel
{
    // protected $guarded = ['id'];
    protected $fillable = array('kodekp', 'kapasitas', 'isi', 'statusaktif', 'kodemk', 'npkdosenpengajar', 'idsemester', 'created_at', 'updated_at', 'deleted_at');
    protected $hidden = array('isisementara');
    // protected $attributes  = array(
    //   'isisementara' => 10
    // );

    public function getisisementaraAttribute()
    {
        if(empty($this->attributes['isisementara']))
        {
            $this->attributes['isisementara'] = 0;
        }
        return $this->attributes['isisementara'];
    }

    public function setisisementaraAttribute($value)
    {
        $this->attributes['isisementara'] = $value;
    }

    public function karyawan()
    {
      return $this->belongsTo('App\Models\Karyawan', 'npkdosenpengajar', 'npk');
    }

    public function mk()
    {
      return $this->belongsTo('App\Models\Matakuliah', 'kodemk', 'kodemk');
    }

    public function jadwalkuliah()
    {
        return $this->hasMany('App\Models\JadwalKuliah', 'idkelasparalel', 'id');
    }

    public function presensiujian()
    {
        return $this->hasMany('App\Models\presensiujian', 'idkelasparalel', 'id');
    }

    public function getStatusPendaftaran($nrp)
    {
      $fpps = Fpp::where('idsemester', Semester::getAktifId())->select('id')->distinct()->get();
      $data = DaftarFppKelasMahasiswa::whereIn('idfpp', $fpps)
                  ->where('idkelasparalel', $this->id)
                  ->where('nrp', $nrp)
                  ->first();
      if(!empty($data))
      {
        if($data->status != 'Pending')
        {
          return $data->status;
        }
        else
        {
          return "-";
        }
      }
      else
        return "-";
    }
    
    public function cekAda($nrp)
    {
      //return code:
      /*
        0 = blm ada data dan harusnya muncul tombol daftar
        1 = dia uda daftar kp itu -> muncul hapus
        2 = dia uda daftar mk itu -> nda muncul apa2 utk kp lain mk itu 
        3 = KK dan uda daftar (atau kp lain dr mk itu uda daftar) -> nda muncul apa2
        4 = KK dan blm daftar atau ditolak -> muncul daftar
      */

      $fppskrg = Fpp::getSekarang();
      $fpps = Fpp::where('idsemester', Semester::getAktifId())->select('id')->distinct()->get();
      if($fppskrg->urutanterima == "Batch")
      {
        //cek per mk dulu
        $idkps = KelasParalel::where('kodemk', $this->kodemk)->select('id')->get();
        $jumlah = DaftarFppKelasMahasiswa::where('nrp', $nrp)
                    ->whereIn('idkelasparalel', $idkps)
                    ->whereIn('idfpp', $fpps)
                    ->count();
        if($jumlah > 0)
        {
          //cek apa kp ini yg didaftar dan di fpp ini
          $jumlah = DaftarFppKelasMahasiswa::where('nrp', $nrp)
                      ->where('idkelasparalel', $this->id)
                      ->where('idfpp', $fppskrg->id)
                      ->count();
          if($jumlah > 0)
            return 1;
          else
            return 2; //liat return code 2
        }
        else
          return 0;
      }
      else //kl dia langsung
      {
        //cek per mk dulu, nda prlu cek per kp krn nda bs batalin jg
        $idkps = KelasParalel::where('kodemk', $this->kodemk)->select('id')->get();
        $jumlah = DaftarFppKelasMahasiswa::where('nrp', $nrp)
                    ->whereIn('idkelasparalel', $idkps)
                    ->whereIn('idfpp', $fpps)
                    ->count();
        if($jumlah > 0)
          return 3;
        else
          return 4;
      }
    }

    public function hitungIsi()
    {
        $jumlah = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)->count();
        return $jumlah;
    }

    public function hitungIsiKeterima()
    {
        $jumlah = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)->where('status', "Diterima")->count();
        return $jumlah;
    }

    public function getJumlahKursiKosong()
    {
      $jumlahketerima = $this->hitungIsiKeterima();
      $hasil = $this->kapasitas - $jumlahketerima;
      return $hasil;
    }

    public function isFull()
    {
        $jumlah = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                    ->where('status', "Diterima")
                    ->count();
                    
        if($jumlah >= $this->kapasitas)
            return true;
        else
            return false;
    }

    public function pesertaKuliah()
    {
        $peserta = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                    ->where('status', "Diterima")
                    ->select('nrp')
                    ->orderBy('nrp', 'asc')
                    ->get();
        if($peserta->count() > 0)
        {
            $mahasiswa = Mahasiswa::whereIn('nrp', $peserta)->orderBy('nrp', 'asc')->get();
            if(!empty($mahasiswa))
            {
                return $mahasiswa;
            }
        } 
    }

    public function adaPesertaKuliah()
    {
        $peserta = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                    ->where('status', "Diterima")
                    ->select('nrp')
                    ->orderBy('nrp', 'asc')
                    ->get();
        if($peserta->count() > 0)
        {
          $mahasiswa = Mahasiswa::whereIn('nrp', $peserta)->orderBy('nrp', 'asc')->get();
          if(!empty($mahasiswa))
          {
              return true;
          }
        }
        else
            return false;
    }

    public function hitungPesertaKuliah()
    {
      $jmlpeserta = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                  ->where('status', "Diterima")
                  // ->orderBy('nrp', 'asc')
                  // ->get()
                  ->count();
      return $jmlpeserta;            
    }

    public function getIdJadwalSama()
    {
      $data = KelasParalel::where('kodemk', $this->kodemk)
              ->where('idsemester', $this->idsemester)
              ->where('statusaktif', 'Ya')
              ->where('kodekp', '!=', $this->kodekp)
              ->get();
      // dd($data);
      $hasil = array();
      $jadwalkpini = JadwalKuliah::where('idkelasparalel', $this->id)
          ->select('hari', 'jammasuk', 'jamkeluar')->get();
      foreach ($data as $kp) {
        $jadwalkpambil = JadwalKuliah::where('idkelasparalel', $kp->id)
            ->select('hari', 'jammasuk', 'jamkeluar')->get();
        if(empty($jadwalkpini) || empty($jadwalkpambil))
        {
          array_push($hasil, $kp->id);
        }
        else if($jadwalkpini == $jadwalkpambil)
        {
          array_push($hasil, $kp->id);
        }
      }
      return $hasil;
    }

    public function cekStatusPublikasi()
    {
      $data = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)->where('statuspublikasi', 'Tidak')->count();
      if($data > 0)
      {
        //masih ada yg status publikasinya tidak
        return false;
      }
      else
      {
        return true;
      }
    }

    public function getStatusHasilFpp()
    {
      $idfpps = Fpp::where('idsemester', Semester::getAktifId())
            ->where('idfakultas', Auth::guard('karyawan')->user()->idfakultas)
            ->select('id')->get();
      $jmlseluruh = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                        ->whereIn('idfpp', $idfpps)
                        ->count();
      $jmlpending = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                        ->whereIn('idfpp', $idfpps)
                        ->where('status', 'Pending')
                        ->count();
      if($jmlpending == 0)
      {
        return "Selesai diproses";
      }
      else if($jmlpending > 0 && $jmlpending <= $jmlseluruh)
      {
        return "Belum diproses";
      }
    }

    public function getPesertaAsisten()
    {
      $hasil = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                ->where('status', 'Diterima')
                ->where('alasan', 'LIKE', '%Asisten%')
                ->select('nrp')->get();
      return $hasil;
    }

    public function getPesertaSemesternya()
    {
      $hasil = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                ->where('status', 'Diterima')
                ->where('alasan', 'LIKE', '%Semesternya%')
                ->select('nrp')->get();
      return $hasil;
    }

    public function getPesertaAngkatanTua()
    {
      $hasil = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                ->where('status', 'Diterima')
                ->where('alasan', 'LIKE', '%Tua%')
                ->select('nrp')->get();
      return $hasil;
    }

    public function getPesertaSettingNrp()
    {
      $hasil = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                ->where('status', 'Diterima')
                ->where('alasan', 'LIKE', '%NRP%')
                ->select('nrp')->get();
      return $hasil;
    }

    public function getPesertaLainnya()
    {
      $hasil = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                ->where('status', 'Diterima')
                ->where('alasan', 'NOT LIKE', '%Asisten%')
                ->where('alasan', 'NOT LIKE', '%Semesternya%')
                ->where('alasan', 'NOT LIKE', '%Tua%')
                ->where('alasan', 'NOT LIKE', '%NRP%')
                ->select('nrp')->get();
      return $hasil;
    }

    public function getPesertaperFPP($idfpp)
    {
      $hasil = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                ->where('status', 'Diterima')
                ->where('idfpp', $idfpp)
                ->select('nrp')
                ->get();
      return $hasil;
    }

    public function prosesPerwalian($idfpp)
    {
        /* [v] dari auth user nnti harus bisa dpt idjurusan dan fakultas. ini dummy dulu*/
        $idfakultas = Auth::guard('karyawan')->user()->idfakultas;
        // $idfakultas = 6;
        $data = 0;
        $prioritas = PrioritasTerima::where('idfakultas', $idfakultas)
                      ->where('statusaktif', 'Ya')
                      ->orderBy('urutan', 'asc')
                      ->select('kodekomponen')->get();
        $biasa = new PrioritasTerima(array(
                    'kodekomponen' => 'Biasa',
                  ));
        $prioritas->push($biasa);
        // dd($prioritas);
        // exit();
        // $urutanterima = SettingFakultas::where('namasetting', 'Urutan Terima')->where('idfakultas', $idfakultas)->get();
        $urutanterima = Fpp::whereId($idfpp)->first()->urutanterima;
        if($urutanterima == "Batch") //random
        {
            $data = DaftarFppKelasMahasiswa::where('idkelasparalel', $this->id)
                        ->where('idfpp', $idfpp)
                        ->where('status', 'Pending')
                        ->with('mahasiswa')
                        ->get();
            // shuffle($data);
            $data = $data->shuffle();
            // print("<pre>");
            // print_r($data);
            // print("</pre>");
        }

        if($this->mk->flagskripsi == "Ya")
        {
          foreach ($data as $datafpp) {
            $datafpp->status = "Diterima";
            $datafpp->alasan = "Otomatis terima flag skripsi";
            $datafpp->save();
            $data->forget($datafpp->id);
            echo $data->count() . "\n";
          }
          if(!$data->isEmpty())
          {
            echo "hayo masih nggak empty loh <br><br>";
          }
          else
          {
            echo "yeay gini dong <br><br>";
          }
        }
        else
        {
          $data = $data->keyBy('id'); //spy bisa di forget
          $idkpjadwalsamapersis = KelasParalel::whereId($this->id)->first()->getIdJadwalSama();

          //terima2 bds prioritasnya dulu
          foreach ($prioritas as $p) 
          {
            echo "<br>Masuk foreach prioritas $p<br>";
            if(!$data->isEmpty())
            {
              echo "<br>data nggak empty, ini jumlahnya: <br>";
              // print_r($data);
              echo $data->count();
              echo "<br><br>";
              foreach ($data as $datafpp) 
              {
                echo "<br>Masuk foreach data $datafpp. Jadwal kp sama persis: <br>";
                $idkpjadwalsamapersis = $datafpp->kp->getIdJadwalSama();
                print_r($idkpjadwalsamapersis);
                if($datafpp->status == "Pending") //data ini masih prlu diproses
                {
                  echo "<br>Masuk yang statusnya pending nama: $datafpp->mhs->nama<br>";
                  if(!$this->isFull()) //kl kelasnya blm full
                  {
                    echo "<br>kelasnya nggak full</br>";
                    if($p->kodekomponen == "AS")
                    {
                      if($datafpp->mahasiswa->isAsisten())
                      {
                        $datafpp->status = "Diterima";
                        $datafpp->alasan = "Status Asisten";
                        $datafpp->save();
                        $data->forget($datafpp->id);
                        echo $data->count() . "\n";
                      }
                    }
                    //cek pada smsternya
                    else if($p->kodekomponen == "MS")
                    {
                      if($datafpp->cekSemesternya())
                      {
                        $datafpp->status = "Diterima";
                        $datafpp->alasan = "Pada Semesternya";
                        $datafpp->save();
                        $data->forget($datafpp->id);
                        echo $data->count() . "\n";
                      }
                    }
                    // cek mhs angkatan tua
                    else if($p->kodekomponen == "AT")
                    {
                      if($datafpp->mahasiswa->isAngkatanTua())
                      {
                        $datafpp->status = "Diterima";
                        $datafpp->alasan = "Status Angkatan Tua";
                        $datafpp->save();
                        $data->forget($datafpp->id);
                        echo $data->count() . "\n";
                      }
                    }
                    //cek setting nrp
                    else if(SettingNrp::where('idkelasparalel', $this->id)->exists())
                    {
                      if($p->kodekomponen == "SNRP")
                      {
                        if($datafpp->mahasiswa->isInSNRP($this->id))
                        {
                          $datafpp->status = "Diterima";
                          $datafpp->alasan = "Status Setting NRP";
                          $datafpp->save();
                          $data->forget($datafpp->id);
                          echo $data->count() . "\n";
                        }
                      }
                    }
                    else
                    {
                      echo "<br>Masuk else status biasa Total jadi: </br>";
                      $datafpp->status = "Diterima";
                      $datafpp->alasan = "Sesuai Kapasitas";
                      $datafpp->save();
                      $data->forget($datafpp->id);
                      echo $data->count() . "\n";
                    }
                  }
                  else if(!empty($idkpjadwalsamapersis)) //kl ada kelas lain yang jadwalnya sama
                  {
                    echo "<br>kelasnya full, masuk ke yg sm persis</br>";
                    $kptujuan = KelasParalel::whereIn('id', $idkpjadwalsamapersis)->select('id', 'kapasitas')->get();
                    foreach ($kptujuan as $key => $d) 
                    {
                      echo "<br>masuk foreach kp $d->id; jumlah psrta kuliah: ". $d->hitungPesertaKuliah() . ", ". $d->kapasitas."</br>";
                      if($d->hitungPesertaKuliah() < $d->kapasitas) //kl kelas itu blm full
                      {
                        echo "<br>kelasnya nggak full</br>";
                        if($p->kodekomponen == "AS")
                        {
                          if($datafpp->mahasiswa->isAsisten())
                          {
                            $datafpp->status = "Diterima";
                            $datafpp->alasan = "Status Asisten - Transfer dari KP: " . $datafpp->kp->kodekp;
                            $datafpp->idkelasparalel = $d->id;
                            $datafpp->save();
                            $data->forget($datafpp->id);
                            echo $data->count() . "\n";
                            break;
                          }
                        }
                        //cek pada smsternya
                        else if($p->kodekomponen == "MS")
                        {
                          if($datafpp->cekSemesternya())
                          {
                            $datafpp->status = "Diterima";
                            $datafpp->alasan = "Pada Semesternya - Transfer dari KP: " . $datafpp->kp->kodekp;
                            $datafpp->idkelasparalel = $d->id;
                            $datafpp->save();
                            $data->forget($datafpp->id);
                            echo $data->count() . "\n";
                            break;
                          }
                        }
                        // cek mhs angkatan tua
                        else if($p->kodekomponen == "AT")
                        {
                          if($datafpp->mahasiswa->isAngkatanTua())
                          {
                            $datafpp->status = "Diterima";
                            $datafpp->alasan = "Status Angkatan Tua - Transfer dari KP: " . $datafpp->kp->kodekp;
                            $datafpp->idkelasparalel = $d->id;
                            $datafpp->save();
                            $data->forget($datafpp->id);
                            echo $data->count() . "\n";
                            break;
                          }
                        }
                        //cek setting nrp
                        else if(SettingNrp::where('idkelasparalel', $this->id)->exists())
                        {
                          if($p->kodekomponen == "SNRP")
                          {
                            if($datafpp->mahasiswa->isInSNRP($this->id))
                            {
                              $datafpp->status = "Diterima";
                              $datafpp->alasan = "Status Setting NRP - Transfer dari KP: " . $datafpp->kp->kodekp;
                              $datafpp->idkelasparalel = $d->id;
                              $datafpp->save();
                              $data->forget($datafpp->id);
                              echo $data->count() . "\n";
                              break;
                            }
                          }
                        }
                        else
                        {
                          echo "<br>Masuk else status biasa Total jadi: </br>";
                          $datafpp->status = "Diterima";
                          $datafpp->alasan = "Sesuai Kapasitas - Transfer dari KP: " . $datafpp->kp->kodekp;
                          $datafpp->idkelasparalel = $d->id;
                          $datafpp->save();
                          $data->forget($datafpp->id);
                          echo $data->count() . "\n";
                          break;
                        }
                      }
                      else
                      {
                        echo "<br>kelasnya full</br>";
                        unset($kptujuan[$key]); //cara forgetnya
                        echo "<br>isi array skrg:";
                        print_r($kptujuan);
                        echo "<br><br>";
                        if($kptujuan->isEmpty())
                        {
                          echo "<br>Masuk else ditolak krn kelas persis empty jadi: </br>";
                          $datafpp->status = "Ditolak";
                          $datafpp->alasan = "Kelas Penuh";
                          $datafpp->save();
                          $data->forget($datafpp->id);
                          echo $data->count() . "\n";
                        }
                        echo "<br> Masih ada: " . $data->count() . "\n";
                      }
                    }
                  }
                  else
                  {
                    echo "<br>Masuk else ditolak Total jadi: </br>";
                    $datafpp->status = "Ditolak";
                    $datafpp->alasan = "Kelas Penuh";
                    $datafpp->save();
                    $data->forget($datafpp->id);
                    echo $data->count() . "\n";
                  }
                }
              }
            }
          }

          echo "<br>selesai foreach prioritas<br>";
          
          // $idkpjadwalsamapersis = $datafpp->kp->getIdJadwalSama();
          //yg prioritas uda slsai tapi data blm smua diproses

          if(!$data->isEmpty())
          {
            echo "hayo masih nggak empty loh <br><br>";
          }
          else
          {
            echo "yeay gini dong <br><br>";
          }
        }  
    }

}
