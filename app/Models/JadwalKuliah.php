<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalKuliah extends BaseModel
{
    protected $guarded = ['id'];

    public function ruangan()
    {
    	return $this->belongsTo('App\Models\Ruangan', 'idruangan', 'id');
    }

    public function kelas()
    {
    	return $this->belongsTo('App\Models\KelasParalel', 'idkelasparalel', 'id');
    }
}
