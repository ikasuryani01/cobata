<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KaryawanNonjagaUjian extends BaseModel
{
    protected $guarded = ['id'];

    public function karyawan()
    {
    	return $this->belongsTo('App\Models\Karyawan', 'npkkaryawan', 'npk');
    }
}
