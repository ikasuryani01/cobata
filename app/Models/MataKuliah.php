<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MataKuliah extends BaseModel
{
    protected $primaryKey = 'kodemk';
    public $incrementing = false;
    protected $guarded = [];

    public function aktifkan()
    {
        try 
        {
            $this->statusbuka = 'Ya';
            $this->save();
            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }

    public function hasjurusan()
    {
        return $this->hasMany('App\Models\JurusanDaftarMatakuliah', 'kodemk', 'kodemk');
    }

    public function getJurusan()
    {
        $idjurusan = JurusanDaftarMatakuliah::where('kodemk', $this->kodemk)->select('idjurusan')->distinct()->get();
        $jurusan = Jurusan::whereIn('IdJurusan', $idjurusan)->get();
        return $jurusan;
    }

    public function adaJurusan($idjur)
    {
        $jml = JurusanDaftarMatakuliah::where('kodemk', $this->kodemk)->where('idjurusan', $idjur)->count();
        if($jml > 0)
            return true;
        else
            return false;
    }

    public function kp()
    {
        $idsemesteraktif = Semester::getAktifId();
    	return $this->hasMany('App\Models\KelasParalel', 'kodemk', 'kodemk')->where('idsemester', $idsemesteraktif);
    }

    public function getPengajar()
    {
        $arrdosen = array();
        foreach ($this->kp as $kp) {
            $dosen = Karyawan::where('npk', $kp->npkdosenpengajar)->first();
            if(!in_array($dosen, $arrdosen))
            {
                array_push($arrdosen, $dosen);
            }
        }
        return $arrdosen;
    }

    public function getRuanganUjian()
    {
        $arrruangan = array();
        $pres = array();
        $idkps = $this->kp;
        $idkps = $idkps->pluck('id')->all();

        if(!empty($idkps))
        {
            $pres = PresensiUjian::whereIn('idkelasparalel', $idkps)
                                ->groupBy('idruangan')
                                ->selectRaw('sum(jumlahpeserta) as isi, idruangan')
                                ->get();
        }
        return $pres;
    }

    public function jadwalujian()
    {
        $idsemesteraktif = Semester::getAktifId();
        return $this->hasMany('App\Models\JadwalUjian', 'kodemk', 'kodemk')->where('idsemester', $idsemesteraktif);
    }

    public function getTotalPeserta()
    {
        $jumlah = 0;
        foreach ($this->kp as $kelas) {
            //asumsi 1 id kelas cm bakal di 1 semester jd nda prlu dicek smster dan fppnya
            $jumlah += DaftarFppKelasMahasiswa::where('idkelas', $kelas->id)->where('status', 'Diterima')->count('nrp');
        }
        return $jumlah;
    }

    public function cekLulusMk($nisbi)
    {
        // $nisbi ini dr transkrip, utk kodemk ini
        // method ini bakal return true kl dia uda lulus mk ini
        if(PrasyaratMataKuliah::where('kodemksyarat', $this->kodemk)->exists()) //kl mk ini jd prasyarat mk lain
        {
            $getnisbiprasyarat = PrasyaratMataKuliah::where('kodemksyarat', $this->kodemk)->first()->nisbi;

            $nilainisbiprasyarat = 0;
            switch ($getnisbiprasyarat) {
                case 'A':
                    $nilainisbiprasyarat = 6;
                    break;
                case 'AB':
                    $nilainisbiprasyarat = 5;
                    break;
                case 'B':
                    $nilainisbiprasyarat = 4;
                    break;
                case 'BC':
                    $nilainisbiprasyarat = 3;
                    break;
                case 'C':
                    $nilainisbiprasyarat = 2;
                    break;
                case 'D':
                    $nilainisbiprasyarat = 1;
                    break;
                case 'E':
                    $nilainisbiprasyarat = 0;
                    break;
                
                default:
                    $nilainisbiprasyarat = 0;
                    break;
            }

            $nilainisbimhs = 0;
            switch ($nisbi) {
                case 'A':
                    $nilainisbimhs = 6;
                    break;
                case 'AB':
                    $nilainisbimhs = 5;
                    break;
                case 'B':
                    $nilainisbimhs = 4;
                    break;
                case 'BC':
                    $nilainisbimhs = 3;
                    break;
                case 'C':
                    $nilainisbimhs = 2;
                    break;
                case 'D':
                    $nilainisbimhs = 1;
                    break;
                case 'E':
                    $nilainisbimhs = 0;
                    break;
                
                default:
                    $nilainisbimhs = 0;
                    break;
            }

            if($nisbi != "E")
            {
                if($nilainisbimhs >= $nilainisbiprasyarat)
                    return true;
                else
                    return false;
            }
            else //nisbinya E, nda lulus
            {
                return false;
            }
        }
        else if($nisbi != "E") //kl nda jd prasyarat, asal bukan E uda lulus (utk mk yg minimal C atau D harus dimasukin prasyarat)
        {
            return true;
        }
        else //dia nisbinya E
        {
            return false;
        }
    }

    public function cekLulusMkDenganNrpDanNisbi($nrp, $nisbi)
    {
        $transkripmkitu = Transkrip::where('nrpmhs', $nrp)->where('kodemk', $this->kodemk)->first();
        if(!empty($transkripmkitu))
        {
            $nisbimhs = $transkripmkitu->nisbi;

            $nilainisbicek = 0;
            switch ($nisbi) {
                case 'A':
                    $nilainisbicek = 6;
                    break;
                case 'AB':
                    $nilainisbicek = 5;
                    break;
                case 'B':
                    $nilainisbicek = 4;
                    break;
                case 'BC':
                    $nilainisbicek = 3;
                    break;
                case 'C':
                    $nilainisbicek = 2;
                    break;
                case 'D':
                    $nilainisbicek = 1;
                    break;
                case 'E':
                    $nilainisbicek = 0;
                    break;
                
                default:
                    $nilainisbicek = 0;
                    break;
            }

            $nilainisbimhs = 0;
            switch ($nisbimhs) {
                case 'A':
                    $nilainisbimhs = 6;
                    break;
                case 'AB':
                    $nilainisbimhs = 5;
                    break;
                case 'B':
                    $nilainisbimhs = 4;
                    break;
                case 'BC':
                    $nilainisbimhs = 3;
                    break;
                case 'C':
                    $nilainisbimhs = 2;
                    break;
                case 'D':
                    $nilainisbimhs = 1;
                    break;
                case 'E':
                    $nilainisbimhs = 0;
                    break;
                
                default:
                    $nilainisbimhs = 0;
                    break;
            }

            if($nilainisbimhs >= $nilainisbicek)
                return true;
            else
                return false;
        }
        else
        {
            return false; //dia blm ambil mk itu, blm lulus
        }
        
    }

    public function cekLulusMkDenganNrp($nrp)
    {
        $transkripmkitu = Transkrip::where('nrpmhs', $nrp)->where('kodemk', $this->kodemk)->first();
        if(!empty($transkripmkitu))
        {
            return $this->cekLulusMk($transkripmkitu->nisbi);
        }
        return false;
    }

    public function getSemester($idjurusan)
    {
        $hasil = JurusanDaftarMatakuliah::where('kodemk', $this->kodemk)
                    ->where('idjurusan', $idjurusan)
                    ->exists();
        if($hasil)
        {
            $hasil = JurusanDaftarMatakuliah::where('kodemk', $this->kodemk)
                    ->where('idjurusan', $idjurusan)
                    ->first()->semester;
        }
        return $hasil;
    }

    public function getKurikulum($idjurusan)
    {
        $hasil = JurusanDaftarMatakuliah::where('kodemk', $this->kodemk)
                    ->where('idjurusan', $idjurusan)
                    ->exists();
        if($hasil)
        {
            $hasil = JurusanDaftarMatakuliah::where('kodemk', $this->kodemk)
                    ->where('idjurusan', $idjurusan)
                    ->first()->kurikulum;
        }
        return $hasil;
    }

    public function getSKSminimal($idjurusan)
    {
        $hasil = JurusanDaftarMatakuliah::where('kodemk', $this->kodemk)
                    ->where('idjurusan', $idjurusan)
                    ->exists();
        if($hasil)
        {
            $hasil = JurusanDaftarMatakuliah::where('kodemk', $this->kodemk)
                    ->where('idjurusan', $idjurusan)
                    ->first()->sksminimal;
        }
        return $hasil;
    }

}
