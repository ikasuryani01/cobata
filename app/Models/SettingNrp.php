<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingNrp extends BaseModel
{
    protected $guarded = ['id'];

    public function kelas()
    {
    	return $this->belongsTo('App\Models\KelasParalel', 'idkelasparalel', 'id');
    }
}
