<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transkrip extends BaseModel
{
    protected $guarded = ['id'];

    public function mk()
    {
    	return $this->belongsTo('App\Models\MataKuliah', 'kodemk', 'kodemk');
    }

    public function mhs()
    {
    	return $this->belongsTo('App\Models\Mahasiswa', 'nrpmhs', 'nrp');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Models\Semester', 'idsemester', 'id');
    }
}
