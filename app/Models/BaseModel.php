<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class BaseModel extends Model
{
    use SoftDeletes;

    public static function getEnum($name){
	    $instance = new static; // create an instance of the model to be able to get the table name
	    $type = DB::select( DB::raw('SHOW COLUMNS FROM '.$instance->getTable().' WHERE Field = "'.$name.'"') )[0]->Type;
	    preg_match('/^enum\((.*)\)$/', $type, $matches);
	    $enum = array();
	    foreach(explode(',', $matches[1]) as $value){
	        $v = trim( $value, "'" );
	        $enum[] = $v;
	    }
	    return $enum;
	}
}
