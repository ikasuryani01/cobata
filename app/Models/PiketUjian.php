<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PiketUjian extends BaseModel
{
    protected $guarded = ['id'];

    public function karyawan()
    {
    	return $this->belongsTo('App\Models\Karyawan', 'npk', 'npk');
    }
    
}
