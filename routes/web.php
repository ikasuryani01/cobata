<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['uses' => 'GuestController@Redirects', 'as' => 'redirect']);
Route::get('/menujadwalkuliah', ['uses' => 'GuestController@JadkulIndex', 'as' => 'jadkul.menu']);
Route::get('/jk-{idjur}', ['uses' => 'GuestController@JadkulJurusan', 'as' => 'jadkul.pilih']);
Route::get('/menujadwalujian', ['uses' => 'GuestController@JadwalujianIndex', 'as' => 'jujian.menu']);
Route::get('/ju-{idjur}', ['uses' => 'GuestController@JadwalujianJurusan', 'as' => 'jujian.pilih']); 

Auth::routes();

Route::get('/home', 'GuestController@viewBeranda')->name('guest.beranda');

Route::get('/customredirect', ['uses' => 'GuestController@Redirects', 'as' => 'redirect']);

Route::group(['middleware' => 'mhs'], function() {
    Route::group(['prefix' => 'mhs'],function(){
        Route::get('/', ['uses' => 'MhsController@showBeranda', 'as' => 'mhs.beranda']);
        Route::get('/profil', ['uses' => 'MhsController@showProfil', 'as' => 'mhs.profil']);
        Route::get('/jkuliah', ['uses' => 'MhsController@showJadwalKuliah', 'as' => 'mhs.jkuliah']);
        Route::get('/jujian', ['uses' => 'MhsController@showJadwalUjian', 'as' => 'mhs.jujian']);
        Route::get('/daftarmk', ['uses' => 'MhsController@showDaftarMataKuliah', 'as' => 'mhs.daftarmk']);
        Route::group(['prefix' => 'daftarmk'], function(){
            Route::get('/t', ['uses' => 'MhsController@showTableDaftarMk', 'as' => 'mhs.daftarmk.tabel']);
            Route::get('/add', ['uses' => 'MhsController@addDaftarMk', 'as' => 'mhs.daftarmk.add']);
            Route::get('/delete', ['uses' => 'MhsController@deleteDaftarMk', 'as' => 'mhs.daftarmk.del']);
            Route::get('/detail-{id}', ['uses' => 'MhsController@lihatDetailMk', 'as' => 'mhs.daftarmk.detail']);
        });
        Route::get('/informasimk', ['uses' => 'MhsController@showInformasiMataKuliah', 'as' => 'mhs.informasimk']);
        Route::get('/transkrip', ['uses' => 'MhsController@showTranskrip', 'as' => 'mhs.transkrip']);
        Route::get('/riwayat', ['uses' => 'MhsController@showRiwayatPerwalian', 'as' => 'mhs.riwayat']);
    });
});

Route::group(['middleware' => 'paj'], function() {
  Route::group(['prefix' => 'paj'], function(){

    Route::get('/', ['uses' => 'PajController@showBeranda', 'as' => 'paj.beranda']);

    Route::group(['prefix' => 'mastermhs'], function(){
        Route::get('/', ['uses' => 'MahasiswaController@showDataMahasiswa', 'as' => 'paj.mastermhs']);
        Route::get('/s', ['uses' => 'MahasiswaController@showDataMahasiswas', 'as' => 'paj.mastermhss']);
        Route::get('/add', ['uses' => 'MahasiswaController@addDataMahasiswa', 'as' => 'paj.mastermhs.add']);
        Route::post('/add', ['uses' => 'MahasiswaController@storeDataMahasiswa', 'as' => 'paj.mastermhs.store']);
        Route::get('/addext', ['uses' => 'MahasiswaController@addDataMahasiswaExternal', 'as' => 'paj.mastermhs.addext']);
        Route::get('/storeext', ['uses' => 'MahasiswaController@storeDataMahasiswaExternal', 'as' => 'paj.mastermhs.storeext']);
        Route::get('/edit-{nrp}', ['uses' => 'MahasiswaController@editDataMahasiswa', 'as' => 'paj.mastermhs.edit']);
        Route::post('/edit-{nrp}', ['uses' => 'MahasiswaController@updateDataMahasiswa', 'as' => 'paj.mastermhs.update']);
        Route::get('/h-{nrp}', ['uses' => 'MahasiswaController@deleteDataMahasiswa', 'as' => 'paj.mastermhs.delete']);
        Route::get('/r-{nrp}', ['uses' => 'MahasiswaController@restoreDataMahasiswa', 'as' => 'paj.mastermhs.restore']);
    });

    Route::group(['prefix' => 'masterkaryawan'], function(){
        Route::get('/', ['uses' => 'KaryawanController@showDataKaryawan', 'as' => 'paj.masterkaryawan']);
        Route::get('/s', ['uses' => 'KaryawanController@showDataKaryawans', 'as' => 'paj.masterkaryawans']);
        Route::get('/add', ['uses' => 'KaryawanController@addDataKaryawan', 'as' => 'paj.masterkaryawan.add']);
        Route::post('/add', ['uses' => 'KaryawanController@storeDataKaryawan', 'as' => 'paj.masterkaryawan.store']);
        Route::get('/edit-{npk}', ['uses' => 'KaryawanController@editDataKaryawan', 'as' => 'paj.masterkaryawan.edit']);
        Route::post('/edit-{npk}', ['uses' => 'KaryawanController@updateDataKaryawan', 'as' => 'paj.masterkaryawan.update']);
        Route::get('/h-{npk}', ['uses' => 'KaryawanController@deleteDataKaryawan', 'as' => 'paj.masterkaryawan.delete']);
        Route::get('/r-{npk}', ['uses' => 'KaryawanController@restoreDataKaryawan', 'as' => 'paj.masterkaryawan.restore']);
    });

    Route::group(['prefix' => 'mastermk'], function(){
        Route::get('/', ['uses' => 'MataKuliahController@showDataMataKuliah', 'as' => 'paj.mastermk']);
        Route::get('/s', ['uses' => 'MataKuliahController@showDataMatakuliahs', 'as' => 'paj.mastermks']);
        Route::get('/add', ['uses' => 'MataKuliahController@addDataMatakuliah', 'as' => 'paj.mastermk.add']);
        Route::post('/add', ['uses' => 'MataKuliahController@storeDataMatakuliah', 'as' => 'paj.mastermk.store']);
        Route::get('/addext', ['uses' => 'MataKuliahController@addDataMatakuliahExternal', 'as' => 'paj.mastermk.addext']);
        Route::get('/storeext', ['uses' => 'MataKuliahController@storeDataMatakuliahExternal', 'as' => 'paj.mastermk.storeext']);
        Route::get('/aktifkan', ['uses' => 'MataKuliahController@aktifkansemuaMataKuliah', 'as' => 'paj.mastermk.aktifkan']);
        Route::get('/a-{kodemk}', ['uses' => 'MataKuliahController@aktifkanMataKuliah', 'as' => 'paj.mastermk.aktifkansatu']);
        Route::get('/edit-{kodemk}', ['uses' => 'MataKuliahController@editDataMatakuliah', 'as' => 'paj.mastermk.edit']);
        Route::post('/edit-{kodemk}', ['uses' => 'MataKuliahController@updateDataMatakuliah', 'as' => 'paj.mastermk.update']);
        Route::get('/h-{kodemk}', ['uses' => 'MataKuliahController@deleteDataMatakuliah', 'as' => 'paj.mastermk.delete']);
        Route::get('/r-{kodemk}', ['uses' => 'MataKuliahController@restoreDataMatakuliah', 'as' => 'paj.mastermk.restore']);
        Route::get('/f-{kodemk}', ['uses' => 'MataKuliahController@forcedeleteDataMatakuliah', 'as' => 'paj.mastermk.forcedelete']);
    });

    Route::group(['prefix' => 'masterujian'], function(){
        Route::get('/', ['uses' => 'JadwalUjianController@showDataUjian', 'as' => 'paj.masterujian']);
        Route::get('/s', ['uses' => 'JadwalUjianController@showDataUjians', 'as' => 'paj.masterujians']);
        Route::get('/add', ['uses' => 'JadwalUjianController@addDataUjian', 'as' => 'paj.masterujian.add']);
        Route::post('/add', ['uses' => 'JadwalUjianController@storeDataUjian', 'as' => 'paj.masterujian.store']);
        Route::get('/edit-{kodeujian}', ['uses' => 'JadwalUjianController@editDataUjian', 'as' => 'paj.masterujian.edit']);
        Route::post('/edit-{kodeujian}', ['uses' => 'JadwalUjianController@updateDataUjian', 'as' => 'paj.masterujian.update']);
        Route::get('/h-{kodeujian}', ['uses' => 'JadwalUjianController@deleteDataUjian', 'as' => 'paj.masterujian.delete']);
        Route::get('/r-{kodeujian}', ['uses' => 'JadwalUjianController@restoreDataUjian', 'as' => 'paj.masterujian.restore']);
        Route::get('/f-{kodeujian}', ['uses' => 'JadwalUjianController@forcedeleteDataUjian', 'as' => 'paj.masterujian.forcedelete']);
    });

    Route::group(['prefix' => 'masterkp'], function(){
        Route::get('/', ['uses' => 'KelasParalelController@showDataKelasParalel', 'as' => 'paj.masterkp']);
        Route::get('/s', ['uses' => 'KelasParalelController@showDataKelasParalels', 'as' => 'paj.masterkps']);
        Route::get('/a-{id}', ['uses' => 'KelasParalelController@aktifkanKelasParalel', 'as' => 'paj.masterkp.a']);
        Route::get('/n-{id}', ['uses' => 'KelasParalelController@nonaktifkanKelasParalel', 'as' => 'paj.masterkp.n']);
        Route::get('/add', ['uses' => 'KelasParalelController@addDataKelasParalel', 'as' => 'paj.masterkp.add']);
        Route::post('/add', ['uses' => 'KelasParalelController@storeDataKelasParalel', 'as' => 'paj.masterkp.store']);
        Route::get('/edit-{id}', ['uses' => 'KelasParalelController@editDataKelasParalel', 'as' => 'paj.masterkp.edit']);
        Route::post('/edit-{id}', ['uses' => 'KelasParalelController@updateDataKelasParalel', 'as' => 'paj.masterkp.update']);
        Route::get('/h-{id}', ['uses' => 'KelasParalelController@deleteDataKelasParalel', 'as' => 'paj.masterkp.delete']);
        Route::get('/r-{id}', ['uses' => 'KelasParalelController@restoreDataKelasParalel', 'as' => 'paj.masterkp.restore']);
        Route::get('/f-{id}', ['uses' => 'KelasParalelController@forcedeleteDataKelasParalel', 'as' => 'paj.masterkp.forcedelete']);
    });

    Route::group(['prefix' => 'masterkuliah'], function(){
        Route::get('/', ['uses' => 'JadwalKuliahController@showDataKuliah', 'as' => 'paj.masterkuliah']);
        Route::get('/s', ['uses' => 'JadwalKuliahController@showDataKuliahs', 'as' => 'paj.masterkuliahs']);
        Route::get('/add', ['uses' => 'JadwalKuliahController@addDataKuliah', 'as' => 'paj.masterkuliah.add']);
        Route::post('/add', ['uses' => 'JadwalKuliahController@storeDataKuliah', 'as' => 'paj.masterkuliah.store']);
        Route::get('/edit-{id}', ['uses' => 'JadwalKuliahController@editDataKuliah', 'as' => 'paj.masterkuliah.edit']);
        Route::post('/edit-{id}', ['uses' => 'JadwalKuliahController@updateDataKuliah', 'as' => 'paj.masterkuliah.update']);
        Route::get('/h-{id}', ['uses' => 'JadwalKuliahController@deleteDataKuliah', 'as' => 'paj.masterkuliah.delete']);
        Route::get('/r-{id}', ['uses' => 'JadwalKuliahController@restoreDataKuliah', 'as' => 'paj.masterkuliah.restore']);
        Route::get('/f-{id}', ['uses' => 'JadwalKuliahController@forcedeleteDataKuliah', 'as' => 'paj.masterkuliah.forcedelete']);
    });

    Route::group(['prefix' => 'masterkonversi'], function(){
        Route::get('/', ['uses' => 'KonversiMkController@showDataKonversi', 'as' => 'paj.masterkonversi']);
        Route::get('/s', ['uses' => 'KonversiMkController@showDataKonversis', 'as' => 'paj.masterkonversis']);
        Route::get('/add', ['uses' => 'KonversiMkController@addDataKonversi', 'as' => 'paj.masterkonversi.add']);
        Route::post('/add', ['uses' => 'KonversiMkController@storeDataKonversi', 'as' => 'paj.masterkonversi.store']);
        Route::get('/edit-{id}', ['uses' => 'KonversiMkController@editDataKonversi', 'as' => 'paj.masterkonversi.edit']);
        Route::post('/edit-{id}', ['uses' => 'KonversiMkController@updateDataKonversi', 'as' => 'paj.masterkonversi.update']);
        Route::get('/h-{id}', ['uses' => 'KonversiMkController@deleteDataKonversi', 'as' => 'paj.masterkonversi.delete']);
        Route::get('/r-{id}', ['uses' => 'KonversiMkController@restoreDataKonversi', 'as' => 'paj.masterkonversi.restore']);
        Route::get('/f-{id}', ['uses' => 'KonversiMkController@forcedeleteDataKonversi', 'as' => 'paj.masterkonversi.forcedelete']);
    });

    Route::group(['prefix' => 'masterprasyarat'], function(){
        Route::get('/', ['uses' => 'PrasyaratMkController@showDataPrasyarat', 'as' => 'paj.masterprasyarat']);
        Route::get('/s', ['uses' => 'PrasyaratMkController@showDataPrasyarats', 'as' => 'paj.masterprasyarats']);
        Route::get('/add', ['uses' => 'PrasyaratMkController@addDataPrasyarat', 'as' => 'paj.masterprasyarat.add']);
        Route::post('/add', ['uses' => 'PrasyaratMkController@storeDataPrasyarat', 'as' => 'paj.masterprasyarat.store']);
        Route::get('/edit-{id}', ['uses' => 'PrasyaratMkController@editDataPrasyarat', 'as' => 'paj.masterprasyarat.edit']);
        Route::post('/edit-{id}', ['uses' => 'PrasyaratMkController@updateDataPrasyarat', 'as' => 'paj.masterprasyarat.update']);
        Route::get('/h-{id}', ['uses' => 'PrasyaratMkController@deleteDataPrasyarat', 'as' => 'paj.masterprasyarat.delete']);
        Route::get('/r-{id}', ['uses' => 'PrasyaratMkController@restoreDataPrasyarat', 'as' => 'paj.masterprasyarat.restore']);
        Route::get('/f-{id}', ['uses' => 'PrasyaratMkController@forcedeleteDataPrasyarat', 'as' => 'paj.masterprasyarat.forcedelete']);
    });

    Route::group(['prefix' => 'masterdosenmk'], function(){
        Route::get('/', ['uses' => 'DosenPengampuMkController@showDataDosenmk', 'as' => 'paj.masterdosenmk']);
        Route::get('/s', ['uses' => 'DosenPengampuMkController@showDataDosenmks', 'as' => 'paj.masterdosenmks']);
        Route::get('/add', ['uses' => 'DosenPengampuMkController@addDataDosenmk', 'as' => 'paj.masterdosenmk.add']);
        Route::post('/add', ['uses' => 'DosenPengampuMkController@storeDataDosenmk', 'as' => 'paj.masterdosenmk.store']);
        Route::get('/edit-{id}', ['uses' => 'DosenPengampuMkController@editDataDosenmk', 'as' => 'paj.masterdosenmk.edit']);
        Route::post('/edit-{id}', ['uses' => 'DosenPengampuMkController@updateDataDosenmk', 'as' => 'paj.masterdosenmk.update']);
        Route::get('/h-{id}', ['uses' => 'DosenPengampuMkController@deleteDataDosenmk', 'as' => 'paj.masterdosenmk.delete']);
        Route::get('/r-{id}', ['uses' => 'DosenPengampuMkController@restoreDataDosenmk', 'as' => 'paj.masterdosenmk.restore']);
        Route::get('/f-{id}', ['uses' => 'DosenPengampuMkController@forcedeleteDataDosenmk', 'as' => 'paj.masterdosenmk.forcedelete']);
    });

    Route::group(['prefix' => 'mastersettingnrp'], function(){
        Route::get('/', ['uses' => 'SettingNrpController@showDataSettingnrp', 'as' => 'paj.mastersettingnrp']);
        Route::get('/s', ['uses' => 'SettingNrpController@showDataSettingnrps', 'as' => 'paj.mastersettingnrps']);
        Route::get('/add', ['uses' => 'SettingNrpController@addDataSettingnrp', 'as' => 'paj.mastersettingnrp.add']);
        Route::post('/add', ['uses' => 'SettingNrpController@storeDataSettingnrp', 'as' => 'paj.mastersettingnrp.store']);
        Route::get('/edit-{id}', ['uses' => 'SettingNrpController@editDataSettingnrp', 'as' => 'paj.mastersettingnrp.edit']);
        Route::post('/edit-{id}', ['uses' => 'SettingNrpController@updateDataSettingnrp', 'as' => 'paj.mastersettingnrp.update']);
        Route::get('/h-{id}', ['uses' => 'SettingNrpController@deleteDataSettingnrp', 'as' => 'paj.mastersettingnrp.delete']);
        Route::get('/r-{id}', ['uses' => 'SettingNrpController@restoreDataSettingnrp', 'as' => 'paj.mastersettingnrp.restore']);
        Route::get('/f-{id}', ['uses' => 'SettingNrpController@forcedeleteDataSettingnrp', 'as' => 'paj.mastersettingnrp.forcedelete']);
    });

    Route::group(['prefix' => 'mastertranskripmhs'], function(){
        Route::get('/', ['uses' => 'TranskripController@showDataTranskripmhs', 'as' => 'paj.mastertranskripmhs']);
        Route::get('/s', ['uses' => 'TranskripController@showDataTranskripmhss', 'as' => 'paj.mastertranskripmhss']);
        Route::get('/add', ['uses' => 'TranskripController@addDataTranskripmhs', 'as' => 'paj.mastertranskripmhs.add']);
        Route::post('/add', ['uses' => 'TranskripController@storeDataTranskripmhs', 'as' => 'paj.mastertranskripmhs.store']);
        Route::get('/addext', ['uses' => 'TranskripController@addDataTranskripmhsExternal', 'as' => 'paj.mastertranskripmhs.addext']);
        Route::get('/storeext', ['uses' => 'TranskripController@storeDataTranskripmhsExternal', 'as' => 'paj.mastertranskripmhs.storeext']);
        Route::get('/edit-{id}', ['uses' => 'TranskripController@editDataTranskripmhs', 'as' => 'paj.mastertranskripmhs.edit']);
        Route::post('/edit-{id}', ['uses' => 'TranskripController@updateDataTranskripmhs', 'as' => 'paj.mastertranskripmhs.update']);
        Route::get('/h-{id}', ['uses' => 'TranskripController@deleteDataTranskripmhs', 'as' => 'paj.mastertranskripmhs.delete']);
        Route::get('/r-{id}', ['uses' => 'TranskripController@restoreDataTranskripmhs', 'as' => 'paj.mastertranskripmhs.restore']);
        Route::get('/f-{id}', ['uses' => 'TranskripController@forcedeleteDataTranskripmhs', 'as' => 'paj.mastertranskripmhs.forcedelete']);
    });

    Route::group(['prefix' => 'laporan'], function(){
        Route::get('/', ['uses' => 'PajController@showMenuLaporan', 'as' => 'paj.laporanmenu']);
        
    });

  });
});


Route::group(['middleware' => 'maf'], function() {
    Route::group(['prefix' => 'maf'],function(){
        Route::get('/', ['uses' => 'MafController@showBeranda', 'as' => 'maf.beranda']);

        Route::group(['prefix' => 'mastermhs'], function(){
            Route::get('/', ['uses' => 'MahasiswaController@showDataMahasiswa', 'as' => 'maf.mastermhs']);
            Route::get('/s', ['uses' => 'MahasiswaController@showDataMahasiswas', 'as' => 'maf.mastermhss']);
            Route::get('/add', ['uses' => 'MahasiswaController@addDataMahasiswa', 'as' => 'maf.mastermhs.add']);
            Route::post('/add', ['uses' => 'MahasiswaController@storeDataMahasiswa', 'as' => 'maf.mastermhs.store']);
            Route::get('/addext', ['uses' => 'MahasiswaController@addDataMahasiswaExternal', 'as' => 'maf.mastermhs.addext']);
            Route::get('/storeext', ['uses' => 'MahasiswaController@storeDataMahasiswaExternal', 'as' => 'maf.mastermhs.storeext']);
            Route::get('/edit-{nrp}', ['uses' => 'MahasiswaController@editDataMahasiswa', 'as' => 'maf.mastermhs.edit']);
            Route::post('/edit-{nrp}', ['uses' => 'MahasiswaController@updateDataMahasiswa', 'as' => 'maf.mastermhs.update']);
            Route::get('/h-{nrp}', ['uses' => 'MahasiswaController@deleteDataMahasiswa', 'as' => 'maf.mastermhs.delete']);
            Route::get('/r-{nrp}', ['uses' => 'MahasiswaController@restoreDataMahasiswa', 'as' => 'maf.mastermhs.restore']);
            Route::get('/f-{nrp}', ['uses' => 'MahasiswaController@forcedeleteDataMahasiswa', 'as' => 'paj.mastermhs.forcedelete']);
        });

        Route::group(['prefix' => 'masterkaryawan'], function(){
            Route::get('/', ['uses' => 'KaryawanController@showDataKaryawan', 'as' => 'maf.masterkaryawan']);
            Route::get('/s', ['uses' => 'KaryawanController@showDataKaryawans', 'as' => 'maf.masterkaryawans']);
            Route::get('/add', ['uses' => 'KaryawanController@addDataKaryawan', 'as' => 'maf.masterkaryawan.add']);
            Route::post('/add', ['uses' => 'KaryawanController@storeDataKaryawan', 'as' => 'maf.masterkaryawan.store']);
            Route::get('/addext', ['uses' => 'KaryawanController@addDataKaryawanExternal', 'as' => 'maf.masterkaryawan.addext']);
            Route::get('/storeext', ['uses' => 'KaryawanController@storeDataKaryawanExternal', 'as' => 'maf.masterkaryawan.storeext']);
            Route::get('/edit-{npk}', ['uses' => 'KaryawanController@editDataKaryawan', 'as' => 'maf.masterkaryawan.edit']);
            Route::post('/edit-{npk}', ['uses' => 'KaryawanController@updateDataKaryawan', 'as' => 'maf.masterkaryawan.update']);
            Route::get('/h-{npk}', ['uses' => 'KaryawanController@deleteDataKaryawan', 'as' => 'maf.masterkaryawan.delete']);
            Route::get('/r-{npk}', ['uses' => 'KaryawanController@restoreDataKaryawan', 'as' => 'maf.masterkaryawan.restore']);
            Route::get('/f-{npk}', ['uses' => 'KaryawanController@forcedeleteDataKaryawan', 'as' => 'paj.masterkaryawan.forcedelete']);
        });

        Route::group(['prefix' => 'jurusanmk'], function() {
            Route::get('/', ['uses' => 'JurusanMkController@showDataJurusanMk', 'as' => 'maf.jurusanmk']);
            Route::post('/', ['uses' => 'JurusanMkController@simpanJurusanMK', 'as' => 'maf.simpanjurusanmk']);
            Route::get('/jur-{id}', ['uses' => 'JurusanMkController@showAllMk', 'as' => 'maf.jurusanpilihmk']);
            Route::get('/addjurmk', ['uses' => 'JurusanMkController@addJurusanPilihMk', 'as' => 'maf.addjurusanpilihmk']);
            Route::get('/deljurmk', ['uses' => 'JurusanMkController@deleteJurusanPilihMk', 'as' => 'maf.deljurusanpilihmk']);
        });

        Route::group(['prefix' => 'hasilfpp'], function() {
          Route::get('/', ['uses' => 'ProsesHasilFPPController@showHasilFppIndex', 'as' => 'maf.hasilfpp']);
          Route::get('/prosessemua', ['uses' => 'ProsesHasilFPPController@prosesSemua', 'as' => 'maf.prosessemua']);
          Route::get('/dtl-{id}', ['uses' => 'ProsesHasilFPPController@lihatDetailKelas', 'as' => 'maf.detailkelas']);
          Route::get('/p-{nama}', ['uses' => 'ProsesHasilFPPController@publikasiSemua', 'as' => 'maf.publikasisemua']);
          Route::get('/pk-{idkls}', ['uses' => 'ProsesHasilFPPController@publikasibyKelas', 'as' => 'maf.publikasikelas']);
          Route::get('/tpk-{idkls}', ['uses' => 'ProsesHasilFPPController@tidakPublikasibyKelas', 'as' => 'paj.publikasikelas']);
          Route::get('/ed-{iddata}', ['uses' => 'ProsesHasilFPPController@editHasilFppbyId', 'as' => 'maf.editbyid']);
          Route::post('/ed-{iddata}', ['uses' => 'ProsesHasilFPPController@simpaneditHasilFppbyId', 'as' => 'maf.simpaneditbyid']);
          Route::get('/edk-{idkls}', ['uses' => 'ProsesHasilFPPController@editHasilFppbyKelas', 'as' => 'maf.editbykelas']);
          Route::post('/edk-{idkls}', ['uses' => 'ProsesHasilFPPController@simpaneditHasilFppbyKelas', 'as' => 'maf.simpaneditbykelas']);
          Route::get('/tr-{idkls}', ['uses' => 'ProsesHasilFPPController@transferMhsindex', 'as' => 'maf.transfermhs']);
          Route::post('/tr-{idkls}', ['uses' => 'ProsesHasilFPPController@simpanTransferMhs', 'as' => 'maf.simpantransfermhs']);

        });

        Route::group(['prefix' => 'ruangan'], function(){
            Route::get('/', ['uses' => 'RuanganController@showDataRuangan', 'as' => 'maf.masterruangan']);
            Route::get('/s', ['uses' => 'RuanganController@showDataRuangans', 'as' => 'maf.masterruangans']);
            Route::get('/add', ['uses' => 'RuanganController@addDataRuangan', 'as' => 'maf.masterruangan.add']);
            Route::post('/add', ['uses' => 'RuanganController@storeDataRuangan', 'as' => 'maf.masterruangan.store']);
            Route::get('/edit-{id}', ['uses' => 'RuanganController@editDataRuangan', 'as' => 'maf.masterruangan.edit']);
            Route::post('/edit-{id}', ['uses' => 'RuanganController@updateDataRuangan', 'as' => 'maf.masterruangan.update']);
            Route::get('/h-{id}', ['uses' => 'RuanganController@deleteDataRuangan', 'as' => 'maf.masterruangan.delete']);
            Route::get('/r-{id}', ['uses' => 'RuanganController@restoreDataRuangan', 'as' => 'maf.masterruangan.restore']);
            Route::get('/f-{id}', ['uses' => 'RuanganController@forcedeleteDataRuangan', 'as' => 'maf.masterruangan.forcedelete']);
        });

        Route::group(['prefix' => 'nonjaga'], function(){
            Route::get('/', ['uses' => 'JadwalNonjagaController@showDataNonjaga', 'as' => 'maf.masternonjaga']);
            Route::get('/s', ['uses' => 'JadwalNonjagaController@showDataNonjagas', 'as' => 'maf.masternonjagas']);
            Route::get('/add', ['uses' => 'JadwalNonjagaController@addDataNonjaga', 'as' => 'maf.masternonjaga.add']);
            Route::post('/add', ['uses' => 'JadwalNonjagaController@storeDataNonjaga', 'as' => 'maf.masternonjaga.store']);
            Route::get('/edit-{id}', ['uses' => 'JadwalNonjagaController@editDataNonjaga', 'as' => 'maf.masternonjaga.edit']);
            Route::post('/edit-{id}', ['uses' => 'JadwalNonjagaController@updateDataNonjaga', 'as' => 'maf.masternonjaga.update']);
            Route::get('/h-{id}', ['uses' => 'JadwalNonjagaController@deleteDataNonjaga', 'as' => 'maf.masternonjaga.delete']);
            Route::get('/r-{id}', ['uses' => 'JadwalNonjagaController@restoreDataNonjaga', 'as' => 'maf.masternonjaga.restore']);
            Route::get('/f-{id}', ['uses' => 'JadwalNonjagaController@forcedeleteDataNonjaga', 'as' => 'maf.masternonjaga.forcedelete']);
        });

        Route::group(['prefix' => 'kelas'], function(){
            Route::get('/', ['uses' => 'MafController@showDataKelasParalel', 'as' => 'maf.masterkp']);
            Route::get('/s', ['uses' => 'MafController@showDataKelasParalels', 'as' => 'maf.masterkps']);
            Route::get('/a-{id}', ['uses' => 'MafController@aktifkanKelasParalel', 'as' => 'maf.masterkp.a']);
            Route::get('/n-{id}', ['uses' => 'MafController@nonaktifkanKelasParalel', 'as' => 'maf.masterkp.n']);
            Route::get('/add', ['uses' => 'MafController@addDataKelasParalel', 'as' => 'maf.masterkp.add']);
            Route::get('/adddummy', ['uses' => 'MafController@tambahDummyDataKelasParalel', 'as' => 'maf.masterkp.adddummy']);
            Route::post('/add', ['uses' => 'MafController@storeDataKelasParalel', 'as' => 'maf.masterkp.store']);
            Route::get('/edit-{id}', ['uses' => 'MafController@editDataKelasParalel', 'as' => 'maf.masterkp.edit']);

            Route::post('/edit-{id}', ['uses' => 'MafController@updateDataKelasParalel', 'as' => 'maf.masterkp.update']);
            Route::get('/h-{id}', ['uses' => 'MafController@deleteDataKelasParalel', 'as' => 'maf.masterkp.delete']);
            Route::get('/r-{id}', ['uses' => 'MafController@restoreDataKelasParalel', 'as' => 'maf.masterkp.restore']);
            Route::get('/f-{id}', ['uses' => 'MafController@forcedeleteDataKelasParalel', 'as' => 'maf.masterkp.forcedelete']);
        });

        Route::get('/pengaturan-{menu}', ['uses' => 'MafController@showPengaturan', 'as' => 'maf.pengaturan']);

        Route::group(['prefix' => 'mastersemester'], function(){
            Route::get('/', ['uses' => 'SemesterController@showDataSemester', 'as' => 'maf.mastersemester']);
            Route::get('/s', ['uses' => 'SemesterController@showDataSemesters', 'as' => 'maf.mastersemesters']);
            Route::get('/add', ['uses' => 'SemesterController@addDataSemester', 'as' => 'maf.mastersemester.add']);
            Route::post('/add', ['uses' => 'SemesterController@storeDataSemester', 'as' => 'maf.mastersemester.store']);
            Route::get('/aktifkan-{id}', ['uses' => 'SemesterController@aktifkanSemester', 'as' => 'maf.mastersemester.aktifkan']);
            Route::get('/edit-{id}', ['uses' => 'SemesterController@editDataSemester', 'as' => 'maf.mastersemester.edit']);
            Route::post('/edit-{id}', ['uses' => 'SemesterController@updateDataSemester', 'as' => 'maf.mastersemester.update']);
            Route::get('/h-{id}', ['uses' => 'SemesterController@deleteDataSemester', 'as' => 'maf.mastersemester.delete']);
            Route::get('/r-{id}', ['uses' => 'SemesterController@restoreDataSemester', 'as' => 'maf.mastersemester.restore']);
            Route::get('/f-{id}', ['uses' => 'SemesterController@forcedeleteDataSemester', 'as' => 'maf.mastersemester.forcedelete']);
        });

        Route::group(['prefix' => 'masterjadwalfpp'], function(){
            Route::get('/', ['uses' => 'FppController@showDataJadwalFpp', 'as' => 'maf.masterjadwalfpp']);
            Route::get('/s', ['uses' => 'FppController@showDataJadwalFpps', 'as' => 'maf.masterjadwalfpps']);
            Route::get('/add', ['uses' => 'FppController@addDataJadwalfpp', 'as' => 'maf.masterjadwalfpp.add']);
            Route::post('/add', ['uses' => 'FppController@storeDataJadwalfpp', 'as' => 'maf.masterjadwalfpp.store']);
            Route::get('/edit-{id}', ['uses' => 'FppController@editDataJadwalfpp', 'as' => 'maf.masterjadwalfpp.edit']);
            Route::post('/edit-{id}', ['uses' => 'FppController@updateDataJadwalfpp', 'as' => 'maf.masterjadwalfpp.update']);
            Route::get('/h-{id}', ['uses' => 'FppController@deleteDataJadwalfpp', 'as' => 'maf.masterjadwalfpp.delete']);
            Route::get('/r-{id}', ['uses' => 'FppController@restoreDataJadwalfpp', 'as' => 'maf.masterjadwalfpp.restore']);
            Route::get('/f-{id}', ['uses' => 'FppController@forcedeleteDataJadwalfpp', 'as' => 'maf.masterjadwalfpp.forcedelete']);
        });

        Route::group(['prefix' => 'masterprioritas'], function(){
            Route::get('/', ['uses' => 'PrioritasTerimaController@showDataPrioritas', 'as' => 'maf.masterprioritas']);
            Route::get('/s', ['uses' => 'PrioritasTerimaController@showDataPrioritass', 'as' => 'maf.masterprioritass']);
            Route::get('/add', ['uses' => 'PrioritasTerimaController@addDataPrioritas', 'as' => 'maf.masterprioritas.add']);
            Route::post('/add', ['uses' => 'PrioritasTerimaController@storeDataPrioritas', 'as' => 'maf.masterprioritas.store']);
            Route::get('/aktifkan-{id}', ['uses' => 'PrioritasTerimaController@aktifkanPrioritas', 'as' => 'maf.masterprioritas.aktifkan']);
            Route::get('/edit-{id}', ['uses' => 'PrioritasTerimaController@editDataPrioritas', 'as' => 'maf.masterprioritas.edit']);
            Route::post('/edit-{id}', ['uses' => 'PrioritasTerimaController@updateDataPrioritas', 'as' => 'maf.masterprioritas.update']);
            Route::get('/h-{id}', ['uses' => 'PrioritasTerimaController@deleteDataPrioritas', 'as' => 'maf.masterprioritas.delete']);
            Route::get('/r-{id}', ['uses' => 'PrioritasTerimaController@restoreDataPrioritas', 'as' => 'maf.masterprioritas.restore']);
            Route::get('/f-{id}', ['uses' => 'PrioritasTerimaController@forcedeleteDataPrioritas', 'as' => 'maf.masterprioritas.forcedelete']);
        });

        Route::group(['prefix' => 'mastersettingfakultas'], function(){
            Route::get('/', ['uses' => 'SettingFakultasController@showDataPrioritas', 'as' => 'maf.mastersettingfakultas']);
            Route::get('/s', ['uses' => 'SettingFakultasController@showDataPrioritass', 'as' => 'maf.mastersettingfakultass']);
            Route::get('/add', ['uses' => 'SettingFakultasController@addDataPrioritas', 'as' => 'maf.mastersettingfakultas.add']);
            Route::post('/add', ['uses' => 'SettingFakultasController@storeDataPrioritas', 'as' => 'maf.mastersettingfakultas.store']);
            Route::get('/aktifkan-{id}', ['uses' => 'SettingFakultasController@aktifkanPrioritas', 'as' => 'maf.mastersettingfakultas.aktifkan']);
            Route::get('/edit-{id}', ['uses' => 'SettingFakultasController@editDataPrioritas', 'as' => 'maf.mastersettingfakultas.edit']);
            Route::post('/edit-{id}', ['uses' => 'SettingFakultasController@updateDataPrioritas', 'as' => 'maf.mastersettingfakultas.update']);
            Route::get('/h-{id}', ['uses' => 'SettingFakultasController@deleteDataPrioritas', 'as' => 'maf.mastersettingfakultas.delete']);
            Route::get('/r-{id}', ['uses' => 'SettingFakultasController@restoreDataPrioritas', 'as' => 'maf.mastersettingfakultas.restore']);
            Route::get('/f-{id}', ['uses' => 'SettingFakultasController@forcedeleteDataPrioritas', 'as' => 'maf.mastersettingfakultas.forcedelete']);
        });

        Route::group(['prefix' => 'jabatan'], function(){
            Route::get('/', ['uses' => 'JabatanController@showDataJabatan', 'as' => 'maf.kelolajabatan']);
            Route::post('/', ['uses' => 'JabatanController@storeDataJabatan', 'as' => 'maf.kelolajabatan.store']);
            Route::get('/s', ['uses' => 'JabatanController@showDataJabatans', 'as' => 'maf.kelolajabatans']);
            Route::post('/s', ['uses' => 'JabatanController@storeDataJabatan', 'as' => 'maf.kelolajabatan.store']);
            Route::post('/add', ['uses' => 'JabatanController@storeDataJabatan', 'as' => 'maf.kelolajabatan.store']);
            Route::post('/edit', ['uses' => 'JabatanController@updateDataJabatan', 'as' => 'maf.kelolajabatan.update']);
            
            Route::get('/h-{npk}-{idjabatan}', ['uses' => 'JabatanController@deleteDataJabatan', 'as' => 'maf.kelolajabatan.delete']);
            Route::get('/r-{npk}-{idjabatan}', ['uses' => 'JabatanController@restoreDataJabatan', 'as' => 'maf.kelolajabatan.restore']);
            Route::get('/f-{npk}-{idjabatan}', ['uses' => 'JabatanController@forcedeleteDataJabatan', 'as' => 'maf.kelolajabatan.forcedelete']);
        });

        Route::group(['prefix' => 'jurusan'], function(){
            Route::get('/', ['uses' => 'KaryawanJurusanController@showDataKJurusan', 'as' => 'maf.kelolajurusan']);
            Route::post('/', ['uses' => 'KaryawanJurusanController@storeDataKJurusan', 'as' => 'maf.kelolajurusan.store']);
            Route::get('/s', ['uses' => 'KaryawanJurusanController@showDataKJurusans', 'as' => 'maf.kelolajurusans']);
            Route::post('/s', ['uses' => 'KaryawanJurusanController@storeDataKJurusan', 'as' => 'maf.kelolajurusan.store']);
            Route::post('/add', ['uses' => 'KaryawanJurusanController@storeDataKJurusan', 'as' => 'maf.kelolajurusan.store']);
            Route::post('/edit', ['uses' => 'KaryawanJurusanController@updateDataKJurusan', 'as' => 'maf.kelolajurusan.update']);
            
            Route::get('/h-{npk}-{idjurusan}', ['uses' => 'KaryawanJurusanController@deleteDataKJurusan', 'as' => 'maf.kelolajurusan.delete']);
            Route::get('/r-{npk}-{idjurusan}', ['uses' => 'KaryawanJurusanController@restoreDataKJurusan', 'as' => 'maf.kelolajurusan.restore']);
            Route::get('/f-{npk}-{idjurusan}', ['uses' => 'KaryawanJurusanController@forcedeleteDataKJurusan', 'as' => 'maf.kelolajurusan.forcedelete']);
        });

        Route::group(['prefix' => 'presensikuliah'], function() {
            Route::get('/', ['uses' => 'MafController@showDataPresensiKuliah', 'as' => 'maf.presensikuliah']);
            Route::get('/dtl-{idjadwalkuliah}', ['uses' => 'MafController@showDetailPresensiKuliah', 'as' => 'maf.dtlpresensikuliah']);
        });

        Route::group(['prefix' => 'presensiujian'], function() {
            Route::get('/', ['uses' => 'RuanganUjianController@showJadwalUjian', 'as' => 'maf.jadwalujian']);
            Route::get('/pre-{idjadwalujian}', ['uses' => 'RuanganUjianController@showDataPresensiUjian', 'as' => 'maf.presensiujian']);
            Route::get('/dtl-{idpresensiujian}', ['uses' => 'RuanganUjianController@showDetailPresensiUjian', 'as' => 'maf.dtlpresensiujian']);
            Route::get('/prosesruangan', ['uses' => 'RuanganUjianController@prosesRuanganUjian', 'as' => 'maf.prosesruangan']);
            Route::get('/crlain-{idjadwalujian}', ['uses' => 'RuanganUjianController@cariRuanganLain', 'as' => 'maf.cariruanganlain']);
            Route::post('/crlain-{idjadwalujian}', ['uses' => 'RuanganUjianController@refreshNRP', 'as' => 'maf.refnrp']);
            Route::get('/cplain-{idjadwalujian}', ['uses' => 'RuanganUjianController@cariPenjagaLain', 'as' => 'maf.caripenjagalain']);
            Route::post('/cplain-{idjadwalujian}', ['uses' => 'RuanganUjianController@refreshPenjagaUjian', 'as' => 'maf.refpenjaga']);
            Route::get('/prosespenjagaujian', ['uses' => 'RuanganUjianController@prosesPenjagaUjian', 'as' => 'maf.prosespenjagaujian']);
            Route::post('/', ['uses' => 'RuanganUjianController@updatePiket', 'as' => 'maf.presensiujian.piketupdate']);
            Route::get('/edit-{kodeujian}', ['uses' => 'JadwalUjianController@editDataUjian', 'as' => 'maf.masterujian.edit']);
            Route::post('/edit-{kodeujian}', ['uses' => 'JadwalUjianController@updateDataUjian', 'as' => 'maf.masterujian.update']);
        });

        Route::group(['prefix' => 'laporan'], function() {
            Route::get('/', ['uses' => 'LaporanController@showLaporan', 'as' => 'maf.laporan']);
            //////
            Route::get('/hasilperwalianmhs', ['uses' => 'LaporanController@laporanHasilPerwalianMhs', 'as' => 'maf.laphasilperwalianmhs']);
            Route::post('/hasilperwalianmhs', ['uses' => 'LaporanController@laporanHasilPerwalianMhsFilter', 'as' => 'maf.laphasilperwalianmhsfilter']);
            //////
            Route::get('/kapkelas', ['uses' => 'LaporanController@laporanKapkelas', 'as' => 'maf.lapkapkelas']);
            Route::post('/kapkelas', ['uses' => 'LaporanController@laporanKapkelasFilter', 'as' => 'maf.lapkapkelasfilter']);
            //////
            Route::get('/kelastutup', ['uses' => 'LaporanController@laporanKelasTutup', 'as' => 'maf.lapkelastutup']);
            Route::post('/kelastutup', ['uses' => 'LaporanController@laporanKelasTutupFilter', 'as' => 'maf.lapkelastutupfilter']);
            //////
            Route::get('/rekappeserta', ['uses' => 'LaporanController@laporanRekapPeserta', 'as' => 'maf.rekappeserta']);
            Route::post('/rekappeserta', ['uses' => 'LaporanController@laporanRekapPesertaFilter', 'as' => 'maf.rekappesertafilter']);
            //////
            Route::get('/formrekap', ['uses' => 'LaporanController@laporanFormRekap', 'as' => 'maf.formrekap']);
            Route::post('/formrekap', ['uses' => 'LaporanController@laporanFormRekapFilter', 'as' => 'maf.formrekapfilter']);
            //////
            Route::get('/daftarisikelas', ['uses' => 'LaporanController@laporanDaftarIsiKelas', 'as' => 'maf.daftarisikelas']);
            Route::post('/daftarisikelas', ['uses' => 'LaporanController@laporanDaftarIsiKelasFilter', 'as' => 'maf.daftarisikelasfilter']);
            //////
            Route::get('/lapujiannrp', ['uses' => 'LaporanController@laporanUjianNrp', 'as' => 'maf.lapujiannrp']);
            Route::post('/lapujiannrp', ['uses' => 'LaporanController@laporanUjianNrpFilter', 'as' => 'maf.lapujiannrpfilter']);
            //////
            Route::get('/lapujianpenjaga', ['uses' => 'LaporanController@laporanUjianPenjaga', 'as' => 'maf.lapujianpenjaga']);
            Route::post('/lapujianpenjaga', ['uses' => 'LaporanController@laporanUjianPenjagaFilter', 'as' => 'maf.lapujianpenjagafilter']);
            //////
            Route::get('/ujianruangan', ['uses' => 'LaporanController@laporanUjianRuangan', 'as' => 'maf.lapujianruangan']);
            Route::post('/ujianruangan', ['uses' => 'LaporanController@laporanUjianRuanganFilter', 'as' => 'maf.lapujianruanganfilter']);
            //////
            Route::get('/jagaujiankaryawan', ['uses' => 'LaporanController@laporanJagaUjianKaryawan', 'as' => 'maf.jagaujiankaryawan']);
            Route::post('/jagaujiankaryawan', ['uses' => 'LaporanController@laporanJagaUjianKaryawanFilter', 'as' => 'maf.jagaujiankaryawanfilter']);
            //////

        });

        Route::group(['prefix' => 'mastermk'], function(){
            Route::get('/', ['uses' => 'MataKuliahController@showDataMataKuliah', 'as' => 'maf.mastermk']);
            Route::get('/s', ['uses' => 'MataKuliahController@showDataMatakuliahs', 'as' => 'maf.mastermks']);
            Route::get('/add', ['uses' => 'MataKuliahController@addDataMatakuliah', 'as' => 'maf.mastermk.add']);
            Route::post('/add', ['uses' => 'MataKuliahController@storeDataMatakuliah', 'as' => 'maf.mastermk.store']);
            Route::get('/addext', ['uses' => 'MataKuliahController@addDataMatakuliahExternal', 'as' => 'maf.mastermk.addext']);
            Route::get('/storeext', ['uses' => 'MataKuliahController@storeDataMatakuliahExternal', 'as' => 'maf.mastermk.storeext']);
            Route::get('/aktifkan', ['uses' => 'MataKuliahController@aktifkansemuaMataKuliah', 'as' => 'maf.mastermk.aktifkan']);
            Route::get('/edit-{kodemk}', ['uses' => 'MataKuliahController@editDataMatakuliah', 'as' => 'maf.mastermk.edit']);
            Route::post('/edit-{kodemk}', ['uses' => 'MataKuliahController@updateDataMatakuliah', 'as' => 'maf.mastermk.update']);
            Route::get('/h-{kodemk}', ['uses' => 'MataKuliahController@deleteDataMatakuliah', 'as' => 'maf.mastermk.delete']);
            Route::get('/r-{kodemk}', ['uses' => 'MataKuliahController@restoreDataMatakuliah', 'as' => 'maf.mastermk.restore']);
        });
    });
});

Route::group(['middleware' => 'dosen'], function() {
  Route::group(['prefix' => 'dosen'],function(){
    Route::get('/', 'DosenController@showBeranda')->name('dosen.beranda');

    Route::group(['prefix' => 'kelas'], function() {
      Route::get('/', 'DosenController@showDataKelas')->name('dosen.kelas');
    });
    Route::group(['prefix' => 'ajar'], function() {
      Route::get('/', 'DosenController@showDataMengajar')->name('dosen.ajar');
    });
    Route::group(['prefix' => 'jaga'], function() {
      Route::get('/', 'DosenController@showDataJaga')->name('dosen.jaga');
    });
    
    Route::group(['prefix' => 'kk'], function() {
      Route::get('/', 'DosenController@showKKIndex')->name('dosen.kk');
      Route::get('/batal-{id}', 'DosenController@batalMkKK')->name('dosen.batalmk');
      Route::post('/tambah', 'DosenController@tambahmkKK')->name('dosen.tambahmk');
      Route::get('/ubatal-{id}', 'DosenController@undoBatalMkKK')->name('dosen.ubatalmk');
    });
      
  });
});