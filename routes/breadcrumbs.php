<?php

// MAF's Home
Breadcrumbs::register('maf.beranda', function($breadcrumbs)
{
    $breadcrumbs->push('Beranda', route('maf.beranda'));
});

// MAF's Home > Data Semester
Breadcrumbs::register('maf.mastersemester', function($breadcrumbs)
{
    $breadcrumbs->parent('maf.beranda');
    $breadcrumbs->push('Data Semester', route('maf.mastersemester'));
});



// Home > Blog
// Breadcrumbs::register('blog', function($breadcrumbs)
// {
//     $breadcrumbs->parent('home');
//     $breadcrumbs->push('Blog', route('blog'));
// });

// // Home > Blog > [Category]
// Breadcrumbs::register('category', function($breadcrumbs, $category)
// {
//     $breadcrumbs->parent('blog');
//     $breadcrumbs->push($category->title, route('category', $category->id));
// });

// // Home > Blog > [Category] > [Page]
// Breadcrumbs::register('page', function($breadcrumbs, $page)
// {
//     $breadcrumbs->parent('category', $page->category);
//     $breadcrumbs->push($page->title, route('page', $page->id));
// });