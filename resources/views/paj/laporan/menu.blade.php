@extends('paj._layouts.base')

@section('title', 'Laporan')

@section('content')
<div class="row" align="center">
  <h2>Lihat Laporan</h2>
  <br>
  <br>
  <a href="#">
    <button class="btn btn-primary btn-lg" style="margin: 10px 10px 10px 10px">Hasil FPP</button>
  </a>
  <br>
  <a href="#">
    <button class="btn btn-primary btn-lg" style="margin: 10px 10px 10px 10px">Transaksi dengan Pertamina</button>
  </a>

</div>
@endsection