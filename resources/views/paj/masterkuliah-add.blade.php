@extends('paj._layouts.base')

@section('title', 'Master Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
@if($mode == "add")
    <div class="card">
        <div class="header">
            <h4 class="title" style="display: inline;">Tambah Jadwal Kuliah</h4>
            <p>NB: Otomatis ditambahkan pada semester yang sedang aktif</p>
        </div>
        <div class="content">
            <form class="form-horizontal" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label class="col-sm-3 control-label">Hari: </label>
                <div class="col-sm-9">
                    <select name="hari" class=" form-control border-input">
                    @foreach($enumHari as $d)
                    <option value="{{$d}}">{{$d}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Jam Masuk:</label>
                <div class="col-sm-9">
                    <input type="time" class="form-control border-input" name="jammasuk" value="{{ old('jammasuk') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Jam Keluar:</label>
                <div class="col-sm-9">
                    <input type="time" class="form-control border-input" name="jamkeluar" value="{{ old('jamkeluar') }}">
                </div>
            </div>

            <div class='form-group ui-widget'>
                <label class='col-sm-3 control-label'>Ruangan: </label>
                <div class='col-sm-9'>
                    <input type='text' id='ruangans' class='form-control border-input' placeholder='Cari Nama Ruangan' name='namaruangan' required>
               </div>
            </div>
            <input id="ruangans-id" type="hidden" name="idruangan">

            <div class='form-group ui-widget'>
                <label class='col-sm-3 control-label'>Kelas Paralel: </label>
                <div class='col-sm-9'>
                    <input type='text' id='kelasparalels' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='namakelas' required>
               </div>
            </div>
            <input id="kelas-id" type="hidden" name="idkelasparalel">

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('paj.masterkuliah') }}" class="btn btn-danger"> Kembali</a>
                </div>
            </div>
            </form>
        </div>
    </div>

@elseif ($mode == "edit")
    <div class="card">
        <div class="header">
            <h4 class="title" style="display: inline;">Ubah data Jadwal Kuliah</h4>
        </div>
        <div class="content">
            <form class="form-horizontal" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label class="col-sm-3 control-label">Hari: </label>
                <div class="col-sm-9">
                    <select name="hari" class=" form-control border-input">
                    @foreach($enumHari as $d)
                    <option value="{{$d}}" {!! ($kuliah->hari == $d ? ' selected' : '') !!}>{{$d}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Jam Masuk:</label>
                <div class="col-sm-9">
                    <input type="time" class="form-control border-input" name="jammasuk" value="{{ $kuliah->jammasuk }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Jam Keluar:</label>
                <div class="col-sm-9">
                    <input type="time" class="form-control border-input" name="jamkeluar" value="{{ $kuliah->jamkeluar }}">
                </div>
            </div>

            <div class='form-group ui-widget'>
                <label class='col-sm-3 control-label'>Ruangan: </label>
                <div class='col-sm-9'>
                    <input type='text' id='ruangans' class='form-control border-input' placeholder='Cari Nama Ruangan' name='namaruangan' required value="{{ $kuliah->ruangan->nama }}">
               </div>
            </div>
            <input id="ruangans-id" type="hidden" name="idruangan" value="{{$kuliah->idruangan}}">

            <div class='form-group ui-widget'>
                <label class='col-sm-3 control-label'>Kelas Paralel: </label>
                <div class='col-sm-9'>
                    <input type='text' id='kelasparalels' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='namakelas' required value="{{$kuliah->kelas->kodemk}} {{$kuliah->kelas->mk->nama}} KP: {{$kuliah->kelas->kodekp}}">
               </div>
            </div>
            <input id="kelas-id" type="hidden" name="idkelasparalel" value="{{$kuliah->idkelasparalel}}">

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('paj.masterkuliah') }}" class="btn btn-danger"> Kembali</a>
                </div>
            </div>
            </form>
        </div>
    </div>
@endif

@endsection

@section('footer_scripts')
    <script>
    var dataruangan = [
        <?php foreach ($ruangan as $r){ ?>
            { value: "<?php echo $r->id;?>", label: "<?php echo $r->nama;?>" },
        <?php } ?>
        ];
    $(function() {
        $("#ruangans").autocomplete({
            source: dataruangan,
            focus: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox
                $(this).val(ui.item.label);
            },
            select: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox and hidden field
                $(this).val(ui.item.label);
                $("#ruangans-id").val(ui.item.value);
            }
        });
    });
    </script>

    <script>
    var datakelas = [
        <?php foreach ($kelas as $kp){ ?>
            { value: "<?php echo $kp->id;?>", label: "<?php echo $kp->mk->kodemk . " " . $kp->mk->nama . " KP: ". $kp->kodekp;?>" },
        <?php } ?>
        ];
    $(function() {
        $("#kelasparalels").autocomplete({
            source: datakelas,
            focus: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox
                $(this).val(ui.item.label);
            },
            select: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox and hidden field
                $(this).val(ui.item.label);
                $("#kelas-id").val(ui.item.value);
            }
        });
    });
    </script>
@endsection

