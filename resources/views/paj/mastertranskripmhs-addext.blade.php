@extends('paj._layouts.base')

@section('title', 'Master Transkrip Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Transkrip Sumber Extern</h4>

        <a href="{{URL::route('paj.mastertranskripmhs.storeext')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambahkan Semua</span>
          </button>
        </a>

    </div>
    <form class="form-inline">
    <div class="content">
      <table id="tabelTranskrip" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>NRP</th>
            <th>Kode MK</th>
            <th>Nisbi</th>
            <th>Semester</th>
            <th>Tahun</th>
          </tr>
        </thead>
        <tbody>
          @foreach($datas as $data)
            <tr>
              <td>{{ $data->nrp }}</a></td>
              <td>{{ $data->kodeMK }}</td>
              <td>{{ $data->nisbi }}</td>
              <td>{{ $data->semester }}</td>
              <td>{{ $data->tahun }}</td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>NRP</th>
            <th>Kode MK</th>
            <th>Nisbi</th>
            <th>Semester</th>
            <th>Tahun</th>
          </tr>
        </tfoot>
      </table>
    </div>
    </form>
</div>

@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelTranskrip").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection

