@extends('paj._layouts.base')

@section('title', 'Master Prasyarat')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<form class="form-horizontal" method="GET">
  <div class="form-group">
      <div class="visible-lg visible-md">
        <label class="col-md-3 control-label" style="margin-top: 5px;">Semester: </label>
      </div>
      <div class="col-xs-8 col-md-6">
          <select class="form-control border-input" name="ids">
              @foreach($semester as $s)
                <option value="{{$s->id}}" {!! ($ids == $s->id ? ' selected' : '') !!}>{{$s->tahunajaran}} {{ $s->semester }}</option>
              @endforeach
          </select>
      </div>
      <div class="col-xs-2 col-md-3">
        <button class="btn btn-success"><i class="fa fa-search"></i></button>
      </div>
  </div>
</form>
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Prasyarat</h4>
    
        <a href="{{URL::route('paj.masterprasyarat.add')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('paj.masterprasyarats')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('paj.masterprasyarat')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelPrasyarat" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th>Aksi</th>
          <th>MK</th> <!-- kurikulum sama ma-->
          <th>MK Syarat</th>
          <th>Nisbi</th>
        </tr>
        </thead>
        <tbody>
          @foreach($prasyarats as $prasyarat)
            <tr>
              <td>
              @if($prasyarat->deleted_at == null)
                <a href="{!! action('PrasyaratMkController@deleteDataPrasyarat', $prasyarat->id) !!}">Hapus</a> | 
                <a href="{!! action('PrasyaratMkController@editDataPrasyarat', $prasyarat->id) !!}">Edit</a>
              @else
                <a href="{!! action('PrasyaratMkController@restoreDataPrasyarat', $prasyarat->id) !!}">Aktifkan</a>|
                <a href="{!! action('PrasyaratMkController@forcedeleteDataPrasyarat', $prasyarat->id) !!}">Hapus Lagi</a>
              @endif
              </td>
              <td>{{ $prasyarat->kodemk }}</td>
              <td>{{ $prasyarat->kodemksyarat }}</td>
              <td>{{ $prasyarat->nisbi }}</td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>Aksi</th>
          <th>MK</th> <!-- kurikulum sama ma-->
          <th>MK Syarat</th>
          <th>Nisbi</th>
        </tr>
        </tfoot>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelPrasyarat").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection