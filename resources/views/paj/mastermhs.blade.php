@extends('paj._layouts.base')

@section('title', 'Master Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')

<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Mahasiswa</h4>
    
        <a href="{{URL::route('paj.mastermhs.add')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('paj.mastermhss')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('paj.mastermhs')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelMhs" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead >
        <tr>
          <th>Aksi</th>
          <th width="20px">No</th>
          <th>NRP</th>
          <th>Nama</th>
          <th>Jurusan</th>
          <th>IPK tanpa E</th>
          <th>TOEFL</th>
          <th>Asisten</th>
          <th>Status</th>
          <th>SKS Maks Semester Depan</th>
          <th>Tahun Akademik Terima</th>
          <th>Semester Terima</th>
          <th>Tahun Akademik Akhir</th>
          <th>Semester Akhir</th>
          <th>Tanggal Status</th>
          <th>Status Lunas UPP</th>
          <th>IPK dengan E</th>
          <th>SKSKm dengan E</th>
          <th>SKSKm tanpa E</th>
          <th>Total BSS</th>
          <th>Total MSS</th>
          <th>Masa Studi</th>
        </tr>
        </thead>
        <tbody>
          @foreach($mahasiswas as $mhs)
            <tr>
              <td>
              @if($mhs->deleted_at == null)
                <a href="{!! action('MahasiswaController@deleteDataMahasiswa', $mhs->nrp) !!}">Hapus</a>
              @else
                <a href="{!! action('MahasiswaController@restoreDataMahasiswa', $mhs->nrp) !!}">Aktifkan</a> |
                <a href="{!! action('MahasiswaController@forcedeleteDataMahasiswa', $mhs->nrp) !!}">Hapus Lagi</a>
              @endif
              </td>
              <td>{{ $loop->iteration }}</td>
              <td><a href="{!! action('MahasiswaController@editDataMahasiswa', $mhs->nrp) !!}">{{ $mhs->nrp }}</a></td>
              <td>{{ $mhs->nama }}</td>
              <td>{{ $mhs->jurusan->Nama }}</td>
              <td>{{ $mhs->ipktanpae }}</td>
              <td>{{ $mhs->nilaitoefl }}</td>
              <td>
                @if($mhs->asisten == 0)
                  {{ "Tidak" }}
                @else
                  {{ "Ya" }}
                @endif
              </td>
              <td>{{ $mhs->kodestatus }}</td>
              <td>{{ $mhs->sksmaxdepan }}</td>
              <td>{{ $mhs->tahunakademikterima }}</td>
              <td>{{ $mhs->semesterterima }}</td>
              <td>{{ $mhs->tahunakademikakhir }}</td>
              <td>{{ $mhs->semesterakhir }}</td>
              <td>{{ $mhs->tglstatus }}</td>
              <td>{{ $mhs->statuslunasupp }}</td>
              <td>{{ $mhs->ipkdengane }}</td>
              <td>{{ $mhs->skskumdengane }}</td>
              <td>{{ $mhs->skskumtanpae }}</td>
              <td>{{ $mhs->totalbss }}</td>
              <td>{{ $mhs->totalmss }}</td>
              <td>{{ $mhs->masastudi }}</td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>Aksi</th>
          <th>No</th>
          <th>NRP</th>
          <th>Nama</th>
          <th>Jurusan</th>
          <th>IPK tanpa E</th>
          <th>TOEFL</th>
          <th>Asisten</th>
          <th>Status</th>
          <th>SKS Maks Semester Depan</th>
          <th>Tahun Akademik Terima</th>
          <th>Semester Terima</th>
          <th>Tahun Akademik Akhir</th>
          <th>Semester Akhir</th>
          <th>Tanggal Status</th>
          <th>Status Lunas UPP</th>
          <th>IPK dengan E</th>
          <th>SKSKm dengan E</th>
          <th>SKSKm tanpa E</th>
          <th>Total BSS</th>
          <th>Total MSS</th>
          <th>Masa Studi</th>
        </tr>
        </tfoot>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelMhs").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection