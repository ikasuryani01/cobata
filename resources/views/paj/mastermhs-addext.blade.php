@extends('paj._layouts.base')

@section('title', 'Master Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Mahasiswa Sumber Extern</h4>

        <a href="{{URL::route('paj.mastermhs.storeext')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambahkan Semua</span>
          </button>
        </a>

    </div>
    <form class="form-inline">
    <div class="content">
      <table id="tabelMahasiswa" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>NRP</th>
            <th>Nama</th>
            <th>Tahun Akademik Terima</th>
            <th>Semester Terima</th>
            <th>Jurusan</th>
            <th>Tahun Akademik Akhir</th>
            <th>Semester Akhir</th>
            <th>Kode Status</th>
            <th>Tanggal Status</th>
            <th>Status Lunas UPP</th>
            <th>IPK Dengan E</th>
            <th>IPK Tanpa E</th>
            <th>SKS Kum Dengan E</th>
            <th>SKS Kum Tanpa E</th>
            <th>IPS Akhir</th>
            <th>SKS Max Depan</th>
            <th>Total BSS</th>
            <th>Total MSS</th>
            <th>Masa Studi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($mhs as $m)
            <tr>
              <td>{{ $m->NRP }}</td>
              <td>{{ $m->Nama }}</td>
              <td>{{ $m->ThnAkademikTerima }}</td>
              <td>{{ $m->SemesterTerima }}</td>
              <td>{{ $m->IdJurusan }}</td>
              <td>{{ $m->ThnakademikAkhir }}</td>
              <td>{{ $m->SemesterAkhir }}</td>
              <td>{{ $m->KodeStatus }}</td>
              <td>{{ $m->TglStatus }}</td>
              <td>{{ $m->StatusLunasUPP }}</td>
              <td>{{ $m->IPKDenganE }}</td>
              <td>{{ $m->IPKTanpaE }}</td>
              <td>{{ $m->SKSKumDenganE }}</td>
              <td>{{ $m->SKSKumTanpaE }}</td>
              <td>{{ $m->IPSAkhir }}</td>
              <td>{{ $m->SksMaxDepan }}</td>
              <td>{{ $m->TotalBSS }}</td>
              <td>{{ $m->TotalMSS }}</td>
              <td>{{ $m->MasaStudi }}</td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>NRP</th>
            <th>Nama</th>
            <th>Tahun Akademik Terima</th>
            <th>Semester Terima</th>
            <th>Jurusan</th>
            <th>Tahun Akademik Akhir</th>
            <th>Semester Akhir</th>
            <th>Kode Status</th>
            <th>Tanggal Status</th>
            <th>Status Lunas UPP</th>
            <th>IPK Dengan E</th>
            <th>IPK Tanpa E</th>
            <th>SKS Kum Dengan E</th>
            <th>SKS Kum Tanpa E</th>
            <th>IPS Akhir</th>
            <th>SKS Max Depan</th>
            <th>Total BSS</th>
            <th>Total MSS</th>
            <th>Masa Studi</th>
          </tr>
        </tfoot>
      </table>
    </div>
    </form>
</div>

@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelMahasiswa").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection

