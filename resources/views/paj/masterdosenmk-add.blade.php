@extends('paj._layouts.base')

@section('title', 'Master Dosen Pengampu MK')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline;">Tambah Data</h4>
                <p>NB: Otomatis ditambahkan pada semester yang sedang aktif</p>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class='form-group ui-widget'>
                    <label class='col-sm-3 control-label'>Mata Kuliah: </label>
                    <div class='col-sm-9'>
                        <input type='text' id='kodemk' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='kodemk' required>
                   </div>
                </div>
                <div class='form-group ui-widget'>
                    <label class='col-sm-3 control-label'>Dosen: </label>
                    <div class='col-sm-9'>
                        <input type='text' id='npkdosen' class='form-control border-input' placeholder='Cari Nama/Kode Dosen' name='npkdosen' required>
                   </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>

    @elseif ($mode == "edit")
        <div class="card">
            <div class="header">
                <h4 class="title">Ubah Data Ujian</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Mata Kuliah Tersedia:</label>
                    <div class="col-sm-9">
                      <select name="kodemk" class="selectpicker form-control" data-live-search="true" title="Pilih Mata Kuliah">
                        @foreach($matakuliah as $mk)
                        <option value="{{ $mk->kodemk }}">{{ $mk->kodemk }} - {{ $mk->nama }}</option>
                        @endforeach
                        <option value="{{ $ujian->kodemk }}" selected>{{$ujian->kodemk}} - {{$ujian->mk->nama}}</option>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Minggu Ke: </label>
                    <div class="col-sm-9">
                        <input type="number" max="4" min="1" class="form-control border-input" name="mingguke" value="{{ $ujian->mingguke }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Hari: </label>
                    <div class="col-sm-9">
                        <select name="hari" class=" form-control border-input">
                        @foreach($enumHari as $d)
                        <option value="{{$d}}" {!! ($ujian->hari == $d ? ' selected' : '') !!}>{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jam Ke:</label>
                    <div class="col-sm-9">
                        <input type="number" max="4" min="1" class="form-control border-input" name="jamke" value="{{ $ujian->jamke }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endif



@endsection

@section('footer_scripts')
@if($mode == "add")
<script>
$( function() {
    var matakuliahs = [
        <?php foreach ($matakuliah as $mk){ ?>
            "<?php echo $mk->kodemk . " - " . $mk->nama;?>",
        <?php } ?>
    ];
    var dosens = [
        <?php foreach ($dosens as $d){ ?>
            "<?php echo $d->npk . " - " . $d->nama;?>",
        <?php } ?>
    ];
    $( "#kodemk" ).autocomplete({
      source: matakuliahs
    });
    $( "#npkdosen" ).autocomplete({
      source: dosens
    });
});
</script>
@endif
@endsection