@extends('paj._layouts.base')

@section('title', 'Master Prasyarat')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;

    }

    .input-group .bootstrap-select.form-control {
      z-index: inherit !important;
    }

</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline;">Tambah Prasyarat</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class='form-group ui-widget'>
                    <label class='col-sm-3 control-label'>MK: </label>
                    <div class='col-sm-9'>
                        <input type='text' id='kodemk' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='kodemk' required>
                    </div>
                </div>

                <div class='form-group ui-widget'>
                    <label class='col-sm-3 control-label'>MK Prasyarat: </label>
                    <div class='col-sm-9'>
                        <input type='text' id='kodemkp' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='kodemksyarat' required>
                   </div>
                </div>

                <div class="form-group">
                   <label class="col-sm-3 control-label">Nisbi Minimal: </label>
                   <div class="col-sm-9">
                       <select class="form-control" name="nisbi">
                           @foreach($enumNisbi as $d)
                            <option value="{{$d}}">{{$d}}</option>
                           @endforeach
                       </select>
                   </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12" align="center">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>

    @elseif ($mode == "edit")

    @endif



@endsection

@section('footer_scripts')
@if($mode == "add")
<script>
$( function() {
    var matakuliahs = [
        <?php foreach ($matakuliahs as $mk){ ?>
            "<?php echo $mk->kodemk . " - " . $mk->nama;?>",
        <?php } ?>
    ];
    $( "#kodemk" ).autocomplete({
      source: matakuliahs
    });
    $( "#kodemkp" ).autocomplete({
      source: matakuliahs
    });
});
</script>
@endif
@endsection