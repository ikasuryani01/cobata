@extends('paj._layouts.base')

@section('title', 'Master Konversi')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;

    }

    .input-group .bootstrap-select.form-control {
      z-index: inherit !important;
    }

</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline;">Tambah Konversi</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="form-group col-sm-6">
                    <label class="col-sm-5 control-label">Jumlah MK Lama: </label>
                    <div class="col-sm-6">
                        <input type="number" min="1" value="0" id="jmlmklama" class="form-control border-input" name="jmlmklama" value="{{ old('jmlmklama') }}" onchange="tambahmklama()">
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <label class="col-sm-5 control-label">Jumlah MK Baru: </label>
                    <div class="col-sm-6">
                        <input type="number" min="1" value="0" id="jmlmkbaru" class="form-control border-input" name="jmlmkbaru" value="{{ old('jmlmkbaru') }}" onchange="tambahmkbaru()">
                    </div>
                </div>
                <!-- <div class="clearfix"></div> -->
                <hr>
                
                <div class="row">
                    <div class="col-sm-6" id="tambahanmklama">
                        
                    </div>
                    <div class="col-sm-6" id="tambahanmkbaru">
                        
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Operand: </label>
                    <div class="col-sm-8">
                        <select name="operand" class="form-control">
                            @foreach($enumOperand as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kurikulum: </label>
                    <div class="col-sm-8">
                        <select name="kurikulum" class="form-control">
                            @foreach($enumKurikulum as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal Berlaku: </label>
                    <div class="col-sm-8">
                        <input type="date" class="form-control" name="tanggalberlaku">
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-12" align="center">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>

    @elseif ($mode == "edit")

    @endif



@endsection

@section('footer_scripts')
@if($mode == "add")
<script>
    function tambahmklama(argument) {
        var matakuliahs = [
            <?php foreach ($matakuliahs as $mk){ ?>
                "<?php echo $mk->kodemk . " - " . $mk->nama;?>",
            <?php } ?>
            "lalala",
        ];
        
        var jmlmklama = $("#jmlmklama").val();
        var jmlmkbaru = $("#jmlmkbaru").val();

        if((jmlmkbaru > 1 && jmlmklama == 0) || (jmlmklama > 1 && jmlmkbaru == 0) || jmlmkbaru == 1 || jmlmklama == 1 )
        {
            $("#tambahanmklama").empty(); 
            for (var i = 1; i <= jmlmklama; i++) {
                var element = "<div class='form-group ui-widget'>" +
                                    "<label class='col-sm-3 control-label'>MK Lama: </label>" +
                                    "<div class='col-sm-8'>" +
                                        "<input type='text' id='mklama-" + i + "' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='kodemklama-" + i + "' required>" +
                                   "</div>" +
                               "</div>";
                $("#tambahanmklama").append(element);
                $( "#mklama-" + i).autocomplete({
                  source: matakuliahs
                });
            }
        }
        else
        {
            alert("Tidak dapat menambahkan. Jumlah MK Lama atau Jumlah MK Baru harus = 1");
            $('#jmlmklama').val(1);
        }
        
    }

    function tambahmkbaru(argument) {
        var matakuliahs = [
            <?php foreach ($matakuliahs as $mk){ ?>
                "<?php echo $mk->kodemk . " - " . $mk->nama;?>",
            <?php } ?>
            "lalala",
        ];
        
        var jmlmklama = $("#jmlmklama").val();
        var jmlmkbaru = $("#jmlmkbaru").val();

        if((jmlmkbaru > 1 && jmlmklama == 0) || (jmlmklama > 1 && jmlmkbaru == 0) || jmlmkbaru == 1 || jmlmklama == 1 )
        {
            $("#tambahanmkbaru").empty(); 
            for (var i = 1; i <= jmlmkbaru; i++) {
                var element = "<div class='form-group ui-widget'>" +
                                    "<label class='col-sm-3 control-label'>MK baru: </label>" +
                                    "<div class='col-sm-8'>" +
                                        "<input type='text' id='mkbaru-" + i + "' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='kodemkbaru-" + i + "' required>" +
                                   "</div>" +
                               "</div>";
                $("#tambahanmkbaru").append(element);
                $( "#mkbaru-" + i).autocomplete({
                  source: matakuliahs
                });
            }
        }
        else
        {
            alert("Tidak dapat menambahkan. Jumlah MK Lama atau Jumlah MK Baru harus = 1");
            $('#jmlmkbaru').val(1);
        }
        
    }
</script>
@endif
@endsection