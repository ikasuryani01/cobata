@extends('paj._layouts.base')

@section('title', 'Master Karyawan')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')

<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Karyawan</h4>
    
        <a href="{{URL::route('paj.masterkaryawan.add')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('paj.masterkaryawans')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('paj.masterkaryawan')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelKaryawan" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th>Aksi</th>
          <th>NPK</th>
          <th>Nama</th>
          <th>Tipe</th>
          <th>Jenis</th>
          <th>Status</th>
          <th>Jaga Ujian</th>
          <th>Fakultas</th>
          <!-- <th>Satuan Kerja</th> -->
          <th>Kode Dosen</th>
        </tr>
        </thead>
        <tbody>
          @foreach($karyawans as $karyawan)
            <tr>
              <td>
              @if($karyawan->deleted_at == null)
                <a href="{!! action('KaryawanController@deleteDataKaryawan', $karyawan->npk) !!}">Hapus</a> | <a href="{!! action('KaryawanController@editDataKaryawan', $karyawan->npk) !!}">Ubah</a>
              @else
                <a href="{!! action('KaryawanController@restoreDataKaryawan', $karyawan->npk) !!}">Aktifkan</a> |
                <a href="{!! action('KaryawanController@forcedeleteDataKaryawan', $karyawan->npk) !!}">Hapus Lagi</a>
              @endif
              </td>
              <td>{{ $karyawan->npk }}</td>
              <td>{{ $karyawan->gelardepan }}{{ $karyawan->nama }}{{ $karyawan->gelarbelakang }}</td>
              <td>{{ $karyawan->tipe }}</td>
              <td>{{ $karyawan->jenis }}</td>
              <td>{{ $karyawan->idstatusaktif }}</td>
              <td>
                @if($karyawan->aktifjagaujian == 0)
                {{ "Tidak" }}
                @else
                {{ "Ya" }}
                @endif
              </td>
              <td>{{ $karyawan->fakultas->Nama }}</td>
              
              <td>{{ $karyawan->kodedosen }}</td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>Aksi</th>
          <th>NPK</th>
          <th>Nama</th>
          <th>Tipe</th>
          <th>Jenis</th>
          <th>Status</th>
          <th>Jaga Ujian</th>
          <th>Fakultas</th>
          <!-- <th>Satuan Kerja</th> -->
          <th>Kode Dosen</th>
        </tr>
        </tfoot>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKaryawan").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection