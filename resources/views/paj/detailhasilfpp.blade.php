@extends('paj._layouts.base')

@section('title', 'Detail Hasil FPP')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')

@if($mode == "show")
<div class="card">
  <div class="header">
    <h4 class="title" style="display: inline; line-height: 1.5em;">Hasil FPP {{$kp->mk->nama}} KP: {{$kp->kodekp}}</h4>
    <!--  editHasilFppbyId editHasilFppbyKelas -->
    <a href="{!! action('PajController@editHasilFppbyKelas', $datapendaftar[0]->idkelas) !!}">
      <button class="btn btn-info btn-sm" style="float: right;">
        <i class="fa fa-edit"></i>
        <span class="hidden-sm hidden-xs">Edit Hasil Perwalian Kelas Ini</span>
      </button>
    </a>

    @if($datapendaftar[0]->kp->cekStatusPublikasi())
    <a href="{!! action('PajController@tidakPublikasibyKelas', $datapendaftar[0]->idkelas) !!}">
      <button class="btn btn-danger btn-sm" style="float: right; margin-right: 5px;">
        <i class="fa fa-sun-o"></i>
        <span class="hidden-sm hidden-xs">Jangan Publikasi Kelas Ini</span>
      </button>
    </a>
    @else
    <a href="{!! action('PajController@publikasibyKelas', $datapendaftar[0]->idkelas) !!}">
      <button class="btn btn-success btn-sm" style="float: right; margin-right: 5px;">
        <i class="fa fa-sun-o"></i>
        <span class="hidden-sm hidden-xs">Publikasi Kelas Ini</span>
      </button>
    </a>
    
    @endif
  </div>
  
  <div class="content">
    <table id="tabelKonversi" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
      <thead>
      <tr>
        <th>No</th>
        <th>NRP</th>
        <th>Nama Mahasiswa</th>
        <th>Terima</th>
        <th>Alasan</th>
        <th>Publikasi</th>
        <th>Edit</th>
      </tr>
      </thead>
      <tbody>
        @foreach($datapendaftar as $data)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $data->nrp }}</a></td>
            <td>{{ $data->mahasiswa->nama }}</td>
            <td>{{ $data->status }}</td>
            <td>{{ $data->alasan }}</td>
            <td>{{ $data->statuspublikasi }}</td>
            <td><a href="{!! action('PajController@editHasilFppbyId', $data->id) !!}">Edit</a></td>
          </tr>
        @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>No</th>
        <th>NRP</th>
        <th>Nama Mahasiswa</th>
        <th>Terima</th>
        <th>Alasan</th>
        <th>Publikasi</th>
        <th>Edit</th>
      </tr>
      </tfoot>
    </table>
  </div>
</div>
@elseif($mode == "edit")
<div class="card">
  <div class="header">
    
  </div>
  <div class="content">
    
  </div>
</div>
@else($mode == "editsatu")
<div class="card">
  <div class="header">
    
  </div>
  <div class="content">
    
  </div>
</div>
@endif

@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKonversi").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection