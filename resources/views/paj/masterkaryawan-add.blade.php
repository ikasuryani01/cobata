@extends('paj._layouts.base')

@section('title', 'Master Karyawan')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Ubah Data Karyawan</h4>
                <!-- <a href="{{URL::route('paj.masterkaryawan.add')}}">
                  <button class="btn btn-warning" style="float: right;">
                    <i class="fa fa-database"></i>
                    <span class="hidden-sm hidden-xs">Ambil Data dari BAAK</span>
                  </button>
                </a>
                <br> -->
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">NPK: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="npk" value="{{ old('npk') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nama" value="{{ old('nama') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Gelar Depan:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="gelardepan" value="{{ old('gelardepan') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Gelar Belakang:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="gelarbelakang" value="{{ old('gelarbelakang') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="tipe">
                            @foreach($enumTipe as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="jenis">
                            @foreach($enumJenis as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Fakultas</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="idfakultas" readonly>
                            <option value="{{Auth::guard('karyawan')->user()->IdFakultas}}">{{Auth::guard('karyawan')->user()->fakultas->Nama}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jurusan</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="idjurusan" readonly>
                            <option value="{{Auth::guard('karyawan')->user()->getIdJurusan()}}">{{Auth::guard('karyawan')->user()->getJurusan()->Nama}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Aktif :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="idstatusaktif" value="{{ old('idstatusaktif') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jaga Ujian :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="aktifjagaujian">
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @elseif ($mode == "edit")
                <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Karyawan</h4>
                <br>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">NPK: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="npk" value="{{ $karyawan->npk }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">NIDN:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nidn" value="{{ $karyawan->nidn }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nama" value="{{ $karyawan->nama }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Gelar Depan:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="gelardepan" value="{{ $karyawan->gelardepan }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Gelar Belakang:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="gelarbelakang" value="{{ $karyawan->gelarbelakang }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="tipe">
                            @foreach($enumTipe as $d)
                                <option value="{{$d}}" {!! ($karyawan->tipe == $d ? ' selected' : '') !!}>{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="jenis">
                            @foreach($enumJenis as $d)
                                <option value="{{$d}}" {!! ($karyawan->jenis == $d ? ' selected' : '') !!}>{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Fakultas</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="idfakultas">
                            @foreach($dataFakultas as $d)
                                <option value="{{$d->IdFakultas}}" {!! ($karyawan->idfakultas == $d->IdFakultas ? ' selected' : '') !!}>{{$d->Nama}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Aktif :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="idstatusaktif" value="{{ $karyawan->idstatusaktif }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jaga Ujian :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="aktifjagaujian">
                            <option value="1" {!! ($karyawan->aktifjagaujian == 1 ? ' selected' : '') !!}>Ya</option>
                            <option value="0" {!! ($karyawan->aktifjagaujian == 0 ? ' selected' : '') !!}>Tidak</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endif
@endsection

