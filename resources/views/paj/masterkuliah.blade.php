@extends('paj._layouts.base')

@section('title', 'Master Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<form class="form-horizontal" method="GET">
  <div class="form-group">
      <div class="visible-lg visible-md">
        <label class="col-md-3 control-label" style="margin-top: 5px;">Semester: </label>
      </div>
      <div class="col-xs-8 col-md-6">
          <select class="form-control border-input" name="ids">
              @foreach($semester as $s)
                <option value="{{$s->id}}" {!! ($ids == $s->id ? ' selected' : '') !!}>{{$s->tahunajaran}} {{ $s->semester }}</option>
              @endforeach
          </select>
      </div>
      <div class="col-xs-2 col-md-3">
        <button class="btn btn-success"><i class="fa fa-search"></i></button>
      </div>
  </div>
</form>
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Kuliah</h4>
    
        <a href="{{URL::route('paj.masterkuliah.add')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('paj.masterkuliahs')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('paj.masterkuliah')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelKuliah" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th>Aksi</th>
          <th>Hari</th>
          <th>Jam Masuk</th>
          <th>Jam Keluar</th>
          <th>Nama Kelas</th>
          <th>Ruangan</th>
        </tr>
        </thead>
        <tbody>
          @foreach($kuliahs as $kuliah)
            <tr>
              <td>
              @if($kuliah->deleted_at == null)
                <a href="{!! action('JadwalKuliahController@deleteDataKuliah', $kuliah->id) !!}" onclick="return confirm('Yakin akan menghapus data?')">Hapus</a> | 
                <a href="{!! action('JadwalKuliahController@editDataKuliah', $kuliah->id) !!}">Edit</a>
              @else
                <a href="{!! action('JadwalKuliahController@restoreDataKuliah', $kuliah->id) !!}">Batalkan hapus</a> |
                <a href="{!! action('JadwalKuliahController@forcedeleteDataKuliah', $kuliah->id) !!}" onclick="return confirm('Yakin akan benar-benar menghapus data?')">Hapus Lagi</a>
              @endif
              </td>
              <td>{{ $kuliah->hari }}</td>
              <td>{{ $kuliah->jammasuk }}</td>
              <td>{{ $kuliah->jamkeluar }}</td>
              <td>{{ $kuliah->kelas->mk->nama }} - {{ $kuliah->kelas->kodekp}}</td>
              <td>{{ $kuliah->ruangan->nama }}</td>
              
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>Aksi</th>
          <th>Hari</th>
          <th>Jam Masuk</th>
          <th>Jam Keluar</th>
          <th>Nama Kelas</th>
          <th>Ruangan</th>
        </tr>
        </tfoot>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKuliah").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[1,'desc'], [2, 'asc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection