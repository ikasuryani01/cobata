@extends('paj._layouts.base')

@section('title', 'Master Transkrip Mahasiwa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Transkrip</h4>

                <a href="{{URL::route('paj.mastertranskripmhs.addext')}}">
                  <button class="btn btn-warning" style="float: right;">
                    <i class="fa fa-database"></i>
                    <span class="hidden-sm hidden-xs">Ambil Data dari BAAK</span>
                  </button>
                </a>
                <br>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kode MK: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="kodemk" value="{{ old('kodemk') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">NRP: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nrpmhs" value="{{ old('nrpmhs')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nisbi: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nisbi" value="{{ old('nisbi') }}">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="idsemester">
                            @foreach($dataSemester as $d)
                                <option value="{{$d->id}}"> {{ $d->semester }} {{ $d->tahun }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>

    @elseif ($mode == "edit")
        <div class="card">
            <div class="header">
                <h4 class="title">Ubah Data Mata Kuliah</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kode MK: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="kodemk" value="{{ $transkrip->kodemk }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">NRP: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nrp" value="{{ $transkrip->nrpmhs }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nisbi: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nisbi" value="{{ $transkrip->nisbi }}">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="idsemester">
                            @foreach($dataSemester as $d)
                                <option value="{{$d->id}}" {!! ($transkrip->idsemester == $d->id ? ' selected' : '') !!}>{{ $d->semester }} {{ $d->tahunajaran }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endif
@endsection

