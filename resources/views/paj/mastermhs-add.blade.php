@extends('paj._layouts.base')

@section('title', 'Master Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.5em;">Tambah Mahasiswa</h4>

                <a href="{{URL::route('paj.mastermhs.addext')}}">
                  <button class="btn btn-sm btn-warning" style="float: right;">
                    <i class="fa fa-database"></i>
                    <span class="hidden-sm hidden-xs">Ambil Data dari BAAK</span>
                  </button>
                </a>
                <br>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">NRP: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nrp" value="{{ old('nrp') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Lengkap: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nama" value="{{ old('nama')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Akademik Terima :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="tahunakademikterima" value="{{ old('tahunakademikterima') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester Terima :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="semesterterima">
                            @foreach($enumSemester as $d)
                                <option value="{{$d}}">{{ $d }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jurusan</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="idjurusan">
                            @foreach($dataJurusan as $d)
                                <option value="{{$d->IdJurusan}}">{{ $d->Nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Akademik Akhir :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="tahunakademikakhir" value="{{ old('tahunakademikakhir') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester Akhir :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="semesterakhir">
                            @foreach($enumSemester as $d)
                                <option value="{{$d}}">{{ $d }}</option>
                            @endforeach
                        </select>                    
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="kodestatus" value="{{ old('kodestatus') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Lunas UPP :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="statuslunasupp">
                            <option value="Y">Ya</option>
                            <option value="T">Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">IPK dengan E :</label>
                    <div class="col-sm-9">
                        <input type="number"  class="form-control border-input" name="ipkdengane" value="{{ old('ipkdengane') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">IPK tanpa E :</label>
                    <div class="col-sm-9">
                        <input type="number"  class="form-control border-input" name="ipktanpae" value="{{ old('ipktanpae') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Kum dengan E :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="skskumdengane" value="{{ old('skskumdengane') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Kum tanpa E :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="skskumtanpae" value="{{ old('skskumtanpae') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">IPS akhir :</label>
                    <div class="col-sm-9">
                        <input type="number"  class="form-control border-input" name="ipsakhir" value="{{ old('ipsakhir') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Max depan :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="sksmaxdepan" value="{{ old('sksmaxdepan') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Total BSS :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="totalbss" value="{{ old('totalbss') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Total MSS :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="totalmss" value="{{ old('totalmss') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Masa Studi :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="masastudi" value="{{ old('masastudi') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nilai TOEFL :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="nilaitoefl" value="{{ old('nilaitoefl') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Asisten :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="asisten">
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @elseif ($mode == "edit")
        <div class="card">
            <div class="header">
                <h4 class="title">Ubah Data Mahasiswa</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">NRP: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nrp" value="{{ $mahasiswa->nrp }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Lengkap: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nama" value="{{ $mahasiswa->nama }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Akademik Terima :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="tahunakademikterima" value="{{ $mahasiswa->tahunakademikterima }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester Terima :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="semesterterima">
                            @foreach($enumSemester as $d)
                                <option value="{{$d}}" {!! ($mahasiswa->semesterterima == $d ? ' selected' : '') !!}>{{ $d }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jurusan</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="idjurusan">
                            @foreach($dataJurusan as $d)
                                <option value="{{$d->IdJurusan}}" {!! ($mahasiswa->idjurusan == $d->IdJurusan ? ' selected' : '') !!}>{{ $d }}>{{ $d->Nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Akademik Akhir :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="tahunakademikakhir" value="{{ $mahasiswa->tahunakademikakhir }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester Akhir :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="semesterakhir">
                            @foreach($enumSemester as $d)
                                <option value="{{$d}}" {!! ($mahasiswa->semesterakhir == $d ? ' selected' : '') !!}>{{ $d }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="kodestatus" value="{{ $mahasiswa->kodestatus }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Lunas UPP :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="statuslunasupp">
                            <option value="Y" {!! ($mahasiswa->statuslunasupp == "Y" ? ' selected' : '') !!}>Ya</option>
                            <option value="T" {!! ($mahasiswa->statuslunasupp == "T" ? ' selected' : '') !!}>Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">IPK dengan E :</label>
                    <div class="col-sm-9">
                        <input type="number"  class="form-control border-input" name="ipkdengane" value="{{ $mahasiswa->ipkdengane }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">IPK tanpa E :</label>
                    <div class="col-sm-9">
                        <input type="number"  class="form-control border-input" name="ipktanpae" value="{{ $mahasiswa->ipktanpae }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Kum dengan E :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="skskumdengane" value="{{ $mahasiswa->skskumdengane }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Kum tanpa E :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="skskumtanpae" value="{{ $mahasiswa->skskumtanpae }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">IPS akhir :</label>
                    <div class="col-sm-9">
                        <input type="number"  class="form-control border-input" name="ipsakhir" value="{{ $mahasiswa->ipsakhir }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Max depan :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="sksmaxdepan" value="{{ $mahasiswa->sksmaxdepan }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Total BSS :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="totalbss" value="{{ $mahasiswa->totalbss }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Total MSS :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="totalmss" value="{{ $mahasiswa->totalmss }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Masa Studi :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="masastudi" value="{{ $mahasiswa->masastudi }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nilai TOEFL :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="nilaitoefl" value="{{ $mahasiswa->nilaitoefl }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Asisten :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="asisten">
                            <option value="1" {!! ($mahasiswa->asisten == "1" ? ' selected' : '') !!}>Ya</option>
                            <option value="0" {!! ($mahasiswa->asisten == "0" ? ' selected' : '') !!}>Tidak</option>
                        </select>
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endif



@endsection

