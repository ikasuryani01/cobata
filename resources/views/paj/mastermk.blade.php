@extends('paj._layouts.base')

@section('title', 'Master Mata Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Mata Kuliah </h4>
      
        <a href="{{ route('paj.mastermk.aktifkan')}}">
          <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-star"></i>
            <span class="hidden-sm hidden-xs">Buka Semua MK</span>
          </button>
        </a>

        <a href="{{URL::route('paj.mastermk.add')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{ route('paj.mastermks')}}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{route('paj.mastermk')}}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelMataKuliah" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>Aksi</th>
            <th>Kode MK</th>
            <th>Nama</th>
            <th>SKS</th>
            <th>Jenis</th>
            <th>Buka</th>
            <th>Tipe</th>
            <th>SKS Minimal</th>
            <th>Flag Skripsi</th>
            <th>Semester</th>
            <th>Kurikulum</th>
            <th>Nama Eng</th>

          </tr>
        </thead>
        <tbody>
          @foreach($mks as $mk)
            <tr>
              <td>
              @if($mk->deleted_at == null)
                <a href="{{url('paj/mastermk/h-'.$mk->kodemk)}}" onclick="return confirm('Yakin akan menghapus data?')">Hapus</a> |
                <a href="{{url('paj/mastermk/edit-'.$mk->kodemk)}}">Ubah</a>
              @else
                <a href="{{url('paj/mastermk/r-'.$mk->kodemk)}}">Batalkan hapus</a> |
                <a href="{{url('paj/mastermk/f-'.$mk->kodemk)}}" onclick="return confirm('Yakin akan benar-benar menghapus data?')">Hapus Permanen</a>
              @endif
              </td>
              <td>{{ $mk->kodemk }}</td>
              <td>{{ $mk->nama }}</td>
              <td>{{ $mk->sks }}</td>
              <td>{{ $mk->jenis }}</td>
              <td>
                @if($mk->statusbuka == "Ya") <a href="{{ url('paj/mastermk/a-' . $mk->kodemk)}}" onclick="return confirm('Ubah status menjadi Tidak?')">{{ $mk->statusbuka }}</a> 
                @else <a href="{{ url('paj/mastermk/a-' . $mk->kodemk)}}" onclick="return confirm('Ubah status menjadi Ya?')">{{ $mk->statusbuka }}</a>
                @endif
              </td>
              <td>{{ $mk->tipe }}</td>
              <td>{{ $mk->getSKSminimal(Auth::guard('karyawan')->user()->getJurusan()->IdJurusan) }}</td>
              <td>{{ $mk->flagskripsi }}</td>
              <td>{{ $mk->getSemester(Auth::guard('karyawan')->user()->getJurusan()->IdJurusan) }}</td>
              <td>{{ $mk->getKurikulum(Auth::guard('karyawan')->user()->getJurusan()->IdJurusan) }}</td>
              <td>{{ $mk->namaeng }}</td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>Aksi</th>
            <th>Kode MK</th>
            <th>Nama</th>
            <th>SKS</th>
            <th>Jenis</th>
            <th>Buka</th>
            <th>Tipe</th>
            <th>SKS Minimal</th>
            <th>Flag Skripsi</th>
            <th>Semester</th>
            <th>Kurikulum</th>
            <th>Nama Eng</th>
          </tr>
        </tfoot>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelMataKuliah").dataTable( {
      "pagingType": "full",
      "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
      "stateSave": true,
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection