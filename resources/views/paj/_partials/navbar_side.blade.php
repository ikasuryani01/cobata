<ul class="nav">
    <li {!! (Request::is('paj') ? ' class="active"' : '') !!}>
        <a href="{{ route('paj.beranda') }}">
            <i class="ti-home"></i>
            <p>Beranda</p>
        </a>
    </li>

    <li 
      {!! (Request::is('paj/mastermk*') ? ' class="dropdown active"' : '') !!}
      {!! (Request::is('paj/masterdosenmk*') ? ' class="dropdown active"' : '') !!}
      {!! (Request::is('paj/masterkonversi*') ? ' class="dropdown active"' : '') !!}
      {!! (Request::is('paj/masterprasyarat*') ? ' class="dropdown active"' : '') !!}
      {!! (Request::is('paj/mastersettingnrp*') ? ' class="dropdown active"' : '') !!} >
      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="ti-blackboard"></i>
          <p>Data Mata Kuliah &nbsp; <b class="caret"></b></p>
      <ul class="dropdown-menu" role="menu" style="margin-left: 20px !important;"></a>
      <li class="dropdown-header">Submenu</li>
          <li><a href="{{ route('paj.mastermk') }}">Detail Mata Kuliah</a></li>
          <li><a href="{{ route('paj.masterdosenmk') }}">Data Dosen Pengampu</a></li>
          <li><a href="{{ route('paj.masterkonversi') }}">Konversi MK</a></li>
          <li><a href="{{ route('paj.masterprasyarat') }}">Data Prasyarat</a></li>
          <li><a href="{{ route('paj.mastersettingnrp')}}">Setting NRP</a></li>
      </ul>
    </li>

    <li {!! (Request::is('paj/mastermhs*') ? ' class="active"' : '') !!}>
      <a href="{{ route('paj.mastermhs') }}">
        <i class="ti-bookmark-alt"></i>
        <p>Data Mahasiswa</p>
      </a>
    </li>

    <li {!! (Request::is('paj/masterkaryawan*') ? ' class="active"' : '') !!}>
        <a href="{{ route('paj.masterkaryawan') }}">
            <i class="ti-bookmark-alt"></i>
            <p>Data Karyawan</p>
        </a>
    </li>

    <li {!! (Request::is('paj/masterkp*') ? ' class="active"' : '') !!}>
        <a href="{{ route('paj.masterkp') }}">
            <i class="ti-bag"></i>
            <p>Data Kelas Paralel</p>
        </a>
    </li>

    <li {!! (Request::is('paj/masterujian*') ? ' class="active"' : '') !!}>
        <a href="{{ route('paj.masterujian') }}">
            <i class="ti-medall-alt"></i>
            <p>Jadwal Ujian</p>
        </a>
    </li>

    <li {!! (Request::is('paj/masterkuliah*') ? ' class="active"' : '') !!}>
        <a href="{{ route('paj.masterkuliah') }}">
            <i class="ti-map"></i>
            <p>Jadwal Kuliah</p>
        </a>
    </li>

    <!-- <li>
      <a href="{{ route('paj.mastermk') }}">
        <i class="ti-pencil-alt"></i>
        <p>Data Penjaga Ujian</p>
      </a>
    </li> -->

    <!-- <li {!! (Request::is('paj/mastertranskripmhs*') ? ' class="active"' : '') !!}>
        <a href="{{ route('paj.mastertranskripmhs') }}">
            <i class="ti-medall-alt"></i>
            <p>Data Transkrip</p>
        </a>
    </li>

    <li {!! (Request::is('paj/laporan*') ? ' class="active"' : '') !!}>
        <a href="{{ route('paj.laporanmenu') }}">
            <i class="fa fa-code"></i>
            <p>Laporan</p>
        </a>
    </li> -->

</ul>