<div class="container-fluid">
    <nav class="pull-left">
        <ul>

            <li>
                <a href="http://www.ubaya.ac.id">
                    UBAYA
                </a>
            </li>
            <li>
                <a href="http://perwalianft.ubaya.ac.id">
                   Perwalian Fakultas Teknik
                </a>
            </li>
        </ul>
    </nav>
    <div class="copyright pull-right">
        &copy; <script>document.write(new Date().getFullYear())</script>, by 6134087
    </div>
</div>