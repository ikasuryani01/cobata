@extends('paj._layouts.base')

@section('title', 'Master Transkrip Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Transkrip Mahasiswa </h4>
      
        <a href="{{URL::route('paj.mastertranskripmhs.add')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{!! action('TranskripController@showDataTranskripmhss') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{!! action('TranskripController@showDataTranskripmhs') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelTranskrip" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>Aksi</th>
            <th width="20px">No</th>
            <th>Kode MK</th>
            <th>Nama MK</th>
            <th>NRP</th>
            <th>Nama Mahasiswa</th>
            <th>Nisbi</th>
            <th>Semester</th>
          </tr>
        </thead>
        <tbody>
          @foreach($transkrips as $t)
            <tr>
              <td>
              @if($t->deleted_at == null)
                <a href="{!! action('TranskripController@deleteDataTranskripmhs', $t->id) !!}">Hapus</a>
              @else
                <a href="{!! action('TranskripController@restoreDataTranskripmhs', $t->id) !!}">Aktifkan</a> |
                <a href="{!! action('TranskripController@forcedeleteDataTranskripmhs', $t->id) !!}">Hapus Lagi</a>
              @endif
              </td>
              <td>{{ $loop->iteration }}</td>
              <td><a href="{!! action('TranskripController@editDataTranskripmhs', $t->id) !!}">{{$t->kodemk}}</a></td>
              <td>{{ $t->mk->nama }}</td>
              <td>{{ $t->nrpmhs }}</td>
              <td>{{ $t->mhs->nama }}</td>
              <td>{{ $t->nisbi }}</td>
              <td>{{ $t->semester->semester }} {{ $t->semester->tahunajaran }}</td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>Aksi</th>
            <th width="20px">No</th>
            <th>Kode MK</th>
            <th>Nama MK</th>
            <th>NRP</th>
            <th>Nama Mahasiswa</th>
            <th>Nisbi</th>
            <th>Semester</th>
          </tr>
        </tfoot>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelTranskrip").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection