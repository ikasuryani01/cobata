@extends('paj._layouts.base')

@section('title', 'Master Mata Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
@if($mode == "add")
    <div class="card">
        <div class="header">
            <h4 class="title" style="display: inline; line-height: 1.5em;">Tambah Mata Kuliah</h4>

            <a href="{{URL::route('paj.mastermk.addext')}}">
              <button class="btn btn-sm btn-warning" style="float: right;">
                <i class="fa fa-database"></i>
                <span class="hidden-sm hidden-xs">Ambil Data Eksternal</span>
              </button>
            </a>
            <br>
        </div>
        <div class="content">
            <form class="form-horizontal" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label class="col-sm-3 control-label">Kode MK: </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control border-input" name="kodemk" value="{{ old('kodemk') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nama: </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control border-input" name="nama" value="{{ old('nama')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nama dalam bahasa Inggris: </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control border-input" name="namaeng" value="{{ old('namaeng')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">SKS:</label>
                <div class="col-sm-9">
                    <input type="number" class="form-control border-input" name="sks" value="{{ old('sks') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Jenis</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="jenis">
                        @foreach($enumJenis as $d)
                        <option value="{{$d}}">{{$d}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Flag Skripsi:</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="flagskripsi">
                        <option value="Ya">Ya</option>
                        <option value="Tidak">Tidak</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tipe:</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="tipe">
                        @foreach($enumTipe as $d)
                            <option value="{{$d}}">{{ $d }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Status Buka:</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="statusbuka">
                        <option value="Ya">Buka</option>
                        <option value="Tidak">Tutup</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-3 control-label">Jurusan:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control border-input" name="jurusan" readonly value="{{ Auth::guard('karyawan')->user()->getJurusan()->Nama }}">
                </div>
            </div>
            <input type="hidden" name="idjurusan" value="{{ Auth::guard('karyawan')->user()->getJurusan()->IdJurusan }}">

            <div class="form-group">
                <label class="col-sm-3 control-label">Semester diambil mahasiswa:</label>
                <div class="col-sm-9">
                    <input type="number" class="form-control border-input" name="semester" value="{{ old('semester') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">SKS minimal:</label>
                <div class="col-sm-9">
                    <input type="number" class="form-control border-input" name="sksminimal" value="{{ old('sksminimal') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Kurikulum:</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="kurikulum">
                        @foreach($enumKurikulum as $d)
                            <option value="{{$d}}">{{ $d }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('paj.mastermk') }}" class="btn btn-danger"> Kembali</a>
                </div>
            </div>
            </form>
        </div>
    </div>

@elseif ($mode == "edit")
    <div class="card">
        <div class="header">
            <h4 class="title">Ubah Data Mata Kuliah</h4>
        </div>
        <div class="content">
            <form class="form-horizontal" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label class="col-sm-3 control-label">Kode MK: </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control border-input" name="kodemk" value="{{ $mk->kodemk }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nama: </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control border-input" name="nama" value="{{ $mk->nama }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nama dalam bahasa Inggris: </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control border-input" name="namaeng" value="{{ $mk->namaeng }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">SKS:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control border-input" name="sks" value="{{ $mk->sks  }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Jenis</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="jenis">
                        @foreach($enumJenis as $d)
                        <option value="{{$d}}" {!! ($mk->jenis == $d ? ' selected' : '') !!}>Wajib</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Flag Skripsi:</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="flagskripsi">
                        <option value="Ya" {!! ($mk->flagskripsi == "Ya" ? ' selected' : '') !!}>Ya</option>
                        <option value="Tidak" {!! ($mk->flagskripsi == "Tidak" ? ' selected' : '') !!}>Tidak</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tipe:</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="tipe">
                        @foreach($enumTipe as $d)
                            <option value="{{$d}}" {!! ($mk->tipe == $d ? ' selected' : '') !!}>{{ $d }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Status Buka:</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="statusbuka">
                        <option value="Ya" {!! ($mk->statusbuka == "Ya" ? ' selected' : '') !!}>Buka</option>
                        <option value="Tidak" {!! ($mk->statusbuka == "Tidak" ? ' selected' : '') !!}>Tutup</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-3 control-label">Jurusan:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control border-input" name="jurusan" readonly value="{{ Auth::guard('karyawan')->user()->getJurusan()->Nama }}">
                </div>
            </div>
            <input type="hidden" name="idjurusan" value="{{ Auth::guard('karyawan')->user()->getJurusan()->IdJurusan }}">

            <div class="form-group">
                <label class="col-sm-3 control-label">Semester diambil mahasiswa:</label>
                <div class="col-sm-9">
                    <input type="number" class="form-control border-input" name="semester" value="{{ $mk->getSemester(Auth::guard('karyawan')->user()->getJurusan()->IdJurusan) }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">SKS minimal:</label>
                <div class="col-sm-9">
                    <input type="number" class="form-control border-input" name="sksminimal" value="{{ $mk->getSKSminimal(Auth::guard('karyawan')->user()->getJurusan()->IdJurusan) }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Kurikulum:</label>
                <div class="col-sm-9">
                    <select class="form-control border-input" name="kurikulum">
                        @foreach($enumKurikulum as $d)
                            <option value="{{$d}}" {!! ($mk->getKurikulum(Auth::guard('karyawan')->user()->getJurusan()->IdJurusan) == $d ? ' selected' : '') !!}>{{ $d }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('paj.mastermk') }}" class="btn btn-danger"> Kembali</a>
                </div>
            </div>
            </form>
        </div>
    </div>
@endif



@endsection

