@extends('maf._layouts.base')

@section('title', 'Daftar Presensi Ujian')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Ujian {{ $mk->nama }} ({{ $mk->kodemk}})</h4>

        <div class="btn-group" style="float: right;" align="right">
          <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right;">
            <i class="fa fa-edit"></i>
            <span class="">Ubah</span>
          </button>
          <ul class="dropdown-menu" style="left: -80px;">
            <li>
              <a href="{!! action('RuanganUjianController@cariRuanganLain', $idjadwalujian) !!}">Ubah Ruangan Ujian</a>
            </li>
            <li>
              <a href="{!! action('RuanganUjianController@cariPenjagaLain', $idjadwalujian) !!}">Ubah Penjaga Ujian</a>
            </li>
          </ul>
        </div>

    </div>
    <div class="content">
      <table id="tabelUjian" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>

          <th>No</th>
          <th>Kode KP</th>
          <th>Ruangan</th>
          <th>Dosen Jaga</th>
          <th>Karyawan Jaga</th>
          <th>NRP</th>

        </tr>
        </thead>
        <tbody>
          @if(!empty($datas))
          @foreach($datas as $data)
            <tr>

              <td>{{ $loop->iteration }}</td>
              <td>{{ $data->kp->kodekp }}</td>
              <td>@if($data->idruangan != null) {{ $data->ruangan->nama }} @else {{ "Belum Ditentukan" }}@endif</td>
              <td>@if($data->npkdosenjaga != null)
                   {{ $data->npkdosenjaga }} {{ $data->dosenjaga->nama }} 
                  @else 
                    {{ "Belum Ditentukan" }}
                  @endif
              </td>
              <td>@if($data->npkkaryawanjaga != null) 
                    {{ $data->karyawanjaga->npk }} {{ $data->karyawanjaga->nama }} 
                  @else 
                    {{ "Belum Ditentukan" }}
                  @endif
              </td>
              <td>{{ $data->nrpawal }} - {{ $data->nrpakhir }} ({{ $data->jumlahpeserta }})</td>

            </tr>
          @endforeach
          @endif
        </tbody>
        <tfoot>
        <tr>
          
          <th>No</th>
          <th>Kode KP</th>
          <th>Ruangan</th>
          <th>Dosen Jaga</th>
          <th>Karyawan Jaga</th>
          <th>NRP</th>

        </tr>
        </tfoot>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelUjian").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection