@extends('maf._layouts.base')

@section('title', 'Master Jadwal Ujian')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline;">Tambah Ujian</h4>
                <p>NB: Otomatis ditambahkan pada semester yang sedang aktif</p>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class='form-group ui-widget'>
                    <label class='col-sm-3 control-label'>Mata Kuliah: </label>
                    <div class='col-sm-9'>
                        <input type='text' id='mks' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='kodemk' required>
                   </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Minggu Ke: </label>
                    <div class="col-sm-9">
                        <input type="number" max="4" min="1" class="form-control border-input" name="mingguke" value="{{ old('mingguke')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Hari: </label>
                    <div class="col-sm-9">
                        <select name="hari" class=" form-control border-input">
                        @foreach($enumHari as $d)
                        <option value="{{$d}}">{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jam Ke:</label>
                    <div class="col-sm-9">
                        <input type="number" max="4" min="1" class="form-control border-input" name="jamke" value="{{ old('jamke') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe Ruangan UTS: </label>
                    <div class="col-sm-9">
                        <select name="tiperuanganuts" class=" form-control border-input">
                        @foreach($enumtiperuanganuts as $d)
                        <option value="{{$d}}">{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe Ujian UTS: </label>
                    <div class="col-sm-9">
                        <select name="tipeujianuts" class=" form-control border-input">
                        @foreach($enumtipeujianuts as $d)
                        <option value="{{$d}}">{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe Ruangan UAS: </label>
                    <div class="col-sm-9">
                        <select name="tiperuanganuas" class=" form-control border-input">
                        @foreach($enumtiperuanganuas as $d)
                        <option value="{{$d}}">{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe Ujian UAS: </label>
                    <div class="col-sm-9">
                        <select name="tipeujianuas" class=" form-control border-input">
                        @foreach($enumtipeujianuas as $d)
                        <option value="{{$d}}">{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ route('paj.masterujian') }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>

    @elseif ($mode == "edit")
        <div class="card">
            <div class="header">
                <h4 class="title">Ubah Data Ujian</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Mata Kuliah:</label>
                    <div class="col-sm-9">
                      <input type="text" name="namamk" class=" form-control" readonly value="{{$ujian->kodemk}} - {{$ujian->mk->nama}}">
                      <input type="hidden" name="kodemk" value="{{$ujian->kodemk}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Minggu Ke: </label>
                    <div class="col-sm-9">
                        <input type="number" max="4" min="1" class="form-control border-input" name="mingguke" value="{{ $ujian->mingguke }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Hari: </label>
                    <div class="col-sm-9">
                        <select name="hari" class=" form-control border-input">
                        @foreach($enumHari as $d)
                        <option value="{{$d}}" {!! ($ujian->hari == $d ? ' selected' : '') !!}>{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jam Ke:</label>
                    <div class="col-sm-9">
                        <input type="number" max="4" min="1" class="form-control border-input" name="jamke" value="{{ $ujian->jamke }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe Ruangan UTS: </label>
                    <div class="col-sm-9">
                        <select name="tiperuanganuts" class=" form-control border-input">
                        @foreach($enumtiperuanganuts as $d)
                        <option value="{{$d}}" {!! ($ujian->tiperuanganuts == $d ? ' selected' : '') !!}>{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe Ujian UTS: </label>
                    <div class="col-sm-9">
                        <select name="tipeujianuts" class=" form-control border-input">
                        @foreach($enumtipeujianuts as $d)
                        <option value="{{$d}}" {!! ($ujian->tipeujianuts == $d ? ' selected' : '') !!}>{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe Ruangan UAS: </label>
                    <div class="col-sm-9">
                        <select name="tiperuanganuas" class=" form-control border-input">
                        @foreach($enumtiperuanganuas as $d)
                        <option value="{{$d}}" {!! ($ujian->tiperuanganuas == $d ? ' selected' : '') !!}>{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipe Ujian UAS: </label>
                    <div class="col-sm-9">
                        <select name="tipeujianuas" class=" form-control border-input">
                        @foreach($enumtipeujianuas as $d)
                        <option value="{{$d}}" {!! ($ujian->tipeujianuas == $d ? ' selected' : '') !!}>{{$d}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endif
@endsection

@section('footer_scripts')
@if($mode == "add")
<script>
$( function() {
    var mks = [
        <?php foreach ($matakuliah as $mk){ ?>
            "<?php echo $mk->kodemk . " " . $mk->nama;?>",
        <?php } ?>
    ];
    $( "#mks" ).autocomplete({
      source: mks
    });
});
</script>
@endif
@endsection

