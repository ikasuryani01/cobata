@extends('maf._layouts.base')

@section('title', 'Daftar Jadwal Ujian')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
    .modal-dialog{
        margin-top: 100px !important;
        /*margin-left: 500px !important;*/
    }
</style>
@endsection

@section('content')
<center>
<form class="form-horizontal" method="GET">
  <div class="form-group">
      <div class="visible-lg visible-md">
        <label class="col-md-4 control-label" style="margin-top: 5px;">Tampilkan: </label>
      </div>
      <div class="col-xs-8 col-md-5">
          <select class="form-control border-input" name="ids">
              <option value="mk" {!! ($ids == "mk" ? ' selected' : '') !!}>Mata Kuliah</option>
              <!-- <option value="r" {!! ($ids == "r" ? ' selected' : '') !!}>Ruangan</option>
              <option value="ju" {!! ($ids == "ju" ? ' selected' : '') !!}>Jadwal per Jam Ujian</option> -->
              <option value="pju" {!! ($ids == "pju" ? ' selected' : '') !!}>Penjaga Ujian dan Piket</option>
              <!-- <option value="sd" {!! ($ids == "sd" ? ' selected' : '') !!}>Semua Data</option> -->
          </select>
      </div>
      <div class="col-xs-2 col-md-3" style="padding-left: 5px;" align="left">
        <button class="btn btn-success">
          <i class="fa fa-check"></i>
            <span class="hidden-sm hidden-xs">Pilih</span>
        </button>
      </div>
  </div>
</form>
</center>
@if($ids == "mk")
  @if(session('status') != "")
<div class="alert alert-info">{!! session('status') !!}</label>
<br></div>
@endif
<div class="card">
  <div class="header">
    <h4 class="title" style="display: inline; line-height: 1.5em;">Data Ujian</h4>
    <div class="btn-group" style="float:right;">
      <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right; margin-right: 5px;">
        <i class="fa fa-info-circle"></i>
        <span class="hidden-sm hidden-xs">Mulai Proses</span>
      </button>
      <ul class="dropdown-menu" style="left: -90px;">
        <li style="padding: 2px;">
          <a data-toggle="modal" data-target="#konfirmasiProsesRuangan">1. Ruangan Ujian</a>
        </li>
        <li style="padding: 2px;">
          <a data-toggle="modal" data-target="#konfirmasiProsesPenjaga">2. Penjaga Ujian</a>
        </li>
      </ul>
    </div>
    <p>*Menampilkan data mata kuliah buka yang memiliki KP</p>
  </div>
  <div class="content">
    <table id="tabelUjian" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
      <thead>
      <tr>
        <th width="10px">Aksi</th>
        <th>Nama MK</th>
        <th>Minggu</th>
        <th>Hari</th>
        <th>Jam</th>
        <th>T. Ujian</th>
        <th>T. Ruangan</th>

      </tr>
      </thead>
      <tbody>
        @foreach($datas as $data)
          <tr>
            <td>
              <a href="{!! action('JadwalUjianController@editDataUjian', $data->id) !!}">Ubah Jadwal</a>
            </td>
            <td>[<a href="" data-toggle="modal" data-target="#myModal-{{ $data->kodemk }}">KP</a>] {{ $data->kodemk }}<br>{{ $data->mk->nama }} 
            @if($data->presensiUjian()->count() > 0)
            [<a href="{!! action('RuanganUjianController@showDataPresensiUjian', $data->id) !!}">Presensi</a>]
            @else
            [-]
            @endif
            </td>
            <td>{{ $data->mingguke }}</td>
            <td>{{ $data->hari }}</td>
            <td>{{ $data->jamke }}</td>
            <td>{{ $data->tipeujian }}</td>
            <td>{{ $data->tiperuangan }}</td>
          </tr>

        @endforeach
      </tbody>
    </table>
  </div>
</div>

@foreach($datas as $data) 
<!-- Modal Detail KP per MK-->
<div class="modal fade" id="myModal-{{$data->kodemk}}" tabindex="-1" role="dialog" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{{ $data->kodemk }} - {{ $data->mk->nama }}</h4>
      </div>
      <div class="modal-body">
        <table class="table table-condensed table-bordered">
          <tr>
            <th>Kode KP</th>
            <th>Isi</th>
            <th>Kapasitas</th>
          </tr>
          @foreach($data->mk->kp as $d)
          <tr>
            <td>{{ $d->kodekp }}</td>
            <td>
              @if($d->adaPesertaKuliah())
                {{ $d->hitungPesertaKuliah() }}
              @else
                {{ "-" }}
              @endif
            </td>
            <td>{{ $d->kapasitas }}</td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</div>
@endforeach

<!-- modal konfirm proses ruangan -->
<div class="modal fade" id="konfirmasiProsesRuangan" tabindex="-1" role="dialog" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah Anda yakin untuk memproses pembagian ruangan ujian untuk semua kelas buka pada semester ini?
        <br>*Tindakan ini tidak dapat dibatalkan atau diulangi*
        </p>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Tidak</button>
          <a href="{{URL::route('maf.prosesruangan')}}"><button type="button" class="btn btn-success">Ya</button></a>
        </center>
      </div>
    </div>
  </div>
</div>

<!-- modal konfirm proses penjaga -->
<div class="modal fade" id="konfirmasiProsesPenjaga" tabindex="-1" role="dialog" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah Anda yakin untuk memproses pembagian penjaga ujian untuk semua jadwal ujian pada semester ini?
        <br>*Tindakan ini tidak dapat dibatalkan atau diulangi*
        </p>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Tidak</button>
          <a href="{{URL::route('maf.prosespenjagaujian')}}"><button type="button" class="btn btn-success">Ya</button></a>
        </center>
      </div>
    </div>
  </div>
</div>

<!-- per ruangan -->
@elseif ($ids == "r")
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Ujian</h4>
        <br><br>
        <form action="{{ route('maf.lapujianruangan') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="idretester" value="idusernntidrguard">
        <table class="table table-condensed table-bordered">
          <tr class="active">
            <th colspan="7">
              Filter Data
            </th>
          </tr>
          <tr>
            <td style="width: 10px;">
              <label class="radio" style="min-height: 0px !important; ">
                <input type="radio" name="filtertanggal" data-toggle="radio" id="optionsRadios1" value="tanggal">
              </label>
            </td>
            <td>Tanggal: </td>
            <td colspan="5">
              <input type="date" name="tanggalmulai" value=""> sampai <input type="date" name="tanggalselesai" value="">
            </td>
          </tr>
          <tr>
            <td style="width: 10px;">
              <label class="radio" style="min-height: 0px !important; ">
                <input type="radio" name="filtertanggal" data-toggle="radio" id="optionsRadios1" value="tanggal">
              </label>
            </td>
            <td>Minggu: </td>
            <td>
              <select>
                <option>1</option>
                <option>2</option>
                <option>3</option>
              </select>
            </td>
            <td>Hari: </td>
            <td>
              <select>
                <option>Senin</option>
                <option>Selasa</option>
                <option>Rabu</option>
              </select>
            </td>
            <td>Jam: </td>
            <td>
              <select>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </select>
            </td>
          </tr>
          
          <tr>
            <td colspan="2">&nbsp;</td>
            <td colspan="5">
              <input type="submit" class="btn btn-success btn-filter" name="submitfilter" value="Filter">
              <a href="{{ route('maf.lapujianruangan') }}" class="btn btn-danger btn-filter">Reset Filter</a>
              <input type="submit" class="btn btn-primary btn-filter" name="submitfilter" value="PDF">
              <input type="submit" class="btn btn-primary btn-filter" name="submitfilter" value="Excel">
            </td>
          </tr>
        </table>
        </form>

        <br>
    </div>
    <div class="content">
      <table id="tabelRuangan" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>

          <th>No</th>
          <th>Nama</th>
          <th>Kaps/Cad</th>
          <th>Isi</th>
          <th>Nama MK (jumlah)</th>
          <th>Dosen Jaga</th>
          <th>Karyawan Jaga</th>

        </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
</div>
@elseif ($ids == "ju")
<!-- jadwal ujian per jam -->

@elseif ($ids == "pju")
<!-- penjaga ujian dan piket -->
@if(session('status') != "")
<div class="alert alert-danger">{!! session('status') !!}</label>
<br></div>
@endif
<!-- piket -->
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Daftar Piket {{strtoupper($semesteraktif->getutsuas())}} Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}
          <a href="#" onclick="return false;" id="showviewtabel" style="font-size: 16px; display: none;">[Tampilkan]</a>
          <a href="#" onclick="return false;" id="hideviewtabel" style="font-size: 16px;">[Sembunyikan]</a>
        </h4>
    </div>

    <div class="content">
      <div id="tabelPiket">
        <table id="tabeldaftarpiket" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
          <thead>
          <tr>
            <th width="1px" class="hidden-xs">No</th>
            <th>Tgl</th>
            <th>Karyawan</th>
          </tr>
          </thead>
          <tbody>
            @foreach($datapikets as $rut)
              <tr>
                <td class="hidden-xs">{{$loop->iteration}}</td>
                <td> 
                  @if(date('l', strtotime($rut->tanggal)) == "Monday")
                    Senin,
                  @elseif(date('l', strtotime($rut->tanggal)) == "Tuesday")
                    Selasa,
                  @elseif(date('l', strtotime($rut->tanggal)) == "Wednesday")
                    Rabu,
                  @elseif(date('l', strtotime($rut->tanggal)) == "Thursday")
                    Kamis,
                  @elseif(date('l', strtotime($rut->tanggal)) == "Friday")
                    Jumat,
                  @elseif(date('l', strtotime($rut->tanggal)) == "Saturday")
                    Sabtu,
                  @else
                    Minggu,
                  @endif
                  <span class="hidden-xs">
                    {{date('j', strtotime($rut->tanggal))}}-{{date('m', strtotime($rut->tanggal))}}-{{date('Y', strtotime($rut->tanggal))}}
                  </span>
                  <span class="visible-xs">
                    {{date('j', strtotime($rut->tanggal))}} @if(date('M', strtotime($rut->tanggal)) == "Dec") Des @endif
                  </span>
                </td>
                <td>
                  {{$rut->karyawan->npk}} - {{ $rut->karyawan->namalengkap }} [<a href="#edit-{{$rut->id}}" data-toggle="modal">Ubah</a>]
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        <hr>
      </div>
    </div>

    <!-- modal ubah piket -->
    @foreach($datapikets as $d)
    <div class="modal fade" id="edit-{{$d->id}}" tabindex="-1" role="dialog" data-backdrop="false">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Edit Piket</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" name="edit-{{$d->id}}" method="POST" action="{{route('maf.presensiujian.piketupdate')}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="idfakultas" value="{{Auth::guard('karyawan')->user()->idfakultas}}">
              <input type="hidden" name="id" value="{{$d->id}}">

              <div class="form-group">
                <label class="col-sm-3 control-label">Tanggal</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control border-input" name="tanggal-{{$d->id}}" value="{{$d->tanggal}}" readonly>
                </div>
              </div>
              <div class='form-group ui-widget'>
                <label class='col-sm-3 control-label'>Karyawan: </label>
                <div class='col-sm-9'>
                  <input type='text' class='form-control border-input' name='namakaryawan' id="karyawans-{{$d->id}}" value="<?php echo $d->npk; ?> - <?php echo $d->karyawan->namalengkap;?>">
               </div>
              </div>
              <input id="karyawans-npk-{{$d->id}}" type="hidden" name="karyawans-npk-{{$d->id}}" value="{{$d->npk}}">
              
              <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3">
                      <input type="submit" class="btn btn-success" name="simpan" value="Simpan">
                  </div>
              </div>
          </form>
          </div>
        </div>
      </div>
    </div>
    @endforeach
</div>

<!-- dosen jaga -->
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Daftar Dosen Jaga {{strtoupper($semesteraktif->getutsuas())}} Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}
        <br>
        <p>*NB: Jika slot tidak muncul berarti tidak ada jadwal ujian pada slot tersebut.</p>
          <a href="#" onclick="return false;" id="showviewtabel1" style="font-size: 16px; display: none;">[Tampilkan]</a>
          <a href="#" onclick="return false;" id="hideviewtabel1" style="font-size: 16px;">[Sembunyikan]</a>
        </h4>
    </div>

    <div class="content">
      <div id="tabeldosenjaga">
        <table id="tabelDosenJaga" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
          <thead>
          <tr>
            <th>Nama</th>
            <th>Slot</th>
            <th>Ruangan</th>
            <th>MK</th>
          </tr>
          </thead>
          <tbody>
            @foreach($semuadosen as $sd)
              @foreach($slotujian as $slot)
                <tr>
                  <td><span class="visible-xs">{{ $sd->npk }} - </span>{{ $sd->namalengkap }}</td>
                  <td>{{ $slot->mingguke }}/{{ $slot->hari }}/{{ $slot->jamke }}</td>
                  <td>
                  @if(count($sd->getruanganjaga($slot->mingguke, $slot->hari, $slot->jamke)) > 0)
                    @foreach($sd->getruanganjaga($slot->mingguke, $slot->hari, $slot->jamke) as $d)
                      {{$d}}
                      @if(count($sd->getruanganjaga($slot->mingguke, $slot->hari, $slot->jamke)) > 1)
                       <br>
                      @endif
                    @endforeach
                  @else
                    -
                  @endif
                  </td>
                  <td>
                    @if(count($sd->getruanganjaga($slot->mingguke, $slot->hari, $slot->jamke)) != 0)
                     @foreach($sd->getMKjaga($slot->mingguke, $slot->hari, $slot->jamke) as $d)
                      {{$d}}
                      @if(count($sd->getMKjaga($slot->mingguke, $slot->hari, $slot->jamke)) > 1 && $loop->iteration != count($sd->getMKjaga($slot->mingguke, $slot->hari, $slot->jamke)))
                       <br>
                      @endif
                    @endforeach
                    @elseif($sd->isPiket($slot->mingguke, $slot->hari, $semesteraktif->getutsuas()))
                      Piket
                    @else
                      Tidak Menjaga
                    @endif
                  </td>

                </tr>
              @endforeach
            @endforeach
          </tbody>
        </table>
        <hr>
      </div>
    </div>
</div>

<!-- karyawan jaga -->
<div class="card">
  <div class="header">
    <h4 class="title" style="display: inline; line-height: 1.5em;">Daftar Karyawan Jaga {{strtoupper($semesteraktif->getutsuas())}} Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}
    <br>
    <p>*NB: Jika slot tidak muncul berarti tidak ada jadwal ujian pada slot tersebut.</p>
      <a href="#" onclick="return false;" id="showviewtabel2" style="font-size: 16px; display: none;">[Tampilkan]</a>
      <a href="#" onclick="return false;" id="hideviewtabel2" style="font-size: 16px;">[Sembunyikan]</a>
    </h4>
  </div>

  <div class="content">
    <div id="tabelkaryawanjaga">
      <table id="tabelKaryawanJaga" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th>Nama</th>
          <th>Slot</th>
          <th>Ruangan</th>
          <th>MK</th>
        </tr>
        </thead>
        <tbody>
          @foreach($semuakaryawan as $sd)
            @foreach($slotujian as $slot)
              <tr>
                <td><span class="visible-xs">{{ $sd->npk }} - </span>{{ $sd->namalengkap }}</td>
                <td>{{ $slot->mingguke }}/{{ $slot->hari }}/{{ $slot->jamke }}</td>
                <td>
                @if(count($sd->getruanganjaga($slot->mingguke, $slot->hari, $slot->jamke)) > 0)
                  @foreach($sd->getruanganjaga($slot->mingguke, $slot->hari, $slot->jamke) as $d)
                    {{$d}}
                    @if(count($sd->getruanganjaga($slot->mingguke, $slot->hari, $slot->jamke)) > 1)
                     <br>
                    @endif
                  @endforeach
                @else
                  -
                @endif
                </td>
                <td>
                  @if(count($sd->getruanganjaga($slot->mingguke, $slot->hari, $slot->jamke)) != 0)
                   @foreach($sd->getMKjaga($slot->mingguke, $slot->hari, $slot->jamke) as $d)
                    {{$d}}
                    @if(count($sd->getMKjaga($slot->mingguke, $slot->hari, $slot->jamke)) > 1 && $loop->iteration != count($sd->getMKjaga($slot->mingguke, $slot->hari, $slot->jamke)))
                     <br>
                    @endif
                  @endforeach
                  @else
                   Tidak Menjaga
                  @endif
                </td>
              </tr>
            @endforeach
          @endforeach
        </tbody>
      </table>
      <hr>
    </div>
  </div>
</div>
@elseif ($ids == "sd")
<!-- semua data -->

@endif

@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelUjian").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[2, 'asc'], [3,'desc']],
      "lengthMenu": [[-1, 100, 50, 25, 10, 5], ["All", 100, 50, 25, 10, 5]],
      "stateSave": true,
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
    $("#tabelRuangan").dataTable( {
      "pagingType": "full",
      "lengthMenu": [[-1, 100, 50, 25, 10, 5], ["All", 100, 50, 25, 10, 5]],
      "stateSave": true,
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });

    $("#tabelDosenJaga").dataTable( {
      "pagingType": "full",
      "lengthMenu": [[-1, 100, 50, 25, 10, 5], ["All", 100, 50, 25, 10, 5]],
      "stateSave": true,
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });

    $("#tabelKaryawanJaga").dataTable( {
      "pagingType": "full",
      "lengthMenu": [[-1, 100, 50, 25, 10, 5], ["All", 100, 50, 25, 10, 5]],
      "stateSave": true,
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });

    $("#tabeldaftarpiket").dataTable( {
      "pagingType": "full",
      "lengthMenu": [[-1, 100, 50, 25, 10, 5], ["All", 100, 50, 25, 10, 5]],
      "stateSave": true,
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
  });
  (function(){
      $('#showviewtabel').click(function() {
        $('#tabelPiket').toggle();
        $('#hideviewtabel').toggle();
        $('#showviewtabel').toggle();
      });

      $('#hideviewtabel').click(function() {
        $('#tabelPiket').toggle();
        $('#hideviewtabel').toggle();
        $('#showviewtabel').toggle();
      });
  })();
  (function(){
      $('#showviewtabel1').click(function() {
        $('#tabeldosenjaga').toggle();
        $('#hideviewtabel1').toggle();
        $('#showviewtabel1').toggle();
      });

      $('#hideviewtabel1').click(function() {
        $('#tabeldosenjaga').toggle();
        $('#hideviewtabel1').toggle();
        $('#showviewtabel1').toggle();
      });
  })();
  (function(){
      $('#showviewtabel2').click(function() {
        $('#tabelkaryawanjaga').toggle();
        $('#hideviewtabel2').toggle();
        $('#showviewtabel2').toggle();
      });

      $('#hideviewtabel2').click(function() {
        $('#tabelkaryawanjaga').toggle();
        $('#hideviewtabel2').toggle();
        $('#showviewtabel2').toggle();
      });
  })();
</script>
@if($ids == "pju")
<script>
  var datakaryawan = [
      <?php foreach ($semuadosen as $r){ ?>
          { value: "<?php echo $r->npk;?>", label: "<?php echo $r->npk;?> - <?php echo $r->namalengkap;?>" },
      <?php } ?>
      ];
  
    $(function() {
      <?php foreach ($datapikets as $data) { ?>
        $("#karyawans-<?php echo $data->id;?>").autocomplete({
            source: datakaryawan,
            focus: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox
                $(this).val(ui.item.label);
            },
            select: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox and hidden field
                $(this).val(ui.item.label);
                $("#karyawans-npk-<?php echo $data->id;?>").val(ui.item.value);
                // alert($("#ruangans-id-<?php echo $data->id;?>").val(ui.item.value));
            }
        });
      <?php } ?>
    });   
</script>
@endif
@endsection