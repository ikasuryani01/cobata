@extends('maf._layouts.base')

@section('title', 'Ubah Ruangan Ujian')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info">{!! session('status') !!}</label>
<br></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Ruangan Tersedia
          <a href="#" onclick="return false;" id="showviewtabel" style="font-size: 16px; display: none;">[Tampilkan]</a>
          <a href="#" onclick="return false;" id="hideviewtabel" style="font-size: 16px;">[Sembunyikan]</a>
        </h4>
    
        <a href="{{ route('maf.presensiujian', $idjadwalujian) }}">
          <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-caret-left"></i>
            <span class="hidden-sm hidden-xs">Kembali</span>
          </button>
        </a>

    </div>
    <div class="content">
      <div id="tabelruangan">
        <table id="tabelRuanganTersedia" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
          <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Kapasitas</th>
            <th>Cadangan</th>
            <th>Isi</th>
            <th>Sisa</th>
          </tr>
          </thead>
          <tbody>
            @foreach($ruangantersedia as $rut)
              <tr>

                <td>{{ $loop->iteration }}</td>
                <td>{{ $rut->nama }}</td>
                <td>{{ $rut->kapasitasujian }}</td>
                <td>{{ $rut->cadangankapujian }}</td>
                <td>{{ $rut->hitungIsiPerJadwal($idjadwalujian) }}</td>
                <td>{{ $rut->sisaKapPerJadwal($idjadwalujian) }}</td>

              </tr>
            @endforeach
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Kapasitas</th>
            <th>Cadangan</th>
            <th>Isi</th>
            <th>Sisa</th>

          </tr>
          </tfoot>
        </table>
        <hr>
      </div>
    </div>

    <div class="header" style="margin-top: -25px;">
      <h4 class="title" style="display: inline; line-height: 1.5em;">Edit Presensi Ujian {{ $mk->nama }} ({{ $mk->kodemk}})</h4>
    </div>
    <div class="content">
      <table id="tabeleditruangan" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>

          <th>No</th>
          <th>KP</th>
          <th>Ruangan</th>
          <th>NRP</th>
          <th>Jumlah</th>
          <th>Aksi</th>

        </tr>
        </thead>
        <tbody>
          @foreach($datas as $data)
            <tr>
              <form class="form-horizontal" method="POST" name="editpres-{{ $data->id }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="idpresensiujian" value="{{ $data->id }}">
              <input type="hidden" name="idjadwalujian" value="{{ $idjadwalujian }}">
                <td>{{ $loop->iteration }}</td>
                <td>{{ $data->kp->kodekp }}</td>
                <td style="padding-bottom: 0px;">
                  @if($data->idruangan != null)
                  <div class='form-group ui-widget'>
                    <input type='text' style="width: 100%" id='ruangans-{{ $data->id }}' class='form-control border-input' placeholder='Cari Nama Ruangan' name='namaruangan' required value="{{ $data->ruangan->nama }}">
                  </div>
                  <input id="ruangans-id-{{ $data->id }}" type="hidden" name="idruangan" value="{{ $data->idruangan }}">
                  @else
                  <div class='form-group ui-widget'>
                    <input type='text' style="width: 100%" id='ruangans-{{ $data->id }}' class='form-control border-input' placeholder='Cari Nama Ruangan' name='namaruangan' required>
                  </div>
                  <input id="ruangans-id-{{ $data->id }}" type="hidden" name="idruangan">
                  @endif
                </td>
                <td>
                  <a href="#DaftarMhs-{{$data->id}}" data-toggle="modal">
                    {{ $data->nrpawal }} - {{ $data->nrpakhir }}
                  </a>
                </td>
                <td align="text">
                  <input type='text' style="width: 100%;" name="jumlahpeserta" class='form-control border-input' required value="{{ $data->jumlahpeserta }}">
                </td>
                <td align="center" style="padding-left: 0px; padding-right: 0px; padding-top: 0px; padding-bottom: 0px;">
                  <!-- <button class="btn btn-success btn-sm"><i class="fa fa-save"></i></button> -->
                  
                  <button class="btn btn-success btn-sm">
                    <i class="fa fa-refresh"></i>
                    <span class="hidden-sm hidden-xs">Perbarui NRP</span>
                  </button>
                  <!-- <input type="submit" class="btn btn-success btn-sm" name="{{ $data->id }}" value="PN"> -->
                </td>
              </form>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          
          <th>No</th>
          <th>KP</th>
          <th>Ruangan</th>
          <th>NRP</th>
          <th>Jumlah</th>
          <th>Aksi</th>

        </tr>
        </tfoot>
      </table>
    </div>      

    @foreach($datas as $data)
      <!-- Modal -->
      <div class="modal fade" id="DaftarMhs-{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Daftar Mhs</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                @foreach($data->getDaftarMhs() as $d)
                  <div class="col-sm-3">
                    {{ $loop->iteration }}. {{ $d }}
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    @endforeach
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelRuanganTersedia").dataTable( {
      "pagingType": "full",
      "lengthMenu": [[5, 10, 15], [5, 10, 15]],
      "stateSave": true,
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    $("#tabeleditruangan").dataTable( {
      "pagingType": "full",
      "lengthMenu": [[5, 10, 15], [5, 10, 15]],
      "stateSave": true,
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
  });
  
  (function(){
      $('#showviewtabel').click(function() {
        $('#tabelruangan').toggle();
        $('#hideviewtabel').toggle();
        $('#showviewtabel').toggle();
      });

      $('#hideviewtabel').click(function() {
        $('#tabelruangan').toggle();
        $('#hideviewtabel').toggle();
        $('#showviewtabel').toggle();
      });
  })();
</script>
<script>
  var dataruangan = [
      <?php foreach ($semuaruangan as $r){ ?>
          { value: "<?php echo $r->id;?>", label: "<?php echo $r->nama;?>" },
      <?php } ?>
      ];
  
    $(function() {
      <?php foreach ($datas as $data) { ?>
        $("#ruangans-<?php echo $data->id;?>").autocomplete({
            source: dataruangan,
            focus: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox
                $(this).val(ui.item.label);
            },
            select: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox and hidden field
                $(this).val(ui.item.label);
                $("#ruangans-id-<?php echo $data->id;?>").val(ui.item.value);
                // alert($("#ruangans-id-<?php echo $data->id;?>").val(ui.item.value));
            }
        });
      <?php } ?>
    });   
</script>
@endsection