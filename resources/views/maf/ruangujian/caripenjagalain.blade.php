@extends('maf._layouts.base')

@section('title', 'Ubah Penjaga Ujian')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info">{!! session('status') !!}</label>
<br></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Edit Penjaga Ujian</h4>
    
        <a href="{{ route('maf.presensiujian', $idjadwalujian) }}">
          <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-caret-left"></i>
            <span class="hidden-sm hidden-xs">Kembali</span>
          </button>
        </a>

    </div>
    <div class="content">
      <table id="" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <!-- <th>No</th> -->
          <th>MK</th>
          <th>KP</th>
          <th>Ruangan</th>
          <th>Dosen</th>
          <th>Karyawan</th>
          <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
          @foreach($datas as $data)
            <tr>
              <form class="form-horizontal" method="POST" name="editpres-{{ $data->id }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="idpresensiujian" value="{{ $data->id }}">
              <input type="hidden" name="tanggal" value="{{ $data->tanggal }}">
              <input type="hidden" name="idjadwalujian" value="{{ $idjadwalujian }}">
                <!-- <td>{{ $loop->iteration }}</td> -->
                <td>{{ $data->kp->mk->nama }} ({{ $data->kp->kodemk }})</td>
                <td>{{ $data->kp->kodekp }}</td>
                <td style="padding-bottom: 0px;">
                  @if($data->idruangan != null)
                    {{$data->ruangan->nama}}
                  @else
                    {{ "-" }}
                  @endif
                </td>
                <td style="padding-bottom: 0px;">
                  @if($data->npkdosenjaga != null)
                  <div class='form-group ui-widget'>
                    <input type='text' style="width: 100%" id='dosens-{{ $data->id }}' class='form-control border-input' placeholder='Cari Nama atau NPK' name='namanpkdosenjaga' required value="{{ $data->npkdosenjaga }} - {{ $data->dosenjaga->nama }}">
                  </div>
                  <input id="npkdosenjaga-{{ $data->id }}" type="hidden" name="npkdosenjaga" value="{{ $data->npkdosenjaga }}">
                  @else
                  <div class='form-group ui-widget'>
                    <input type='text' style="width: 100%" id='dosens-{{ $data->id }}' class='form-control border-input' placeholder='Cari Nama atau NPK' name='namanpkdosenjaga' required>
                  </div>
                  <input id="npkdosenjaga-{{ $data->id }}" type="hidden" name="npkdosenjaga">
                  @endif
                </td>

                <td style="padding-bottom: 0px;">
                  @if($data->npkkaryawanjaga != null)
                  <div class='form-group ui-widget'>
                    <input type='text' style="width: 100%" id='karyawans-{{ $data->id }}' class='form-control border-input' placeholder='Cari Nama atau NPK' name='namanpkkaryawanjaga' required value="{{ $data->npkkaryawanjaga }} - {{ $data->karyawanjaga->nama }}">
                  </div>
                  <input id="npkkaryawanjaga-{{ $data->id }}" type="hidden" name="npkkaryawanjaga" value="{{ $data->npkkaryawanjaga }}">
                  @else
                  <div class='form-group ui-widget'>
                    <input type='text' style="width: 100%" id='karyawans-{{ $data->id }}' class='form-control border-input' placeholder='Cari Nama atau NPK' name='namanpkkaryawanjaga' required>
                  </div>
                  <input id="npkkaryawanjaga-{{ $data->id }}" type="hidden" name="npkkaryawanjaga">
                  @endif
                </td>

                <td align="center" style="padding-right: 5px; padding-top: 0px; padding-bottom: 0px;">
                  <!-- <button class="btn btn-success btn-sm"><i class="fa fa-save"></i></button> -->
                  
                  <button class="btn btn-success btn-sm">
                    <i class="fa fa-save"></i>
                    <!-- <span class="hidden-sm hidden-xs">Simpan</span> -->
                  </button>
                  <!-- <input type="submit" class="btn btn-success btn-sm" name="{{ $data->id }}" value="PN"> -->
                </td>
              </form>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          
          <!-- <th>No</th> -->
          <th>MK</th>
          <th>KP</th>
          <th>Ruangan</th>
          <th>Dosen</th>
          <th>Karyawan</th>
          <th>Aksi</th>

        </tr>
        </tfoot>
      </table>
    </div> 

    <div class="header">
      <h4 class="title" style="display: inline; line-height: 1.5em; margin-top: 25px;">Dosen Tersedia
        <a href="#" onclick="return false;" id="showviewtabeld" style="font-size: 16px; display: none;">[Tampilkan]</a>
        <a href="#" onclick="return false;" id="hideviewtabeld" style="font-size: 16px;">[Sembunyikan]</a>
      </h4>
    </div>
    <div class="content">
      <div id="tabeldosen">
        <table id="tabelDosenTersedia" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
           <thead>
          <tr>
            <th>No</th>
            <th>NPK</th>
            <th>Nama</th>
            <th>Jurusan</th>
            <th>Jumlah Jaga</th>
          </tr>
          </thead>
          <tbody>
            @foreach($dosentersedia as $rut)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $rut->npk }}</td>
                <td>{{ $rut->nama }}</td>
                <td>{{ $rut->getJurusan()->Nama }}</td>
                <td>{{ $rut->getJumlahJaga() }}</td>
              </tr>
            @endforeach
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>NPK</th>
            <th>Nama</th>
            <th>Jurusan</th>
            <th>Jumlah Jaga</th>
          </tr>
          </tfoot>
        </table>
        <hr>
      </div>
    </div>

    <div class="header">
      <h4 class="title" style="display: inline; line-height: 1.5em; margin-top: 25px;">Karyawan Tersedia
        <a href="#" onclick="return false;" id="showviewtabelk" style="font-size: 16px; display: none;">[Tampilkan]</a>
        <a href="#" onclick="return false;" id="hideviewtabelk" style="font-size: 16px;">[Sembunyikan]</a>
      </h4>
    </div>
    <div class="content">
      <div id="tabelkaryawan">
        <table id="tabelKaryawanTersedia" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
          <thead>
          <tr>
            <th>No</th>
            <th>NPK</th>
            <th>Nama</th>
            <th>Jumlah Jaga</th>
          </tr>
          </thead>
          <tbody>
            @foreach($karyawantersedia as $rut)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $rut->npk }}</td>
                <td>{{ $rut->nama }}</td>
                <td>{{ $rut->getJumlahJaga() }}</td>
              </tr>
            @endforeach
          </tbody>
          <tfoot>
          <tr>
            <th>No</th>
            <th>NPK</th>
            <th>Nama</th>
            <th>Jumlah Jaga</th>
          </tr>
          </tfoot>
        </table>
        <hr>
      </div>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKaryawanTersedia").dataTable( {
      "pagingType": "full",
      "aaSorting" : [['3', 'desc']],
      "lengthMenu": [[5, 10, 15], [5, 10, 15]],
      "stateSave": true,
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
  });

  $(function () {
    $("#tabelDosenTersedia").dataTable( {
      "pagingType": "full",
      "aaSorting" : [['4', 'desc']],
      "lengthMenu": [[5, 10, 15], [5, 10, 15]],
      "stateSave": true,
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
  });
  
  (function(){
      $('#showviewtabelk').click(function() {
        $('#tabelkaryawan').toggle();
        $('#hideviewtabelk').toggle();
        $('#showviewtabelk').toggle();
      });

      $('#hideviewtabelk').click(function() {
        $('#tabelkaryawan').toggle();
        $('#hideviewtabelk').toggle();
        $('#showviewtabelk').toggle();
      });
  })();

  (function(){
      $('#showviewtabeld').click(function() {
        $('#tabeldosen').toggle();
        $('#hideviewtabeld').toggle();
        $('#showviewtabeld').toggle();
      });

      $('#hideviewtabeld').click(function() {
        $('#tabeldosen').toggle();
        $('#hideviewtabeld').toggle();
        $('#showviewtabeld').toggle();
      });
  })();
</script>
<script>
  var datadosen = [
    <?php foreach ($dosentersedia as $r){ ?>
        { value: "<?php echo $r->npk;?>", label: "<?php echo  "$r->npk - $r->nama";?>" },
    <?php } ?>
  ];

  var datakaryawan = [
    <?php foreach ($karyawantersedia as $r){ ?>
        { value: "<?php echo $r->npk;?>", label: "<?php echo  "$r->npk - $r->nama";?>" },
    <?php } ?>
  ];
  
  $(function() {
    <?php foreach ($datas as $data) { ?>
      $("#dosens-<?php echo $data->id;?>").autocomplete({
          source: datadosen,
          focus: function(event, ui) {
              // prevent autocomplete from updating the textbox
              event.preventDefault();
              // manually update the textbox
              $(this).val(ui.item.label);
          },
          select: function(event, ui) {
              // prevent autocomplete from updating the textbox
              event.preventDefault();
              // manually update the textbox and hidden field
              $(this).val(ui.item.label);
              $("#npkdosenjaga-<?php echo $data->id;?>").val(ui.item.value);
              // alert($("#ruangans-id-<?php echo $data->id;?>").val(ui.item.value));
          }
      });

      $("#karyawans-<?php echo $data->id;?>").autocomplete({
          source: datakaryawan,
          focus: function(event, ui) {
              // prevent autocomplete from updating the textbox
              event.preventDefault();
              // manually update the textbox
              $(this).val(ui.item.label);
          },
          select: function(event, ui) {
              // prevent autocomplete from updating the textbox
              event.preventDefault();
              // manually update the textbox and hidden field
              $(this).val(ui.item.label);
              $("#npkkaryawanjaga-<?php echo $data->id;?>").val(ui.item.value);
              // alert($("#ruangans-id-<?php echo $data->id;?>").val(ui.item.value));
          }
      });
    <?php } ?>
  });   
</script>
@endsection