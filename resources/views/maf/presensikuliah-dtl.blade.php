@extends('maf._layouts.base')

@section('title', 'Detail Presensi Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="header">
      <h4 class="title" style="display: inline; line-height: 1.5em;">Data Peserta Kelas</h4>
      <a href="{{ URL::previous() }}">
          <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-caret-left"></i>
            <span class="hidden-sm hidden-xs">Kembali</span>
          </button>
        </a>
    </div>
    <div class="content">
      <table id="tabelKuliah" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th width="7px">No</th>
          <th class="hidden-xs">NRP</th>
          <th>Mahasiswa</th>
        </tr>
        </thead>
        <tbody>
          @foreach($kuliah->kelas->pesertaKuliah() as $mhs)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td class="hidden-xs">{{ $mhs->nrp }}</td>
              <td><span class="visible-xs">{{ $mhs->nrp }} - </span>{{ $mhs->nama }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKuliah").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "dom": "<'row'<'col-sm-6'><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection