@extends('maf._layouts.base')

@section('title', 'Master Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Mahasiswa</h4>

                <a href="{{URL::route('maf.mastermhs.addext')}}">
                  <button class="btn btn-warning" style="float: right;">
                    <i class="fa fa-database"></i>
                    <span class="hidden-sm hidden-xs">Ambil Data dari BAAK</span>
                  </button>
                </a>
                <br>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">NRP: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nrp" value="{{ old('nrp') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Lengkap: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nama" value="{{ old('nama')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Akademik Terima :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="tahunakademikterima" value="{{ old('tahunakademikterima') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester Terima :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="semesterterima">
                            @foreach($enumSemester as $d)
                                <option value="{{$d}}">{{ $d }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jurusan</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="idjurusan">
                            @foreach($dataJurusan as $d)
                                <option value="{{$d->IdJurusan}}">{{ $d->Nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="kodestatus" value="{{ old('kodestatus') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Lunas UPP :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="statuslunasupp">
                            <option value="Ya">Ya</option>
                            <option value="Tidak">Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">IPK tanpa E :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="ipktanpae" value="{{ old('ipktanpae') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Kum tanpa E :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="skskumtanpae" value="{{ old('skskumtanpae') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Max depan :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="sksmaxdepan" value="{{ old('sksmaxdepan') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Masa Studi :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="masastudi" value="{{ old('masastudi') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nilai TOEFL :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="nilaitoefl" value="{{ old('nilaitoefl') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Asisten :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="asisten">
                            <option value="Ya">Ya</option>
                            <option value="Tidak">Tidak</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @elseif ($mode == "edit")
        <div class="card">
            <div class="header">
                <h4 class="title">Ubah Data Mahasiswa</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">NRP: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nrp" value="{{ $mahasiswa->nrp }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Lengkap: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nama" value="{{ $mahasiswa->nama }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Akademik Terima :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="tahunakademikterima" value="{{ $mahasiswa->tahunakademikterima }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester Terima :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="semesterterima">
                            @foreach($enumSemester as $d)
                                <option value="{{$d}}" {!! ($mahasiswa->semesterterima == $d ? ' selected' : '') !!}>{{ $d }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jurusan</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="idjurusan">
                            @foreach($dataJurusan as $d)
                                <option value="{{$d->IdJurusan}}" {!! ($mahasiswa->idjurusan == $d->IdJurusan ? ' selected' : '') !!}>{{ $d }}>{{ $d->Nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="kodestatus" value="{{ $mahasiswa->kodestatus }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Lunas UPP :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="statuslunasupp">
                            <option value="Ya" {!! ($mahasiswa->statuslunasupp == "Ya" ? ' selected' : '') !!}>Ya</option>
                            <option value="Tidak" {!! ($mahasiswa->statuslunasupp == "Tidak" ? ' selected' : '') !!}>Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">IPK tanpa E :</label>
                    <div class="col-sm-9">
                        <input type="number"  class="form-control border-input" name="ipktanpae" value="{{ $mahasiswa->ipktanpae }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Kum tanpa E :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="skskumtanpae" value="{{ $mahasiswa->skskumtanpae }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">SKS Max depan :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="sksmaxdepan" value="{{ $mahasiswa->sksmaxdepan }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Masa Studi :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="masastudi" value="{{ $mahasiswa->masastudi }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nilai TOEFL :</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="nilaitoefl" value="{{ $mahasiswa->nilaitoefl }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Asisten :</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="asisten">
                            <option value="Ya" {!! ($mahasiswa->asisten == "Ya" ? ' selected' : '') !!}>Ya</option>
                            <option value="Tidak" {!! ($mahasiswa->asisten == "Tidak" ? ' selected' : '') !!}>Tidak</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endif



@endsection

