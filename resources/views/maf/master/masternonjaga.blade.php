@extends('maf._layouts.base')

@section('title', 'Karyawan Non Jaga')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')

<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.8em;">Non-jaga Ujian</h4>
    
        <a href="{{URL::route('maf.masternonjaga.add')}}">
          <button class="btn btn-sm btn-success" style="float: right;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('maf.masternonjagas')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('maf.masternonjaga')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelnonjaga" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th width="10px">Aksi</th>
          <th>NPK - Nama</th>
          <th>Tanggal</th>
          <th>Jam Ke</th>
          <th>Keterangan</th>
        </tr>
        </thead>
        <tbody>
          @foreach($nonjagas as $data)
            <tr>
              <td>
              @if($data->deleted_at == null)
                <a href="{!! action('JadwalNonjagaController@deleteDataNonjaga', $data->id) !!}">Hapus</a> | 
                <a href="{!! action('JadwalNonjagaController@editDataNonjaga', $data->id) !!}">Edit</a>
              @else
                <a href="{!! action('JadwalNonjagaController@restoreDataNonjaga', $data->id) !!}">Aktifkan</a> |
                <a href="{!! action('JadwalNonjagaController@forceDeleteDataNonjaga', $data->id) !!}">Hapus Lagi</a>
              @endif
              </td>
              <td>{{ $data->npkkaryawan }} - {{ $data->karyawan->nama }}</td>
              <td>{{ $data->tanggal }}</td>
              <td>{{ $data->jamke }}</td>
              <td>{{ $data->keterangan }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelnonjaga").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection