@extends('maf._layouts.base')

@section('title', 'Master Jurusan - Mata Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
    .modal-dialog{
      overflow-y: initial !important
    }
    .modal-body{
        height: 400px;
        overflow-y: auto;
    }
</style>
@endsection

@section('content')
<form class="form-horizontal" method="GET">
  <div class="form-group">
    <div class="visible-lg visible-md">
      <label class="col-md-3 control-label" style="margin-top: 5px;">Lihat: </label>
    </div>
    <div class="col-xs-8 col-md-6">
      <select class="form-control border-input" name="ids">
        <option value="jur" {!! ($ids == "jur" ? ' selected' : '') !!}>Berdasarkan Jurusan</option>
        <option value="mk" {!! ($ids == "mk" ? ' selected' : '') !!}>Berdasarkan Mata Kuliah</option>
      </select>
    </div>
    <div class="col-xs-2 col-md-3">
      <button class="btn btn-success">Pilih</button>
    </div>
  </div>
</form>
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.8em;">Data {!! ($ids == "jur" ? 'Jurusan' : 'Mata Kuliah') !!}</h4>

    </div>
    @if($ids == "jur")
    <div class="content">
      <table id="tabelJurusan" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th width="10px">No</th>
            <th>Nama</th>
            <th>Jml <span class="hidden-xs">MK</span></th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($datas as $data)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $data->Nama }}</td>
              <td>{{ $data->countMK() }}</td>
              <td>
                <a href="{!! action('JurusanMkController@showAllMk', $data->IdJurusan) !!}">Edit MK</a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    @elseif($ids == "mk")
    <div class="content">
      <table id="tabelMk" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th width="20px">Buka</th>
          <th>Kode MK</th>
          <th>Nama</th>
          <th>SKS</th>
          <th>Jurusan</th>
          <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
          @foreach($datas as $data)
            <tr>
              <td>{{ $data->statusbuka }}</td>
              <td>{{ $data->kodemk }}</td>
              <td>{{ $data->nama }}</td>
              <td>{{ $data->sks }}</td>
              <td>
              @if($data->getJurusan()->count() > 0)
                @foreach($data->getJurusan() as $jur)
                  {{ $jur->Nama }}<br>
                @endforeach
              @else
                {{ "-" }}
              @endif
              </td>
              <td><a href="#myModal-{{$data->kodemk}}" data-toggle="modal">Edit Jurusan</a></td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th width="20px">Buka</th>
            <th>Kode MK</th>
            <th>Nama</th>
            <th>SKS</th>
            <th>Jurusan</th>
            <th>Aksi</th>
          </tr>
        </tfoot>
      </table>
    </div>

    @foreach($datas as $d)
    <div class="modal fade" id="myModal-{{$d->kodemk}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false" style="margin-top: 50px;">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">{{$d->kodemk}} - {{$d->nama}}</h4>
          </div>
          <div class="modal-body">
            <form method="POST" name="{{$d->kodemk}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="kodemk" value="{{ $d->kodemk }}">
              <div class="form-group">
              @foreach($jurusans as $jur)
                <label class="checkbox">
                  <input type="checkbox" name="idjurusan[]" value="{{$jur->IdJurusan}}" {!! ($d->adaJurusan($jur->IdJurusan) ? ' checked' : ' ') !!}>{{$jur->Nama}}
                </label>
                @if($loop->iteration != $jurusans->count())
                <br>
                @endif
              @endforeach
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <input type="submit" name="simpanjurmk" value="Simpan" class="btn btn-primary">
          </div>
          </form>
        </div>
      </div>
    </div>
    @endforeach

    @endif
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelJurusan").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });

    $("#tabelMk").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[4, 'asc']],
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection