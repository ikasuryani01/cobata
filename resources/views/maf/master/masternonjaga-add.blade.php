@extends('maf._layouts.base')

@section('title', 'Master Karyawan Non Jaga')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Data Non Jaga</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class='form-group ui-widget'>
                    <label class='col-sm-3 control-label'>Nama Karyawan: </label>
                    <div class='col-sm-9'>
                        <input type='text' id='npkkaryawan' class='form-control border-input' placeholder='Cari Nama/Kode Karyawan dan Dosen' name='npkkaryawan' required>
                   </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal: </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control border-input" name="tanggal" value="{{ old('tanggal') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jam Ke:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="jamke">
                            @foreach($enumJamke as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Keterangan:</label>
                    <div class="col-sm-9">
                         <textarea name="keterangan" class="form-control border-input">{{ old('keterangan') }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @elseif ($mode == "edit")
                <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Data Non Jaga</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class='form-group ui-widget'>
                    <label class='col-sm-3 control-label'>Nama Karyawan: </label>
                    <div class='col-sm-9'>
                        <input type='text' id='npkkaryawan' class='form-control border-input' placeholder='Cari Nama/Kode Karyawan dan Dosen' name='npkkaryawan' required>
                   </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal: </label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control border-input" name="tanggal" value="{{ $data->tanggal }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jam Ke:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="jamke">
                            @foreach($enumJamke as $d)
                                <option value="{{$d}}"  {!! ($data->jamke == $d ? ' selected' : '') !!}>{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Keterangan:</label>
                    <div class="col-sm-9">
                         <textarea name="keterangan" class="form-control border-input">{{ $data->keterangan }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endif
@endsection

@section('footer_scripts')
<script>
$( function() {
    var karyawans = [
        <?php foreach ($karyawans as $d){ ?>
            "<?php echo $d->npk . " - " . $d->nama;?>",
        <?php } ?>
    ];
    $( "#npkkaryawan" ).autocomplete({
      source: karyawans
    });
});
</script>
@endsection
