@extends('maf._layouts.base')

@section('title', 'Master Mata Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<form class="form-horizontal" method="GET">
  <div class="form-group">
      <div class="visible-lg visible-md">
        <label class="col-md-3 control-label" style="margin-top: 5px;">Jurusan: </label>
      </div>
      <div class="col-xs-8 col-md-6">
          <select class="form-control border-input" name="ids">
            <option value="0" {!! ($ids == 0 ? ' selected' : '') !!}>Semua Jurusan</option>
              @foreach($jurusans as $s)
                <option value="{{$s->IdJurusan}}" {!! ($ids == $s->IdJurusan ? ' selected' : '') !!}>{{$s->tahunajaran}} {{ $s->Nama }}</option>
              @endforeach
          </select>
      </div>
      <div class="col-xs-2 col-md-3">
        <button class="btn btn-success"><i class="fa fa-search"></i></button>
      </div>
  </div>
</form>
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Mata Kuliah </h4>
      
        <a href="{!! action('MataKuliahController@aktifkansemuaMataKuliah') !!}">
          <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-star"></i>
            <span class="hidden-sm hidden-xs">Buka Semua MK</span>
          </button>
        </a>

        <a href="{{URL::route('maf.mastermk.add')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{!! action('MataKuliahController@showDataMatakuliahs') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{!! action('MataKuliahController@showDataMataKuliah') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelMataKuliah" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th width="10px">Aksi</th>
            <th>MK</th>
            <th class="hidden-xs">Nama</th>
            <th>SKS</th>
            <th>Jenis</th>
            <th>Buka</th>
            <th>Tipe</th>
            <th>Flag Skripsi</th>
            <th>Nama Eng</th>
          </tr>
        </thead>
        <tbody>
          @foreach($mks as $mk)
            <tr>
              <td>
              @if($mk->deleted_at == null)
                <a href="{{url('maf/mastermk/h-'.$mk->kodemk)}}" onclick="return confirm('Yakin akan menghapus data?')">Hapus</a> | 
                <a href="{{url('maf/mastermk/edit-'.$mk->kodemk)}}">Ubah</a>
              @else
                <a href="{{url('maf/mastermk/r-'.$mk->kodemk)}}">Batalkan hapus</a> |
                <a href="{{url('maf/mastermk/f-'.$mk->kodemk)}}" onclick="return confirm('Yakin akan benar-benar menghapus data?')">Hapus Permanen</a>
              @endif
              </td>
              <td>{{ $mk->kodemk }}
                <span class="visible-xs"> - {{ $mk->nama }}</span>
              </td>
              <td class="hidden-xs">{{ $mk->nama }}</td>
              <td>{{ $mk->sks }}</td>
              <td>{{ $mk->jenis }}</td>
              <td>
                @if($mk->statusbuka == "Ya") <a href="{{ url('maf/mastermk/a-' . $mk->kodemk)}}" onclick="return confirm('Ubah status menjadi Tidak?')">{{ $mk->statusbuka }}</a> 
                @else <a href="{{ url('maf/mastermk/a-' . $mk->kodemk)}}" onclick="return confirm('Ubah status menjadi Ya?')">{{ $mk->statusbuka }}</a>
                @endif
              </td>
              <td>{{ $mk->tipe }}</td>
              <td>{{ $mk->flagskripsi }}</td>
              <td>{{ $mk->namaeng }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelMataKuliah").dataTable( {
      "pagingType": "full",
      "lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
      "stateSave": true,
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'pdf', 'colvis'
        ]
    });
    
  });
</script>
@endsection