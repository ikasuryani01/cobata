@extends('maf._layouts.base')

@section('title', 'Master Ruangan')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')

<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.8em;">Data Ruangan</h4>
    
        <a href="{{URL::route('maf.masterruangan.add')}}">
          <button class="btn btn-sm btn-success" style="float: right;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('maf.masterruangans')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('maf.masterruangan')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelRuangan" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th>Aksi</th>
          <th>Nama</th>
          <th>Jenis</th>
          <th>Kuliah</th>
          <th>Kapasitas Kuliah</th>
          <th>Ujian</th>
          <th>Kapasitas Ujian</th>
          <th>Gedung</th>
          <th>Lantai</th>
          <th>No. Ruang</th>
        </tr>
        </thead>
        <tbody>
          @foreach($ruangans as $ruangan)
            <tr>
              <td>
              @if($ruangan->deleted_at == null)
                <a href="{!! action('RuanganController@deleteDataRuangan', $ruangan->id) !!}">Hapus</a> |
                <a href="{!! action('RuanganController@editDataRuangan', $ruangan->id) !!}">Ubah</a>
              @else
                <a href="{!! action('RuanganController@restoreDataRuangan', $ruangan->id) !!}">Aktifkan</a>
              @endif
              </td>
              <td>{{ $ruangan->nama }}</td>
              <td>{{ $ruangan->jenis }}</td>
              <td>{{ $ruangan->untukkuliah }}</td>
              <td>{{ $ruangan->kapasitas }}</td>
              <td>{{ $ruangan->untukujian }}</td>
              <td>{{ $ruangan->kapasitasujian }}</td>
              <td>{{ $ruangan->gedung }}</td>
              <td>{{ $ruangan->lantai }}</td>
              <td>{{ $ruangan->noruang }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelRuangan").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[2, 'asc']],
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection