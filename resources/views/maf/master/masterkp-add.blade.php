@extends('maf._layouts.base')

@section('title', 'Master Kelas Paralel')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }

    @media only screen and (min-width: 768px) and (max-width: 1200px) {
        .spesial {
            margin-left: 20px;
        }
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info">{!! session('status') !!}</label>
<br></div>
@endif
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline;">Tambah Kelas Paralel</h4>
                <p>NB: Otomatis ditambahkan pada semester yang sedang aktif</p>

            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class='form-group ui-widget'>
                    <label class='col-sm-3 control-label'>Mata Kuliah: </label>
                    <div class='col-sm-9'>
                        <input type='text' id='mks' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='kodemk' required>
                   </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jumlah KP: </label>
                    <div class="col-sm-9">
                        <input type="number" id="jumlahkp" class="form-control border-input" name="jumlahkp" value="{{ old('jumlahkp') }}" onchange="ubahkolomnama()">
                    </div>
                </div>
                <hr>
                <div id="rinciankp">
                    <!-- bakal diisi dari script, dynamic element -->
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ route('maf.masterkp') }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>

    @elseif ($mode == "edit")
        <div class="card">
            <div class="header">
                <h4 class="title">Ubah Data Kelas Paralel</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Mata Kuliah: </label>
                    <div class="col-sm-9">
                        <input type="text" readonly class="form-control border-input" name="kodemk" value="{{ $kp->kodemk }} - {{$kp->mk->nama }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kode KP: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="kodekp" value="{{ $kp->kodekp }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kapasitas: </label>
                    <div class="col-sm-9">
                        <input type="number" min="0" class="form-control border-input" name="kapasitas" value="{{ $kp->kapasitas }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Aktif</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="statusaktif">
                            <option value="Ya" {!! ($kp->jenis == "Ya" ? ' selected' : '') !!}>Ya</option>
                            <option value="Tidak" {!! ($kp->jenis == "Tidak" ? ' selected' : '') !!}>Tidak</option>
                        </select>
                    </div>
                </div>
               
                <div class="form-group">
                    <label class="col-sm-3 control-label">Langsung Nisbi:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="npkdosenpengajar">
                        
                        @foreach($dosen as $d)
                            <option value="{{$d->npk}}" {!! ($d->npk == $kp->npkdosenpengajar ? ' selected' : '') !!}>{{$d->npk}} - {{$d->nama}}</option>
                        @endforeach
                            
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ route('maf.masterkp') }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endif

@endsection


@section('footer_scripts')
@if($mode == "add")
<script>
    function ubahkolomnama(argument) {
        // buat hapus div tmpatnya element baru
        $("#rinciankp").empty(); 
        // ambil nilai yang diinput dr textbox
        var jumlah = $("#jumlahkp").val(); 
        // ngeloop nge append tambah element
        for (var i = 1; i <= jumlah; i++) { 
            var element = 
                "<div class='form-group'>" + 
                    "<label class='col-sm-3 control-label'>Nama KP: </label>" +
                    "<div class='col-sm-2'>" +
                        "<input type='text' required class='form-control border-input' name='kodekp-" + i + "' value='" + String.fromCharCode(64+i) + "' >" +
                    "</div>" +
                    "<label class='col-sm-1 control-label'>Kapasitas: </label>" +
                    "<div class='col-sm-2 spesial'>" +
                        "<input type='number' class='form-control border-input' name='kapasitas-" + i + "' required>" +
                    "</div>" +
                    "<label class='col-sm-1 control-label'>Status: </label>" +
                    "<div class='col-sm-2'>" +
                        "<select name='statusaktif-" + i + "' class='form-control border-input' required>" +
                            "<option value='Ya'>Buka</option>" +
                            "<option value='Tidak'>Tutup</option>" +
                        "</select>" +
                    "</div>" +
                "</div>" +
                "<div class='form-group'>" +
                    "<label class='col-sm-3 control-label'>Dosen Pengajar: </label>" +
                    "<div class='col-sm-9'>" +
                        "<select name='npkdosenpengajar-" + i + "' class='form-control' required>" +
                        <?php foreach ($dosen as $dos){ ?>
                     "<option value='<?php echo $dos->npk;?>'><?php echo $dos->npk . " - " . $dos->nama;?></option>"+
                        <?php } ?>
                    "</select>" +
                    "</div>" +
                "</div>";
            $("#rinciankp").append(element);
        }
    }
</script>

<script>
$( function() {
    var mks = [
        <?php foreach ($matakuliah as $mk){ ?>
            "<?php echo $mk->kodemk . " " . $mk->nama;?>",
        <?php } ?>
    ];
    $( "#mks" ).autocomplete({
      source: mks
    });
});
</script>


@endif
@endsection