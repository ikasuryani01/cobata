@extends('maf._layouts.base')

@section('title', 'Master Jurusan - Mata Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
    .modal-dialog{
      overflow-y: initial !important
    }
    .modal-body{
        height: 400px;
        overflow-y: auto;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.8em;">Data Mata Kuliah</h4>
        <p>Tambah untuk Jurusan {{ $nama }}</p>

    </div>
    <div class="content">
      <table id="tabelMk" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th width="20px">Aksi</th>
          <th>MK</th>
          <th class="hidden-xs">Nama</th>
          <th>SKS</th>
          <th>Jurusan</th>
        </tr>
        </thead>
        <tbody>
          @foreach($datamks as $data)
            <tr>
              <td>
                @if($data->adaJurusan($idj))
                  <a class="btn btn-sm btn-warning" href="{!! action('JurusanMkController@deleteJurusanPilihMk', 'j='.$idj.'&km='.$data->kodemk) !!}">Hapus</a>
                @else
                  <a class="btn btn-sm btn-success" href="{!! action('JurusanMkController@addJurusanPilihMk', 'j='.$idj.'&km='.$data->kodemk) !!}">Tambah</a>
                @endif
              </td>
              <td>{{ $data->kodemk }} <span class="visible-xs">{{$data->nama}}</span></td>
              <td class="hidden-xs">{{ $data->nama }}</td>
              <td>{{ $data->sks }}</td>
              <td>
              @if($data->getJurusan()->count() > 0)
                @foreach($data->getJurusan() as $jur)
                  {{ $jur->Nama }}<br>
                @endforeach
              @else
                {{ "-" }}
              @endif
              </td>
              
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelMk").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[4, 'asc']],
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection