@extends('maf._layouts.base')

@section('title', 'Master Karyawan')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Tambah Karyawan Sumber Extern</h4>

        <a href="{{URL::route('maf.masterkaryawan.storeext')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambahkan Semua</span>
          </button>
        </a>

    </div>
    <form class="form-inline">
    <div class="content">
      <table id="tabelKaryawan" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>NPK</th>
            <th>Nama</th>
            <th>Tipe</th>
            <th>Jenis</th>
            <th>Status</th>
            <th>Jaga Ujian</th>
            <th>Fakultas</th>
            <th>Kode Dosen</th>
          </tr>
        </thead>
        <tbody>
          @foreach($karyawans as $karyawan)
            <tr>
              <td>{{ $karyawan->npk }}</td>
              <td>{{ $karyawan->gelardepan }}{{ $karyawan->nama }}{{ $karyawan->gelarbelakang }}</td>
              <td>{{ $karyawan->tipe }}</td>
              <td>{{ $karyawan->jenis }}</td>
              <td>{{ $karyawan->idstatusaktif }}</td>
              <td>{{ $karyawan->aktifjagaujian}}</td>
              <td>{{ $karyawan->fakultas->Nama }}</td>
              <td>{{ $karyawan->kodedosen }}</td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>NPK</th>
            <th>Nama</th>
            <th>Tipe</th>
            <th>Jenis</th>
            <th>Status</th>
            <th>Jaga Ujian</th>
            <th>Fakultas</th>
            <th>Kode Dosen</th>
          </tr>
        </tfoot>
      </table>
    </div>
    </form>
</div>

@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKaryawan").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection

