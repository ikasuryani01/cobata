@extends('maf._layouts.base')

@section('title', 'Master Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<form class="form-horizontal" method="GET">
  <div class="form-group">
      <div class="visible-lg visible-md">
        <label class="col-md-3 control-label" style="margin-top: 5px;">Lihat Jurusan: </label>
      </div>
      <div class="col-xs-8 col-md-6">
          <select class="form-control border-input" name="ids">
              <option value="0" {!! ($ids == 0 ? ' selected' : '') !!}>Semua Jurusan</option>
            @foreach($jurusans as $j)
              <option value="{{$j->IdJurusan}}" {!! ($ids == $j->IdJurusan ? ' selected' : '') !!}>{{$j->Nama}}</option>
            @endforeach
          </select>
      </div>
      <div class="col-xs-2 col-md-3">
        <button class="btn btn-success">Pilih</button>
      </div>
  </div>
</form>
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Mahasiswa</h4>
        <a href="{{URL::route('maf.mastermhs.add')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('maf.mastermhss')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('maf.mastermhs')}}">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif
        <br>
        <p style="font-size: 0.8em;">Menampilkan {{$namajurusan}}</p>

    </div>
    <div class="content">
      <table id="tabelMhs" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead >
        <tr>
          <th width="10px">Aksi</th>
          <th>NRP</th>
          <th class="hidden-xs">Nama</th>
          <th>Jurusan</th>
          <th>TOEFL</th>
          <th>Status</th>
          <th>SKS Maks Depan</th>
          <th>Asisten</th>
          <th>IPK tanpa E</th>
          <th>Tahun Akademik Terima</th>
          <th>Semester Terima</th>
          <th>Tanggal Status</th>
          <th>Status Lunas UPP</th>
          <th>SKSKm tanpa E</th>
          <th>Masa Studi</th>
        </tr>
        </thead>
        <tbody>
          @foreach($mahasiswas as $mhs)
            <tr>
              <td>
              @if($mhs->deleted_at == null)
                <a href="{!! action('MahasiswaController@deleteDataMahasiswa', $mhs->nrp) !!}">Hapus</a> | 
                <a href="{!! action('MahasiswaController@editDataMahasiswa', $mhs->nrp) !!}">Ubah</a>
              @else
                <a href="{!! action('MahasiswaController@restoreDataMahasiswa', $mhs->nrp) !!}">Aktifkan</a> |
                <a href="{!! action('MahasiswaController@forcedeleteDataMahasiswa', $mhs->nrp) !!}" onclick="return confirm('Data akan benar-benar dihapus dari basis data. Konfirmasi?')">Hapus Lagi</a>
              @endif
              </td>
              <td>{{ $mhs->nrp }}<span class="visible-xs"> - {{ $mhs->nama }}</span></td>
              <td class="hidden-xs">{{ $mhs->nama }}</td>
              <td>{{ $mhs->jurusan->Nama }}</td>
              <td>{{ $mhs->nilaitoefl }}</td>
              <td>{{ $mhs->kodestatus }}</td>
              <td>{{ $mhs->sksmaxdepan }}</td>
              <td>{{ $mhs->asisten }}</td>
              <td>{{ $mhs->ipktanpae }}</td>
              <td>{{ $mhs->tahunakademikterima }}</td>
              <td>{{ $mhs->semesterterima }}</td>
              <td>{{ $mhs->tglstatus }}</td>
              <td>{{ $mhs->statuslunasupp }}</td>
              <td>{{ $mhs->skskumtanpae }}</td>
              <td>{{ $mhs->masastudi }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelMhs").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "stateSave": true,
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection