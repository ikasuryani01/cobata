@extends('maf._layouts.base')

@section('title', 'Master Ruangan')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Ubah Data Ruangan</h4>
                <a href="{{URL::route('maf.masterruangan.add')}}">
                  <button class="btn btn-warning" style="float: right;">
                    <i class="fa fa-database"></i>
                    <span class="hidden-sm hidden-xs">Ambil Data dari BAAK</span>
                  </button>
                </a>
                <br>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Ruangan: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nama" value="{{ old('nama') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Gedung: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="gedung" value="{{ old('gedung') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Lantai:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="lantai" value="{{ old('lantai') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">No. Ruang:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="noruang" value="{{ old('noruang') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="jenis">
                            @foreach($enumJenis as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Untuk Kuliah:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="untukkuliah">
                            <option value="Ya">Ya</option>
                            <option value="Tidak">Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kapasitas Kuliah:</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="kapasitas" value="{{ old('kapasitas') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Untuk Ujian:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="untukujian">
                            <option value="Ya">Ya</option>
                            <option value="Tidak">Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kapasitas Ujian:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="kapasitasujian" value="{{ old('kapasitasujian') }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @elseif ($mode == "edit")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Ruangan</h4>

                <!-- <a href="{{URL::route('maf.masterruangan.add')}}">
                  <button class="btn btn-warning" style="float: right;">
                    <i class="fa fa-database"></i>
                    <span class="hidden-sm hidden-xs">Ambil Data dari BAAK</span>
                  </button>
                </a> -->
                <br>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Ruangan: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="nama" value="{{ $ruangan->nama }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Gedung: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="gedung" value="{{ $ruangan->gedung }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Lantai:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="lantai" value="{{ $ruangan->lantai }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">No. Ruang:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="noruang" value="{{ $ruangan->noruang }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="jenis">
                            @foreach($enumJenis as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Untuk Kuliah:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="untukkuliah">
                            <option value="Ya">Ya</option>
                            <option value="Tidak">Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kapasitas Kuliah:</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control border-input" name="kapasitas" value="{{ $ruangan->kapasitas }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Untuk Ujian:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="untukujian">
                            <option value="Ya">Ya</option>
                            <option value="Tidak">Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kapasitas Ujian:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="kapasitasujian" value="{{ $ruangan->kapasitasujian }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endif



@endsection

