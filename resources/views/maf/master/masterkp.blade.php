@extends('maf._layouts.base')

@section('title', 'Master Kelas Paralel')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<form class="form-horizontal" method="GET">
  <div class="form-group">
      <div class="visible-lg visible-md">
        <label class="col-md-3 control-label" style="margin-top: 5px;">Semester: </label>
      </div>
      <div class="col-xs-8 col-md-6">
          <select class="form-control border-input" name="ids">
              @foreach($semester as $s)
                <option value="{{$s->id}}" {!! ($ids == $s->id ? ' selected' : '') !!}>{{$s->tahunajaran}} {{ $s->semester }}</option>
              @endforeach
          </select>
      </div>
      <div class="col-xs-2 col-md-3">
        <button class="btn btn-success"><i class="fa fa-search"></i></button>
      </div>
  </div>
</form>
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Kelas Paralel</h4>
      
        <a href="{{URL::route('maf.masterkp.adddummy')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{!! action('MafController@showDataKelasParalels') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{!! action('MafController@showDataKelasParalel') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelKelasParalel" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>Aksi</th>
            <th class="hidden-xs">Kode MK</th>
            <th>Nama</th>
            <th class="hidden-xs">Status</th>
            <th>Dosen</th>
            <th>Kaps</th>
            <th>Isi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($kps as $kp)
            <tr>
              <td>
              @if($kp->deleted_at == null)
                <a href="{!! action('MafController@deleteDataKelasParalel', $kp->id) !!}" onclick="return confirm('Yakin akan menghapus data?')">Hapus</a> | 
                <a href="{!! action('MafController@editDataKelasParalel', $kp->id) !!}">Ubah</a>
              @else
                <a href="{!! action('MafController@restoreDataKelasParalel', $kp->id) !!}">Batalkan hapus</a> |
                <a href="{!! action('MafController@forcedeleteDataKelasParalel', $kp->id) !!}" onclick="return confirm('Yakin akan benar-benar menghapus data?')">Hapus Permanen</a>
              @endif
              </td>
              <td class="hidden-xs">{{$kp->kodemk}}</td>
              <td><span class="visible-xs">{{$kp->kodemk}} - </span>{{ $kp->mk->nama }} KP: {{$kp->kodekp}}
                @if($kp->statusaktif == "Tidak")
                  <span class="visible-xs"><br>Tutup - [<a href="{!! action('MafController@aktifkanKelasParalel', $kp->id) !!}">Aktifkan</a>]</span>
                @else
                  <span class="visible-xs"><br>Buka - [<a href="{!! action('MafController@nonaktifkanKelasParalel', $kp->id) !!}">NonAktifkan</a>]</span>
                @endif
              </td>
              <td class="hidden-xs">
               @if($kp->statusaktif == "Tidak")
                  <span class="hidden-xs">Tutup - [<a href="{!! action('MafController@aktifkanKelasParalel', $kp->id) !!}">Aktifkan</a>]</span>
                @else
                  <span class="hidden-xs">Buka - [<a href="{!! action('MafController@nonaktifkanKelasParalel', $kp->id) !!}">NonAktifkan</a>]</span>
                @endif
              </td>
              <td>{{ $kp->npkdosenpengajar }} - {{ $kp->karyawan->nama }}</td>
              <td>{{ $kp->kapasitas }}</td>
              <td>{{ $kp->isi }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKelasParalel").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection