@extends('maf._layouts.base')

@section('title', 'Hasil FPP')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Hasil FPP</h4>
        
        <a data-toggle="modal" data-target="#konfirmasi">
          <button class="btn btn-info btn-sm" style="float: right;">
            <i class="fa fa-caret-right"></i>
            <span class="hidden-sm hidden-xs">Proses Perwalian</span>
          </button>
        </a>

        
        <!-- <a > -->
          <div class="btn-group" style="float:right;">
            <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right; margin-right: 5px;">
              <i class="fa fa-sun-o"></i>
              <span class="hidden-sm hidden-xs">Publikasi Semua</span>
            </button>
            <ul class="dropdown-menu" style="left: -80px;">
              <li style="padding: 2px;">
                <a data-toggle="modal" data-target="#konfirmasiPublikasi1">FPP 1</a>
              </li>
              <li style="padding: 2px;">
                <a data-toggle="modal" data-target="#konfirmasiPublikasi2">FPP 2</a>
              </li>
              <li style="padding: 2px;">
                <a data-toggle="modal" data-target="#konfirmasiPublikasi3">KK</a>
              </li>
            </ul>
          </div>
        <!-- </a> -->
        

    </div>
    <div class="content">
      <table id="tabelKonversi" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th><span class="visible-xs">Mata Kuliah (KP)</span><span class="hidden-xs">Kode MK</span></th>
          <th class="hidden-xs">Nama (KP)</th>
          <!-- <th>KP</th> -->
          <th>Isi / Kap</th>
          <th>Status</th>
        </tr>
        </thead>
        <tbody>
          @foreach($datas as $data)
            <tr>
              <td>{{ $data->kodemk }} <span class="visible-xs">{{ $data->mk->nama }} ({{ $data->kodekp }}) 
              <a href="{!! action('ProsesHasilFPPController@lihatDetailKelas', $data->id) !!}">[Detail]</a></span></td>
              <td class="hidden-xs">{{ $data->mk->nama }} ({{ $data->kodekp }}) 
              <a href="{!! action('ProsesHasilFPPController@lihatDetailKelas', $data->id) !!}">[Detail]</a></td>
              <td>{{ $data->hitungIsiKeterima() }} / {{ $data->kapasitas }}</td>
              <td>{{ $data->getStatusHasilFpp() }}</td> <!-- Pending, Pending sebagian, Selesai di proses -->
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>

<div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah Anda yakin untuk memproses hasil fpp untuk semua mata kuliah pada semester ini?
        <br>Catatan: Tindakan ini tidak dapat dibatalkan
        </p>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Tidak</button>
          <a href="{{URL::route('maf.prosessemua')}}">
            <button type="button" class="btn btn-success">Ya</button>
          </a>
        </center>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="konfirmasiPublikasi1" tabindex="-1" role="dialog" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah Anda yakin untuk mempublikasi hasil fpp untuk semua mata kuliah pada semester ini kepada mahasiswa?
        <br>Catatan: Tindakan ini tidak dapat dibatalkan
        </p>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Tidak</button>
          <a href="{!! action('ProsesHasilFPPController@publikasiSemua', 'FPP 1') !!}">
            <button type="button" class="btn btn-success">Ya</button>
          </a>
        </center>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="konfirmasiPublikasi2" tabindex="-1" role="dialog" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah Anda yakin untuk mempublikasi hasil fpp untuk semua mata kuliah pada semester ini kepada mahasiswa?
        <br>Catatan: Tindakan ini tidak dapat dibatalkan
        </p>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Tidak</button>
          <a href="{!! action('ProsesHasilFPPController@publikasiSemua', 'FPP 2') !!}">
            <button type="button" class="btn btn-success">Ya</button>
          </a>
        </center>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="konfirmasiPublikasi3" tabindex="-1" role="dialog" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah Anda yakin untuk mempublikasi hasil fpp untuk semua mata kuliah pada semester ini kepada mahasiswa?
        <br>Catatan: Tindakan ini tidak dapat dibatalkan
        </p>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Tidak</button>
          <a href="{!! action('ProsesHasilFPPController@publikasiSemua', 'KK') !!}">
            <button type="button" class="btn btn-success">Ya</button>
          </a>
        </center>
      </div>
    </div>
  </div>
</div>

@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKonversi").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[3, 'desc']],
      "stateSave" : "true",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection