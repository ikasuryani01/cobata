@extends('maf._layouts.base')

@section('title', 'Detail Hasil FPP')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')

@if($mode == "show")
<div class="card">
  <div class="header">
    <h4 class="title" style="display: inline; line-height: 1.5em;">Hasil FPP {{$kp->mk->nama}} KP: {{$kp->kodekp}}</h4>
    <!--  editHasilFppbyId editHasilFppbyKelas -->
    <a href="{{ route('maf.hasilfpp') }}">
      <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
        <i class="fa fa-caret-left"></i>
        <span class="hidden-sm hidden-xs">Kembali</span>
      </button>
    </a>

    <div class="btn-group" style="float:right;">
      <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right; margin-right: 5px;">
        <i class="fa fa-bars"></i>
        <span class="hidden-sm hidden-xs">Menu</span>
      </button>
      <ul class="dropdown-menu" style="left: -80px;">
        <li style="padding: 2px;">
          <a href="{!! action('ProsesHasilFPPController@editHasilFppbyKelas', $kp->id) !!}">
            Edit Hasil Kelas Ini
          </a>
        </li>
        @if($kp->cekStatusPublikasi())
        <li style="padding: 2px;">
          <a href="{!! action('ProsesHasilFPPController@tidakPublikasibyKelas', $kp->id) !!}">
            Jangan Publikasi Kelas Ini
          </a>
        </li>
        @else
        <li style="padding: 2px;">
          <a href="{!! action('ProsesHasilFPPController@publikasibyKelas', $kp->id) !!}">
            Publikasi Kelas Ini
          </a>
        </li>
        @endif
        <li style="padding: 2px;">
          <a href="{!! action('ProsesHasilFPPController@transferMhsindex', $kp->id) !!}">
            Transfer Mahasiswa
          </a>
        </li>
        
      </ul>
    </div>
  </div>
  
  <div class="content">
    <table id="tabelKonversi" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
      <thead>
      <tr>
        <th width="10px">No</th>
        <th>Mahasiswa</th>
        <th class="hidden-xs">Nama</th>
        <th>Terima</th>
        <th>Alasan</th>
        <th>Publikasi</th>
        <th>Edit</th>
      </tr>
      </thead>
      <tbody>
        @foreach($datapendaftar as $data)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $data->nrp }} <span class="visible-xs">{{ $data->mahasiswa->nama }}</span></td>
            <td class="hidden-xs">{{ $data->mahasiswa->nama }}</td>
            <td>{{ $data->status }}</td>
            <td>{{ $data->alasan }}</td>
            <td>{{ $data->statuspublikasi }}</td>
            <td><a href="{!! action('ProsesHasilFPPController@editHasilFppbyId', $data->id) !!}">Edit</a></td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@elseif($mode == "edit")
<div class="card">
  <div class="header">
    <h4 class="title" style="display: inline; line-height: 1.5em;">Edit Hasil FPP {{$datapendaftarkelas[0]->kp->mk->nama}} KP: {{$datapendaftarkelas[0]->kp->kodekp}}</h4>
    <form class="form-horizontal" method="POST" name="editkelas" style="display: inline;">
    <input type="submit" class="btn btn-success btn-sm" name="submit" value="Simpan" style="float: right; margin-left: 2px;">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="idkelas" value="{{ $datapendaftarkelas[0]->idkelasparalel }}">
    <a href="{{ route('maf.detailkelas', $datapendaftarkelas[0]->idkelasparalel) }}">
      <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
        <i class="fa fa-caret-left"></i>
        <span class="hidden-sm hidden-xs">Kembali</span>
      </button>
    </a>
  </div>
  <!-- <br> -->
  <div class="content">
    <table class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%">
      <thead>
      <tr>
        <th>Mahasiswa</th>
        <th>Terima</th>
        <th>Alasan</th>
        <th>Publikasi</th>
        <th>Edit</th>
      </tr>
      </thead>
      <tbody>
        @foreach($datapendaftarkelas as $data)
          <tr>
            <td>{{ $data->nrp }} - {{ $data->mahasiswa->nama }}</a></td>
            <td align="center">
              <select class="form-control border-input" name="status-{{$data->id}}">
                  @foreach($enumStatusTerima as $d)
                      <option value="{{$d}}" {!! ($data->status == $d ? ' selected' : '') !!}>{{$d}}</option>
                  @endforeach
              </select>
            </td>
            <td>{{ $data->alasan }}</td>
            <td>{{ $data->statuspublikasi }}</td>
            <td><a href="{!! action('ProsesHasilFPPController@editHasilFppbyId', $data->id) !!}">Edit</a></td>
          </tr>
        @endforeach
        </form>
      </tbody>
      <tfoot>
      <tr>
        <th>Mahasiswa</th>
        <th>Terima</th>
        <th>Alasan</th>
        <th>Publikasi</th>
        <th>Edit</th>
      </tr>
      </tfoot>
    </table>
  </div>
</div>
@elseif($mode == "editsatu")
<div class="card">
  <div class="header">
    <h4 class="title" style="display: inline; line-height: 1.5em;">Edit Data</h4>
  </div>
  <div class="content">
    <form class="form-horizontal" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="iddata" value="{{ $datapendaftarkelas->id }}">
      <div class="form-group">
          <label class="col-sm-3 control-label">Mahasiswa: </label>
          <div class="col-sm-9">
              <input type="text" class="form-control border-input" name="mhs" disabled value="{{ $datapendaftarkelas->nrp }} - {{ $datapendaftarkelas->mahasiswa->nama }}">
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-3 control-label">Kelas: </label>
          <div class="col-sm-9">
              <input type="text" class="form-control border-input" name="kelas" disabled value="{{ $datapendaftarkelas->kp->kodemk }} - {{ $datapendaftarkelas->kp->mk->nama }} KP: {{$datapendaftarkelas->kp->kodekp}}">
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-3 control-label">Status Terima:</label>
          <div class="col-sm-9">
              <select class="form-control border-input" name="status">
                  @foreach($enumStatusTerima as $d)
                      <option value="{{$d}}" {!! ($datapendaftarkelas->status == $d ? ' selected' : '') !!}>{{ $d }}</option>
                  @endforeach
              </select>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-3 control-label">Alasan: </label>
          <div class="col-sm-9">
              <input type="text" class="form-control border-input" name="alasan" value="{{ $datapendaftarkelas->alasan }}">
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-3 control-label">Alasan:</label>
          <div class="col-sm-9">
              <select class="form-control border-input" name="statuspublikasi">
                  <option value="Ya" {!! ($datapendaftarkelas->statuspublikasi == "Ya" ? ' selected' : '') !!}>Ya</option>
                  <option value="Tidak" {!! ($datapendaftarkelas->statuspublikasi == "Tidak" ? ' selected' : '') !!}>Tidak</option>
              </select>
          </div>
      </div>

      <div class="form-group">
          <div class="col-sm-9 col-sm-offset-3">
              <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
              <a href="{{ URL::previous() }}" class="btn btn-danger"> Kembali</a>
          </div>
      </div>
      </form>
  </div>
</div>
@elseif($mode == "transfer")
<div class="card">
  @if($statempty == 1)
  <div class="header" style="margin-bottom: -15px;">
    <h4 class="title" style="display: inline; line-height: 1.5em;">Edit Hasil FPP {{$datapendaftarkelas[0]->kp->mk->nama}} KP: {{$datapendaftarkelas[0]->kp->kodekp}}</h4>
    <form class="form-horizontal" method="POST" name="editkelas" style="display: inline;">
    <input type="submit" class="btn btn-success btn-sm" name="submit" value="Simpan" style="float: right; margin-left: 2px;">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="idkelas" value="{{ $datapendaftarkelas[0]->idkelasparalel }}">
    <input type="hidden" name="kodekpasal" value="{{ $datapendaftarkelas[0]->kp->kodekp }}">
    <a href="{{ route('maf.detailkelas', $datapendaftarkelas[0]->idkelasparalel) }}">
      <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
        <i class="fa fa-caret-left"></i>
        <span class="hidden-sm hidden-xs">Kembali</span>
      </button>
    </a>

    <br>
    <p>Data Tercentang: <label name="jmlcentang"><b>0</b></label></p>
    <div class="form-group">
        <div class="visible-lg visible-md">
          <label class="col-xs-4 col-md-2 control-label" style="margin-top: 5px;">Kelas Tujuan: </label>
        </div>
        <div class="col-xs-8 col-md-10">
            <select class="form-control border-input" name="idkelas">
                @foreach($kps as $kp)
                  <option value="{{$kp->id}}">{{ $kp->mk->nama }} KP: {{$kp->kodekp}}</option>
                @endforeach
            </select>
        </div>
    </div>
  </div>
  <div class="content">
    <table class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%">
      <thead>
      <tr>
        <th width="10px">Cek</th>
        <th>Mahasiswa</th>
        <th>Terima</th>
        <th>Alasan</th>
        <th>Publikasi</th>
        <th>Edit</th>
      </tr>
      </thead>
      <tbody>
        @foreach($datapendaftarkelas as $data)
          <tr>
            <td>
              <label class="checkbox">
                <input type="checkbox" name="cek[]" class="cb" value="{{$data->id}}">
              </label>
            </td>
            <td>{{ $data->nrp }} - {{ $data->mahasiswa->nama }}</a></td>
            <td align="center">
              <select class="form-control border-input" name="status-{{$data->id}}">
                  @foreach($enumStatusTerima as $d)
                      <option value="{{$d}}" {!! ($data->status == $d ? ' selected' : '') !!}>{{$d}}</option>
                  @endforeach
              </select>
            </td>
            <td>{{ $data->alasan }}</td>
            <td>{{ $data->statuspublikasi }}</td>
            <td><a href="{!! action('ProsesHasilFPPController@editHasilFppbyId', $data->id) !!}">Edit</a></td>
          </tr>
        @endforeach
        </form>
      </tbody>
      <tfoot>
      <tr>
        <th width="10px">Cek</th>
        <th>Mahasiswa</th>
        <th>Terima</th>
        <th>Alasan</th>
        <th>Publikasi</th>
        <th>Edit</th>
      </tr>
      </tfoot>
    </table>
  </div>
  @else
  <div class="alert alert-info">
    <p>Tidak ada kelas lain dengan jadwal sama yang dapat dijadikan tujuan transfer [<a href="{{ route('maf.detailkelas', $datapendaftarkelas[0]->idkelasparalel) }}"><b>Kembali</b></a>]</p>
  </div>
  @endif
</div>
@endif

@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKonversi").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
  $('.cb').change(function () {
    var a = $('[name="cek[]"]:checked').length;
    // alert(a);
    $("label[name='jmlcentang']").text(a);
   });
  
</script>
@endsection