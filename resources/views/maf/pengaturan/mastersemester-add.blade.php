@extends('maf._layouts.base')

@section('title', 'Master Semester')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if(session('status') != "")
    <div class="alert alert-info"><center>{!! session('status') !!}</label>
    <br></center></div>
    @endif
    @if($mode == "add")
    <div class="card">
        <div class="header">
            <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Semester</h4>
        </div>
        <div class="content">
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Ajaran: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="tahunajaran" value="{{ old('tahunajaran') }}" placeholder="contoh: 2017/2018 ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester: </label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="semester">
                            @foreach($enumSemester as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">UTS:</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control border-input" name="tanggalmulaiuts" value="{{ old('tanggalmulaiuts') }}">
                    </div>
                    <label class="col-sm-1 control-label" style="margin-left: -25px;">sampai</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control border-input" name="tanggalselesaiuts" value="{{ old('tanggalselesaiuts') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">UAS:</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control border-input" name="tanggalmulaiuas" value="{{ old('tanggalmulaiuas') }}">
                    </div>
                    <label class="col-sm-1 control-label" style="margin-left: -25px;">sampai</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control border-input" name="tanggalselesaiuas" value="{{ old('tanggalselesaiuas') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Aktif:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="statusaktif">
                            <option value="Ya">Ya</option>
                            <option value="Tidak">Tidak</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ route('maf.mastersemester') }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @elseif ($mode == "edit")
    <div class="card">
        <div class="header">
            <h4 class="title" style="display: inline; line-height: 1.8em;">Edit Semester</h4>
            <br>
        </div>
        <div class="content">
            <form class="form-horizontal" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Ajaran: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="tahunajaran" value="{{ $semester->tahunajaran }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester: </label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="semester">
                            @foreach($enumSemester as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">UTS:</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control border-input" name="tanggalmulaiuts" value="{{ $semester->tanggalmulaiuts }}">
                    </div>
                    <label class="col-sm-1 control-label" style="margin-left: -25px;">sampai</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control border-input" name="tanggalselesaiuts" value="{{ $semester->tanggalselesaiuts }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">UAS:</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control border-input" name="tanggalmulaiuas" value="{{ $semester->tanggalmulaiuas }}">
                    </div>
                    <label class="col-sm-1 control-label" style="margin-left: -25px;">sampai</label>
                    <div class="col-sm-4">
                        <input type="date" class="form-control border-input" name="tanggalselesaiuas" value="{{ $semester->tanggalselesaiuas }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Aktif:</label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="statusaktif">
                            <option value="Ya">Ya</option>
                            <option value="Tidak">Tidak</option>
                        </select>
                    </div>
                </div>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ route('maf.mastersemester') }}" class="btn btn-danger"> Kembali</a>
                </div>
            </div>
            </form>
        </div>
    </div>
    @endif
@endsection