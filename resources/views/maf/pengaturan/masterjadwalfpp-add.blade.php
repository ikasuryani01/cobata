@extends('maf._layouts.base')

@section('title', 'Master Jadwal FPP')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if(session('status') != "")
    <div class="alert alert-info"><center>{!! session('status') !!}</label>
    <br></center></div>
    @endif
    @if($mode == "add")
    <div class="card">
        <div class="header">
            <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Jadwal FPP</h4>
        </div>
        <div class="content">
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester: </label>
                    <div class="col-sm-9">
                    <select class="form-control border-input" name="idsemester">
                        @foreach($enumSemester as $d)
                            <option value="{{$d->id}}">{{ $d->semester }} {{ $d->tahunajaran }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                <input type="hidden" name="idfakultas" value="{{ Auth::guard('karyawan')->user()->idfakultas }}">

                <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis FPP: </label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="jenis">
                            @foreach($enumJenis as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Waktu Buka:</label>
                    <div class="col-sm-5">
                        <input type="date" class="form-control border-input" name="tanggalbuka" value="{{ old('tanggalbuka') }}">
                    </div>
                    <div class="col-sm-4">
                        <input type="time" class="form-control border-input" name="waktubuka" value="{{ old('waktubuka') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Waktu Selesai:</label>
                    <div class="col-sm-5">
                        <input type="date" class="form-control border-input" name="tanggalselesai" value="{{ old('tanggalselesai') }}">
                    </div>
                    <div class="col-sm-4">
                        <input type="time" class="form-control border-input" name="waktuselesai" value="{{ old('waktuselesai') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Urutan Terima: </label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="urutanterima">
                            @foreach($enumUrutanTerima as $d)
                                <option value="{{$d}}">{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ route('maf.masterjadwalfpp') }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @elseif ($mode == "edit")
    <div class="card">
        <div class="header">
            <h4 class="title" style="display: inline; line-height: 1.5em;">Ubah Jadwal FPP</h4>
        </div>
        <div class="content">
            <form class="form-horizontal" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Semester: </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control border-input" name="tahunajaran" readonly value="{{ $data->semester->semester }} {{ $data->semester->tahunajaran }}">
                    </div>
                </div>
                <input type="hidden" name="idsemester" value="{{ $data->idsemester }}">
                <input type="hidden" name="idfakultas" value="{{ Auth::guard('karyawan')->user()->idfakultas }}">

                <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis FPP: </label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" readonly name="jenis">
                            @foreach($enumJenis as $d)
                                <option value="{{$d}}" {!!($data->jenis == $d ? ' selected' : '')!!}>{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @php
                    $datawaktubuka = explode(" ", $data->waktubuka);
                    $datawaktuselesai = explode(" ", $data->waktuselesai);
                @endphp
                <div class="form-group">
                    <label class="col-sm-3 control-label">Waktu Buka:</label>
                    <div class="col-sm-5">
                        <input type="date" class="form-control border-input" name="tanggalbuka" value="{{ $datawaktubuka[0] }}">
                    </div>
                    <div class="col-sm-4">
                        <input type="time" class="form-control border-input" name="waktubuka" value="{{ $datawaktubuka[1] }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Waktu Selesai:</label>
                    <div class="col-sm-5">
                        <input type="date" class="form-control border-input" name="tanggalselesai" value="{{ $datawaktuselesai[0] }}">
                    </div>
                    <div class="col-sm-4">
                        <input type="time" class="form-control border-input" name="waktuselesai" value="{{ $datawaktuselesai[1] }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Urutan Terima: </label>
                    <div class="col-sm-9">
                        <select class="form-control border-input" name="urutanterima">
                            @foreach($enumUrutanTerima as $d)
                                <option value="{{$d}}" {!!($data->urutanterima == $d ? ' selected' : '')!!}>{{$d}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="{{ route('maf.masterjadwalfpp') }}" class="btn btn-danger"> Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endif



@endsection