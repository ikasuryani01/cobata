@extends('maf._layouts.base')

@section('title', 'Master Prioritas Terima')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Prioritas</h4>
    
        <a href="{{URL::route('maf.masterprioritas.add')}}">
          <button class="btn btn-sm btn-success" style="float: right;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('maf.masterprioritass')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('maf.masterprioritas')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelPrioritas" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th width="20px">Aksi</th>
          <th width="20px" class="visible-xs">Nama</th>
          <th class="hidden-xs">Nama Komponen</th>
          <th>Kode Komponen</th>
          <th>Urutan</th>
          <th>Status Aktif</th>
        </tr>
        </thead>
        <tbody>
          @foreach($data as $d)
            <tr>
              <td>
                @if($d->deleted_at == null)
                  <a href="{!! action('PrioritasTerimaController@deleteDataPrioritas', $d->id) !!}" onclick="return confirm('Yakin akan menghapus data?')">Hapus</a> |<br>
                  <a href="{!! action('PrioritasTerimaController@editDataPrioritas', $d->id) !!}">Ubah</a>
                @else
                  <a href="{!! action('PrioritasTerimaController@restoreDataPrioritas', $d->id) !!}">Batalkan hapus</a> ||<br>
                  <a href="{!! action('PrioritasTerimaController@forcedeleteDataPrioritas', $d->id) !!}" onclick="return confirm('Yakin akan benar-benar menghapus data?')">Hapus permanen</a>
                @endif
              </td>
              <td class="visible-xs">
                {{ $d->namakomponen }}
                @if($d->statusaktif != "Ya")
                  [<a href="{!! action('PrioritasTerimaController@aktifkanPrioritas', $d->id) !!}">Aktifkan</a>]
                @else
                  [Aktif]
                @endif
              </td>
              <td class="hidden-xs">
                {{ $d->namakomponen }}
                @if($d->statusaktif != "Ya")
                  [<a href="{!! action('PrioritasTerimaController@aktifkanPrioritas', $d->id) !!}">Aktifkan</a>]
                @else
                  [Aktif]
                @endif
              </td>
              <td>{{ $d->kodekomponen }}</td>
              <td>{{ $d->urutan }}</td>
              <td>{{ $d->statusaktif }}</td>
              
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelPrioritas").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection