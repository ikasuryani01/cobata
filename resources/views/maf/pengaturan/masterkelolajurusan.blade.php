@extends('maf._layouts.base')

@section('title', 'Kelola Jurusan')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 5px;
    }
</style>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Data</h4>
  </div>
  <div class="content">
    <form class="form-horizontal" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="idfakultas" value="{{Auth::guard('karyawan')->user()->idfakultas}}">

        <div class='form-group ui-widget'>
          <label class='col-sm-2 control-label'>Karyawan: </label>
          <div class='col-sm-10'>
            <input type='text' id='karyawans' class='form-control border-input' placeholder='Cari Nama Karyawan' name='namakaryawan' required>
         </div>
        </div>
        <input id="karyawans-id" type="hidden" name="npk">

        <div class="form-group">
          <label class="col-sm-2 control-label">Jurusan: </label>
          <div class="col-sm-10">
            <select class="form-control border-input" name="idjurusan">
              @foreach($jurusans as $j)
                <option value="{{$j->IdJurusan}}">{{$j->Nama}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Status Aktif: </label>
          <div class="col-sm-10">
            <select class="form-control border-input" name="statusaktif">
              @foreach($enumStatusAktif as $j)
                <option value="{{$j}}">{{$j}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Utama: </label>
          <div class="col-sm-10">
            <select class="form-control border-input" name="isutama">
                @foreach($enumIsUtama as $j)
                <option value="{{$j}}">{{$j}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Status Jaga Ujian: </label>
          <div class="col-sm-10">
            <select class="form-control border-input" name="statusjagaujian">
              @foreach($enumJagaUjian as $j)
                <option value="{{$j}}">{{$j}}</option>
              @endforeach
            </select>
          </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>
    </form>
  </div>
  <hr style="margin-top: -10px;">
  <div class="header" style="margin-top: -20px;">
    <h4 class="title" style="display: inline; line-height: 1em;">Data Karyawan - Jurusan</h4>
    @if($datahapus == "tidak")
    <a href="{{URL::route('maf.kelolajurusans')}}">
      <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
        <i class="fa fa-trash"></i>
        <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
      </button>
    </a>
    @elseif($datahapus == "ya")
    <a href="{{URL::route('maf.kelolajurusan')}}">
      <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
        <i class="fa fa-star"></i>
        <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
      </button>
    </a>
    @endif
  </div>
  <div class="content">
    <table id="tabelKaryawanJabatan" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
      <thead>
      <tr>
        <th>Karyawan</th>
        <th>Jurusan</th>
        <th>Aktif</th>
        <th>Utama</th>
        <th>Jaga Ujian</th>
        <th>Aksi</th>
      </tr>
      </thead>
      <tbody>
        @foreach($data as $d)
          <tr>
            <td>{{ $d->karyawan->nama }} ({{ $d->karyawan->npk }})</td>
            <td>
              <span class="hidden-sm hidden-xs">{{$d->jurusan->Nama}}</span>
              <span class="hidden-md hidden-lg">{{strtoupper($d->jurusan->Singkatan)}}</span>
            </td>
            <td>{{ $d->statusaktif }}</td>
            <td>{{ $d->isutama }}</td>
            <td>{{ $d->statusjagaujian }}</td>
            <td>
              @if($d->deleted_at == null)
                <a href="{!! action('KaryawanJurusanController@deleteDataKJurusan', [$d->npkkaryawan, $d->idjurusan]) !!}" onclick="return confirm('Yakin akan menghapus data?')">Hapus</a> | 
                <a href="#edit-{{$d->npkkaryawan}}" data-toggle="modal">Ubah</a>
              @else
                <a href="{!! action('KaryawanJurusanController@restoreDataKJurusan', [$d->npkkaryawan, $d->idjurusan]) !!}">Batalkan hapus</a> |
                <a href="{!! action('KaryawanJurusanController@forcedeleteDataKJurusan', [$d->npkkaryawan, $d->idjurusan]) !!}" onclick="return confirm('Yakin akan benar-benar menghapus data?')">Hapus Lagi</a>
              @endif
            </td>
          </tr>
        @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>Karyawan</th>
        <th>Jurusan</th>
        <th>Aktif</th>
        <th>Utama</th>
        <th>Jaga Ujian</th>
        <th>Aksi</th>
      </tr>
      </tfoot>
    </table>
  </div>
</div>

@foreach($data as $di)
<div class="modal fade" id="edit-{{$di->npkkaryawan}}" tabindex="-1" role="dialog" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" name="edit-{{$di->npkkaryawan}}" method="POST" action="{{route('maf.kelolajurusan.update')}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="idfakultas" value="{{Auth::guard('karyawan')->user()->idfakultas}}">
          <!-- <input type="hidden" name="id" value="{{$di->id}}"> -->

          <div class='form-group ui-widget'>
            <label class='col-sm-3 control-label'>Karyawan: </label>
            <div class='col-sm-9'>
              <input type='text' class='form-control border-input' name='namakaryawan' readonly value="<?php echo $di->karyawan->nama;?> (<?php echo $di->npkkaryawan; ?>)">
           </div>
          </div>
          <input id="karyawans-id" type="hidden" name="npkkaryawan" value="{{$di->npkkaryawan}}">

          <div class="form-group">
            <label class="col-sm-3 control-label">Jurusan: </label>
            <div class="col-sm-9">
              <select class="form-control border-input" name="idjabatan">
                @foreach($jurusans as $j)
                  <option value="{{$j->IdJurusan}}" {!! ($di->idjurusan == $j->IdJurusan ? ' selected' : '') !!}>{{$j->Nama}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Status Aktif: </label>
            <div class="col-sm-9">
              <select class="form-control border-input" name="statusaktif">
                @foreach($enumStatusAktif as $j)
                  <option value="{{$j}}" {!! ($di->statusaktif == $j ? ' selected' : '') !!}>{{$j}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Utama: </label>
            <div class="col-sm-9">
              <select class="form-control border-input" name="isutama">
                  @foreach($enumIsUtama as $j)
                  <option value="{{$j}}" {!! ($di->isutama == $j ? ' selected' : '') !!}>{{$j}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Status Jaga Ujian: </label>
            <div class="col-sm-9">
              <select class="form-control border-input" name="statusjagaujian">
                @foreach($enumJagaUjian as $j)
                  <option value="{{$j}}" {!! ($di->statusjagaujian == $j ? ' selected' : '') !!}>{{$j}}</option>
                @endforeach
              </select>
            </div>
          </div>
          
          <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3">
                  <!-- <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button> -->
                  <input type="submit" class="btn btn-success" name="simpan" value="Simpan">
              </div>
          </div>
      </form>
      </div>
    </div>
  </div>
</div>
@endforeach

@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKaryawanJabatan").dataTable( {
      "pagingType": "full",
      "stateSave": true,
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
<script>
var datakaryawans = [
    <?php foreach ($karyawans as $k){ ?>
        { value: "<?php echo $k->npk;?>", label: "<?php echo $k->nama;?> (<?php echo $k->npk; ?>)" },
    <?php } ?>
    ];
$(function() {
    $("#karyawans").autocomplete({
        source: datakaryawans,
        focus: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
            // manually update the textbox
            $(this).val(ui.item.label);
        },
        select: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
            // manually update the textbox and hidden field
            $(this).val(ui.item.label);
            $("#karyawans-id").val(ui.item.value);
        }
    });
});
</script>
@endsection