@extends('maf._layouts.base')

@section('title', 'Master Jadwal FPP')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
  <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.8em;">Jadwal FPP</h4>
    
        <a href="{{URL::route('maf.masterjadwalfpp.add')}}">
          <button class="btn btn-sm btn-success" style="float: right;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('maf.masterjadwalfpps')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('maf.masterjadwalfpp')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif</div>
  <div class="content">
    <table id="tabelJadwalFPP" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
      <thead>
      <tr>
        <th width="20px">Aksi</th>
        <th>Jenis</th>
        <th>Waktu Buka</th>
        <th>Waktu Selesai</th>
        <th>Urutan</th>
        <th>Semester</th>
      </tr>
      </thead>
      <tbody>
        @foreach($data as $d)
            <tr>
                <td>
                  @if($d->deleted_at == null)
                    <a href="{!! action('FppController@deleteDataJadwalfpp', $d->id) !!}" onclick="return confirm('Yakin akan menghapus data?')">Hapus</a> |
                    <a href="{!! action('FppController@editDataJadwalfpp', $d->id) !!}">Ubah</a>
                  @else
                    <a href="{!! action('FppController@restoreDataJadwalfpp', $d->id) !!}">Batalkan hapus</a> | <br>
                    <a href="{!! action('FppController@forcedeleteDataJadwalfpp', $d->id) !!}" onclick="return confirm('Yakin akan benar-benar menghapus data?')">Hapus permanen</a>
                  @endif
                </td>
                <td>{{ $d->jenis }}</td>
                <td>{{ $d->waktubuka }}</td>
                <td>{{ $d->waktuselesai }}</td>
                <td>{{ $d->urutanterima }}</td>
                <td>{{ $d->semester->semester }} {{ $d->semester->tahunajaran }}</td>
                
            </tr>
        @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>Aksi</th>
        <th>Jenis</th>
        <th>Waktu Buka</th>
        <th>Waktu Selesai</th>
        <th>Urutan</th>
        <th>Semester</th>
      </tr>
      </tfoot>
    </table>
  </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelJadwalFPP").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection