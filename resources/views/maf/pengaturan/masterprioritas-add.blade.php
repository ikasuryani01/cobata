@extends('maf._layouts.base')

@section('title', 'Master Prioritas Terima')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
    @if(session('status') != "")
    <div class="alert alert-info"><center>{!! session('status') !!}</label>
    <br></center></div>
    @endif
    @if($mode == "edit")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Edit Prioritas</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idfakultas" value="{{Auth::guard('karyawan')->user()->idfakultas}}">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama Komponen: </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control border-input" name="namakomponen" value="{{ $prioritas->namakomponen }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kode Komponen: </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control border-input" name="kodekomponen" value="{{ $prioritas->kodekomponen }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Urutan: </label>
                        <div class="col-sm-9">
                            <input type="number" min="1" class="form-control border-input" name="urutan" value="$prioritas->urutan }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status Aktif: </label>
                        <div class="col-sm-9">
                            <select class="form-control border-input" name="statusaktif">
                                @foreach($enumStatusAktif as $d)
                                    <option value="{{$d}}" {!! ($prioritas->statusaktif == $d ? ' selected' : '') !!}>{{$d}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                            <a href="{{ route('maf.masterprioritas') }}" class="btn btn-danger"> Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @elseif ($mode == "add")
        <div class="card">
            <div class="header">
                <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Prioritas</h4>
            </div>
            <div class="content">
                <form class="form-horizontal" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idfakultas" value="6">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama Komponen: </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control border-input" name="namakomponen" value="{{ old('namakomponen') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kode Komponen: </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control border-input" name="kodekomponen" value="{{ old('kodekomponen') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Urutan: </label>
                        <div class="col-sm-9">
                            <input type="number" min="1" class="form-control border-input" name="urutan" value="{{ old('urutan') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status Aktif: </label>
                        <div class="col-sm-9">
                            <select class="form-control border-input" name="statusaktif">
                                @foreach($enumStatusAktif as $d)
                                    <option value="{{$d}}">{{$d}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                            <a href="{{ route('maf.masterprioritas') }}" class="btn btn-danger"> Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif



@endsection