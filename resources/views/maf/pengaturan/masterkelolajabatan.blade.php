@extends('maf._layouts.base')

@section('title', 'Kelola Jabatan')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 5px;
    }
</style>
@endsection

@section('content')
<div class="card">
  <div class="header">
      <h4 class="title" style="display: inline; line-height: 1.8em;">Tambah Data</h4>
  </div>
  <div class="content">
    <form class="form-horizontal" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="idfakultas" value="{{Auth::guard('karyawan')->user()->idfakultas}}">

        <div class='form-group ui-widget'>
          <label class='col-sm-2 control-label'>Karyawan: </label>
          <div class='col-sm-10'>
            <input type='text' id='karyawans' class='form-control border-input' placeholder='Cari Nama Karyawan' name='namakaryawan' required>
         </div>
        </div>
        <input id="karyawans-id" type="hidden" name="npk">

        <div class="form-group">
          <label class="col-sm-2 control-label">Jabatan: </label>
          <div class="col-sm-10">
            <select class="form-control border-input" name="idjabatan">
              @foreach($jabatans as $j)
                <option value="{{$j->id}}">{{$j->nama}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Status Aktif: </label>
          <div class="col-sm-10">
            <select class="form-control border-input" name="isaktif">
              @foreach($enumIsAktif as $j)
                <option value="{{$j}}">{{$j}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Utama: </label>
          <div class="col-sm-10">
            <select class="form-control border-input" name="isutama">
                @foreach($enumIsUtama as $j)
                <option value="{{$j}}">{{$j}}</option>
              @endforeach
            </select>
          </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </div>
    </form>
  </div>
  <hr style="margin-top: -10px;">
  <div class="header" style="margin-top: -20px;">
    <h4 class="title" style="display: inline; line-height: 1em;">Data Karyawan - Jabatan</h4>
    @if($datahapus == "tidak")
    <a href="{{URL::route('maf.kelolajabatans')}}">
      <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
        <i class="fa fa-trash"></i>
        <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
      </button>
    </a>
    @elseif($datahapus == "ya")
    <a href="{{URL::route('maf.kelolajabatan')}}">
      <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
        <i class="fa fa-star"></i>
        <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
      </button>
    </a>
    @endif
  </div>
  <div class="content">
    <table id="tabelKaryawanJabatan" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
      <thead>
      <tr>
        <th>Karyawan</th>
        <th>Jabatan</th>
        <th>Aktif</th>
        <th>Utama</th>
        <th>Aksi</th>
      </tr>
      </thead>
      <tbody>
        @foreach($data as $d)
          <tr>
            <td>{{ $d->karyawan->nama }} ({{ $d->karyawan->npk }})</td>
            <td>
              @php $inst = ""; @endphp
              @if($d->jabatan->lingkup == "Fakultas")
                @php $inst = $d->karyawan->fakultas->Singkatan; @endphp
              @elseif($d->jabatan->lingkup == "Jurusan")
                @php $inst = $d->karyawan->getJurusan()->Singkatan; @endphp
              @endif
              <span class="hidden-sm hidden-xs">{{$d->jabatan->nama}} {{$inst}}</span>
              <span class="hidden-md hidden-lg">{{strtoupper($d->jabatan->namasingkat)}} {{$inst}}</span>
            </td>
            <td>{{ $d->isaktif }}</td>
            <td>{{ $d->isutama }}</td>
            <td>
              @if($d->deleted_at == null)
                <a href="{!! action('JabatanController@deleteDataJabatan', [$d->npk, $d->idjabatan]) !!}" onclick="return confirm('Yakin akan menghapus data?')">Hapus</a> | 
                <a href="#edit-{{$d->npk}}" data-toggle="modal">Ubah</a>
              @else
                <a href="{!! action('JabatanController@restoreDataJabatan', [$d->npk, $d->idjabatan]) !!}">Batalkan hapus</a> |
                <a href="{!! action('JabatanController@forcedeleteDataJabatan', [$d->npk, $d->idjabatan]) !!}" onclick="return confirm('Yakin akan benar-benar menghapus data?')">Hapus Lagi</a>
              @endif
            </td>
          </tr>
        @endforeach
      </tbody>
      <tfoot>
      <tr>
        <th>Karyawan</th>
        <th>Jabatan</th>
        <th>Aktif</th>
        <th>Utama</th>
        <th>Aksi</th>
      </tr>
      </tfoot>
    </table>
  </div>
</div>

@foreach($data as $di)
<div class="modal fade" id="edit-{{$di->npk}}" tabindex="-1" role="dialog" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" name="edit-{{$di->npk}}" method="POST" action="{{route('maf.kelolajabatan.update')}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="idfakultas" value="{{Auth::guard('karyawan')->user()->idfakultas}}">
          <!-- <input type="hidden" name="id" value="{{$di->id}}"> -->

          <div class='form-group ui-widget'>
            <label class='col-sm-3 control-label'>Karyawan: </label>
            <div class='col-sm-9'>
              <input type='text' class='form-control border-input' name='namakaryawan' readonly value="<?php echo $di->karyawan->nama;?> (<?php echo $di->npk; ?>)">
           </div>
          </div>
          <input id="karyawans-id" type="hidden" name="npk" value="{{$di->npk}}">

          <div class="form-group">
            <label class="col-sm-3 control-label">Jabatan: </label>
            <div class="col-sm-9">
              <select class="form-control border-input" name="idjabatan">
                @foreach($jabatans as $j)
                  <option value="{{$j->id}}" {!! ($di->idjabatan == $j->id ? ' selected' : '') !!}>{{$j->nama}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Status Aktif: </label>
            <div class="col-sm-9">
              <select class="form-control border-input" name="isaktif">
                @foreach($enumIsAktif as $j)
                  <option value="{{$j}}" {!! ($di->isaktif == $j ? ' selected' : '') !!}>{{$j}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Utama: </label>
            <div class="col-sm-9">
              <select class="form-control border-input" name="isutama">
                  @foreach($enumIsUtama as $j)
                  <option value="{{$j}}" {!! ($di->isutama == $j ? ' selected' : '') !!}>{{$j}}</option>
                @endforeach
              </select>
            </div>
          </div>
          
          <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3">
                  <!-- <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button> -->
                  <input type="submit" class="btn btn-success" name="simpan" value="Simpan">
              </div>
          </div>
      </form>
      </div>
    </div>
  </div>
</div>
@endforeach

@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKaryawanJabatan").dataTable( {
      "pagingType": "full",
      "stateSave": true,
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
<script>
var datakaryawans = [
    <?php foreach ($karyawans as $k){ ?>
        { value: "<?php echo $k->npk;?>", label: "<?php echo $k->nama;?> (<?php echo $k->npk; ?>)" },
    <?php } ?>
    ];
$(function() {
    $("#karyawans").autocomplete({
        source: datakaryawans,
        focus: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
            // manually update the textbox
            $(this).val(ui.item.label);
        },
        select: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
            // manually update the textbox and hidden field
            $(this).val(ui.item.label);
            $("#karyawans-id").val(ui.item.value);
        }
    });
});
</script>
@endsection