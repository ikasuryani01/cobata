@extends('maf._layouts.base')

@section('title', 'Master Semester')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Semester</h4>
    
        <a href="{{URL::route('maf.mastersemester.add')}}">
          <button class="btn btn-sm btn-success" style="float: right;">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah Data</span>
          </button>
        </a>

        @if($datahapus == "tidak")
        <a href="{{URL::route('maf.mastersemesters')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-trash"></i>
            <span class="hidden-sm hidden-xs">Tampilkan yang dihapus</span>
          </button>
        </a>
        @elseif($datahapus == "ya")
        <a href="{{URL::route('maf.mastersemester')}}">
          <button class="btn btn-sm btn-info" style="float: right; margin-right: 10px;">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan data aktif</span>
          </button>
        </a>
        @endif

    </div>
    <div class="content">
      <table id="tabelSemester" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th width="10px">Aksi</th>
          <th>Nama Semester</th>
          <th>Status Aktif</th>
          <th>Tanggal UTS</th>
          <th>Tanggal UAS</th>
        </tr>
        </thead>
        <tbody>
          @foreach($data as $d)
            <tr>
              <td>
                @if($d->deleted_at == null)
                  <a href="{!! action('SemesterController@deleteDataSemester', $d->id) !!}" onclick="return confirm('Yakin akan menghapus data?')">Hapus</a> |
                  <a href="{!! action('SemesterController@editDataSemester', $d->id) !!}">Ubah</a>
                @else
                  <a href="{!! action('SemesterController@restoreDataSemester', $d->id) !!}">Batalkan Hapus</a> || <br>
                    <a href="{!! action('SemesterController@forcedeleteDataSemester', $d->id) !!}" onclick="return confirm('Yakin akan benar-benar menghapus data?')">Hapus permanen</a>
                @endif
              </td>
              <td>{{ $d->semester }} {{ $d->tahunajaran }}
                @if($d->statusaktif != "Ya")
                  [<a href="{!! action('SemesterController@aktifkanSemester', $d->id) !!}">Aktifkan</a>]
                @else
                  [Aktif]
                @endif</td>
              <td>{{ $d->statusaktif }}</td>
              <td>{{ $d->tanggalmulaiuts }} - {{ $d->tanggalselesaiuts}}</td>
              <td>{{ $d->tanggalmulaiuas }} - {{ $d->tanggalselesaiuas}}</td>
              
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>Aksi</th>
          <th>Nama Semester</th>
          <th>Status Aktif</th>
          <th>Tanggal UTS</th>
          <th>Tanggal UAS</th>
        </tr>
        </tfoot>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelSemester").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection