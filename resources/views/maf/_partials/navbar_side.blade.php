<ul class="nav">
    <li {!! (Request::is('maf') ? ' class="active"' : '') !!}>
        <a href="{{ route('maf.beranda') }}">
            <i class="ti-home"></i>
            <p>Beranda</p>
        </a>
    </li>

    <li 
      {!! (Request::is('maf/mastersemester*') ? ' class="dropdown active"' : '') !!} 
      {!! (Request::is('maf/masterjadwalfpp*') ? ' class="dropdown active"' : '') !!} 
      {!! (Request::is('maf/masterprioritas*') ? ' class="dropdown active"' : '') !!} 
      {!! (Request::is('maf/mastersettingfakultas*') ? ' class="dropdown active"' : '') !!} 
      {!! (Request::is('maf/jabatan*') ? ' class="dropdown active"' : '') !!} >
        
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="ti-direction-alt"></i>
            <p>Pengaturan &nbsp; <b class="caret"></b></p>
        <ul class="dropdown-menu" role="menu" style="margin-left: 20px !important;"></a>
          <li class="dropdown-header">Submenu</li>
            <li><a href="{{ route('maf.mastersemester') }}">Semester</a></li>
            <li><a href="{{ route('maf.masterjadwalfpp') }}">Jadwal FPP</a></li>
            <li><a href="{{ route('maf.masterprioritas') }}">Prioritas Terima</a></li>
            <!-- <li><a href="{{ route('maf.mastersettingfakultas') }}">Setting Fakultas</a></li> -->
            <li><a href="{{ route('maf.kelolajabatan')}}">Jabatan Pengguna</a></li>
            <li><a href="{{ route('maf.kelolajurusan')}}">Jurusan Pengguna</a></li>
        </ul>
    </li>    

    <li 
      {!! (Request::is('maf/mastermk*') ? ' class="dropdown active"' : '') !!}
      {!! (Request::is('maf/jurusanmk*') ? ' class="dropdown active"' : '') !!}>
      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="ti-blackboard"></i>
          <p>Data Mata Kuliah &nbsp; <b class="caret"></b></p>
      <ul class="dropdown-menu" role="menu" style="margin-left: 20px !important;"></a>
      <li class="dropdown-header">Submenu</li>
          <li><a href="{{ route('maf.mastermk') }}">Detail Mata Kuliah</a></li>
          <li><a href="{{ route('maf.jurusanmk') }}">Data Jurusan - MK</a></li>
      </ul>
    </li>

    <li {!! (Request::is('maf/kelas*') ? ' class="active"' : '') !!}>
        <a href="{{ route('maf.masterkp') }}">
            <i class="ti-bookmark-alt"></i>
            <p>Data Kelas Paralel</p>
        </a>
    </li>

    <li {!! (Request::is('maf/ruangan*') ? ' class="active"' : '') !!}>
        <a href="{{ route('maf.masterruangan') }}">
            <i class="ti-bookmark-alt"></i>
            <p>Data Ruangan</p>
        </a>
    </li>

    <li {!! (Request::is('maf/mastermhs*') ? ' class="active"' : '') !!}>
      <a href="{{ route('maf.mastermhs') }}">
        <i class="fa fa-male"></i>
        <p>Data Mahasiswa</p>
      </a>
    </li>

    <li {!! (Request::is('maf/masterkaryawan*') ? ' class="active"' : '') !!}>
        <a href="{{ route('maf.masterkaryawan') }}">
            <i class="fa fa-street-view"></i>
            <p>Data Karyawan</p>
        </a>
    </li>
    
    <li {!! (Request::is('maf/nonjaga*') ? ' class="active"' : '') !!}>
        <a href="{{ route('maf.masternonjaga') }}">
            <i class="ti-tag"></i>
            <p>Data Non-jaga Ujian</p>
        </a>
    </li>

    <li {!! (Request::is('maf/hasilfpp*') ? ' class="active"' : '') !!}>
        <a href="{{ route('maf.hasilfpp') }}">
            <i class="ti-clip"></i>
            <p>Data Hasil FPP</p>
        </a>
    </li>

    <li {!! (Request::is('maf/presensiujian*') ? ' class="active"' : '') !!}>
        <a href="{{ route('maf.jadwalujian') }}">
            <i class="ti-flag-alt"></i>
            <p>Kelola Ujian</p>
        </a>
    </li>

    <li {!! (Request::is('maf/presensikuliah*') ? ' class="active"' : '') !!}>
        <a href="{{ route('maf.presensikuliah') }}">
            <i class="ti-envelope"></i>
            <p>Jadwal Kuliah</p>
        </a>
    </li>

    <!-- <li {!! (Request::is('paj/mastermhs*') ? ' class="active"' : '') !!}>
        <a href="{{ route('paj.mastermhs') }}">
            <i class="ti-blackboard"></i>
            <p>Pemberitahuan</p>
        </a>
    </li> -->

    <li {!! (Request::is('maf/laporan*') ? ' class="active"' : '') !!}>
        <a href="{{ route('maf.laporan') }}">
            <i class="ti-clipboard"></i>
            <p>Laporan</p>
        </a>
    </li>
</ul>