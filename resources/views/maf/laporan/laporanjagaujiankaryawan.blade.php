@extends('maf._layouts.base')

@section('title', 'Laporan')

@section('content')
<div class="card">
  <div class="content" >
    <h3 style="display: inline; align: left;">Laporan Jadwal Jaga Ujian Karyawan
    </h3>
    <a href="{{ route('maf.laporan') }}">
      <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
        <i class="fa fa-caret-left"></i>
        <span class="hidden-sm hidden-xs">Kembali</span>
      </button>
    </a>
    <br><br>
    <form class="form-horizontal" action="{{ route('maf.jagaujiankaryawan') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <table class="table table-condensed table-bordered">
      <tr class="active">
        <th colspan="2">
          Filter Data
        </th>
      </tr>
      <tr>
        <td colspan="2">
          <div class='form-group ui-widget'>
            <label class='col-sm-3 control-label'>Pilih Karyawan / Dosen: </label>
            <div class='col-sm-9'>
              <input type='text' id='karyawan' class='form-control border-input' placeholder='Cari Nama / NPK Karyawan' name='karyawan'>
            </div>
          </div>
          <input id="npk" type="hidden" name="npkcari">
          <div class='col-sm-offset-3 col-sm-9' style="margin-top: -10px;">
            <input type="submit" class="btn btn-sm btn-success btn-filter" name="submitfilter" value="Filter">
            <a href="{{ route('maf.jagaujiankaryawan') }}" class="btn btn-sm btn-danger btn-filter">Reset Filter</a>
            <input type="submit" class="btn btn-sm btn-primary btn-filter" name="submitfilter" value="PDF">
          </div>
        </td>
      </tr>
    </table>
    </form>      
    @if($kal != "")
    <p style="font-size: 12px;">{{$kal}}</p>
    @endif
    @foreach($allkaryawan as $k)
      @if($k->getDataJaga()->count() == 0)
        <h5>{{$k->npk}} {{$k->namalengkap}} Tidak Menjaga</h5>
      @else
      <h5>Ujian @if($semesteraktif->getUtsUas() == "uas") Akhir @else Tengah @endif
      Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}
      <br>{{$k->npk}} {{$k->namalengkap}}</h5>

      <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Hari dan Tanggal</th>
          <th>Jam</th>
          <th>Ruang</th>
          <th>Mata Kuliah</th>
          <th>KP</th>
          <th>Isi</th>
        </tr>
        @php
          $tanggalskrg = "";
          $idrskrg = "";
        @endphp
        @foreach($k->getDataJaga() as $pr)
        <tr>
          <td>{{$loop->iteration}}</td>
          @if($tanggalskrg != $pr->tanggal)
            @php
              $tanggalskrg = "";
              $idrskrg = "";
            @endphp
          <td rowspan="{{$k->getDataJaga()->where('tanggal', $pr->tanggal)->count()}}">
            {{ date('l', strtotime($pr->tanggal)) }}, {{ $pr->tanggal }}
          </td>
          @endif
          @if($pr->idruangan != $idrskrg)
          <td rowspan="{{$k->getDataJaga()->where('tanggal', $pr->tanggal)->where('idruangan', $pr->idruangan)->count()}}">{{ $pr->kp->mk->jadwalujian[0]->jamke }}</td>
          <td rowspan="{{$k->getDataJaga()->where('tanggal', $pr->tanggal)->where('idruangan', $pr->idruangan)->count()}}">{{ $pr->ruangan->nama }}</td>
          @endif
          <td>{{ $pr->kp->kodemk }}.{{ $pr->kp->mk->nama }}</td>
          <td>{{ $pr->kp->kodekp }}</td>
          <td>{{ $pr->jumlahpeserta}}</td>
        </tr>
        @php
          $tanggalskrg = $pr->tanggal;
          $idrskrg = $pr->idruangan;
        @endphp
        @endforeach
      </table>
      @endif
    @endforeach
  </div>
</div>

@endsection

@section('footer_scripts')
<script type="text/javascript">
var datadosendankaryawan = [
    <?php foreach ($allkaryawan as $r){ ?>
        { value: "<?php echo $r->npk;?>", label: "<?php echo  "$r->npk - $r->nama";?>" },
    <?php } ?>
  ];
  
$(function() {
    $("#karyawan").autocomplete({
        source: datadosendankaryawan,
        focus: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
            // manually update the textbox
            $(this).val(ui.item.label);
        },
        select: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
            // manually update the textbox and hidden field
            $(this).val(ui.item.label);
            $("#npk").val(ui.item.value);
        }
    });
});
</script>
@endsection