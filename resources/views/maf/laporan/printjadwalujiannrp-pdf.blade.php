<!DOCTYPE html>
<html>
<head>
  <title>Jadwal Ujian Ruang-NRP</title>
  <style>
      body {
        font-family: "Times New Roman", Georgia, Serif;
        font-size: 14px;
        line-height: 20px;
        font-weight: 400;
        /*color: #3b3b3b;*/
        -webkit-font-smoothing: antialiased;
        font-smoothing: antialiased;
      }

      .wrapper {
        margin: 0 auto;
        padding: 40px;
        max-width: 800px;
      }

      .table {
        margin: 0 0 40px 0;
        width: 100%;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
        display: table;
      }

      table {
        border-collapse: collapse;
      }

      table, th, td {
          border: 1px solid black;
      }

      @media screen and (max-width: 580px) {
        .table {
          display: block;
        }
      }

      .row {
        display: table-row;
        /*background: #f6f6f6;*/
      }
      .row:nth-of-type(odd) {
        /*background: #e9e9e9;*/
      }
      .row.header {
        font-weight: 900;
        /*color: #ffffff;*/
        /*background: #ea6153;*/
      }
      .row.green {
        /*background: #27ae60;*/
      }
      .row.blue {
        /*background: #2980b9;*/
      }
      @media screen and (max-width: 580px) {
        .row {
          padding: 8px 0;
          display: block;
        }
      }

      .cell {
        padding: 6px 6px;
        display: table-cell;
      }

      .thcell {
        padding: 6px 8px;
        display: table-cell;
      }

      @media screen and (max-width: 580px) {
        .cell {
          padding: 2px 12px;
          display: block;
        }
      }

  </style>
</head>
<body>
  <h3>Ujian Akhir Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}</h3>
    @foreach($tanggal as $t)
      <?php $date = new DateTime($t);?>
      <h4 style="margin-top: 0px; margin-bottom: 0px;">{{ $presensis->where('tanggal', $t)->first()->kp->mk->jadwalujian[0]->hari }}, {{ date_format($date, "d-m-Y") }}
      <table>
        <tr class="row header blue">
          <th width="30px">Jam</th>
          <th width="330px">Mata Kuliah</th>
          <th width="40px">KP</th>
          <th width="290px">Ruang</th>

        </tr>
        @php
          $jamskrg = "";
          $kodemkskrg = "";
          $kodekpskrg = "";
        @endphp
        @foreach($presensis->where('tanggal', $t) as $pr)
        <tr class="row">
          @if($pr->kp->mk->jadwalujian[0]->jamke != $jamskrg)
            <td rowspan="{{$pr->kp->mk->jadwalujian[0]->jumlahUjianPerJam()}}" align="center">{{$pr->kp->mk->jadwalujian[0]->jamke}}</td>
          @endif
          @if($pr->kp->kodemk != $kodemkskrg)
            <td class="cell" rowspan="{{$pr->getRowspanMk()}}" valign="top" align="center">{{strtoupper($pr->kp->mk->nama)}} ({{$pr->kp->kodemk}})</td>
          @endif
          @if($pr->idkelasparalel != $kodekpskrg)
          <td align="center" class="cell" rowspan="{{$presensis->where('idkelasparalel', $pr->idkelasparalel)->count()}}">{{$pr->kp->kodekp}}</td>
          @endif
          <td class="cell" align="center">{{$pr->ruangan->nama}} ({{ $pr->jumlahpeserta }}) {{$pr->nrpawal}} - {{$pr->nrpakhir}}</td>
        </tr>
          @php
            $jamskrg = $pr->kp->mk->jadwalujian[0]->jamke;
            $kodemkskrg = $pr->kp->kodemk;
            $kodekpskrg = $pr->idkelasparalel;
          @endphp
        @endforeach
      </table>
      <br>
    @endforeach
</body>
</html>