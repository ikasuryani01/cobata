<!DOCTYPE html>
<html>
<head>
  <title>Daftar Isi Kelas sampai KK</title>
  <style>
      body {
        font-family: "Helvetica Neue", Helvetica, Arial;
        font-size: 16px;
        line-height: 20px;
        font-weight: 400;
        color: #3b3b3b;
        -webkit-font-smoothing: antialiased;
        font-smoothing: antialiased;
      }

      .wrapper {
        margin: 0 auto;
        padding: 40px;
        max-width: 800px;
      }

      .table {
        margin: 0 0 40px 0;
        width: 100%;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
        display: table;
      }

      @media screen and (max-width: 580px) {
        .table {
          display: block;
        }
      }

      .row {
        display: table-row;
        background: #f6f6f6;
      }
      .row:nth-of-type(odd) {
        background: #e9e9e9;
      }
      .row.header {
        font-weight: 900;
        color: #ffffff;
        background: #ea6153;
      }
      .row.green {
        background: #27ae60;
      }
      .row.blue {
        background: #2980b9;
      }
      @media screen and (max-width: 580px) {
        .row {
          padding: 8px 0;
          display: block;
        }
      }

      .cell {
        padding: 6px 6px;
        display: table-cell;
      }

      .thcell {
        padding: 6px 8px;
        display: table-cell;
      }

      @media screen and (max-width: 580px) {
        .cell {
          padding: 2px 12px;
          display: block;
        }
      }

  </style>
</head>
<body>
<h3>Daftar Isi Kelas sampai KK Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}
  @if($kal != "") - {{$kal}} @endif 
</h3>
  <table>
  <tr class="row header blue">
    <th>Kode MK</th>
    <th width="300px">Nama</th>
    <th>KP</th>
    <th>Kapasitas</th>
    @foreach($fpp as $f)
    <th>{{$f->jenis}}</th>
    @endforeach
    <th>Jumlah</th>
    <th>Sisa</th>
  </tr>
  @foreach($kelasparalels as $kp)
    <tr class="row">
      <td class="cell">{{$kp->kodemk}}</td>
      <td class="cell">{{$kp->mk->nama}}</td>
      <td class="cell">{{$kp->kodekp}}</td>
      <td class="cell">{{$kp->kapasitas}}</td>
      @foreach($fpp as $f)
        <td class="cell">{{$kp->getPesertaperFPP($f->id)->count()}}</td>
      @endforeach
      <td class="cell">{{$kp->hitungIsiKeterima()}}</td>
      <td class="cell">{{$kp->getJumlahKursiKosong()}}</td>
    </tr>
  @endforeach
  </table>

</body>
</html>