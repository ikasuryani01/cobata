@extends('maf._layouts.base')

@section('title', 'Laporan')

@section('content')
<div class="card">
  <div class="content" >
    <h3 style="display: inline; align: left;">Laporan Hasil Perwalian - Mahasiswa
    </h3>
    <a href="{{ route('maf.laporan') }}">
      <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
        <i class="fa fa-caret-left"></i>
        <span class="hidden-sm hidden-xs">Kembali</span>
      </button>
    </a>
    <br><br>
    <form class="form-horizontal" action="{{ route('maf.laphasilperwalianmhs') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <table class="table table-condensed table-bordered">
      <tr class="active">
        <th colspan="2">
          Filter Data
        </th>
      </tr>
      <tr>
        <td>Jurusan: </td>
        <td>
          <select name="idjurusan">
            <option value="0"{!! ($filterdipakai['idjurusan'] == 0 ? ' selected' : '') !!}>Semua Jurusan</option>
            @foreach($datajurusan as $j)
              <option value="{{$j->IdJurusan}}" {!! ($filterdipakai['idjurusan'] == $j->IdJurusan ? ' selected' : '') !!}>{{$j->Nama}}</option>
            @endforeach
          </select>
        </td>

      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <input type="submit" class="btn btn-sm btn-success btn-filter" name="submitfilter" value="Filter">
          <a href="{{ route('maf.laphasilperwalianmhs') }}" class="btn btn-sm btn-danger btn-filter">Reset Filter</a>
          <input type="submit" class="btn btn-sm btn-primary btn-filter" name="submitfilter" value="PDF">
          <!-- <input type="submit" class="btn btn-sm btn-primary btn-filter" name="submitfilter" value="Excel"> -->
        </td>
      </tr>
    </table>
    </form>

    <h3>
      Hasil Perwalian Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}</h3>
      @if($kal != "")
      <p style="font-size: 12px;">{{$kal}}</p>
      @endif
    <div class="table-responsive">
      <table class="table table-bordered table-condensed">
      <tr>
        <th width="140px">NRP</th>
        <th>Kode MK dan KP</th>
      </tr>
      @foreach($mahasiswas as $mhs)
        <tr>
          <td>{{$mhs->nrp}}</td>
          <td>
            @foreach($mhs->getDaftarKodeMk() as $data)
              {{$data->kp->kodemk}}{{$data->kp->kodekp}}
              <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
              @if($loop->iteration % 8 == 0)
                <br>
              @endif
            @endforeach
          </td>
        </tr>
      @endforeach
      </table>
    </div>
  </div>
</div>

@endsection