@extends('maf._layouts.base')

@section('title', 'Laporan')

@section('header_styles')
<style type="text/css">
  label {
    color: black !important;
  }
</style>
@endsection
@section('content')
<div class="card">
  <div class="content" >
    <h3 style="display: inline; align: left;">Form Rekap Peserta Mata Kuliah
    </h3>
    <a href="{{ route('maf.laporan') }}">
      <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
        <i class="fa fa-caret-left"></i>
        <span class="hidden-sm hidden-xs">Kembali</span>
      </button>
    </a>
    <br><br>
    <form class="form-horizontal" action="{{ route('maf.formrekap') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <table class="table table-condensed table-bordered">
      <tr class="active">
        <th colspan="2">
          Filter Data
        </th>
      </tr>
      <tr>
        <td>Jurusan: </td>
        <td>
          <select name="idjurusan">
            <option value="0"{!! ($filterdipakai['idjurusan'] == 0 ? ' selected' : '') !!}>Semua Jurusan</option>
            @foreach($datajurusan as $j)
              <option value="{{$j->IdJurusan}}" {!! ($filterdipakai['idjurusan'] == $j->IdJurusan ? ' selected' : '') !!}>{{$j->Nama}}</option>
            @endforeach
          </select>
        </td>

      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <input type="submit" class="btn btn-sm btn-success btn-filter" name="submitfilter" value="Filter">
          <a href="{{ route('maf.formrekap') }}" class="btn btn-sm btn-danger btn-filter">Reset Filter</a>
          <input type="submit" class="btn btn-sm btn-primary btn-filter" name="submitfilter" value="PDF">
          <!-- <input type="submit" class="btn btn-sm btn-primary btn-filter" name="submitfilter" value="Excel"> -->
        </td>
      </tr>
    </table>
    </form>

    <h3>Form Rekap Peserta MK {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}</h3>
      @if($kal != "")
      <p style="font-size: 12px;">{{$kal}}</p>
      @endif
    <br>
    @foreach($kelasparalels as $kp)
      <div class="row">
        <label class="col-sm-2">Kode MK:</label>
        <label class="col-sm-10">{{$kp->kodemk}}</label>
        <label class="col-sm-2">Nama:</label>
        <label class="col-sm-10">{{$kp->mk->nama}} KP: {{$kp->kodekp}}</label>
        <label class="col-sm-2">Kapasitas / Isi:</label>
        <label class="col-sm-10">{{$kp->kapasitas}} / {{$kp->hitungIsiKeterima()}}</label>
        <label class="col-sm-2">Status DMB:</label>
        <label class="col-sm-10">@if($kp->statusdmb == "Ya") DMB @else Non DMB @endif</label>

      </div>
      <div class="row">
        <label class="col-sm-4"><b>Mahasiswa Asisten</b></label>
        <label class="col-sm-8">
        @if($kp->getPesertaAsisten()->count() > 0)
          @foreach($kp->getPesertaAsisten() as $p)
            {{$p->nrp}} &nbsp;&nbsp;&nbsp;
          @endforeach
        @else - @endif
        </label>

        <label class="col-sm-4"><b>Mahasiswa Semesternya</b></label>
        <label class="col-sm-8">
        @if($kp->getPesertaSemesternya()->count() > 0)
          @foreach($kp->getPesertaSemesternya() as $p)
            {{$p->nrp}} &nbsp;&nbsp;&nbsp;
          @endforeach
        @else - @endif
        </label>

        <label class="col-sm-4"><b>Mahasiswa Angkatan Tua</b></label>
        <label class="col-sm-8">
        @if($kp->getPesertaAngkatanTua()->count() > 0)
          @foreach($kp->getPesertaAngkatanTua() as $p)
            {{$p->nrp}} &nbsp;&nbsp;&nbsp;
          @endforeach
        @else - @endif
        </label>

        <label class="col-sm-4"><b>Mahasiswa Setting NRP</b></label>
        <label class="col-sm-8">
        @if($kp->getPesertaSettingNrp()->count() > 0)
          @foreach($kp->getPesertaSettingNrp() as $p)
            {{$p->nrp}} &nbsp;&nbsp;&nbsp;
          @endforeach
        @else - @endif
        </label>

        <label class="col-sm-4"><b>Mahasiswa Lainnya</b></label>
        <label class="col-sm-8">
        @if($kp->getPesertaLainnya()->count() > 0)
          @foreach($kp->getPesertaLainnya() as $p)
            {{$p->nrp}} &nbsp;&nbsp;&nbsp;
          @endforeach
        @else - @endif
        </label>
      </div>
      <hr>
    @endforeach
  </div>
</div>

@endsection