@extends('maf._layouts.base')

@section('title', 'Laporan')

@section('content')
<div class="card">
  <div class="content" >
    <h3 style="display: inline; align: left;">Daftar Isi Kelas sampai KK
    </h3>
    <a href="{{ route('maf.laporan') }}">
      <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
        <i class="fa fa-caret-left"></i>
        <span class="hidden-sm hidden-xs">Kembali</span>
      </button>
    </a>
    <br><br>
    <form class="form-horizontal" action="{{ route('maf.daftarisikelas') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <table class="table table-condensed table-bordered">
      <tr class="active">
        <th colspan="2">
          Filter Data
        </th>
      </tr>
      <tr>
        <td>Jurusan: </td>
        <td>
          <select name="idjurusan">
            <option value="0"{!! ($filterdipakai['idjurusan'] == 0 ? ' selected' : '') !!}>Semua Jurusan</option>
            @foreach($datajurusan as $j)
              <option value="{{$j->IdJurusan}}" {!! ($filterdipakai['idjurusan'] == $j->IdJurusan ? ' selected' : '') !!}>{{$j->Nama}}</option>
            @endforeach
          </select>
        </td>

      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <input type="submit" class="btn btn-sm btn-success btn-filter" name="submitfilter" value="Filter">
          <a href="{{ route('maf.daftarisikelas') }}" class="btn btn-sm btn-danger btn-filter">Reset Filter</a>
          <input type="submit" class="btn btn-sm btn-primary btn-filter" name="submitfilter" value="PDF">
          <!-- <input type="submit" class="btn btn-sm btn-primary btn-filter" name="submitfilter" value="Excel"> -->
        </td>
      </tr>
    </table>
    </form>

    <h3>
      Daftar Isi Kelas sampai KK {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}</h3>
      @if($kal != "")
      <p style="font-size: 12px;">{{$kal}}</p>
      @endif
    <div class="table-responsive">
      <table class="table table-bordered table-condensed">
      <tr>
        <th>Kode MK</th>
        <th>Nama</th>
        <th>KP</th>
        <th>Kapasitas</th>
        @foreach($fpp as $f)
        <th>{{$f->jenis}}</th>
        @endforeach
        <th>Jumlah</th>
        <th>Sisa</th>
      </tr>
      @foreach($kelasparalels as $kp)
        <tr>
          <td>{{$kp->kodemk}}</td>
          <td>{{$kp->mk->nama}}</td>
          <td>{{$kp->kodekp}}</td>
          <td>{{$kp->kapasitas}}</td>
          @foreach($fpp as $f)
            <td>
              @if($kp->getPesertaperFpp($f->id)->count() > 0)<a href="#rincian-{{$kp->id}}-{{$f->id}}" data-toggle="modal">{{$kp->getPesertaperFPP($f->id)->count()}}</a>
              @else
              {{$kp->getPesertaperFPP($f->id)->count()}}
              @endif
            </td>
          @endforeach
          <td>{{$kp->hitungIsiKeterima()}}</td>
          <td>{{$kp->getJumlahKursiKosong()}}</td>
        </tr>
      @endforeach
      </table>

      @foreach($kelasparalels as $kp)
        @foreach($fpp as $f)
          @if($kp->getPesertaperFpp($f->id)->count() > 0)
          <div class="modal fade" id="rincian-{{$kp->id}}-{{$f->id}}" tabindex="-1" role="dialog" data-backdrop="false">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">{{$kp->mk->nama}} KP: {{$kp->kodekp}}, {{$f->jenis}}</h4>
                </div>
                <div class="modal-body">
                  @foreach($kp->getPesertaperFpp($f->id) as $k)
                    {{$k->nrp}} &nbsp;&nbsp;&nbsp;
                  @endforeach
                </div>
              </div>
            </div>
          </div>
          @endif
        @endforeach
      @endforeach
    </div>
  </div>
</div>

@endsection