@extends('maf._layouts.base')

@section('title', 'Laporan')

@section('content')
<div class="card">
  <div class="content" >
    <h3 style="display: inline; align: left;">Laporan Jadwal Ujian (Ruangan dan Kapasitas)
    </h3>
    <a href="{{ route('maf.laporan') }}">
      <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
        <i class="fa fa-caret-left"></i>
        <span class="hidden-sm hidden-xs">Kembali</span>
      </button>
    </a>
    <br><br>
    <form class="form-horizontal" action="{{ route('maf.lapujianruangan') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <table class="table table-condensed table-bordered">
      <tr class="active">
        <th colspan="2">
          Filter Data
        </th>
      </tr>
      <tr>
        <td>Tanggal: </td>
        <td>
          <input type="text" name="from" id="from" value="{{$filterdipakai['from']}}"> 
          sampai 
          <input type="text" name="to" id="to" value="{{$filterdipakai['to']}}">
        </td>

      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <input type="submit" class="btn btn-sm btn-success btn-filter" name="submitfilter" value="Filter">
          <a href="{{ route('maf.lapujianruangan') }}" class="btn btn-sm btn-danger btn-filter">Reset Filter</a>
          <input type="submit" class="btn btn-sm btn-primary btn-filter" name="submitfilter" value="PDF">
          <input type="submit" class="btn btn-sm btn-primary btn-filter" name="submitfilter" value="Excel">
        </td>
      </tr>
    </table>
    </form>

    <h3>
      Ujian @if($semesteraktif->getUtsUas() == "uas") Akhir @else Tengah @endif
      Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}</h3>
      @if($kal != "")
      <p style="font-size: 12px;">{{$kal}}</p>
      @endif
    @foreach($tanggal as $t)
      <?php $date = new DateTime($t);?>
      <h5 style="margin-top: 0px; margin-bottom: 0px;">{{ $presensis->where('tanggal', $t)->first()->kp->mk->jadwalujian[0]->hari }}, {{ date_format($date, "d-m-Y") }}</h5>
      <!-- <br> -->
      <p>Piket: {{ $datapikets->where('tanggal', $t)->first()->karyawan->namalengkap }}</p>
      <div class="table-responsive">
      <table class="table table-hover table-bordered" style="font-size: 12px; overflow-x: auto;">
        <tr>
          <th width="20px">Jam</th>
          <th width="250px">Mata Kuliah</th>
          <th width="20px">KP</th>
          <th width="70px">Ruangan</th>
          <th>Penjaga Ujian</th>
          <th width="200px">NRP (Jumlah)</th>

        </tr>
          @php
            $kodemkskrg = "";
            $kodekpskrg = "";
          @endphp
        @foreach($presensis->where('tanggal', $t) as $pr)
        <tr>
          @if($pr->kp->kodemk != $kodemkskrg)
            <td rowspan="{{$pr->getRowspanMk()}}" align="center">{{$pr->kp->mk->jadwalujian[0]->jamke}}</td>
            <td rowspan="{{$pr->getRowspanMk()}}">{{$pr->kp->kodemk}} - {{$pr->kp->mk->nama}}</td>
          @endif
          @if($pr->idkelasparalel != $kodekpskrg)
          <td align="center" rowspan="{{$presensis->where('idkelasparalel', $pr->idkelasparalel)->count()}}">{{$pr->kp->kodekp}}</td>
          @endif
          <td align="center">{{$pr->ruangan->nama}}</td>
          <td style="font-size: 11px;">
            - {{ $pr->dosenjaga->namalengkap }} ({{$pr->npkdosenjaga}})<br>
            - {{ $pr->karyawanjaga->namalengkap }} ({{$pr->npkkaryawanjaga}})
          </td>
          <td>{{$pr->nrpawal}} - {{$pr->nrpakhir}} ({{ $pr->jumlahpeserta }})</td>
        </tr>
          @php
            $kodemkskrg = $pr->kp->kodemk;
            $kodekpskrg = $pr->idkelasparalel;
          @endphp
        @endforeach
      </table>
      </div>
      <br>
    @endforeach
  </div>
</div>

@endsection

@section('footer_scripts')
<script>
  $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from" ).datepicker({
          minDate: new Date ("{{date('m/d/y', strtotime($tanggalmulai))}}"),
          defaultDate: new Date ("{{$tanggalmulai}}"),
          maxDate: new Date ("{{$tanggalselesai}}"),
          changeMonth: true,
          numberOfMonths: 1
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to" ).datepicker({
        minDate: new Date ("{{$tanggalmulai}}"),
        defaultDate: new Date ("{{$tanggalselesai}}"),
        maxDate: new Date ("{{$tanggalselesai}}"),
        changeMonth: true,
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
  </script>
@endsection