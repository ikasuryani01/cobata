@extends('maf._layouts.base')

@section('title', 'Laporan')

@section('header_styles')
<style type="text/css">
  #menulaporan  {
    margin-left: 20px;
    padding-right: 10px;
  }
  #menulaporan > li {
    padding: 5px;
    font-size: 17px;
  }
</style>
@endsection

@section('content')
<div class="card" style="padding-right: 10px;">
  <h3 style="margin: 10px 10px 10px 10px; padding-top: 15px; padding-left: 10px; padding-bottom: 10px;">Daftar Laporan</h3>
  <div class="row">
    <ul id="menulaporan">
      <li>
        <a href="{{ route('maf.laphasilperwalianmhs') }}">Laporan Hasil Perwalian - Mahasiswa</a>
        <p>Berisi daftar mahasiswa dan daftar kode mata kuliah yang diambil dari hasil perwalian</p>
      </li>
      <li>
        <a href="{{ route('maf.lapkapkelas') }}">Laporan Kapasitas Kelas</a>
        <p>Berisi daftar kelas paralel beserta kapasitas dan jumlah kursi kosongnya</p>
      </li>
      <li>
        <a href="{{ route('maf.lapkelastutup') }}">Laporan Kelas Tutup</a>
        <p>Berisi daftar kelas paralel yang ditutup beserta kapasitasnya</p>
      </li>
      <li>
        <a href="{{ route('maf.rekappeserta') }}">Rekap Peserta Mata Kuliah</a>
        <p>Berisi daftar kelas paralel beserta kapasitas dan jumlah mahasiswa yang terdaftar</p>
      </li>
      <li>
        <a href="{{ route('maf.formrekap') }}">Form Rekap Peserta Kuliah</a>
        <p>Merupakan rincian dari laporan rekap peserta mata kuliah. Berisi rincian data kelas paralel beserta daftar NRP yang terdaftar dikelompokkan berdasarkan asisten, angkatan tua, dan kategori lainnya</p>
      </li>
      <li>
        <a href="{{ route('maf.daftarisikelas')}}">Daftar Isi Kelas sampai dengan KK</a>
        <p>Berisi daftar kelas paralel, kapasitas, jumlah pendaftarnya pada masing-masing FPP, jumlah total pendaftar, dan sisa kapasitas</p>
      </li>
      <li>
        <a href="{{route('maf.lapujiannrp')}}">Laporan Jadwal Ujian (Ruangan dan NRP)</a>
        <p>Berisi daftar jadwal ujian beserta nama mata kuliah dan setiap kelas paralel, nama ruangan, dan pembagian NRP</p>
      </li>
      <li>
        <a href="{{route('maf.lapujianpenjaga')}}">Laporan Jadwal Ujian (Penjaga Ujian)</a>
        <p>Berisi daftar jadwal ujian beserta nama mata kuliah, kelas paralel, ruangan, dan nama dosen dan karyawan jaga</p>
      </li>
      <li>
        <a href="{{ route('maf.lapujianruangan') }}">Laporan Jadwal Ujian (Lengkap)</a>
        <p>Berisi daftar jadwal ujian beserta nama mata kuliah, kelas paralel, ruangan, nama dosen dan karyawan jaga, serta pembagian NRP</p>
      </li>
      <li>
        <a href="{{route('maf.jagaujiankaryawan')}}">Laporan Jadwal Jaga Ujian per Dosen dan Karyawan</a>
        <p>Berisi jadwal jaga ujian per karyawan</p>
      </li>
      
    </ul>
  </div>
</div>
@endsection