<!DOCTYPE html>
<html>
<head>
  <title>Jadwal Ujian Lengkap</title>
  <style>
      body {
        font-family: "Helvetica Neue", Helvetica, Arial;
        font-size: 14px;
        line-height: 20px;
        font-weight: 400;
        color: #3b3b3b;
        -webkit-font-smoothing: antialiased;
        font-smoothing: antialiased;
      }

      .wrapper {
        margin: 0 auto;
        padding: 40px;
        max-width: 800px;
      }

      .table {
        margin: 0 0 40px 0;
        width: 100%;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
        display: table;
      }

      @media screen and (max-width: 580px) {
        .table {
          display: block;
        }
      }

      .row {
        display: table-row;
        background: #f6f6f6;
      }
      .row:nth-of-type(odd) {
        background: #e9e9e9;
      }
      .row.header {
        font-weight: 900;
        color: #ffffff;
        background: #ea6153;
      }
      .row.green {
        background: #27ae60;
      }
      .row.blue {
        background: #2980b9;
      }
      @media screen and (max-width: 580px) {
        .row {
          padding: 8px 0;
          display: block;
        }
      }

      .cell {
        padding: 6px 6px;
        display: table-cell;
      }

      .thcell {
        padding: 6px 8px;
        display: table-cell;
      }

      @media screen and (max-width: 580px) {
        .cell {
          padding: 2px 12px;
          display: block;
        }
      }

  </style>
</head>
<body>
  <h3>Ujian Akhir Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}</h3>
    @foreach($tanggal as $t)
      <?php $date = new DateTime($t);?>
      <h4 style="margin-top: 0px; margin-bottom: 0px;">{{ $presensis->where('tanggal', $t)->first()->kp->mk->jadwalujian[0]->hari }}, {{ date_format($date, "d-m-Y") }}, 
      <!-- <br> -->
      Piket: {{ $datapikets->where('tanggal', $t)->first()->karyawan->namalengkap }}</h4>
      <table style="font-size: 12px;">
        <tr class="row header blue">
          <th width="20px">Jam</th>
          <th width="200px">Mata Kuliah</th>
          <th width="20px">KP</th>
          <th width="70px">Ruangan</th>
          <th>Penjaga Ujian</th>
          <th width="150px">NRP (Jumlah)</th>

        </tr>
        @php
          $kodemkskrg = "";
          $kodekpskrg = "";
        @endphp
        @foreach($presensis->where('tanggal', $t) as $pr)
        <tr class="row">
          @if($pr->kp->kodemk != $kodemkskrg)
            <td class="cell" rowspan="{{$pr->getRowspanMk()}}" align="center">{{$pr->kp->mk->jadwalujian[0]->jamke}}</td>
            <td class="cell" rowspan="{{$pr->getRowspanMk()}}">{{$pr->kp->kodemk}} - {{$pr->kp->mk->nama}}</td>
          @endif
          @if($pr->idkelasparalel != $kodekpskrg)
          <td align="center" class="cell" rowspan="{{$presensis->where('idkelasparalel', $pr->idkelasparalel)->count()}}">{{$pr->kp->kodekp}}</td>
          @endif
          <td class="cell" align="center">{{$pr->ruangan->nama}}</td>
          <td class="cell" style="font-size: 11px;">
            - {{ $pr->dosenjaga->namalengkap }} ({{$pr->npkdosenjaga}})<br>
            - {{ $pr->karyawanjaga->namalengkap }} ({{$pr->npkkaryawanjaga}})
          </td>
          <td class="cell">{{$pr->nrpawal}} - {{$pr->nrpakhir}} ({{ $pr->jumlahpeserta }})</td>
        </tr>
          @php
            $kodemkskrg = $pr->kp->kodemk;
            $kodekpskrg = $pr->idkelasparalel;
          @endphp
        @endforeach
      </table>
      <br>
    @endforeach
</body>
</html>