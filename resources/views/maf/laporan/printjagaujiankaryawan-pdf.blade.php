<!DOCTYPE html>
<html>
<head>
  <title>Jadwal Jaga Ujian</title>
  <style>
      body {
        font-family: "Times New Roman", Georgia, Serif;
        font-size: 16px;
        line-height: 20px;
        font-weight: 400;
        /*color: #3b3b3b;*/
        -webkit-font-smoothing: antialiased;
        font-smoothing: antialiased;
      }

      .wrapper {
        margin: 0 auto;
        padding: 40px;
        max-width: 800px;
      }

      .table {
        margin: 0 0 40px 0;
        width: 100%;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
        display: table;
      }

      table {
        border-collapse: collapse;
      }

      table, th, td {
          border: 1px solid black;
      }

      @media screen and (max-width: 580px) {
        .table {
          display: block;
        }
      }

      .row {
        display: table-row;
        /*background: #f6f6f6;*/
      }
      .row:nth-of-type(odd) {
        /*background: #e9e9e9;*/
      }
      .row.header {
        font-weight: 900;
        /*color: #ffffff;*/
        /*background: #ea6153;*/
      }
      .row.green {
        /*background: #27ae60;*/
      }
      .row.blue {
        /*background: #2980b9;*/
      }
      @media screen and (max-width: 580px) {
        .row {
          padding: 8px 0;
          display: block;
        }
      }

      .cell {
        padding: 6px 6px;
        display: table-cell;
      }

      .thcell {
        padding: 6px 8px;
        display: table-cell;
      }

      @media screen and (max-width: 580px) {
        .cell {
          padding: 2px 12px;
          display: block;
        }
      }

  </style>
</head>
<body>
  @foreach($allkaryawan as $k)
      @if($k->getDataJaga()->count() != 0)
      <h5>Ujian @if($semesteraktif->getUtsUas() == "uas") Akhir @else Tengah @endif
      Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}
      <br>{{$k->npk}} {{$k->namalengkap}}</h5>

      <table>
        <tr>
          <th width="20px">No</th>
          <th width="200px">Hari dan Tanggal</th>
          <th width="30px">Jam</th>
          <th width="80px">Ruang</th>
          <th width="300px">Mata Kuliah</th>
          <th width="30px">KP</th>
          <th width="30px">Isi</th>
        </tr>
        @php
          $tanggalskrg = "";
          $idrskrg = "";
        @endphp
        @foreach($k->getDataJaga() as $pr)
        <tr>
          <td class="cell">{{$loop->iteration}}</td>
          @if($tanggalskrg != $pr->tanggal)
            @php
              $tanggalskrg = "";
              $idrskrg = "";
            @endphp
          <td rowspan="{{$k->getDataJaga()->where('tanggal', $pr->tanggal)->count()}}" class="cell">
            {{ date('l', strtotime($pr->tanggal)) }}, {{ $pr->tanggal }}
          </td>
          @endif
          @if($pr->idruangan != $idrskrg)
          <td class="cell" rowspan="{{$k->getDataJaga()->where('tanggal', $pr->tanggal)->where('idruangan', $pr->idruangan)->count()}}">{{ $pr->kp->mk->jadwalujian[0]->jamke }}</td>
          <td class="cell" rowspan="{{$k->getDataJaga()->where('tanggal', $pr->tanggal)->where('idruangan', $pr->idruangan)->count()}}">{{ $pr->ruangan->nama }}</td>
          @endif
          <td class="cell">{{ $pr->kp->kodemk }}.{{ $pr->kp->mk->nama }}</td>
          <td class="cell">{{ $pr->kp->kodekp }}</td>
          <td class="cell">{{ $pr->jumlahpeserta}}</td>
        </tr>
        @php
          $tanggalskrg = $pr->tanggal;
          $idrskrg = $pr->idruangan;
        @endphp
        @endforeach
      </table>
      <br>
      @endif
    @endforeach
</body>
</html>