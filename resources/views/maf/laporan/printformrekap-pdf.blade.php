<!DOCTYPE html>
<html>
<head>
  <title>Form Rekap Peserta Mata Kuliah</title>
  <style>
      body {
        font-family: "Helvetica Neue", Helvetica, Arial;
        font-size: 16px;
        line-height: 20px;
        font-weight: 400;
        color: #3b3b3b;
        -webkit-font-smoothing: antialiased;
        font-smoothing: antialiased;
      }

      .wrapper {
        margin: 0 auto;
        padding: 40px;
        max-width: 800px;
      }

     /* .table {
        margin: 0 0 40px 0;
        width: 100%;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
        display: table;
      }

      @media screen and (max-width: 580px) {
        .table {
          display: block;
        }
      }

      .row {
        display: table-row;
        background: #f6f6f6;
      }
      .row:nth-of-type(odd) {
        background: #e9e9e9;
      }
      .row.header {
        font-weight: 900;
        color: #ffffff;
        background: #ea6153;
      }
      .row.green {
        background: #27ae60;
      }
      .row.blue {
        background: #2980b9;
      }
      @media screen and (max-width: 580px) {
        .row {
          padding: 8px 0;
          display: block;
        }
      }

      .cell {
        padding: 6px 6px;
        display: table-cell;
      }

      .thcell {
        padding: 6px 8px;
        display: table-cell;
      }

      @media screen and (max-width: 580px) {
        .cell {
          padding: 2px 12px;
          display: block;
        }
      }*/

  </style>
</head>
<body>
<h3>Form Rekap Peserta Mata Kuliah Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}
  @if($kal != "") - {{$kal}} @endif 
</h3>
@foreach($kelasparalels as $kp)
  <div class="row">
    <label class="col-sm-2">Kode MK:</label>
    <label class="col-sm-10">{{$kp->kodemk}}</label>
    <br>
    <label class="col-sm-2">Nama:</label>
    <label class="col-sm-10">{{$kp->mk->nama}} KP: {{$kp->kodekp}}</label>
    <br>
    <label class="col-sm-2">Kapasitas / Isi:</label>
    <label class="col-sm-10">{{$kp->kapasitas}} / {{$kp->hitungIsiKeterima()}}</label>
    <br>
    <label class="col-sm-2">Status DMB:</label>
    <label class="col-sm-10">@if($kp->statusdmb == "Ya") DMB @else Non DMB @endif</label>
    <br>
  </div>
  <div class="row">
    <label class="col-sm-4"><b>Mahasiswa Asisten</b></label>
    <label class="col-sm-8">
    @if($kp->getPesertaAsisten()->count() > 0)
      @foreach($kp->getPesertaAsisten() as $p)
        {{$p->nrp}} &nbsp;&nbsp;&nbsp;
      @endforeach
    @else - @endif
    </label>
    <br>
    <label class="col-sm-4"><b>Mahasiswa Semesternya</b></label>
    <label class="col-sm-8">
    @if($kp->getPesertaSemesternya()->count() > 0)
      @foreach($kp->getPesertaSemesternya() as $p)
        {{$p->nrp}} &nbsp;&nbsp;&nbsp;
      @endforeach
    @else - @endif
    </label>
    <br>
    <label class="col-sm-4"><b>Mahasiswa Angkatan Tua</b></label>
    <label class="col-sm-8">
    @if($kp->getPesertaAngkatanTua()->count() > 0)
      @foreach($kp->getPesertaAngkatanTua() as $p)
        {{$p->nrp}} &nbsp;&nbsp;&nbsp;
      @endforeach
    @else - @endif
    </label>
    <br>
    <label class="col-sm-4"><b>Mahasiswa Setting NRP</b></label>
    <label class="col-sm-8">
    @if($kp->getPesertaSettingNrp()->count() > 0)
      @foreach($kp->getPesertaSettingNrp() as $p)
        {{$p->nrp}} &nbsp;&nbsp;&nbsp;
      @endforeach
    @else - @endif
    </label>
    <br>
    <label class="col-sm-4"><b>Mahasiswa Lainnya</b></label>
    <label class="col-sm-8">
    @if($kp->getPesertaLainnya()->count() > 0)
      @foreach($kp->getPesertaLainnya() as $p)
        {{$p->nrp}} &nbsp;&nbsp;&nbsp;
      @endforeach
    @else - @endif
    </label>
  </div>
  <hr>
@endforeach

</body>
</html>