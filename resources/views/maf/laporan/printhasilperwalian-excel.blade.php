<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="_token" content="{{ csrf_token() }}" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Laporan Jadwal Ujian Lengkap</title>
</head>
<body>
<table>
  <tr>
    <td colspan="7">
      <h3>Ujian Akhir Semester {{$semesteraktif->semester}} {{$semesteraktif->tahunajaran}}</h3>
    </td>
  </tr>
</table>
@foreach($tanggal as $t)
  <?php $date = new DateTime($t);?>
  <table>
    <tr>
      <td colspan="7">
        <b>{{ $presensis->where('tanggal', $t)->first()->kp->mk->jadwalujian[0]->hari }}, {{ date_format($date, "d-m-Y") }}, Piket: {{ $datapikets->where('tanggal', $t)->first()->karyawan->namalengkap }}</b>
      </td>
    </tr>
    <tr>
      <td><b>Jam</b></td>
      <td width="40px"><b>Mata Kuliah</b></td>
      <td><b>KP</b></td>
      <td><b>Ruangan</b></td>
      <td><b>Dosen Jaga</b></td>
      <td><b>Karyawan Jaga</b></td>
      <td><b>NRP (Jumlah)</b></td>
    </tr>
    @php
      $kodemkskrg = "";
      $kodekpskrg = "";
      $idruanganskrg = "";
      $npkdosenskrg = "";
      $npkkaryawanskrg = "";
    @endphp
    @foreach($presensis->where('tanggal', $t) as $pr)
    <tr>
      @if($pr->kp->kodemk != $kodemkskrg)
        <td rowspan="{{$pr->getRowspanMk()}}" valign="middle">{{$pr->kp->mk->jadwalujian[0]->jamke}}</td>
        <td rowspan="{{$pr->getRowspanMk()}}" valign="middle">{{$pr->kp->kodemk}} - {{$pr->kp->mk->nama}}</td>
      @else
        <td></td>
        <td></td>
      @endif
      @if($pr->idkelasparalel != $kodekpskrg)
        <td valign="middle" rowspan="{{$presensis->where('idkelasparalel', $pr->idkelasparalel)->count()}}">{{ $pr->kp->kodekp }}</td>
      @else
        <td></td>
      @endif
      @if($pr->idruangan != $idruanganskrg)
        <td valign="middle" rowspan="{{$presensis->where('tanggal', $t)->where('idruangan', $pr->idruangan)->count()}}">{{ $pr->ruangan->nama }}</td>
      @else
        <td></td>
      @endif
      @if($pr->npkdosenjaga != $npkdosenskrg)
        <td valign="middle" rowspan="{{$presensis->where('tanggal', $t)->where('npkdosenjaga', $pr->npkdosenjaga)->count()}}">{{ $pr->dosenjaga->namalengkap }} ({{ $pr->npkdosenjaga }})</td>
      @else
        <td></td>
      @endif
      @if($pr->npkkaryawanjaga != $npkkaryawanskrg)
        <td valign="middle" rowspan="{{$presensis->where('tanggal', $t)->where('npkkaryawanjaga', $pr->npkkaryawanjaga)->count()}}">{{ $pr->karyawanjaga->namalengkap }} ({{ $pr->npkkaryawanjaga }})</td>
      @else
        <td></td>
      @endif
      <td>{{ $pr->nrpawal }} - {{ $pr->nrpakhir }} ({{ $pr->jumlahpeserta }})</td>
    </tr>
      @php
        $kodemkskrg = $pr->kp->kodemk;
        $kodekpskrg = $pr->idkelasparalel;
        $idruanganskrg = $pr->idruangan;
        $npkdosenskrg = $pr->npkdosenjaga;
        $npkkaryawanskrg = $pr->npkkaryawanjaga;
      @endphp
    @endforeach
  </table>
@endforeach
</body>
</html>