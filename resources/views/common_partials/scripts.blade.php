<!--   Core JS Files   -->
<script src="{{{ asset('paperadmin/js/jquery-1.10.2.js')}}}" type="text/javascript"></script>
<script src="{{{ asset('paperadmin/js/bootstrap.min.js')}}}" type="text/javascript"></script>

<!-- Jquery ui -->
<script type="text/javascript" src="{{{ asset('jquery-ui-1.12.1/jquery-ui.js')}}}"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="{{{ asset('paperadmin/js/bootstrap-checkbox-radio.js')}}}"></script>

<!--  Charts Plugin -->
<script src="{{{ asset('paperadmin/js/chartist.min.js')}}}"></script>

<!--  Notifications Plugin    -->
<script src="{{{ asset('paperadmin/js/bootstrap-notify.js')}}}"></script>

<!--  Google Maps Plugin    -->
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="{{{ asset('paperadmin/js/paper-dashboard.js')}}}"></script>

<!-- Bootstrap Tab Collapse -->
<!-- <script src="{{{ asset('js/bootstrap-tabcollapse.js')}}}"></script> -->

<!-- DataTables -->
<script src="{{{ asset('DataTables-1.10.12/media/js/jquery.dataTables.min.js')}}}"></script>
<script src="{{{ asset('DataTables-1.10.12/media/js/dataTables.bootstrap.min.js')}}}"></script>

<script src="{{{ asset('DataTables-1.10.12/extensions/Responsive/js/dataTables.responsive.min.js')}}}"></script>

<script type="text/javascript" src="{{{ asset('DataTables-1.10.12/extensions/ColReorder/js/dataTables.colReorder.min.js')}}}"></script>

<script type="text/javascript" src="{{{ asset('DataTables-1.10.12/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}}"></script>

<script type="text/javascript" src="{{{ asset('DataTables-1.10.12/extensions/Buttons/js/dataTables.buttons.min.js')}}}"></script>
<script type="text/javascript" src="{{{ asset('DataTables-1.10.12/extensions/Buttons/js/buttons.colVis.min.js')}}}"></script>

<!-- Shopping Cart -->
<!-- <script type='text/javascript' src="{{{ asset('shoppingcart/js/jquery.mycart.js')}}}"></script> -->


<!-- Bootstrap Select -->
<script src="{{{ asset('bootstrap-select-1.11.2/dist/js/bootstrap-select.js')}}}"></script>
