<!-- Bootstrap core CSS     -->
<link href="{{{ asset('paperadmin/css/bootstrap.min.css')}}}" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="{{{ asset('paperadmin/css/animate.min.css')}}}" rel="stylesheet"/>

<!-- Jquery UI css -->
<link rel="stylesheet" type="text/css" href="{{{ asset('jquery-ui-1.12.1/jquery-ui.css')}}}">
<link rel="stylesheet" type="text/css" href="{{{ asset('jquery-ui-1.12.1/jquery-ui.structure.css')}}}">
<link rel="stylesheet" type="text/css" href="{{{ asset('jquery-ui-1.12.1/jquery-ui.theme.css')}}}">

<!-- Dari Paper Kit -->
<!-- <link href="{{{ asset('paperkit/bootstrap3/css/bootstrap.css')}}}" rel="stylesheet" />
<link href="{{{ asset('paperkit/assets/css/ct-paper.css')}}}" rel="stylesheet"/> -->

<!--  Paper Dashboard core CSS    -->
<link href="{{{ asset('paperadmin/css/paper-dashboard.css')}}}" rel="stylesheet"/>

<!-- DataTables -->
<link rel="stylesheet" href="{{{ asset('DataTables-1.10.12/media/css/dataTables.bootstrap.css')}}}">

<link rel="stylesheet" href="{{{ asset('DataTables-1.10.12/extensions/Responsive/css/responsive.dataTables.min.css')}}}">

<link rel="stylesheet" type="text/css" href="{{{ asset('DataTables-1.10.12/extensions/FixedHeader/css/fixedHeader.dataTables.min.css')}}}">

<link rel="stylesheet" type="text/css" href="{{{ asset('DataTables-1.10.12/extensions/ColReorder/css/colReorder.dataTables.min.css')}}}">

<link rel="stylesheet" type="text/css" href="{{{ asset('DataTables-1.10.12/extensions/Buttons/css/buttons.dataTables.min.css')}}}">

<!-- Bootstrap Select -->
<link rel="stylesheet" href="{{{ asset('bootstrap-select-1.11.2/dist/css/bootstrap-select.css')}}}">


<!--  Fonts and icons     -->
<link href="{{{ asset('font-awesome-4.4.0/css/font-awesome.min.css')}}}" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
<link href="{{{ asset('paperadmin/css/themify-icons.css')}}}" rel="stylesheet">

<style type="text/css">
	.berita
	{
		margin-left: 5px;
	}

	thead th { white-space: nowrap; }

	.sidebar[data-background-color="black"] .dropdown-menu
	{
		background-color: #333 !important;
	}

	.sidebar[data-background-color="black"] .dropdown-menu > li > a:hover, .sidebar[data-background-color="black"] 	.dropdown-menu > li > a:focus
	{
		background-color: #999;
		color: #333;
		opacity: 1;
		text-decoration: none;
	}

	.nav>li>a 
	{
		padding-bottom: 5px !important;
		padding-right: -5px !important;
	}

	/*.logo
	{
		padding-right: 0px !important;
		margin-left: -5px !important;
	}
*/
	.nav 
	{
		margin-top: 5px !important;
		margin-left: -5px !important;
	}

	.sidebar[data-background-color="black"] .nav, .sidebar[data-background-color="black"] .off-canvas-sidebar .nav
	{
		background-color: #222 !important;
		margin-right: 1px !important;
	}
</style>