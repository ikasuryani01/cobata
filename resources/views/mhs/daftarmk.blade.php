@extends('mhs._layouts.base')

@section('title', 'Daftar Mata Kuliah')


@section('header_styles')
<style>
  .badge-notify{
    background:red;
    position:relative;
    top: -20px;
    right: 10px;
  }
  .my-cart-icon-affix {
    position: fixed;
    z-index: 999;
  }
  body {
    font-size: 12px;
  }
  
  .kp {
    color: blue;
  }

  .card {
    margin: 10px -5px 10px -5px;
    padding: -10px 2px 2px 2px;
    vertical-align: baseline;
  }

  .card .content {
    padding-top: 5px;
  }
</style>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Algoritma dan Pemrograman
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Algoritma dan Pemrograman
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Algoritma dan Pemrograman
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Algoritma dan Pemrograman
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Algoritma dan Pemrograman
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Algoritma dan Pemrograman
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Manajemen Administrasi Basis Data
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Big Data
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Pemrograman Berbasis Objek
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-sm-4 col-xs-6">
        <div class="card">
            <div class="content" align="center">
                
                <h5>Data Mining
                    <br>KP: <strong class="kp">A</strong> - <strong>6 SKS</strong>
                </h5>
                    <button class="btn btn-danger my-cart-btn btn-sm" data-id="1" data-name="product 1" data-summary="summary 1" data-price="10" data-quantity="1" data-image="images/img_1.png"><i class="fa fa-plus"></i>Tambah</button>
                    <!-- <a href="#" class="btn btn-info">Lihat</a> -->
            </div>
        </div>
    </div>

</div>


@endsection

@section('footer_scripts')
<script type="text/javascript">
  $(function () {

    var goToCartIcon = function($addTocartBtn){
      var $cartIcon = $(".my-cart-icon");
      var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
      $addTocartBtn.prepend($image);
      var position = $cartIcon.position();
      $image.animate({
        top: position.top,
        left: position.left
      }, 0 , "linear", function() {
        $image.remove();
      });
    }

    $('.my-cart-btn').myCart({
      classCartIcon: 'my-cart-icon',
      classCartBadge: 'my-cart-badge',
      classProductQuantity: 'my-product-quantity',
      classProductRemove: 'my-product-remove',
      classCheckoutCart: 'my-cart-checkout',
      affixCartIcon: true,
      showCheckoutModal: true,
      cartItems: [
        {id: 1, name: 'product 1', summary: 'summary 1', price: 10, quantity: 1, image: 'images/img_1.png'},
        {id: 2, name: 'product 2', summary: 'summary 2', price: 20, quantity: 2, image: 'images/img_2.png'},
        {id: 3, name: 'product 3', summary: 'summary 3', price: 30, quantity: 1, image: 'images/img_3.png'}
      ],
      clickOnAddToCart: function($addTocart){
        goToCartIcon($addTocart);
      },
      clickOnCartIcon: function($cartIcon, products, totalPrice, totalQuantity) {
        console.log("cart icon clicked", $cartIcon, products, totalPrice, totalQuantity);
      },
      checkoutCart: function(products, totalPrice, totalQuantity) {
        console.log("checking out", products, totalPrice, totalQuantity);
      },
      getDiscountPrice: function(products, totalPrice, totalQuantity) {
        console.log("calculating discount", products, totalPrice, totalQuantity);
        return totalPrice * 0.5;
      }
    });

  });
  </script>

@endsection