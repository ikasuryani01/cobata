@extends('mhs._layouts.base')

@section('title', 'Informasi Mata Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Daftar Mata Kuliah Buka</h4>
    </div>
    <div class="content">
      <table id="tabelIMK" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>Kode MK</th>
            <th>Nama MK</th>
            <th>Kode KP</th>
            <th>SKS</th>
            <th>Isi / Kap</th>
          </tr>
        </thead>
        <tbody>
        @foreach($mks as $d)
          @foreach($d->mk->kp as $kp)
            <tr>
              <td>{{$kp->kodemk}}</td>
              <td><!-- <a href="{!! action('MhsController@lihatDetailMk', $kp->id) !!}"> -->{{ $d->mk->nama }}<!-- </a> --></td>
              <td>{{ $kp->kodekp }}</td>
              <td>{{ $d->mk->sks }}</td>
              <td>{{ $kp->hitungIsiKeterima() }} / {{ $kp->kapasitas }}</td>
              <!-- <td>{{ $kp->statusaktif }}</td> -->
              <!-- <td>{{ $kp->npkdosenpengajar }} - {{ $kp->karyawan->nama }}</td> -->
            </tr>
          @endforeach
        @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>Kode MK</th>
            <th>Nama MK</th>
            <th>Kode KP</th>
            <th>SKS</th>
            <th>Isi / Kap</th>
            <!-- <th>Status</th> -->
            <!-- <th>Dosen</th> -->
          </tr>
        </tfoot>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelIMK").dataTable( {
      "pagingType": "full",
      "stateSave": true,
      "aaSorting" : [[0, 'desc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection