@extends('mhs._layouts.base')

@section('title', 'Histori Perwalian')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Daftar Mata Kuliah Terdaftar</h4>
    </div>
    <div class="content">
    <div class="visible-lg visible-md">
      <table id="tabelHP" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>Kode MK</th>
            <th>Nama MK</th>
            <th>KP</th>
            <th>SKS</th>
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @if($datas->count() > 0)
            <?php $tmpjenis = ""; ?>
            @foreach($datas as $d)
              @if($d->fpp->jenis != $tmpjenis)
                <tr>
                  <td colspan="6" align="center">
                    <b>{{$d->fpp->jenis}}</b>
                  </td>
                </tr>
                <?php $tmpjenis = $d->fpp->jenis; ?>
              @endif
                <tr>
                  <td>{{ $d->kp->mk->kodemk }}</td>
                  <td>{{ $d->kp->mk->nama }}</td>
                  <td>{{ $d->kp->kodekp }}</td>
                  <td>{{ $d->kp->mk->sks }}</td>
                  <td>
                  @if($d->statuspublikasi == "Ya")
                    {{ $d->status }}
                  @else
                    {{ "Menunggu" }}
                  @endif
                  </td>
                  <td>
                  @if(!empty($fpp) && $d->fpp->id == $fpp->id)
                    @if($d->kp->cekAda(Auth::guard('mahasiswa')->user()->nrp) == 1)
                      <a class="btn btn-sm btn-warning" href="{!! action('MhsController@deleteDaftarMk', 'k='.$d->kp->id.'&p='.Auth::guard('mahasiswa')->user()->nrp.'&f='.$d->fpp->id) !!}">Hapus</a>
                    @endif
                  @else
                    {{ "-" }}
                  @endif
                </tr>
              @endforeach
            @endif
        </tbody>
      </table>
    </div>
    <div class="visible-xs visible-sm">
      <table id="tabelHistori" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <th>Kode MK</th>
          <th>Detail MK</th>
        </thead>
        <tbody>
          @if($datas->count() > 0)
            @php $tmpjenis = ""; @endphp
            @foreach($datas as $d)
              @if($d->fpp->jenis != $tmpjenis)
                <tr>
                  <td colspan="2">
                    <b>{{$d->fpp->jenis}}</b>
                  </td>
                  <!-- <td></td> -->
                </tr>
                @php $tmpjenis = $d->fpp->jenis; @endphp
              @endif
                <tr>
                  <td align="center">
                    {{ $d->kp->mk->kodemk }}<br>
                    @if(!empty($fpp) &&  $d->fpp->id == $fpp->id)
                      @if($d->kp->cekAda(Auth::guard('mahasiswa')->user()->nrp) == 1)
                        <a class="btn btn-sm btn-warning" href="{!! action('MhsController@deleteDaftarMk', 'k='.$d->kp->id.'&p='.Auth::guard('mahasiswa')->user()->nrp.'&f='.$d->fpp->id) !!}">Hapus</a>
                      @endif
                    @endif
                  </td>
                  <td>
                    {{ $d->kp->mk->nama }} ({{ $d->kp->kodekp }})<br>
                    {{ $d->kp->mk->sks }} SKS, @if($d->statuspublikasi == "Ya") {{$d->status}} @else {{"Menunggu"}} @endif
                  </td>
                </tr>
            @endforeach
          @endif
        </tbody>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelHP").dataTable( {
      "pagingType": "full",
      "stateSave": true,
      "aaSorting" : [[0, 'desc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    $("#tabelHistori").dataTable( {
      "pagingType": "full",
      "stateSave": true,
      "aaSorting" : [[0, 'desc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-12'tr>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection