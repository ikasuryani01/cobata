<ul class="nav">
    <li {!! (Request::is('mhs') ? ' class="active"' : '') !!}>
        <a href="{{ route('mhs.beranda') }}">
            <i class="ti-home"></i>
            <p>Beranda</p>
        </a>
    </li>

    <li {!! (Request::is('mhs/jkuliah*') ? ' class="active"' : '') !!}>
        <a href="{{ route('mhs.jkuliah') }}">
            <i class="ti-blackboard"></i>
            <p>Jadwal Kuliah</p>
        </a>
    </li>

    <li {!! (Request::is('mhs/jujian*') ? ' class="active"' : '') !!}>
        <a href="{{ route('mhs.jujian') }}">
            <i class="ti-bookmark-alt"></i>
            <p>Jadwal Ujian</p>
        </a>
    </li>

    <li {!! (Request::is('mhs/daftarmk/t') ? ' class="active"' : '') !!}>
        <a href="{{ route('mhs.daftarmk.tabel') }}">
            <i class="ti-pencil-alt"></i>
            <p>Daftar Mata Kuliah</p>
        </a>
    </li>

    <li {!! (Request::is('mhs/informasimk*') ? ' class="active"' : '') !!}>
        <a href="{{ route('mhs.informasimk') }}">
            <i class="ti-info"></i>
            <p>Informasi Mata Kuliah</p>
        </a>
    </li>

    <li {!! (Request::is('mhs/transkrip*') ? ' class="active"' : '') !!}>
        <a href="{{ route('mhs.transkrip') }}">
            <i class="ti-map"></i>
            <p>Transkrip</p>
        </a>
    </li>

    <li {!! (Request::is('mhs/riwayat*') ? ' class="active"' : '') !!}>
        <a href="{{ route('mhs.riwayat') }}">
            <i class="ti-bell"></i>
            <p>Histori Perwalian</p>
        </a>
    </li>

<!--     <li>
        <a href="http://bit.ly/kuisionerfinalta">
            <i class="ti-arrow-right"></i>
            <p>Kuisioner Tugas Akhir</p>
        </a>
    </li> -->
</ul>