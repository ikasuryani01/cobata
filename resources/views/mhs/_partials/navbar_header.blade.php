<div class="collapse navbar-collapse">
    <ul class="nav navbar-nav navbar-right">

        @if (Auth::guard('mahasiswa')->guest())
            <li>
                <a href="{{ url('/login') }}">Login</a>
            </li>
        @else
        <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Selamat datang, <i class="ti-user"></i>
				<p>{{ Auth::guard('mahasiswa')->user()->nama }}</p>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#profil" data-toggle="modal">Profil</a></li>
                <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </ul>
        </li>
        @endif
    </ul>
</div>

<!-- Modal -->
<div class="modal fade" id="profil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Profil Mahasiswa</h4>
      </div>
      <div class="modal-body">
        <table class="table table-responsive table-condensed">
            <tr>
                <td>NRP</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->nrp }}</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->nama }}</td>
            </tr>
            <tr>
                <td>Jurusan</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->jurusan->Nama }}</td>
            </tr>
            <tr>
                <td>SKS Maks Smt Depan</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->sksmaxdepan }}</td>
            </tr>
            <tr>
                <td>IPK Tanpa E</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->ipktanpae }}</td>
            </tr>
            <tr>
                <td>SKS Kum Tanpa E</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->skskumtanpae }}</td>
            </tr>
            <tr>
                <td>Asisten</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->asisten }}</td>
            </tr>
            <tr>
                <td>Nilai TOEFL</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->nilaitoefl }}</td>
            </tr>
            <tr>
                <td>Masa Studi</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->masastudi }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{ Auth::guard('mahasiswa')->user()->kodestatus }}</td>
            </tr>
        </table>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>