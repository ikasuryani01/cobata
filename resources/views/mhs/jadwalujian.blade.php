@extends('mhs._layouts.base')

@section('title', 'Jadwal Ujian Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Jadwal Ujian</h4>
        <!-- <a>
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-list-alt"></i>
            <span class="hidden-sm hidden-xs">Versi Kalender</span>
          </button>
        </a> -->
        @if($viewnrp == "ya")
        <a href="{!! action('MhsController@showJadwalUjian') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-star"></i>
            <span class="hidden-sm hidden-xs">Tampilkan Semua Kelas</span>
          </button>
        </a>
        @elseif($viewnrp == "tidak")
        <a href="{!! action('MhsController@showJadwalUjian', 'nrp=y') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan Kelas Diambil</span>
          </button>
        </a>
        @endif
    </div>
    <div class="content">
      <table id="tabelUjian" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th>Kode MK</th>
          <th>Nama MK</th>
          <th>Minggu</th>
          <th>Hari</th>
          <th>Jam</th>
          <th>Ruangan</th>
        </tr>
        </thead>
        <tbody>
          @foreach($ujians as $ujian)
            <tr>
              <td>{{ $ujian->kodemk }}</td>
              <td>{{ $ujian->mk->nama }}</td>
              <td>{{ $ujian->mingguke }}</td>
              <td>{{ $ujian->hari }}</td>
              <td>{{ $ujian->jamke }}</td>
              <td>
              @if($viewnrp == "ya")
                @if($ujian->getIdruangan(Auth::guard('mahasiswa')->user()->nrp) != null)
                {{ $ujian->idruangan }}
                @else
                {{ "Belum Dibuats" }}
                @endif
              @else
                @if(!empty($ujian->mk->getRuanganUjian()))
                  @foreach($ujian->mk->getRuanganUjian() as $data)
                    {{ $data->ruangan->nama }} ({{$data->isi}})<br>
                  @endforeach
                @endif
              @endif
              </td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>Kode MK</th>
          <th>Nama MK</th>
          <th>Minggu</th>
          <th>Hari</th>
          <th>Jam</th>
          <th>Ruangan</th>
        </tr>
        </tfoot>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelUjian").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[2,'asc'], [3, 'desc'], [4, 'asc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection