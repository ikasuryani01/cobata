@extends('mhs._layouts.base')

@section('title', 'Transkrip mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Transkrip Mahasiswa</h4>
    </div>
    <div class="content">
      <table id="tabelTrans" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>Kode MK</th>
            <th>Nama MK</th>
            <th>SKS</th>
            <th>Semester</th>
            <th>Nisbi</th>
          </tr>
        </thead>
        <tbody>
        @foreach($datas as $d)
          <tr>
            <td>{{ $d->kodemk}}</td>
            <td>{{ $d->mk->nama }}</a></td>
            <td>{{ $d->mk->sks }}</td>
            <td>{{ $d->semester->semester }} {{ $d->semester->tahunajaran }}</td>
            <td>{{ $d->nisbi }}</td>
          </tr>
        @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>Kode MK</th>
            <th>Nama MK</th>
            <th>SKS</th>
            <th>Semester</th>
            <th>Nisbi</th>
          </tr>
        </tfoot>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelTrans").dataTable( {
      "pagingType": "full",
      "stateSave": true,
      "aaSorting" : [[0, 'desc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection