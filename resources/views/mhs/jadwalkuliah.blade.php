@extends('mhs._layouts.base')

@section('title', 'Jadwal Kuliah Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">
          Jadwal Kuliah @if($viewnrp == "ya") {{"Mahasiswa"}} @else {{Auth::guard('mahasiswa')->user()->jurusan->Nama}}@endif
        </h4>
        <!-- <a>
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-list-alt"></i>
            <span class="hidden-sm hidden-xs">Versi Kalender</span>
          </button>
        </a> -->
        @if($viewnrp == "ya")
        <a href="{!! action('MhsController@showJadwalKuliah') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-star"></i>
            <span class="hidden-sm hidden-xs">Tampilkan Semua Kelas</span>
          </button>
        </a>
        @elseif($viewnrp == "tidak")
        <a href="{!! action('MhsController@showJadwalKuliah', 'nrp=y') !!}">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-sun-o"></i>
            <span class="hidden-sm hidden-xs">Tampilkan Kelas Diambil</span>
          </button>
        </a>
        @endif
    </div>
    <div class="content">
      <table id="tabelKuliah" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th>Hari</th>
          <th>Jam</th>
          <th>Nama Kelas</th>
          <th width="100px">Ruangan</th>
          <th width="300px">Pengajar</th>
        </tr>
        </thead>
        <tbody>
          @foreach($kuliahs as $kuliah)
            <tr>
              <td>{{ $kuliah->hari }}</td>
              <td>{{ substr($kuliah->jammasuk, 0, 5) }} - {{ substr($kuliah->jamkeluar, 0, 5) }}</td>
              <td>{{ $kuliah->kelas->mk->nama }} - {{ $kuliah->kelas->kodekp}}</td>
              <td>{{ $kuliah->ruangan->nama }}</td>
              <td>
                @if(!empty($kuliah->kelas->mk->getPengajar()))
                  @foreach($kuliah->kelas->mk->getPengajar() as $dosen)
                    {{$dosen->npk}} - {{$dosen->gelardepan}}{{$dosen->nama}}{{$dosen->gelarbelakang}}<br>
                  @endforeach
                @endif
              </td>              
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>Hari</th>
          <th>Jam</th>
          <th>Nama Kelas</th>
          <th>Ruangan</th>
          <th>Pengajar</th>
        </tr>
        </tfoot>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKuliah").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[0,'desc'], [1, 'asc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection