@extends('mhs._layouts.base')

@section('title', 'Daftar Mata Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-{{session('classnotif')}}"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
@if($fpp != "0")
  <div class="card">
      <div class="header">
          <h4 class="title" style="display: inline; line-height: 1.5em;">{{ $fpp->jenis }} - Daftar Kelas Tersedia</h4>
          <a>
            <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;" data-toggle="modal" data-target="#myModal">
              <i class="fa fa-list-alt"></i>
              <span class="hidden-sm hidden-xs">MK Terdaftar</span>
            </button>
          </a>
      </div>
      <div class="content">
        <table id="tabelFpp" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
          <thead>
            <tr>
              <th>Aksi</th>
              <th>Nama MK</th>
              <th>Kode MK</th>
              <th>SKS</th>
              <th>Isi / Kap</th>
            </tr>
          </thead>
          <tbody>
            @foreach($kps as $kp)
              <tr>
                <td>
                @if($kp->cekAda(Auth::guard('mahasiswa')->user()->nrp) == 1)
                  <a class="btn btn-sm btn-warning" href="{!! action('MhsController@deleteDaftarMk', 'k='.$kp->id.'&p='.Auth::guard('mahasiswa')->user()->nrp.'&f='.$fpp->id) !!}">Hapus</a>
                @elseif($kp->cekAda(Auth::guard('mahasiswa')->user()->nrp) == 0)
                  <a class="btn btn-sm btn-success" href="{!! action('MhsController@addDaftarMk', 'k='.$kp->id.'&p='.Auth::guard('mahasiswa')->user()->nrp.'&f='.$fpp->id) !!}">Daftar</a>
                @elseif($kp->cekAda(Auth::guard('mahasiswa')->user()->nrp) == 4)
                  <a class="btn btn-sm btn-success" href="{!! action('MhsController@addDaftarMk', 'k='.$kp->id.'&p='.Auth::guard('mahasiswa')->user()->nrp.'&f='.$fpp->id) !!}">Daftar</a>
                @else
                  {{$kp->getStatusPendaftaran(Auth::guard('mahasiswa')->user()->nrp)}}
                @endif
                </td>
                <td><!-- <a href="{!! action('MhsController@lihatDetailMk', $kp->id) !!}"> -->{{ $kp->mk->nama }} KP: {{ $kp->kodekp }}<!-- </a> --></td>
                <td>{{ $kp->kodemk }}</td>
                <td>{{ $kp->mk->sks }}</td>
                <td>{{ $kp->hitungIsi() }} / {{ $kp->kapasitas }}</td>
              </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Aksi</th>
              <th>Kode MK</th>
              <th>Nama MK</th>
              <th>SKS</th>
              <th>Isi / Kap</th>
            </tr>
          </tfoot>
        </table>
      </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Histori Perwalian</h4>
        </div>
        <div class="modal-body">
          <p>Sisa SKS: <b>{{ Auth::guard('mahasiswa')->user()->cekSisaSksdanPending() }} SKS</b></p>
          <table id="tabelHistori" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%">
            <thead>
              <th>Kode MK</th>
              <th>Detail MK</th>
            </thead>
            <tbody>
              @if($datas->count() > 0)
                @php $tmpjenis = ""; @endphp
                @foreach($datas as $d)
                  @if($d->fpp->jenis != $tmpjenis)
                    <tr>
                      <td colspan="2" align="center">
                        <b>{{$d->fpp->jenis}}</b>
                      </td>
                    </tr>
                    @php $tmpjenis = $d->fpp->jenis; @endphp
                  @endif
                    <tr>
                      <td>
                        {{ $d->kp->kodemk }}<br>
                        @if($d->fpp->id == $fpp->id)
                          @if($d->kp->cekAda(Auth::guard('mahasiswa')->user()->nrp) == 1)
                            <a class="btn btn-sm btn-warning" href="{!! action('MhsController@deleteDaftarMk', 'k='.$d->idkelasparalel.'&p='.$d->nrp.'&f='.$d->idfpp) !!}">Hapus</a>
                          @endif
                        @endif
                      </td>
                      <td>
                        {{ $d->kp->mk->nama }} ({{ $d->kp->kodekp }})<br>
                        {{ $d->kp->mk->sks }} SKS, @if($d->statuspublikasi == "Ya") {{$d->status}} @else {{"Menunggu"}} @endif
                      </td>
                    </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> -->
      </div>
    </div>
  </div>
@else
  <div class="alert alert-info">
      <p>Tidak ada proses FPP yang sedang berlangsung</p>
  </div>
@endif
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelFpp").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "stateSave": true,
      "aaSorting" : [[0, 'desc']],
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    $("#tabelHistori").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "stateSave": true,
      "aaSorting" : [[0, 'desc']],
      "dom": "<'row'<'col-sm-12'tr>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection