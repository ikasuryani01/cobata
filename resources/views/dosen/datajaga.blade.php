@extends('dosen._layouts.base')

@section('title', 'Data Jaga Ujian')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('statussave') != "")
<div class="alert alert-info"><center>{!! session('statussave') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Jaga Ujian</h4>
    </div>
    <div class="content">
      <table id="tabelKelasParalel" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>Hari, tanggal</th>
            <th>Jam</th>
            <th>Ruang</th>
            <th>Mata kuliah</th>
            <th>KP</th>
            <th>Isi</th>
          </tr>
        </thead>
        <tbody>
        @php
          $tanggal = "";
          $mingguke = 0;
          $hari = "";
          $jamke = 0;
          $idruangan = 0;
        @endphp
        
          @foreach($presensis as $p)
            
              <tr>
                <td>{{date('l', strtotime($p->tanggal))}}, {{ $p->tanggal }}</td>
                <td>{{ $p->kp->mk->jadwalujian[0]->jamke }}</td>
                <td>{{ $p->ruangan->nama }}</td>
                <td>{{ $p->kp->kodemk }}.{{ $p->kp->mk->nama }}</td>
                <td>{{ $p->kp->kodekp }}</td>
                <td>{{ $p->jumlahpeserta}}</td>
              </tr>
              
          @endforeach
        </tbody>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKelasParalel").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection