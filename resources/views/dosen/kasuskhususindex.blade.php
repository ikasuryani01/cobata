@extends('dosen._layouts.base')

@section('title', 'Kasus Khusus')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
@if($fpp->jenis == "KK")
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.8em;">Kasus Khusus</h4>
    </div>
    <form class="form-horizontal" method="GET" style="margin-top: 10px;">
      <div class="form-group">
          <div class="visible-lg visible-md">
            <label class="col-md-3 control-label" style="margin-top: 5px;">NRP: </label>
          </div>
          <div class="hidden-lg hidden-md">
            <label class="col-xs-2 control-label" style="margin-left: 10px; margin-top: 10px; margin-right: -10px;">NRP: </label>
          </div>

          <div class="col-xs-8 col-md-6">
            <input type="text" class="form-control border-input" name="nrpkk" value="{{ $datas[0]->mahasiswa->nrp }}">
          </div>

          <div class="col-xs-2 col-md-3 hidden-md hidden-lg" style="margin-left: -20px;">
            <button class="btn btn-success"><i class="fa fa-search"></i></button>
          </div>
          <div class="col-xs-2 col-md-3 visible-md visible-lg">
            <button class="btn btn-success"><i class="fa fa-search"></i></button>
          </div>
      </div>
    </form>
    <hr>

    @if($statnrp == "sudah")
    <div class="header" style="margin-top: -25px;" align="center">
        <h3 class="title" style="display: inline; line-height: 1.8em;">Histori Perwalian Mahasiswa</h3>
        <p>
          NRP: {{ $datas[0]->mahasiswa->nrp }}<br>
          Nama: {{ $datas[0]->mahasiswa->nama }}<br>
          SKS Maksimal: {{ $datas[0]->mahasiswa->sksmaxdepan }}<br>
          Total SKS Diambil: {{ $datas[0]->mahasiswa->sksmaxdepan - $datas[0]->mahasiswa->cekSisaSks() }}<br>
          Sisa SKS: {{ $datas[0]->mahasiswa->cekSisaSks() }}
        </p>
        <a href="#tambahMK" data-toggle="modal">
          <button class="btn btn-info btn-sm" style="float: right; ">
            <i class="fa fa-plus"></i>
            <span class="hidden-sm hidden-xs">Tambah MK</span>
          </button>
        </a>
        <br>
    </div>
    <div class="modal fade" id="tambahMK" tabindex="-1" role="dialog" data-backdrop="false">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Tambah Mata Kuliah - Kasus Khusus</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" method="POST" action="{{route('dosen.tambahmk')}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="idfpp" value="{{ $fpp->id }}">
              <input type="hidden" name="nrp" value="{{ $datas[0]->mahasiswa->nrp }}">

              <div class='form-group ui-widget'>
                <label class='col-sm-3 control-label'>Kelas Paralel: </label>
                <div class='col-sm-9'>
                    <input type='text' id='kelasparalels' class='form-control border-input' placeholder='Cari Nama/Kode MK' name='namakelas' required>
               </div>
            </div>
            <input id="kelas-id" type="hidden" name="idkelasparalel">
              
              <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3">
                      <!-- <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button> -->
                      <input type="submit" class="btn btn-success" name="simpan" value="Simpan">
                  </div>
              </div>
          </form>
          </div>
        </div>
      </div>
    </div>
    <div class="content">
      <table id="tabelFPP" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Kode MK</th>
            <th>Nama MK</th>
            <th>KP</th>
            <th>SKS</th>
            <th>Status</th>
            <th>Alasan</th>
            <th>Publikasi</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
        @if($datas->count() > 0)
          <?php $tmpjenis = ""; ?>
          @foreach($datas as $d)
            @if($d->fpp->jenis != $tmpjenis)
              <tr>
                <td colspan="8" align="center">
                  <b>{{$d->fpp->jenis}}</b>
                </td>
              </tr>
              <?php $tmpjenis = $d->fpp->jenis; ?>
            @endif
              <tr>
                <td>{{ $d->kp->mk->kodemk }}</td>
                <td>{{ $d->kp->mk->nama }}</td>
                <td>{{ $d->kp->kodekp }}</td>
                <td>{{ $d->kp->mk->sks }}</td>
                <td>{{ $d->status }}</td>
                <td>{{ $d->alasan }}</td>
                <td>{{ $d->statuspublikasi }}</td>
                <td>
                  @if($d->status == "Diterima")
                    <a class="btn btn-sm btn-success" href="{!! action('DosenController@batalMkKK', $d->id) !!}">Batal</a>
                  @elseif($d->deleted_at != null && $d->status != "Pending" && $d->fpp->jenis == "KK")
                    <a class="btn btn-sm btn-warning" href="{!! action('DosenController@undoBatalMkKK', $d->id) !!}">Aktifkan</a>
                  @else
                    {{ "-" }}
                  @endif
                </td>
              </tr>
          @endforeach
        @endif
        </tbody>
        <tfoot>
          <tr>
            <th>Kode MK</th>
            <th>Nama MK</th>
            <th>KP</th>
            <th>SKS</th>
            <th>Status</th>
            <th>Alasan</th>
            <th>Publikasi</th>
            <th>Aksi</th>
          </tr>
        </tfoot>
      </table>
    @endif
</div>
@else
<div class="alert alert-info"><center>{{ "Maaf, Kasus khusus sedang tidak berlangsung" }}</label>
<br></center></div>
@endif

@endsection

@section('footer_scripts')
<script>
/*$("#tabelFPP").dataTable( {
      "pagingType": "full",
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      "stateSave": true,
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });*/
/*  $(function () {
    $("#tabelFPP").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });*/

    var datakelas = [
        <?php foreach ($kelas as $kp){ ?>
            { value: "<?php echo $kp->id;?>", label: "<?php echo $kp->mk->kodemk . " " . $kp->mk->nama . " KP: ". $kp->kodekp;?>" },
        <?php } ?>
        ];
    $(function() {
        $("#kelasparalels").autocomplete({
            source: datakelas,
            focus: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox
                $(this).val(ui.item.label);
            },
            select: function(event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox and hidden field
                $(this).val(ui.item.label);
                $("#kelas-id").val(ui.item.value);
            }
        });
    });
</script>
@endsection