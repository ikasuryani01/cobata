@extends('dosen._layouts.base')

@section('title', 'Beranda')

@section('content')
<div class="alert alert-info">
    <p>Selamat Datang {{ Auth::guard('karyawan')->user()->getJabatan()->nama }}</p>
</div>
@endsection