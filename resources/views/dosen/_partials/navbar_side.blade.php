<div class="visible-xs">
    <br>
</div>
<ul class="nav">
    <li {!! (Request::is('dosen') ? ' class="active"' : '') !!}>
        <a href="{{ route('dosen.beranda') }}">
            <i class="ti-home"></i>
            <p>Beranda</p>
        </a>
    </li>

    <li {!! (Request::is('dosen/kelas*') ? ' class="active"' : '') !!}>
        <a href="{{ route('dosen.kelas') }}">
            <i class="ti-blackboard"></i>
            <p>Data Kelas</p>
        </a>
    </li>

    <li {!! (Request::is('dosen/ajar*') ? ' class="active"' : '') !!}>
        <a href="{{ route('dosen.ajar') }}">
            <i class="ti-bookmark-alt"></i>
            <p>Jadwal Mengajar</p>
        </a>
    </li>

    <li {!! (Request::is('dosen/jaga*') ? ' class="active"' : '') !!}>
        <a href="{{ route('dosen.jaga') }}">
            <i class="ti-pencil-alt"></i>
            <p>Jadwal Jaga Ujian</p>
        </a>
    </li>

    @if(Auth::guard('karyawan')->user()->getJabatan()->nama != "Dosen") {{-- <-- untuk slain dosen (aa kalab kajur) --}}
    <li {!! (Request::is('dosen/kk*') ? ' class="active"' : '') !!}>
        <a href="{{ route('dosen.kk') }}">
            <i class="ti-flag-alt"></i>
            <p>Kasus Khusus</p>
        </a>
    </li>
    @endif
</ul>