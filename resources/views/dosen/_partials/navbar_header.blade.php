<div class="collapse navbar-collapse">
    <ul class="nav navbar-nav navbar-right">
        @if (Auth::guard('karyawan')->guest())
            <li>
                <a href="{{ url('/login') }}">Login</a>
            </li>
        @else
        <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Selamat datang, <i class="ti-user"></i>
				<p>{{ Auth::guard('karyawan')->user()->nama }}</p>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </ul>
        </li>
        @endif
    </ul>

</div>