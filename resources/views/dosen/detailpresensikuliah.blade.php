@extends('maf._layouts.base')

@section('title', 'Detail Presensi Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Data Peserta Kelas</h4>
    
        <a href="{{URL::route('paj.masterkuliah.add')}}">
          <button class="btn btn-success btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-star"></i>
            <span class="hidden-sm hidden-xs">Cetak Semua Presensi</span>
          </button>
        </a>

    </div>
    <div class="content">
      <table id="tabelKuliah" class="display responsive nowrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th>No</th>
          <th>NRP</th>
          <th>Nama</th>
        </tr>
        </thead>
        <tbody>
          @foreach($kuliah->kelas->pesertaKuliah() as $mhs)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $mhs->nrp }}</td>
              <td>{{ $mhs->nama }}</td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
        <tr>
          <th>No</th>
          <th>NRP</th>
          <th>Nama</th>
        </tr>
        </tfoot>
      </table>
    </div>
</div>


@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKuliah").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection