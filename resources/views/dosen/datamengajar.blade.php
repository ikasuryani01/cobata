@extends('dosen._layouts.base')

@section('title', 'Jadwal Mengajar')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
<form class="form-horizontal" method="GET">
  <div class="form-group">
      <div class="visible-lg visible-md">
        <label class="col-md-3 control-label" style="margin-top: 5px;">Semester: </label>
      </div>
      <div class="col-xs-8 col-md-6">
          <select class="form-control border-input" name="ids">
              @foreach($semester as $s)
                <option value="{{$s->id}}" {!! ($ids == $s->id ? ' selected' : '') !!}>{{$s->tahunajaran}} {{ $s->semester }}</option>
              @endforeach
          </select>
      </div>
      <div class="col-xs-2 col-md-3">
        <button class="btn btn-success"><i class="fa fa-search"></i></button>
      </div>
  </div>
</form>
@if(session('statussave') != "")
<div class="alert alert-info"><center>{!! session('statussave') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Jadwal Mengajar</h4>
    </div>
    <div class="content">
      <table id="tabelKelasParalel" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
          <tr>
            <th>Hari</th>
            <th>Jam</th>
            <th>Nama MK</th>
            <th>KP</th>
            <th>Ruangan</th>
          </tr>
        </thead>
        <tbody>
        @foreach($dataajar as $data)
          @foreach($data->jadwalkuliah as $jadwal)
            <tr>
              <td>{{ $jadwal->hari }}</td>
              <td>{{ $jadwal->jammasuk }} - {{ $jadwal->jamkeluar }}</td>
              <td>{{ $jadwal->kelas->mk->nama }}</td>
              <td>{{ $jadwal->kelas->kodekp }}</td>
              <td>{{ $jadwal->ruangan->nama }}</td>
            </tr>
          @endforeach
        @endforeach
        </tbody>
        <!-- <tfoot>
          <tr>
            <th>Kode MK</th>
            <th>Nama Kelas</th>
            <th>KP</th>
            <th>Jumlah Peserta</th>
            <th>Aksi</th>
          </tr>
        </tfoot> -->
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKelasParalel").dataTable( {
      "pagingType": "full",
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection