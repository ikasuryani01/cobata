<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Perwalian FT | Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{{ asset('AdminLTE/bootstrap/css/bootstrap.min.css')}}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{{ asset('font-awesome-4.4.0/css/font-awesome.min.css')}}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{{ asset('ionicons-2.0.1/css/ionicons.min.css')}}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{{ asset('AdminLTE/dist/css/AdminLTE.min.css')}}}">

  <style type="text/css">
    body {
        background-image: url('ubayawp.jpg') !important;
        background-size: cover !important;                      
        background-repeat: no-repeat !important;
        background-position: center center !important;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <center>
    <img src="{{{ asset('logoubaya.png') }}}" width="150px">
  </center>
  <div class="login-logo">
    <a href="{{ url('/') }}" style="color: white; font-weight: bold;">Perwalian FT UBAYA</a>
    <!-- <h5>1.0</h5> -->
  </div>
  @if(session('status') != "")
  <div class="alert alert-danger">{!! session('status') !!}</label>
  <br></div>
  @endif
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Selamat datang, silakan login terlebih dahulu</p>

    <form role="form" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input type="text" name="nrpnpk" class="form-control" placeholder="Username" required autofocus>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <select class="form-control" name="jenis">
          <option value="npk">@staff.ubaya.ac.id</option>
          <option value="nrp">@student.ubaya.ac.id</option>
        </select>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password" required="true">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label style="color: white;">1.0
              <!-- <input type="checkbox" name="remember"> Remember Me -->
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- <a href="{{ url('/password/reset') }}">I forgot my password</a><br> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="{{{ asset('AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{{ asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}}"></script>

</body>
</html>
