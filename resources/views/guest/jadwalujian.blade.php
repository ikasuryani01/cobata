@extends('guest._layouts.base')

@section('title', 'Jadwal Ujian')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">Jadwal Ujian Jurusan {{$namajur}}</h4>
        <a href="{{URL::previous()}}">
          <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-caret-left"></i>
            <span class="hidden-sm hidden-xs">Kembali</span>
          </button>
        </a>
    </div>
    <div class="content">
      <table id="tabelUjian" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th>Mata Kuliah</th>
          <th class="visible-xs">Jadwal</th>
          <th>Minggu</th>
          <th>Hari</th>
          <th>Jam</th>
          <th>Ruangan</th>
        </tr>
        </thead>
        <tbody>
          @foreach($ujians as $ujian)
            <tr>
              <td>{{ $ujian->mk->nama }} ({{$ujian->kodemk}})</td>
              <td class="visible-xs">{{ $ujian->mingguke }} - {{ $ujian->hari }} - {{ $ujian->jamke }}</td>
              <td>{{ $ujian->mingguke }}</td>
              <td>{{ $ujian->hari }}</td>
              <td>{{ $ujian->jamke }}</td>
              <td>
                @if(!empty($ujian->mk->getRuanganUjian()))
                  @foreach($ujian->mk->getRuanganUjian() as $data)
                    {{ $data->ruangan->nama }} ({{$data->isi}})<br>
                  @endforeach
                @else
                  -
                @endif
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelUjian").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[2,'asc'], [3, 'desc'], [4, 'asc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection