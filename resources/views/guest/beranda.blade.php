@extends('guest._layouts.base')

@section('title', 'Beranda')

@section('content')
<div class="alert alert-info">
    <h3>Jadwal Perwalian Semester {{ strtoupper($semester->semester)}} {{$semester->tahunajaran}}</h3>
  <table class="table table-condensed">
      <tr>
          <th>Tahap</th>
          <th>Waktu</th>
      </tr>
      @foreach($semester->fpp as $fpp)
      <tr>
          <td>{{$fpp->jenis}}</td>
          <td>{{$fpp->waktubuka}} s.d. {{$fpp->waktuselesai}}</td>
      </tr>
      @endforeach
  </table>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Pengumuman Workshop Desain Grafis</h4>
                <p class="category">Pengumuman</p>
            </div>
            <div class="content berita">
                Mata Kuliah Workshop Desain Grafis, Jarkom - F dan PBD - E DITUTUP karena peserta kurang dari 10. Untuk penggantian Mata Kuliah mahasiswa bisa menghubungi Jurusan. Demikian, harap diperhatikan.

                <div class="footer">
                    <!-- <div class="chart-legend">
                        <i class="fa fa-circle text-info"></i> Open
                        <i class="fa fa-circle  text-danger"></i> Bounce
                        <i class="fa fa-circle text-warning"></i> Unsubscribe
                    </div> -->
                    <hr>
                    <div class="stats">
                        <i class="ti-timer"></i> Update : 05/Aug/2016 Oleh: Ainur Iza
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection