@extends('guest._layouts.base')

@section('title', 'Jadwal Kuliah Mahasiswa')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<div class="card">
    <div class="header">
        <h4 class="title" style="display: inline; line-height: 1.5em;">
          Jadwal Kuliah Jurusan {{$namajur}}
        </h4>
        <a href="{{URL::previous()}}">
          <button class="btn btn-warning btn-sm" style="float: right; margin-left: 2px;">
            <i class="fa fa-caret-left"></i>
            <span class="hidden-sm hidden-xs">Kembali</span>
          </button>
        </a>
    </div>
    <div class="content">
      <table id="tabelKuliah" class="display responsive wrap table table-bordered table-striped" cellspacing="0" width="100%" >
        <thead>
        <tr>
          <th width="10px">Hari</th>
          <th>Jam: Nama Kelas</th>
          <th width="50px">Ruangan</th>
          <th width="300px">Pengajar</th>
        </tr>
        </thead>
        <tbody>
          @foreach($kuliahs as $kuliah)
            <tr>
              <td>{{ $kuliah->hari }}</td>
              <td>{{ substr($kuliah->jammasuk, 0, 5) }} - {{ substr($kuliah->jamkeluar, 0, 5) }}: {{ $kuliah->kelas->mk->nama }} ({{ $kuliah->kelas->kodekp}})</td>
              <td>{{ $kuliah->ruangan->nama }}</td>
              <td>
                @if(!empty($kuliah->kelas->mk->getPengajar()))
                  @foreach($kuliah->kelas->mk->getPengajar() as $dosen)
                    {{$dosen->npk}} - {{$dosen->gelardepan}}{{$dosen->nama}}{{$dosen->gelarbelakang}}<br>
                  @endforeach
                @endif
              </td>              
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKuliah").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[0,'desc'], [1, 'asc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      // "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
      "dom": "<'row'<'col-sm-6'><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'i><'col-sm-3'><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection