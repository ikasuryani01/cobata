@extends('guest._layouts.base')

@section('title', 'Jadwal Kuliah')

@section('header_styles')
<style type="text/css">
    label {
        margin-top: 10px;
    }
    .card {
      margin: 10px -5px 10px -5px;
      padding: -10px 2px 2px 2px;
      vertical-align: middle;      
    }

    @media screen and (min-width: 1440px) {
      .card {
        width: 267.5px;
        height: 88px;
      }
    }

    @media screen and (min-width: 1024px) {
      .card {
        width: 224.66px;
        height: 88px;
      }
    }

    @media screen and (min-width: 768px) {
      .card {
        width: 236px;
        height: 88px;
      }
    }

   /* @media screen and (min-width: 1024px) {
      .card {
        width: 224.66px;
        height: 88px;
      }
    }*/

    .card .content {
      padding-top: 5px;
    }

    .content {
      vertical-align: middle;
    }
</style>
@endsection

@section('content')
@if(session('status') != "")
<div class="alert alert-info"><center>{!! session('status') !!}</label>
<br></center></div>
@endif
<h4 style="margin-top: -10px;">Fakultas Teknik - Pilih Jurusan</h4>
<div class="row">
  @foreach($datajurusan as $jur)
    <div class="col-lg-3 col-sm-4 col-xs-12">
        <a href="{{ route('jadkul.pilih', $jur->IdJurusan)}}">
          <div class="card">
            <div class="content" align="center">
                <h5>{{$jur->Nama}}</h5>
            </div>
          </div>
        </a>
    </div>
  @endforeach
</div>
@endsection

@section('footer_scripts')
<script>
  $(function () {
    $("#tabelKuliah").dataTable( {
      "pagingType": "full",
      "aaSorting" : [[0,'desc'], [1, 'asc']],
      // "lengthMenu": [ 10, 25, 50, 75, 100 ],
      // "dom": '<"top"i>rt<"bottom"flp><"clear">',
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-3'B><'col-sm-3'i><'col-sm-6'p>>",
        buttons: [
            'colvis'
        ]
    });
    
  });
</script>
@endsection