<div class="collapse navbar-collapse">
    <ul class="nav navbar-nav navbar-right">
        @if (Auth::guard('mahasiswa')->guest() && Auth::guard('karyawan')->guest())
            <li>
                <a href="{{ url('/login') }}">Login</a>
            </li>
        @else
        <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Halo, <i class="ti-user"></i>
				@if(!(Auth::guard('mahasiswa')->guest()))
                <p>{{ Auth::guard('mahasiswa')->user()->nama }}</p>
                @elseif(!(Auth::guard('karyawan')->guest()))
                <p>{{ Auth::guard('karyawan')->user()->nama }}</p>
                @endif
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#">Profil</a></li>
                <li><a href="#">Ubah Password</a></li>
                <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </ul>
        </li>
        @endif
        <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="ti-bell"></i>
                    <p class="notification">5</p>
					<p>Notification</p>
					<b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#">Notification 1 Notification Notification Notification Notification Notification <br> Notification Notification Notification Notification Notification Notification Notification Notification Notification Notification </a></li>
                <li><a href="#">Notification 2</a></li>
                <li><a href="#">Notification 3</a></li>
                <li><a href="#">Notification 4</a></li>
                <li><a href="#">Another notification</a></li>
              </ul>
        </li> -->
		<!-- <li>
            <a href="#">
				<i class="ti-settings"></i>
				<p>Settings</p>
            </a>
        </li> -->
    </ul>

</div>