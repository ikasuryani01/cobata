<ul class="nav">
    <li {!! (Request::is('/home') ? ' class="active"' : '') !!} {!! (Request::is('/') ? ' class="active"' : '') !!}>
        <a href="{{ route('guest.beranda') }}">
            <i class="ti-home"></i>
            <p>Beranda</p>
        </a>
    </li>

    <li {!! (Request::is('menujadwalkuliah*') ? ' class="active"' : '') !!} {!! (Request::is('jk-*') ? ' class="active"' : '') !!}>
        <a href="{{ route('jadkul.menu') }}">
            <i class="ti-blackboard"></i>
            <p>Jadwal Kuliah</p>
        </a>
    </li>

    <li {!! (Request::is('menujadwalujian') ? ' class="active"' : '') !!} {!! (Request::is('ju-*') ? ' class="active"' : '') !!} >
        <a href="{{ route('jujian.menu') }}">
            <i class="ti-bookmark-alt"></i>
            <p>Jadwal Ujian</p>
        </a>
    </li>

</ul>