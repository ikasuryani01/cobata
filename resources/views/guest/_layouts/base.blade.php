<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="{{{ asset('paperadmin/img/apple-icon.png')}}}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{{ asset('paperadmin/img/favicon.png')}}}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Perwalian Fakultas Teknik Universitas Surabaya</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    @include('common_partials.styles')
    
    @yield('header_styles')
    @yield('header_scripts')

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <div class="visible-xs">
                    <br>
                </div>
                <a href="{{ route('guest.beranda') }}" class="simple-text">
                    UBAYA
                </a>
            </div>

            @include('guest._partials.navbar_side')

    	</div>
    </div>

    <div class="main-panel">
    <div class="visible-xs">
        <br>
    </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">@yield('title')</a>
                </div>
                @include('guest._partials.navbar_header')
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>


        <footer class="footer">
            @include('guest._partials.footer')
        </footer>

    </div>
</div>


</body>
    
    @include('common_partials.scripts')

    @yield('footer_scripts')

	<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	$.notify({
            	icon: 'ti-gift',
            	message: "Welcome to <b>Paper Dashboard</b> - a beautiful Bootstrap freebie for your next project."

            },{
                type: 'success',
                timer: 4000
            });

    	});
	</script>

</html>
