<!DOCTYPE html>
<html>
    <head>
        <title>Forbidden</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <!-- Bootstrap core CSS     -->
        <link href="{{{ asset('paperadmin/css/bootstrap.min.css')}}}" rel="stylesheet" />
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
            .button {
                padding: 10px;
                background-color: cyan;
                color: white;
                border-radius: 5px;
                border-color: white;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <!-- <div class="title">Forbidden.</div> -->
                @if (Auth::guard('mahasiswa')->guest() && Auth::guard('karyawan')->guest())
                <div class="title">Sesi habis. Silakan login kembali.</div>
            <!-- <a class="btn btn-info btn-large" href="{{ url('/login') }}">Login</a> -->
                <script type="text/javascript">
                    window.location.href = "{{ url('/login') }}";
                </script>
                @else
                <div class="title">Halaman tidak ditemukan.</div>
                <a class="btn btn-info btn-large" href="{{ URL::previous() }}">Kembali</a>
                @endif
            </div>
        </div>
    </body>
</html>
